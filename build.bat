@echo off

echo Running on %ComputerName%
IF EXIST "build*" (
	echo Moving into build folder
	cd build
)

rem echo %PATH%

python --version 2>NUL
if errorlevel 1 (
	echo Python isn't installed on this computer
	set PythonInstalled=0
) else (
	set PythonInstalled=1
)

call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" x64

set TimeString=%date:~-4,4%%date:~-10,2%%date:~-7,2%%time:~0,2%%time:~3,2%%time:~6,2%

rem mkdir build > NUL 2> NUL
rem echo Running from %cd%
rem echo Time is %TimeString%

set LibDirectory=..\lib\include
set SourceDirectory=..\code
set DataDirectory=..\data
set PlatformFilePath=%SourceDirectory%\win32\win32_main.cpp
set AppFilePath=%SourceDirectory%\app\app.cpp

set CompilePlatform=1
set CompileApplication=1
set DebugBuild=0
set DeveloperBuild=0
set CopyToDataDirectory=1
set AudioEnabled=1
set UseCustomAudio=0
set CrashDumpAsserts=1

set CompilerFlags=%DebugDependantFlags% -DWINDOWS_COMPILATION -DAUDIO_ENABLED=%AudioEnabled% -DUSE_CUSTOM_AUDIO=%UseCustomAudio% -DCRASH_DUMP_ASSERTS=%CrashDumpAsserts% -DDEVELOPER=%DeveloperBuild%
set CompilerFlags=%CompilerFlags% /FC /EHsc /nologo /GS- /Gm- -GR- /EHa- /Fm /Oi /WX /W4
set CompilerFlags=%CompilerFlags% /wd4201 /wd4100 /wd4189 /wd4996 /wd4127 /wd4505 /wd4101 /wd4702 /wd4458 /wd4324
set CompilerFlagsPlat=
set CompilerFlagsApp=
set LinkerFlags=-incremental:no
set IncludeDirectories=/I"%SourceDirectory%" /I"%LibDirectory%" /I"%LibDirectory%\my_glfw\include"
set LibraryDirectories=
set Libraries=opengl32.lib
set PlatformLibraries=glfw3.lib gdi32.lib User32.lib Shell32.lib kernel32.lib winmm.lib Winhttp.lib Shlwapi.lib Ole32.lib
set AppExports=/EXPORT:App_GetVersion /EXPORT:App_GetStartupOptions /EXPORT:App_Initialize /EXPORT:App_Reloaded /EXPORT:App_TaskFinished /EXPORT:App_Update /EXPORT:App_PerformTask /EXPORT:App_Closing
set DllDirective=/DLL /PDB:"%ProjectName%_%TimeString%.pdb"

if "%DebugBuild%"=="1" (
	set CompilerFlags=%CompilerFlags% /Zi /Od -DDEBUG=1
	set CompilerFlagsPlat=%CompilerFlagsPlat% /MTd
	set CompilerFlagsApp=%CompilerFlagsApp% /MTd
	set LibraryDirectories=%LibraryDirectories% /LIBPATH:"..\lib\debug" /LIBPATH:"..\lib\include\my_glfw\debug"
	set Libraries=%Libraries% glew32d.lib
) else (
	set CompilerFlags=%CompilerFlags% /O2 /Ot /Oy -DDEBUG=0
	set CompilerFlagsPlat=%CompilerFlagsPlat% /MT
	set CompilerFlagsApp=%CompilerFlagsApp% /MT
	set LibraryDirectories=%LibraryDirectories% /LIBPATH:"..\lib\release" /LIBPATH:"..\lib\include\my_glfw\release"
	set Libraries=%Libraries% glew32.lib
)

if "%AudioEnabled%"=="1" (
	if "%UseCustomAudio%"=="1" (
		set Libraries=%Libraries% libogg_static.lib libvorbis_static.lib
		set AppExports=%AppExports% /EXPORT:App_GetAudioOutput
	) else (
		set Libraries=%Libraries% OpenAL32.lib libogg_static.lib libvorbis_static.lib
	)
)

set ProjectName=LocoStoko

REM Compile the resources file to generate resources.res which defines our program icon
rc /nologo %SourceDirectory%\resources.rc

del *.pdb > NUL 2> NUL

if "%CompilePlatform%"=="1" (
	echo[
	
	if "%PythonInstalled%"=="1" (
		python ..\IncrementVersionNumber.py %SourceDirectory%\win32\win32_version.h
	)
	
	cl /Fe%ProjectName%.exe %CompilerFlags% %CompilerFlagsPlat% %IncludeDirectories% %PlatformFilePath% /link %LibraryDirectories% %LinkerFlags% %Libraries% %PlatformLibraries% %SourceDirectory%\resources.res
	
	if "%CopyToDataDirectory%"=="1" (
		echo [Copying %ProjectName%.exe to data directory]
		XCOPY ".\%ProjectName%.exe" "%DataDirectory%\" /Y > NUL
	) else (
		echo [Platform Build Finished!]
	)
)

if "%CompileApplication%"=="1" (
	echo[
	
	if "%PythonInstalled%"=="1" (
		python ..\IncrementVersionNumber.py %SourceDirectory%\app\app_version.h
	)
	
	cl /Fe%ProjectName%.dll %CompilerFlags% %CompilerFlagsApp% %IncludeDirectories% %AppFilePath% /link %LibraryDirectories% %LinkerFlags% %Libraries% %AppExports% %DllDirective%
	rem cl /P /C /Fiapp_precomp.cpp %CompilerFlags% %CompilerFlagsApp% %IncludeDirectories% %AppFilePath%
	
	if "%CopyToDataDirectory%"=="1" (
		echo [Copying %ProjectName%.dll to data directory]
		XCOPY ".\%ProjectName%.dll" "%DataDirectory%\" /Y > NUL
	) else (
		echo [Application Build Finished!]
	)
)

echo[

REM Delete the object files that were created during compilation
del *.obj > NUL 2> NUL

rem echo [Done!]