/*
File:   mess.cpp
Author: Taylor Robbins
Date:   10\21\2018
Description: 
	** "Mess" is just a name to indicate that this AppState is just for anything and everything we need to quickly test, render, or visualize.
	** It is just a dumping ground for development related tests and tools that are temporary, not finished, or not large enough to warrant their own AppState
*/

// +--------------------------------------------------------------+
// |                          Initialize                          |
// +--------------------------------------------------------------+
void InitializeMess()
{
	Assert(mess != nullptr);
	
	mess->viewPos = Vec2_Zero;
	mess->targetPos = mess->viewPos;
	
	mess->initialized = true;
}

// +--------------------------------------------------------------+
// |                            Start                             |
// +--------------------------------------------------------------+
bool StartMess(AppState_t oldAppState, TransInfoType_t transInfoType, void* transInfoPntr)
{
	SoftQueueMusicChange(nullptr);
	//TODO: Anything we need to reset the state of on return?
	
	return true;
}

// +--------------------------------------------------------------+
// |                         Deinitialize                         |
// +--------------------------------------------------------------+
void DeinitializeMess()
{
	Assert(mess != nullptr);
	Assert(mess->initialized == true);
	
	mess->initialized = false;
	ClearPointer(mess);
}

// +--------------------------------------------------------------+
// |                       Does Cover Below                       |
// +--------------------------------------------------------------+
bool DoesMessCoverBelow(AppState_t belowState)
{
	return true;
}

// +--------------------------------------------------------------+
// |                         Update Mess                          |
// +--------------------------------------------------------------+
void UpdateMess(AppMenu_t appMenuAbove)
{
	if (app->screenWipeProgress >= 1.0f && mess->activatedWipe)
	{
		WriteLine_I("Leaving mess");
		PopAppState();
	}
	
	// +==============================+
	// |      Main Menu Shortcut      |
	// +==============================+
	if (appMenuAbove == AppMenu_None && ButtonPressed(Button_Tilde) && !mess->activatedWipe)
	{
		HandleButton(Button_Tilde);
		StartScreenWipe(ScreenWipe_BlackWithSpinner, ScreenWipeStencil_BottomUp);
		mess->activatedWipe = true;
		mess->wipeDoneState = AppState_MainMenu;
	}
	
	mess->shaderTime += ElapsedMs;
	
	// +==============================+
	// |         Update View          |
	// +==============================+
	if (appMenuAbove == AppMenu_None)
	{
		r32 viewMoveSpeed = 15.0f;
		if (ButtonDownNoHandling(Button_Shift))   { viewMoveSpeed = 25.0f; }
		if (ButtonDownNoHandling(Button_Control)) { viewMoveSpeed = 2.0f; }
		if (ButtonDown(Button_W)) { HandleButton(Button_W); mess->targetPos.y -= viewMoveSpeed; }
		if (ButtonDown(Button_A)) { HandleButton(Button_A); mess->targetPos.x -= viewMoveSpeed; }
		if (ButtonDown(Button_S)) { HandleButton(Button_S); mess->targetPos.y += viewMoveSpeed; }
		if (ButtonDown(Button_D)) { HandleButton(Button_D); mess->targetPos.x += viewMoveSpeed; }
		if (mess->viewPos != mess->targetPos)
		{
			v2 offset = mess->targetPos - mess->viewPos;
			if (Vec2Length(offset) >= 0.01f)
			{
				mess->viewPos += offset / 10.0f;
			}
			else
			{
				mess->viewPos = mess->targetPos;
			}
		}
		
		if (ButtonPressed(Button_0)) { HandleButton(Button_0); mess->viewPos = Vec2_Zero; mess->targetPos = Vec2_Zero; }
	}
}

// +--------------------------------------------------------------+
// |                         Render Mess                          |
// +--------------------------------------------------------------+
void RenderMess(FrameBuffer_t* renderBuffer, AppState_t appStateAbove, AppMenu_t appMenuAbove)
{
	// +==============================+
	// |         Render Setup         |
	// +==============================+
	RcBegin(GetShader(mainShader), NewRec(Vec2_Zero, RenderScreenSize), GetFont(pixelFont), renderBuffer);
	RcClear(VisBlueGray, 1.0f);
	RcSetViewMatrix(Mat4Translate(NewVec3(-mess->viewPos.x, -mess->viewPos.y, 0)));
	
	v2 worldMousePos = RenderMousePos + mess->viewPos;
	
	#if 1
	// +==============================+
	// |      Draw Gamepad Info       |
	// +==============================+
	for (u32 gIndex = 0; gIndex < MAX_NUM_GAMEPADS; gIndex++)
	{
		const GamepadState_t* gamepad = &appInput->gamepads[gIndex];
		if (gamepad->isConnected)
		{
			RcBindFont(GetFont(pixelFont), 2.0f);
			v2 textPos = NewVec2(10 + gIndex*300.0f, 10 + RcMaxExtendUp());
			r32 advanceY = 5 + RcLineHeight();
			
			RcPrintString(textPos, VisWhite, 1.0f, "Gamepad[%u]: \"%s\"", gIndex, gamepad->name);
			textPos.y += advanceY;
			
			RcPrintString(textPos, VisWhite, 1.0f, "Type: %s", gamepad->isPlaystation ? "Playstation" : "Xbox");
			textPos.y += advanceY;
			
			for (u32 bIndex = 0; bIndex < Gamepad_NumButtons; bIndex++)
			{
				GamepadButtons_t button = (GamepadButtons_t)bIndex;
				const ButtonState_t* btnState = &gamepad->buttons[bIndex];
				RcPrintString(textPos, btnState->isDown ? VisWhite : VisLightGray, 1.0f, "%s: %s", GetGamepadButtonName(button), btnState->isDown ? "Down" : "Up");
				textPos.y += advanceY;
			}
			
			for (u32 aIndex = 0; aIndex < 12; aIndex++)
			{
				r32 axisValue = 0.0f;
				const char* axisName = "Unknown";
				switch (aIndex)
				{
					case 0:  axisValue = gamepad->leftTrigger;  axisName = "Left Trigger"; break;
					case 1:  axisValue = gamepad->rightTrigger; axisName = "Right Trigger"; break;
					case 2:  axisValue = gamepad->leftStick.x;  axisName = "Left Stick X"; break;
					case 3:  axisValue = gamepad->leftStick.y;  axisName = "Left Stick Y"; break;
					case 4:  axisValue = gamepad->rightStick.x; axisName = "Right Stick X"; break;
					case 5:  axisValue = gamepad->rightStick.y; axisName = "Right Stick Y"; break;
					case 6:  axisValue = gamepad->dPad.x; axisName = "D-Pad Y"; break;
					case 7:  axisValue = gamepad->dPad.y; axisName = "D-Pad Y"; break;
					case 8:  axisValue = gamepad->leftStickRaw.x;  axisName = "Left Stick Raw X"; break;
					case 9:  axisValue = gamepad->leftStickRaw.y;  axisName = "Left Stick Raw Y"; break;
					case 10: axisValue = gamepad->rightStickRaw.x; axisName = "Right Stick Raw X"; break;
					case 11: axisValue = gamepad->rightStickRaw.y; axisName = "Right Stick Raw Y"; break;
				}
				
				RcPrintString(textPos, VisWhite, 1.0f, "%s: %f", axisName, axisValue);
				textPos.y += advanceY;
			}
		}
	}
	#endif
	
	#if 0
	// +==============================+
	// |      Color Replace Test      |
	// +==============================+
	RcBindShader(GetShader(tileShader));
	RcBindTexture(GetTexture(colorTest));
	for (u8 cIndex = 0; cIndex < NUM_REPLACE_COLORS; cIndex++)
	{
		Color_t newColor = ColorFromHSV((u16)Oscillate(0, 360, 2000 + cIndex, cIndex*321), 0.9f, 0.95f);
		RcSetReplaceColor(cIndex, newColor);
	}
	RcDrawTexturedRec(NewRec(0, 0, 500, 500), White);
	#endif
}
