/*
File:   app_notifications.h
Author: Taylor Robbins
Date:   01\29\2020
*/

#ifndef _APP_NOTIFICATIONS_H
#define _APP_NOTIFICATIONS_H

struct AppNotification_t
{
	char* message;
	Color_t messageColor;
	r32 age;
	r32 lifeSpan;
	rec drawRec;
};

#endif //  _APP_NOTIFICATIONS_H
