/*
File:   app_input.h
Author: Taylor Robbins
Date:   02\12\2019
*/

#ifndef _APP_INPUT_H
#define _APP_INPUT_H

typedef enum
{
	InputMethod_KeyboardMouse = 0,
	InputMethod_XbController,
	InputMethod_PsController,
	InputMethod_NumInputMethods,
} InputMethod_t;

const char* GetInputMethodStr(InputMethod_t inputMethod)
{
	switch (inputMethod)
	{
		case InputMethod_KeyboardMouse: return "Keyboard/Mouse";
		case InputMethod_XbController:  return "Xbox Controller";
		case InputMethod_PsController:  return "Playstation Controller";
		default: return "Unknown";
	}
}

#endif //  _APP_INPUT_H
