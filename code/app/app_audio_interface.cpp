/*
File:   app_audio_interface.cpp
Author: Taylor Robbins
Date:   03\30\2020
Description: 
	** Holds some general purpose functions for interacting with the audio interface (Right now OpenAL) 
*/

#if AUDIO_ENABLED && !USE_CUSTOM_AUDIO

const char* GetOpenAlErrorStr(ALenum errorCode)
{
	switch (errorCode)
	{
		case AL_NO_ERROR:         return "AL_NO_ERROR";
		case AL_INVALID_NAME:     return "AL_INVALID_NAME";
		case AL_INVALID_ENUM:     return "AL_INVALID_ENUM";
		case AL_INVALID_VALUE:    return "AL_INVALID_VALUE";
		case AL_INVALID_OPERATION:return "AL_INVALID_OPERATION";
		case AL_OUT_OF_MEMORY:    return "AL_OUT_OF_MEMORY";
		default: return "Unknown";
	}
}
bool PrintOpenAlError(const char* lastCallName)
{
	ALenum errorCode = alGetError();
	if (errorCode != AL_NO_ERROR)
	{
		PrintLine_E("OpenAL Error after %s call: %s", (lastCallName != nullptr) ? lastCallName : "some", GetOpenAlErrorStr(errorCode));
		return true;
	}
	else { return false; }
}

const char* GetAlExtensionName(AlExtension_t extension, bool programName)
{
	switch (extension)
	{
		case AlExtension_Alaw:                return programName ? "AL_EXT_ALAW"                  : "Alaw";
		case AlExtension_Bformat:             return programName ? "AL_EXT_BFORMAT"               : "Bformat";
		case AlExtension_Double:              return programName ? "AL_EXT_DOUBLE"                : "Double";
		case AlExtension_ExponentDistance:    return programName ? "AL_EXT_EXPONENT_DISTANCE"     : "ExponentDistance";
		case AlExtension_Float32:             return programName ? "AL_EXT_FLOAT32"               : "Float32";
		case AlExtension_Ima4:                return programName ? "AL_EXT_IMA4"                  : "Ima4";
		case AlExtension_LinearDistance:      return programName ? "AL_EXT_LINEAR_DISTANCE"       : "LinearDistance";
		case AlExtension_Mcformats:           return programName ? "AL_EXT_MCFORMATS"             : "Mcformats";
		case AlExtension_Mulaw:               return programName ? "AL_EXT_MULAW"                 : "Mulaw";
		case AlExtension_MulawBformat:        return programName ? "AL_EXT_MULAW_BFORMAT"         : "MulawBformat";
		case AlExtension_MulawMcformats:      return programName ? "AL_EXT_MULAW_MCFORMATS"       : "MulawMcformats";
		case AlExtension_Offset:              return programName ? "AL_EXT_OFFSET"                : "Offset";
		case AlExtension_SourceDistanceModel: return programName ? "AL_EXT_source_distance_model" : "SourceDistanceModel";
		case AlExtension_Quadriphonic:        return programName ? "AL_LOKI_quadriphonic"         : "Quadriphonic";
		case AlExtension_BlockAlignment:      return programName ? "AL_SOFT_block_alignment"      : "BlockAlignment";
		case AlExtension_BufferSamples:       return programName ? "AL_SOFT_buffer_samples"       : "BufferSamples";
		case AlExtension_BufferSubData:       return programName ? "AL_SOFT_buffer_sub_data"      : "BufferSubData";
		case AlExtension_DeferredUpdates:     return programName ? "AL_SOFT_deferred_updates"     : "DeferredUpdates";
		case AlExtension_DirectChannels:      return programName ? "AL_SOFT_direct_channels"      : "DirectChannels";
		case AlExtension_LoopPoints:          return programName ? "AL_SOFT_loop_points"          : "LoopPoints";
		case AlExtension_Msadpcm:             return programName ? "AL_SOFT_MSADPCM"              : "Msadpcm";
		case AlExtension_SourceLatency:       return programName ? "AL_SOFT_source_latency"       : "SourceLatency";
		case AlExtension_SourceLength:        return programName ? "AL_SOFT_source_length"        : "SourceLength";
		default: return "Unknown";
	}
}
const char* GetAlcExtensionName(AlcExtension_t extension, bool programName)
{
	switch (extension)
	{
		case AlcExtension_EnumerateAllExt:    return programName ? "ALC_ENUMERATE_ALL_EXT"        : "EnumerateAllExt";
		case AlcExtension_EnumerationExt:     return programName ? "ALC_ENUMERATION_EXT"          : "EnumerationExt";
		case AlcExtension_Capture:            return programName ? "ALC_EXT_CAPTURE"              : "Capture";
		case AlcExtension_Dedicated:          return programName ? "ALC_EXT_DEDICATED"            : "Dedicated";
		case AlcExtension_Disconnect:         return programName ? "ALC_EXT_disconnect"           : "Disconnect";
		case AlcExtension_Efx:                return programName ? "ALC_EXT_EFX"                  : "Efx";
		case AlcExtension_ThreadLocalContext: return programName ? "ALC_EXT_thread_local_context" : "ThreadLocalContext";
		case AlcExtension_Hrtf:               return programName ? "ALC_SOFT_HRTF"                : "Hrtf";
		case AlcExtension_Loopback:           return programName ? "ALC_SOFT_loopback"            : "Loopback";
		case AlcExtension_PauseDevice:        return programName ? "ALC_SOFT_pause_device"        : "PauseDevice";
		case AlcExtension_DeviceClock:        return programName ? "ALC_SOFTX_device_clock"       : "DeviceClock";
		default: return "Unknown";
	}
}

#endif //AUDIO_ENABLED
