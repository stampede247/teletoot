/*
File:   app_settings.cpp
Author: Taylor Robbins
Date:   06\17\2019
Description: 
	** Holds functions that help us handle the AppSettings_t structure 
*/

void InitAppSettingsStruct(MemoryArena_t* memArena, AppSettings_t* settings)
{
	Assert(settings != nullptr);
	ClearPointer(settings);
	//TODO: Anything that needs to get allocated?
}

void DestroyAppSettingsStruct(AppSettings_t* settings)
{
	Assert(settings != nullptr);
	//TODO: Anything that needs to get deallocated?
}

u32 GetAppSettingsStructMemoryUsage(const AppSettings_t* settings)
{
	Assert(settings != nullptr);
	return 0;
}

void CopyAppSettingsStruct(MemoryArena_t* memArena, AppSettings_t* dest, const AppSettings_t* src)
{
	Assert(dest != nullptr);
	Assert(src != nullptr);
	MyMemCopy(dest, src, sizeof(AppSettings_t));
	//TODO: Anything that needs to be allocated for the copy?
}

void FillDefaultAppSettings(AppSettings_t* settings)
{
	Assert(settings != nullptr);
	
	settings->masterVolume = 0.75f;
	settings->masterSoundEnabled = true;
	settings->musicVolume = 1.0f;
	settings->musicEnabled = true;
	settings->soundEffectVolume = 1.0f;
	settings->soundEffectsEnabled = true;
	
	settings->resolution = Resolution_1366x768;
	settings->fullscreenEnabled = false;
	settings->autoMinimize = true;
	
	settings->developerConsoleEnabled = (DEVELOPER || DEBUG);
	settings->screenshotCountdownTime = 0.0f; //percent
	settings->gifRecordHotekyEnabled = (DEVELOPER || DEBUG);
	settings->notificationPopupsEnabled = true;
	
	settings->lastPlayedVersion = {0, 0, 0};
	settings->cheatsEnabled = DEVELOPER;
}

u32 GetNumAppSettings() //pre-declared in app_settings.h
{
	u32 result = 0;
	result = NUM_APP_SETTINGS;
	return result;
}

SettingType_t GetSettingInfo(AppSettings_t* settings, u32 settIndex, const char** nameOut, void** valueOut) //pre-declared in app_settings.h
{
	Assert(settings != nullptr);
	Assert(nameOut != nullptr);
	Assert(valueOut != nullptr);
	Assert(settIndex < GetNumAppSettings());
	
	u32 index = 0;
	if (index == settIndex) { *nameOut = "Master Volume";                 *valueOut = (void*)&settings->masterVolume;               return SettingType_r32;     } index++;
	if (index == settIndex) { *nameOut = "Master Sound Enabled";          *valueOut = (void*)&settings->masterSoundEnabled;         return SettingType_Boolean; } index++;
	if (index == settIndex) { *nameOut = "Music Volume";                  *valueOut = (void*)&settings->musicVolume;                return SettingType_r32;     } index++;
	if (index == settIndex) { *nameOut = "Music Enabled";                 *valueOut = (void*)&settings->musicEnabled;               return SettingType_Boolean; } index++;
	if (index == settIndex) { *nameOut = "Sound Effect Volume";           *valueOut = (void*)&settings->soundEffectVolume;          return SettingType_r32;     } index++;
	if (index == settIndex) { *nameOut = "Sound Effects Enabled";         *valueOut = (void*)&settings->soundEffectsEnabled;        return SettingType_Boolean; } index++;
	
	if (index == settIndex) { *nameOut = "Resolution";                    *valueOut = (void*)&settings->resolution;                 return SettingType_u8;      } index++;
	if (index == settIndex) { *nameOut = "Fullscreen Enabled";            *valueOut = (void*)&settings->fullscreenEnabled;          return SettingType_Boolean; } index++;
	if (index == settIndex) { *nameOut = "Auto Minimize";                 *valueOut = (void*)&settings->autoMinimize;               return SettingType_Boolean; } index++;
	
	if (index == settIndex) { *nameOut = "Developer Console";             *valueOut = (void*)&settings->developerConsoleEnabled;    return SettingType_Boolean; } index++;
	if (index == settIndex) { *nameOut = "Gif Record Hotkey";             *valueOut = (void*)&settings->gifRecordHotekyEnabled;     return SettingType_Boolean; } index++;
	if (index == settIndex) { *nameOut = "Screenshot Countdown Time";     *valueOut = (void*)&settings->screenshotCountdownTime;    return SettingType_r32;     } index++;
	if (index == settIndex) { *nameOut = "Notification Popups Enabled";   *valueOut = (void*)&settings->notificationPopupsEnabled;  return SettingType_Boolean; } index++;
	
	if (index == settIndex) { *nameOut = "Last Played Version";           *valueOut = (void*)&settings->lastPlayedVersion;          return SettingType_Version; } index++;
	if (index == settIndex) { *nameOut = "Cheats Enabled";                *valueOut = (void*)&settings->cheatsEnabled;              return SettingType_Boolean; } index++;
	
	Assert(false);
	return SettingType_None;
}
SettingType_t GetSettingInfo(const AppSettings_t* settings, u32 settIndex, const char** nameOut, const void** valueOut) //const variant pre-declared in app_settings.h
{
	return GetSettingInfo((AppSettings_t*)settings, settIndex, nameOut, (void**)valueOut);
}

u32 GetSettingIndexByName(const char* settingName, u32 nameLength) //pre-declared in app_settings.h
{
	Assert(settingName != nullptr);
	
	u32 numSettings = GetNumAppSettings();
	for (u32 sIndex = 0; sIndex < numSettings; sIndex++)
	{
		TempPushMark();
		AppSettings_t* dummySettings = (AppSettings_t*)(0x01); //just something that is not nullptr
		const char* newSettingName = nullptr;
		void* valuePntr = nullptr;
		SettingType_t settingType = GetSettingInfo(dummySettings, sIndex, &newSettingName, &valuePntr);
		Assert(settingType != SettingType_None);
		Assert(newSettingName != nullptr);
		Assert(valuePntr != nullptr);
		if (MyStrLength32(newSettingName) == nameLength && MyStrCompare(newSettingName, settingName, nameLength) == 0)
		{
			TempPopMark();
			return sIndex;
		}
		TempPopMark();
	}
	
	return numSettings;
}
u32 GetSettingIndexByName(const char* settingName) //pre-declared in app_settings.h
{
	u32 nameLength = MyStrLength32(settingName);
	return GetSettingIndexByName(settingName, nameLength);
}

u32 GetSettingIndexByPntr(const AppSettings_t* settings, const void* pntr) //pre-declared in app_settings.h
{
	Assert(((const u8*)pntr) >= &((const u8*)settings)[0] && ((const u8*)pntr) < &((const u8*)settings)[sizeof(AppSettings_t)]);
	
	const char* settingName = nullptr;
	const void* settingValuePntr = nullptr;
	u32 numSettings = GetNumAppSettings();
	
	for (u32 sIndex = 0; sIndex < numSettings; sIndex++)
	{
		SettingType_t settingType = GetSettingInfo(settings, sIndex, &settingName, &settingValuePntr);
		if (settingValuePntr == pntr) { return sIndex; }
	}
	
	return numSettings;
}

const char* GetSettingName(u32 settIndex) //pre-declared in app_settings.h
{
	const char* settingName = nullptr;
	void* settingValuePntr = nullptr;
	AppSettings_t* dummySettings = (AppSettings_t*)(0x01); //just something that is not nullptr
	SettingType_t settingType = GetSettingInfo(dummySettings, settIndex, &settingName, &settingValuePntr);
	if (settingType == SettingType_None) { return "Unknown"; }
	if (settingName == nullptr) { return "Unknown"; }
	return settingName;
}

bool GetSettingBool(AppSettings_t* settings, u32 settIndex) //pre-declared in app_settings.h
{
	Assert(settings != nullptr);
	const char* settingName = nullptr;
	void* settingValuePntr = nullptr;
	SettingType_t settingType = GetSettingInfo(settings, settIndex, &settingName, &settingValuePntr);
	Assert(settingType == SettingType_Boolean);
	Assert(settingValuePntr != nullptr);
	return *((bool*)settingValuePntr);
}
u8 GetSettingU8(AppSettings_t* settings, u32 settIndex) //pre-declared in app_settings.h
{
	Assert(settings != nullptr);
	const char* settingName = nullptr;
	void* settingValuePntr = nullptr;
	SettingType_t settingType = GetSettingInfo(settings, settIndex, &settingName, &settingValuePntr);
	Assert(settingType == SettingType_u8);
	Assert(settingValuePntr != nullptr);
	return *((u8*)settingValuePntr);
}
u32 GetSettingU32(AppSettings_t* settings, u32 settIndex) //pre-declared in app_settings.h
{
	Assert(settings != nullptr);
	const char* settingName = nullptr;
	void* settingValuePntr = nullptr;
	SettingType_t settingType = GetSettingInfo(settings, settIndex, &settingName, &settingValuePntr);
	Assert(settingType == SettingType_u32);
	Assert(settingValuePntr != nullptr);
	return *((u32*)settingValuePntr);
}
i32 GetSettingI32(AppSettings_t* settings, u32 settIndex) //pre-declared in app_settings.h
{
	Assert(settings != nullptr);
	const char* settingName = nullptr;
	void* settingValuePntr = nullptr;
	SettingType_t settingType = GetSettingInfo(settings, settIndex, &settingName, &settingValuePntr);
	Assert(settingType == SettingType_i32);
	Assert(settingValuePntr != nullptr);
	return *((i32*)settingValuePntr);
}
r32 GetSettingR32(AppSettings_t* settings, u32 settIndex) //pre-declared in app_settings.h
{
	Assert(settings != nullptr);
	const char* settingName = nullptr;
	void* settingValuePntr = nullptr;
	SettingType_t settingType = GetSettingInfo(settings, settIndex, &settingName, &settingValuePntr);
	Assert(settingType == SettingType_r32);
	Assert(settingValuePntr != nullptr);
	return *((r32*)settingValuePntr);
}
Version_t GetSettingVersion(AppSettings_t* settings, u32 settIndex) //pre-declared in app_settings.h
{
	Assert(settings != nullptr);
	const char* settingName = nullptr;
	void* settingValuePntr = nullptr;
	SettingType_t settingType = GetSettingInfo(settings, settIndex, &settingName, &settingValuePntr);
	Assert(settingType == SettingType_Version);
	Assert(settingValuePntr != nullptr);
	return *((Version_t*)settingValuePntr);
}

void SetSettingBool(AppSettings_t* settings, u32 settIndex, bool newValue) //pre-declared in app_settings.h
{
	Assert(settings != nullptr);
	const char* settingName = nullptr;
	void* settingValuePntr = nullptr;
	SettingType_t settingType = GetSettingInfo(settings, settIndex, &settingName, &settingValuePntr);
	Assert(settingType == SettingType_Boolean);
	Assert(settingValuePntr != nullptr);
	*((bool*)settingValuePntr) = newValue;
}
void SetSettingU8(AppSettings_t* settings, u32 settIndex, u8 newValue) //pre-declared in app_settings.h
{
	Assert(settings != nullptr);
	const char* settingName = nullptr;
	void* settingValuePntr = nullptr;
	SettingType_t settingType = GetSettingInfo(settings, settIndex, &settingName, &settingValuePntr);
	Assert(settingType == SettingType_u8);
	Assert(settingValuePntr != nullptr);
	*((u8*)settingValuePntr) = newValue;
}
void SetSettingU32(AppSettings_t* settings, u32 settIndex, u32 newValue) //pre-declared in app_settings.h
{
	Assert(settings != nullptr);
	const char* settingName = nullptr;
	void* settingValuePntr = nullptr;
	SettingType_t settingType = GetSettingInfo(settings, settIndex, &settingName, &settingValuePntr);
	Assert(settingType == SettingType_u32);
	Assert(settingValuePntr != nullptr);
	*((u32*)settingValuePntr) = newValue;
}
void SetSettingI32(AppSettings_t* settings, u32 settIndex, i32 newValue) //pre-declared in app_settings.h
{
	Assert(settings != nullptr);
	const char* settingName = nullptr;
	void* settingValuePntr = nullptr;
	SettingType_t settingType = GetSettingInfo(settings, settIndex, &settingName, &settingValuePntr);
	Assert(settingType == SettingType_i32);
	Assert(settingValuePntr != nullptr);
	*((i32*)settingValuePntr) = newValue;
}
void SetSettingR32(AppSettings_t* settings, u32 settIndex, r32 newValue) //pre-declared in app_settings.h
{
	Assert(settings != nullptr);
	const char* settingName = nullptr;
	void* settingValuePntr = nullptr;
	SettingType_t settingType = GetSettingInfo(settings, settIndex, &settingName, &settingValuePntr);
	Assert(settingType == SettingType_r32);
	Assert(settingValuePntr != nullptr);
	*((r32*)settingValuePntr) = newValue;
}
void SetSettingVersion(AppSettings_t* settings, u32 settIndex, Version_t newValue) //pre-declared in app_settings.h
{
	Assert(settings != nullptr);
	const char* settingName = nullptr;
	void* settingValuePntr = nullptr;
	SettingType_t settingType = GetSettingInfo(settings, settIndex, &settingName, &settingValuePntr);
	Assert(settingType == SettingType_Version);
	Assert(settingValuePntr != nullptr);
	*((Version_t*)settingValuePntr) = newValue;
}

u8 GetSettingNumOptions(u32 settIndex)
{
	if (settIndex == GetSettingIndexByPntr(sett, &sett->resolution)) { return Resolution_NumOptions; }
	return 0;
}
u8 GetSettingOptionValue(u32 settIndex, u8 valueIndex, const char** valueStrOut)
{
	Assert(valueStrOut != nullptr);
	
	if (settIndex == GetSettingIndexByPntr(sett, &sett->resolution))
	{
		switch (valueIndex)
		{
			case 0:  *valueStrOut = "640x480";   return Resolution_640x480;
			case 1:  *valueStrOut = "720x480";   return Resolution_720x480;
			case 2:  *valueStrOut = "720x576";   return Resolution_720x576;
			case 3:  *valueStrOut = "800x600";   return Resolution_800x600;
			case 4:  *valueStrOut = "1024x768";  return Resolution_1024x768;
			case 5:  *valueStrOut = "1152x864";  return Resolution_1152x864;
			case 6:  *valueStrOut = "1176x664";  return Resolution_1176x664;
			case 7:  *valueStrOut = "1280x720";  return Resolution_1280x720;
			case 8:  *valueStrOut = "1280x768";  return Resolution_1280x768;
			case 9:  *valueStrOut = "1280x800";  return Resolution_1280x800;
			case 10: *valueStrOut = "1280x960";  return Resolution_1280x960;
			case 11: *valueStrOut = "1280x1024"; return Resolution_1280x1024;
			case 12: *valueStrOut = "1360x768";  return Resolution_1360x768;
			case 13: *valueStrOut = "1366x768";  return Resolution_1366x768;
			case 14: *valueStrOut = "1440x900";  return Resolution_1440x900;
			case 15: *valueStrOut = "1600x900";  return Resolution_1600x900;
			case 16: *valueStrOut = "1600x1024"; return Resolution_1600x1024;
			case 17: *valueStrOut = "1600x1200"; return Resolution_1600x1200;
			case 18: *valueStrOut = "1680x1050"; return Resolution_1680x1050;
			case 19: *valueStrOut = "1768x992";  return Resolution_1768x992;
			case 20: *valueStrOut = "1920x1080"; return Resolution_1920x1080;
			case 21: *valueStrOut = "1920x1200"; return Resolution_1920x1200;
			case 22: *valueStrOut = "1920x1440"; return Resolution_1920x1440;
			case 23: *valueStrOut = "2048x1536"; return Resolution_2048x1536;
			case 24: *valueStrOut = "2560x1440"; return Resolution_2560x1440;
			case 25: *valueStrOut = "2560x1600"; return Resolution_2560x1600;
			case 26: *valueStrOut = "3840x2160"; return Resolution_3840x2160;
		}
	}
	
	*valueStrOut = "Unknown";
	return valueIndex;
}
u8 GetSettingOptionValueIndex(u32 settIndex, u8 value)
{
	u32 numOptions = GetSettingNumOptions(settIndex);
	for (u8 oIndex = 0; oIndex < numOptions; oIndex++)
	{
		const char* valueStr = nullptr;
		u8 newValue = GetSettingOptionValue(settIndex, oIndex, &valueStr);
		if (newValue == value) { return oIndex; }
	}
	return 0;
}
