/*
File:   app_states.cpp
Author: Taylor Robbins
Date:   02\01\2019
Description: 
	** Holds functions that help the application handle various states
	** Also some helper functions for those states do various things
*/

// +--------------------------------------------------------------+
// |                      AppState Functions                      |
// +--------------------------------------------------------------+
// +==============================+
// |          App States          |
// +==============================+
void InitializeAppState(AppState_t appState)
{
	PrintLine_Dx(DbgFlag_Inverted, "Initializing %s...", GetAppStateName(appState));
	app->calledAppState = appState;
	switch (appState)
	{
		case AppState_Game:         InitializeGame(); break;
		case AppState_Mess:         InitializeMess(); break;
		case AppState_Giffer:       InitializeGiffer(); break;
		case AppState_MainMenu:     InitializeMainMenu(); break;
		default: NotifyPrint_W("Unhandled app state Initialize 0x%02X", appState); break;
	};
	PreloadResourcesForAppState(appState);
	app->calledAppState = AppState_None;
	PrintLine_Dx(DbgFlag_Inverted, "Finished initializing %s", GetAppStateName(appState));
}

void DeinitializeAppState(AppState_t appState)
{
	PrintLine_Dx(DbgFlag_Inverted, "Deinitializing %s...", GetAppStateName(appState));
	//originalAppState is required because KillAppState is instant and can happen within another AppState's update/render
	AppState_t originalAppState = app->calledAppState;
	app->calledAppState = appState;
	switch (appState)
	{
		case AppState_Game:         DeinitializeGame(); break;
		case AppState_Mess:         DeinitializeMess(); break;
		case AppState_Giffer:       DeinitializeGiffer(); break;
		case AppState_MainMenu:     DeinitializeMainMenu(); break;
		default: NotifyPrint_W("Unhandled app state Deinitialize 0x%02X", appState); break;
	};
	app->calledAppState = originalAppState;
	PrintLine_Dx(DbgFlag_Inverted, "Finished deinitializing %s", GetAppStateName(appState));
}

bool StartAppState(AppState_t appState, AppState_t oldAppState, TransInfoType_t transInfoType, void* transInfoPntr)
{
	PrintLine_Dx(DbgFlag_Inverted, "Starting %s...", GetAppStateName(appState));
	app->calledAppState = appState;
	bool startResult = false;
	switch (appState)
	{
		case AppState_Game:         startResult = StartGame         (oldAppState, transInfoType, transInfoPntr); break;
		case AppState_Mess:         startResult = StartMess         (oldAppState, transInfoType, transInfoPntr); break;
		case AppState_Giffer:       startResult = StartGiffer       (oldAppState, transInfoType, transInfoPntr); break;
		case AppState_MainMenu:     startResult = StartMainMenu     (oldAppState, transInfoType, transInfoPntr); break;
		default: NotifyPrint_W("Unhandled app state Start 0x%02X", appState); break;
	};
	app->calledAppState = AppState_None;
	if (startResult)
	{
		PrintLine_Dx(DbgFlag_Inverted, "Finished starting %s", GetAppStateName(appState));
	}
	else
	{
		NotifyPrint_W("Failed to start %s", GetAppStateName(appState));
	}
	return startResult;
}

void UpdateAppState(AppState_t appState, AppMenu_t appMenuAbove)
{
	app->allowAppStateChanges = true;
	app->calledAppState = appState;
	switch (appState)
	{
		case AppState_Game:         UpdateGame         (appMenuAbove); break;
		case AppState_Mess:         UpdateMess         (appMenuAbove); break;
		case AppState_Giffer:       UpdateGiffer       (appMenuAbove); break;
		case AppState_MainMenu:     UpdateMainMenu     (appMenuAbove); break;
		default: WriteLine_W("Unhandled AppState"); break;
	}
	app->calledAppState = AppState_None;
	app->allowAppStateChanges = false;
}

void RenderAppState(AppState_t appState, FrameBuffer_t* renderBuffer, AppState_t appStateAbove, AppMenu_t appMenuAbove)
{
	app->calledAppState = appState;
	switch (appState)
	{
		case AppState_Game:         RenderGame         (renderBuffer, appStateAbove, appMenuAbove); break;
		case AppState_Mess:         RenderMess         (renderBuffer, appStateAbove, appMenuAbove); break;
		case AppState_Giffer:       RenderGiffer       (renderBuffer, appStateAbove, appMenuAbove); break;
		case AppState_MainMenu:     RenderMainMenu     (renderBuffer, appStateAbove, appMenuAbove); break;
		default: WriteLine_W("Unhandled AppState"); break;
	}
	app->calledAppState = AppState_None;
}

bool DoesAppStateCoverBelow(AppState_t appState, AppState_t belowState)
{
	bool result = true;
	app->calledAppState = appState;
	switch (appState)
	{
		case AppState_Game:         result = DoesGameCoverBelow         (belowState); break;
		case AppState_Mess:         result = DoesMessCoverBelow         (belowState); break;
		case AppState_Giffer:       result = DoesGifferCoverBelow       (belowState); break;
		case AppState_MainMenu:     result = DoesMainMenuCoverBelow     (belowState); break;
		default: WriteLine_W("Unhandled AppState"); break;
	}
	app->calledAppState = AppState_None;
	return result;
}

// +==============================+
// |          App Menus           |
// +==============================+
void InitializeAppMenu(AppMenu_t appMenu)
{
	PrintLine_Dx(DbgFlag_Inverted, "Initializing %s...", GetAppMenuName(appMenu));
	app->calledAppMenu = appMenu;
	switch (appMenu)
	{
		case AppMenu_Test: /*Do nothing*/ break;
		default: NotifyPrint_W("Unhandled app menu Initialize 0x%02X", appMenu); break;
	}
	app->calledAppMenu = AppMenu_None;
	PrintLine_Dx(DbgFlag_Inverted, "Finished initializing %s", GetAppMenuName(appMenu));
}

void DeinitializeAppMenu(AppMenu_t appMenu)
{
	PrintLine_Dx(DbgFlag_Inverted, "Deinitializing %s...", GetAppMenuName(appMenu));
	app->calledAppMenu = appMenu;
	switch (appMenu)
	{
		case AppMenu_Test: /*Do nothing*/ break;
		default: NotifyPrint_W("Unhandled app menu Deinitialize 0x%02X", appMenu); break;
	}
	app->calledAppMenu = AppMenu_None;
	PrintLine_Dx(DbgFlag_Inverted, "Finished deinitializing %s", GetAppMenuName(appMenu));
}

void StartAppMenu(AppMenu_t appMenu, AppMenu_t oldAppMenu, AppState_t appStateBelow, TransInfoType_t transInfoType, void* transInfoPntr)
{
	PrintLine_Dx(DbgFlag_Inverted, "Starting %s...", GetAppMenuName(appMenu));
	app->calledAppMenu = appMenu;
	switch (appMenu)
	{
		case AppMenu_Test: /*Do nothing*/ break;
		default: NotifyPrint_W("Unhandled app menu Start 0x%02X", appMenu); break;
	}
	app->calledAppMenu = AppMenu_None;
	PrintLine_Dx(DbgFlag_Inverted, "Finished starting %s", GetAppMenuName(appMenu));
}

void UpdateAppMenu(AppMenu_t appMenu, AppState_t appStateBelow, AppMenu_t appMenuAbove)
{
	app->allowAppStateChanges = true;
	app->calledAppMenu = appMenu;
	switch (appMenu)
	{
		case AppMenu_Test: /*Do nothing*/ break;
		default: WriteLine_W("Unhandled AppMenu"); break;
	}
	app->calledAppMenu = AppMenu_None;
	app->allowAppStateChanges = false;
}

void RenderAppMenu(AppMenu_t appMenu, FrameBuffer_t* renderBuffer, AppState_t appStateBelow, AppMenu_t appMenuAbove)
{
	app->calledAppMenu = appMenu;
	switch (appMenu)
	{
		case AppMenu_Test: /*Do nothing*/ break;
		default: WriteLine_W("Unhandled AppMenu"); break;
	}
	app->calledAppMenu = AppMenu_None;
}

// +--------------------------------------------------------------+
// |                       Helper Functions                       |
// +--------------------------------------------------------------+
void KillAppStateByIndex(u32 activeIndex, bool stayInitialized = false)
{
	Assert(activeIndex < app->numActiveStates);
	
	AppState_t appState = app->activeStates[activeIndex];
	if (!stayInitialized)
	{
		if (IsAppStateInitialized(appState))
		{
			DeinitializeAppState(appState);
			app->appStateInitialized[appState] = false;
		}
		else
		{
			PrintLine_W("AppState_%s already deinitialized!", GetAppStateName(appState));
		}
	}
	else
	{
		PrintLine_R("AppState_%s staying initialized...", GetAppStateName(appState));
	}
	
	//Move everything down by 1
	for (u32 sIndex = activeIndex; sIndex+1 < app->numActiveStates; sIndex++)
	{
		app->activeStates[sIndex] = app->activeStates[sIndex+1];
	}
	app->numActiveStates--;
}

void KillAppMenuByIndex(u32 activeIndex, bool stayInitialized = false)
{
	Assert(activeIndex < app->numActiveMenus);
	
	if (!stayInitialized)
	{
		AppMenu_t appMenu = app->activeMenus[activeIndex];
		if (IsAppMenuInitialized(appMenu))
		{
			DeinitializeAppMenu(appMenu);
			app->appMenuInitialized[appMenu] = false;
		}
		else
		{
			PrintLine_W("AppMenu_%s already deinitialized!", GetAppMenuName(appMenu));
		}
	}
	
	//Move everything down by 1
	for (u32 mIndex = activeIndex; mIndex+1 < app->numActiveMenus; mIndex++)
	{
		app->activeMenus[mIndex] = app->activeMenus[mIndex+1];
	}
	app->numActiveMenus--;
}

// +================================+
// | Real Push/Pop/Change Functions |
// +================================+
void PushAppState_(AppStateChange_t* change)
{
	Assert(change != nullptr && change->type == AppStateChangeType_Push);
	Assert(!IsAppStateActive(change->newAppState));
	Assert(app->numActiveStates < ArrayCount(app->activeStates));
	
	AppState_t oldAppState = CurrentAppState();
	app->activeStates[app->numActiveStates] = change->newAppState;
	app->numActiveStates++;
	
	if (change->forceResetNewState && IsAppStateInitialized(change->newAppState))
	{
		DeinitializeAppState(change->newAppState);
		app->appStateInitialized[change->newAppState] = false;
	}
	if (!IsAppStateInitialized(change->newAppState))
	{
		InitializeAppState(change->newAppState);
		app->appStateInitialized[change->newAppState] = true;
	}
	if (!StartAppState(change->newAppState, oldAppState, app->transInfoType, app->transInfo))
	{
		DeinitializeAppState(change->newAppState);
		Assert(oldAppState != AppState_None);
		app->appStateInitialized[change->newAppState] = false;
		oldAppState = change->newAppState;
		app->numActiveStates--;
		if (app->numActiveStates > 0)
		{
			bool startSuccess = StartAppState(app->activeStates[app->numActiveStates-1], oldAppState, TransInfoType_Nothing, nullptr);
			//NOTE: The lower AppState is not allowed to fail here
			Assert(startSuccess);
		}
	}
	app->transInfoType = TransInfoType_Nothing;
	app->transInfo = nullptr;
	app->screenWipeActive = false;
}
void PopAppState_(AppStateChange_t* change)
{
	Assert(change != nullptr && change->type == AppStateChangeType_Pop);
	Assert(app->numActiveStates > 0);
	
	AppState_t oldAppState = CurrentAppState();
	KillAppStateByIndex(app->numActiveStates-1, change->stayInitialized);
	
	if (app->numActiveStates > 0)
	{
		AppState_t newAppState = CurrentAppState();
		if (change->forceResetNewState && IsAppStateInitialized(newAppState))
		{
			DeinitializeAppState(newAppState);
			app->appStateInitialized[newAppState] = false;
		}
		if (!IsAppStateInitialized(newAppState))
		{
			InitializeAppState(newAppState);
			app->appStateInitialized[newAppState] = true;
		}
		bool startSuccess = StartAppState(newAppState, oldAppState, app->transInfoType, app->transInfo);
		//NOTE: An appState is not allowed to reject the landing of an upper state popping
		Assert(startSuccess); 
	}
	app->transInfoType = TransInfoType_Nothing;
	app->transInfo = nullptr;
	app->screenWipeActive = false;
}
void ChangeAppState_(AppStateChange_t* change)
{
	Assert(change != nullptr && change->type == AppStateChangeType_Change);
	Assert(app->numActiveStates > 0);
	Assert(!IsAppStateActive(change->newAppState));
	
	AppState_t oldAppState = CurrentAppState();
	KillAppStateByIndex(app->numActiveStates-1, change->stayInitialized);
	
	app->activeStates[app->numActiveStates] = change->newAppState;
	app->numActiveStates++;
	
	if (change->forceResetNewState && IsAppStateInitialized(change->newAppState))
	{
		DeinitializeAppState(change->newAppState);
		app->appStateInitialized[change->newAppState] = false;
	}
	if (!IsAppStateInitialized(change->newAppState))
	{
		InitializeAppState(change->newAppState);
		app->appStateInitialized[change->newAppState] = true;
	}
	if (!StartAppState(change->newAppState, oldAppState, app->transInfoType, app->transInfo))
	{
		DeinitializeAppState(change->newAppState);
		app->appStateInitialized[change->newAppState] = false;
		oldAppState = change->newAppState;
		app->numActiveStates--;
		if (app->numActiveStates > 0)
		{
			bool startSuccess = StartAppState(app->activeStates[app->numActiveStates-1], oldAppState, TransInfoType_Nothing, nullptr);
			//NOTE: The lower AppState is not allowed to fail here
			Assert(startSuccess);
		}
	}
	app->transInfoType = TransInfoType_Nothing;
	app->transInfo = nullptr;
	app->screenWipeActive = false;
}

void PushAppMenu_(AppMenuChange_t* change)
{
	Assert(change != nullptr && change->type == AppStateChangeType_Push);
	Assert(!IsAppMenuActive(change->newAppMenu));
	Assert(app->numActiveMenus < ArrayCount(app->activeMenus));
	
	AppMenu_t oldAppMenu = CurrentAppMenu();
	app->activeMenus[app->numActiveMenus] = change->newAppMenu;
	app->numActiveMenus++;
	
	if (change->forceResetNewMenu && IsAppMenuInitialized(change->newAppMenu))
	{
		DeinitializeAppMenu(change->newAppMenu);
		app->appMenuInitialized[change->newAppMenu] = false;
	}
	if (!IsAppMenuInitialized(change->newAppMenu))
	{
		InitializeAppMenu(change->newAppMenu);
		app->appMenuInitialized[change->newAppMenu] = true;
	}
	StartAppMenu(change->newAppMenu, oldAppMenu, CurrentAppState(), app->transInfoType, app->transInfo);
	app->transInfoType = TransInfoType_Nothing;
	app->transInfo = nullptr;
}
void PopAppMenu_(AppMenuChange_t* change)
{
	Assert(change != nullptr && change->type == AppStateChangeType_Pop);
	Assert(app->numActiveMenus > 0);
	
	AppMenu_t oldAppMenu = CurrentAppMenu();
	KillAppMenuByIndex(app->numActiveMenus-1, change->stayInitialized);
	
	if (app->numActiveMenus > 0)
	{
		AppMenu_t newAppMenu = CurrentAppMenu();
		if (change->forceResetNewMenu && IsAppMenuInitialized(newAppMenu))
		{
			DeinitializeAppMenu(newAppMenu);
			app->appMenuInitialized[newAppMenu] = false;
		}
		if (!IsAppMenuInitialized(newAppMenu))
		{
			InitializeAppMenu(newAppMenu);
			app->appMenuInitialized[newAppMenu] = true;
		}
		StartAppMenu(newAppMenu, oldAppMenu, CurrentAppState(), app->transInfoType, app->transInfo);
	}
	app->transInfoType = TransInfoType_Nothing;
	app->transInfo = nullptr;
}
void ChangeAppMenu_(AppMenuChange_t* change)
{
	Assert(change != nullptr && change->type == AppStateChangeType_Change);
	Assert(app->numActiveMenus > 0);
	Assert(!IsAppMenuActive(change->newAppMenu));
	
	AppMenu_t oldAppMenu = CurrentAppMenu();
	KillAppMenuByIndex(app->numActiveMenus-1, change->stayInitialized);
	
	app->activeMenus[app->numActiveMenus] = change->newAppMenu;
	app->numActiveMenus++;
	
	if (change->forceResetNewMenu && IsAppMenuInitialized(change->newAppMenu))
	{
		DeinitializeAppMenu(change->newAppMenu);
		app->appMenuInitialized[change->newAppMenu] = false;
	}
	if (!IsAppMenuInitialized(change->newAppMenu))
	{
		InitializeAppMenu(change->newAppMenu);
		app->appMenuInitialized[change->newAppMenu] = true;
	}
	StartAppMenu(change->newAppMenu, oldAppMenu, CurrentAppState(), app->transInfoType, app->transInfo);
	app->transInfoType = TransInfoType_Nothing;
	app->transInfo = nullptr;
}

// +--------------------------------------------------------------+
// |                       Public Functions                       |
// +--------------------------------------------------------------+
void PutTransitionInfo(TransInfoType_t type, void* infoPntr)
{
	Assert(type != TransInfoType_Nothing || infoPntr == nullptr);
	app->transInfoType = type;
	app->transInfo = infoPntr;
}


// +==============================+
// |          App States          |
// +==============================+
bool IsAppStateInitialized(AppState_t appState)
{
	return app->appStateInitialized[appState];
}

bool IsAppStateActive(AppState_t appState, u32* activeIndexOut)
{
	for (u32 sIndex = 0; sIndex < app->numActiveStates; sIndex++)
	{
		if (app->activeStates[sIndex] == appState)
		{
			if (activeIndexOut != nullptr) { *activeIndexOut = sIndex; }
			return true;
		}
	}
	return false;
}

AppState_t CurrentAppState()
{
	if (app->numActiveStates > 0) { return app->activeStates[app->numActiveStates-1]; }
	else { return AppState_None; }
}
AppState_t AppStateBelowMe(u32 numBelow)
{
	Assert(app->calledAppState != AppState_None);
	Assert(numBelow > 0);
	u32 activeIndex;
	if (IsAppStateActive(app->calledAppState, &activeIndex))
	{
		if (activeIndex >= numBelow)
		{
			return app->activeStates[activeIndex-numBelow];
		}
	}
	return AppState_None;
}
AppState_t AppStateAboveMe(u32 numAbove)
{
	Assert(app->calledAppState != AppState_None);
	Assert(numAbove > 0);
	u32 activeIndex;
	if (IsAppStateActive(app->calledAppState, &activeIndex))
	{
		if (activeIndex+numAbove < app->numActiveStates)
		{
			return app->activeStates[activeIndex+numAbove];
		}
	}
	return AppState_None;
}

bool KillAppState(AppState_t appState, bool stayInitialized)
{
	Assert(app->allowAppStateChanges);
	
	for (u32 sIndex = 0; sIndex < app->numActiveStates; sIndex++)
	{
		if (app->activeStates[sIndex] == appState)
		{
			KillAppStateByIndex(sIndex, stayInitialized);
			return true;
		}
	}
	
	if (!stayInitialized && IsAppStateInitialized(appState))
	{
		DeinitializeAppState(appState);
		app->appStateInitialized[appState] = false;
	}
	
	return false;
}

void PushAppState(AppState_t newAppState, bool forceResetNewState)
{
	Assert(app->allowAppStateChanges && !app->appStateChanged);
	Assert(!IsAppStateActive(newAppState));
	Assert(app->numActiveStates < ArrayCount(app->activeStates));
	app->appStateChange.type = AppStateChangeType_Push;
	app->appStateChange.newAppState = newAppState;
	app->appStateChange.forceResetNewState = forceResetNewState;
	app->appStateChanged = true;
}

void PushAppStateExt(AppState_t newAppState, TransInfoType_t type, void* infoPntr, bool forceResetNewState)
{
	PutTransitionInfo(type, infoPntr);
	PushAppState(newAppState, forceResetNewState);
}

void PopAppState(bool stayInitialized, bool forceResetNewState)
{
	Assert(app->allowAppStateChanges && !app->appStateChanged);
	Assert(app->numActiveStates > 0);
	app->appStateChange.type = AppStateChangeType_Pop;
	app->appStateChange.stayInitialized = stayInitialized;
	app->appStateChange.forceResetNewState = forceResetNewState;
	app->appStateChanged = true;
}

void PopAppStateExt(TransInfoType_t type, void* infoPntr, bool stayInitialized, bool forceResetNewState)
{
	PutTransitionInfo(type, infoPntr);
	PopAppState(stayInitialized, forceResetNewState);
}

void ChangeAppState(AppState_t newAppState, bool stayInitialized, bool forceResetNewState)
{
	Assert(app->allowAppStateChanges && !app->appStateChanged);
	Assert(app->numActiveStates > 0);
	app->appStateChange.type = AppStateChangeType_Change;
	app->appStateChange.newAppState = newAppState;
	app->appStateChange.stayInitialized = stayInitialized;
	app->appStateChange.forceResetNewState = forceResetNewState;
	app->appStateChanged = true;
}

void ChangeAppStateExt(AppState_t newAppState, TransInfoType_t type, void* infoPntr, bool stayInitialized, bool forceResetNewState)
{
	PutTransitionInfo(type, infoPntr);
	ChangeAppState(newAppState, stayInitialized, forceResetNewState);
}

// +==============================+
// |          App Menus           |
// +==============================+
bool IsAppMenuInitialized(AppMenu_t appMenu)
{
	return app->appMenuInitialized[appMenu];
}

bool IsAppMenuActive(AppMenu_t appMenu, u32* activeIndexOut)
{
	for (u32 mIndex = 0; mIndex < app->numActiveMenus; mIndex++)
	{
		if (app->activeMenus[mIndex] == appMenu)
		{
			if (activeIndexOut != nullptr) { *activeIndexOut = mIndex; }
			return true;
		}
	}
	return false;
}

AppMenu_t CurrentAppMenu()
{
	if (app->numActiveMenus > 0) { return app->activeMenus[app->numActiveMenus-1]; }
	else { return AppMenu_None; }
}
AppMenu_t AppMenuBelowMe(u32 numBelow)
{
	Assert(numBelow > 0);
	if (app->calledAppMenu != AppMenu_None)
	{
		u32 activeIndex;
		if (IsAppMenuActive(app->calledAppMenu, &activeIndex))
		{
			if (activeIndex >= numBelow)
			{
				return app->activeMenus[activeIndex-numBelow];
			}
		}
		return AppMenu_None;
	}
	else if (app->calledAppState != AppState_None)
	{
		return AppMenu_None;
	}
	else
	{
		Assert(false); //This can only be called from within an AppState/AppMenu function
		return AppMenu_None;
	}
}
AppMenu_t AppMenuAboveMe(u32 numAbove)
{
	Assert(numAbove > 0);
	if (app->calledAppMenu != AppMenu_None)
	{
		u32 activeIndex;
		if (IsAppMenuActive(app->calledAppMenu, &activeIndex))
		{
			if (activeIndex+numAbove < app->numActiveMenus)
			{
				return app->activeMenus[activeIndex+numAbove];
			}
		}
		return AppMenu_None;
	}
	else if (app->calledAppState != AppState_None)
	{
		if (app->numActiveMenus >= numAbove)
		{
			return app->activeMenus[numAbove-1];
		}
		return AppMenu_None;
	}
	else
	{
		Assert(false); //This can only be called from within an AppState/AppMenu function
		return AppMenu_None;
	}
}

bool KillAppMenu(AppMenu_t appMenu, bool stayInitialized)
{
	Assert(app->allowAppStateChanges);
	
	for (u32 mIndex = 0; mIndex < app->numActiveMenus; mIndex++)
	{
		if (app->activeMenus[mIndex] == appMenu)
		{
			KillAppMenuByIndex(mIndex, stayInitialized);
			return true;
		}
	}
	
	if (!stayInitialized && IsAppMenuInitialized(appMenu))
	{
		DeinitializeAppMenu(appMenu);
		app->appMenuInitialized[appMenu] = false;
	}
	
	return false;
}

void PushAppMenu(AppMenu_t newAppMenu, bool forceResetNewMenu)
{
	Assert(app->allowAppStateChanges && !app->appMenuChanged);
	Assert(!IsAppMenuActive(newAppMenu));
	Assert(app->numActiveMenus < ArrayCount(app->activeMenus));
	app->appMenuChange.type = AppStateChangeType_Push;
	app->appMenuChange.newAppMenu = newAppMenu;
	app->appMenuChange.forceResetNewMenu = forceResetNewMenu;
	app->appMenuChanged = true;
}

void PushAppMenuExt(AppMenu_t newAppMenu, TransInfoType_t type, void* infoPntr, bool forceResetNewMenu)
{
	PutTransitionInfo(type, infoPntr);
	PushAppMenu(newAppMenu, forceResetNewMenu);
}

void PopAppMenu(bool stayInitialized, bool forceResetNewMenu)
{
	Assert(app->allowAppStateChanges && !app->appMenuChanged);
	Assert(app->numActiveMenus > 0);
	app->appMenuChange.type = AppStateChangeType_Pop;
	app->appMenuChange.stayInitialized = stayInitialized;
	app->appMenuChange.forceResetNewMenu = forceResetNewMenu;
	app->appMenuChanged = true;
}

void PopAppMenuExt(TransInfoType_t type, void* infoPntr, bool stayInitialized, bool forceResetNewMenu)
{
	PutTransitionInfo(type, infoPntr);
	PopAppMenu(stayInitialized, forceResetNewMenu);
}

void ChangeAppMenu(AppMenu_t newAppMenu, bool stayInitialized, bool forceResetNewMenu)
{
	Assert(app->allowAppStateChanges && !app->appMenuChanged);
	Assert(app->numActiveMenus > 0);
	app->appMenuChange.type = AppStateChangeType_Change;
	app->appMenuChange.newAppMenu = newAppMenu;
	app->appMenuChange.stayInitialized = stayInitialized;
	app->appMenuChange.forceResetNewMenu = forceResetNewMenu;
	app->appMenuChanged = true;
}

void ChangeAppMenuExt(AppMenu_t newAppMenu, TransInfoType_t type, void* infoPntr, bool stayInitialized, bool forceResetNewMenu)
{
	PutTransitionInfo(type, infoPntr);
	ChangeAppMenu(newAppMenu, stayInitialized, forceResetNewMenu);
}

