/*
File:   app_console.h
Author: Taylor Robbins
Date:   03\22\2019
*/

#ifndef _APP_CONSOLE_H
#define _APP_CONSOLE_H

enum
{
	DbgFlag_UserInput     = 0x10,
	DbgFlag_PlatformLayer = 0x20,
	DbgFlag_TaskThread    = 0x40,
	// DbgFlag_App4       = 0x80,
};

struct DbgConsole_t
{
	bool isOpen;
	bool openLarge;
	bool fullyOpaque;
	r32 openAnimProgress;
	
	ConsoleArena_t arena;
	
	rec drawRec;
	rec inputBoxRec;
	rec viewRec;
	rec scrollGutterRec;
	rec scrollBarRec;
	
	bool lineWrap;
	bool showGutterNumbers;
	bool showFileNames;
	bool showLineNumbers;
	bool showFuncNames;
	bool showTaskNumbers;
	bool scrollToEnd;
	v2 scroll;
	v2 scrollGoto;
	bool remeasureContent;
	r32 maxFileNameWidth;
	r32 maxLineNumWidth;
	r32 maxFuncNameWidth;
	r32 maxGutterNumWidth;
	v2 contentSize;
	TextBox_t inputBox;
	bool inputBoxSelected;
	i32 mouseHoverLineIndex;
	i32 mouseHoverTextOffset;
	
	DynArray_t prevCommands;
	u32 recallIndex;
	char* topRecallCommand;
};

ConsoleLine_t* DbgConsoleOutput(DbgConsole_t* console, u8 flags, const char* filePath, u32 lineNumber, const char* funcName, DbgLevel_t dbgLevel, const char* message);

#endif //  _APP_CONSOLE_H
