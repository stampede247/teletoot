/*
File:   app_render_context.cpp
Author: Taylor Robbins
Date:   11\04\2017
Description: 
	** Lots of functions that facilitate drawing different
	** primitives and common things 
*/

#if RECORD_RC_DEBUG_INFO
#define RcDebugIncrement(variableName) if (app->rcDebugInfo != nullptr && app->rcDebugInfoSize == sizeof(RcDebugInfo_t)) { app->rcDebugInfo->variableName++; }
#define RcDebugIncrementBy(variableName, amount) if (app->rcDebugInfo != nullptr && app->rcDebugInfoSize == sizeof(RcDebugInfo_t)) { app->rcDebugInfo->variableName += (amount); }
#else
#define RcDebugIncrement(variableName) //nothing
#define RcDebugIncrementBy(variableName, amount) //nothing
#endif

void InitializeRenderContext()
{
	Assert(rc != nullptr);
	ClearPointer(rc);
	
	Vertex_t squareVertices[] =
	{
		{  {0.0f, 0.0f, 0.0f}, NewVec4FromColor(White), {0.0f, 0.0f} },
		{  {1.0f, 0.0f, 0.0f}, NewVec4FromColor(White), {1.0f, 0.0f} },
		{  {0.0f, 1.0f, 0.0f}, NewVec4FromColor(White), {0.0f, 1.0f} },
		
		{  {0.0f, 1.0f, 0.0f}, NewVec4FromColor(White), {0.0f, 1.0f} },
		{  {1.0f, 0.0f, 0.0f}, NewVec4FromColor(White), {1.0f, 0.0f} },
		{  {1.0f, 1.0f, 0.0f}, NewVec4FromColor(White), {1.0f, 1.0f} },
	};
	rc->squareBuffer = CreateVertexBuffer(squareVertices, ArrayCount(squareVertices));
	
	r32 triangleHeight = SqrtR32(3)/2.0f;
	Vertex_t equilTriangleVertices[] =
	{
		{  { 0.0f, 0.0f,           0.0f}, NewVec4FromColor(White), {0.5f, 0.0f} },
		{  { 0.5f, triangleHeight, 0.0f}, NewVec4FromColor(White), {1.0f, 1.0f} },
		{  {-0.5f, triangleHeight, 0.0f}, NewVec4FromColor(White), {0.0f, 1.0f} },
	};
	rc->equilTriangleBuffer = CreateVertexBuffer(equilTriangleVertices, ArrayCount(equilTriangleVertices));
	
	//TODO: We should just generate this texture rather than loading it from a file since it's a really straightforward gradient from black to white
	rc->gradientTexture = LoadTexture(TEXTURES_FLDR "gradient.png", false, false);
	
	Color_t textureData = White;
	rc->dotTexture = CreateTexture((u8*)&textureData, 1, 1);
	
	rc->strPosSnapEnabled = true;
	rc->worldMatrix = Mat4_Identity;
	rc->viewMatrix = Mat4_Identity;
	rc->projectionMatrix = Mat4_Identity;
	rc->viewport = NewRec(0, 0, (r32)RenderScreenSize.width, (r32)RenderScreenSize.height);
	rc->primaryColor = White;
	rc->secondaryColor = White;
	rc->replaceColors[0]  = NewVec4FromColor(ReplacePink1);
	rc->replaceColors[1]  = NewVec4FromColor(ReplacePink2);
	rc->replaceColors[2]  = NewVec4FromColor(ReplacePink3);
	rc->replaceColors[3]  = NewVec4FromColor(ReplacePink4);
	rc->replaceColors[4]  = NewVec4FromColor(ReplacePink5);
	rc->replaceColors[5]  = NewVec4FromColor(ReplacePink6);
	rc->replaceColors[6]  = NewVec4FromColor(ReplacePink7);
	rc->replaceColors[7]  = NewVec4FromColor(ReplacePink8);
	rc->replaceColors[8]  = NewVec4FromColor(ReplacePink9);
	rc->replaceColors[9]  = NewVec4FromColor(ReplacePink10);
	rc->replaceColors[10] = NewVec4FromColor(ReplacePink11);
	rc->replaceColors[11] = NewVec4FromColor(ReplacePink12);
	rc->replaceColors[12] = NewVec4FromColor(ReplacePink13);
	rc->replaceColors[13] = NewVec4FromColor(ReplacePink14);
	rc->replaceColors[14] = NewVec4FromColor(ReplacePink15);
	rc->replaceColors[15] = NewVec4FromColor(ReplacePink16);
	rc->sourceRectangle = NewRec(0, 0, 1, 1);
	rc->altSourceRectangle = NewRec(0, 0, 1, 1);
	rc->maskRectangle = NewRec(0, 0, 0, 0);
	rc->shiftVec = Vec2_Zero;
	rc->gradientEnabled = false;
	rc->circleRadius = 0.0f;
	rc->circleInnerRadius = 0.0f;
	rc->saturation = 1.0f;
	rc->brightness = 1.0f;
	rc->time = 0.0f;
	rc->depth = 1.0f;
	rc->value0 = 0.0f;
	rc->value1 = 0.0f;
	rc->value2 = 0.0f;
	rc->value3 = 0.0f;
	rc->value4 = 0.0f;
	rc->mousePos = RenderMousePos;
	rc->screenSize = RenderScreenSize;
}

// +--------------------------------------------------------------+
// |                    State Change Functions                    |
// +--------------------------------------------------------------+
void RcBindShader(const Shader_t* shaderPntr, bool forceUpdate = false)
{
	if (rc->boundShader == shaderPntr && !forceUpdate) { return; }
	rc->boundShader = shaderPntr;
	
	RcDebugIncrement(numShaderBinds);
	// if (ButtonDownNoHandling(Button_Space)) { MyDebugBreak(); }
	
	if (shaderPntr == nullptr)
	{
		glUseProgram(0);
	}
	else
	{
		glUseProgram(shaderPntr->programId);
		
		if (rc->boundBuffer != nullptr)
		{
			glBindVertexArray(rc->boundShader->vertexArray);
			glBindBuffer(GL_ARRAY_BUFFER, rc->boundBuffer->id);
			glVertexAttribPointer(rc->boundShader->locations.positionAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex_t), (void*)0);
			glVertexAttribPointer(rc->boundShader->locations.colorAttrib,    4, GL_FLOAT, GL_FALSE, sizeof(Vertex_t), (void*)sizeof(v3));
			glVertexAttribPointer(rc->boundShader->locations.texCoordAttrib, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex_t), (void*)(sizeof(v3)+sizeof(v4)));
		}
		else
		{
			glBindVertexArray(0);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
		}
		
		glActiveTexture(GL_TEXTURE0);
		if (rc->boundTexture != nullptr)
		{
			glBindTexture(GL_TEXTURE_2D, rc->boundTexture->id);
			glUniform1i(rc->boundShader->locations.texture, 0);
			glUniform2f(rc->boundShader->locations.textureSize, (r32)rc->boundTexture->width, (r32)rc->boundTexture->height);
		}
		else
		{
			glBindTexture(GL_TEXTURE_2D, rc->dotTexture.id);
		}
		
		glActiveTexture(GL_TEXTURE1);
		if (rc->boundAltTexture != nullptr)
		{
			glBindTexture(GL_TEXTURE_2D, rc->boundAltTexture->id);
			glUniform1i(rc->boundShader->locations.altTexture, 1);
			glUniform2f(rc->boundShader->locations.altTextureSize, (r32)rc->boundAltTexture->width, (r32)rc->boundAltTexture->height);
		}
		else
		{
			glBindTexture(GL_TEXTURE_2D, 0);
		}
		
		glUniformMatrix4fv(rc->boundShader->locations.worldMatrix,      1, GL_FALSE, &rc->worldMatrix.values[0][0]);
		glUniformMatrix4fv(rc->boundShader->locations.viewMatrix,       1, GL_FALSE, &rc->viewMatrix.values[0][0]);
		glUniformMatrix4fv(rc->boundShader->locations.projectionMatrix, 1, GL_FALSE, &rc->projectionMatrix.values[0][0]);
		
		v4 colorVec = NewVec4FromColor(rc->primaryColor);
		glUniform4f(rc->boundShader->locations.primaryColor, colorVec.r, colorVec.g, colorVec.b, colorVec.a);
		colorVec = NewVec4FromColor(rc->secondaryColor);
		glUniform4f(rc->boundShader->locations.secondaryColor, colorVec.r, colorVec.g, colorVec.b, colorVec.a);
		glUniform4fv(rc->boundShader->locations.replaceColors, NUM_REPLACE_COLORS, &rc->replaceColors[0].values[0]);
		// colorVec = NewVec4FromColor(rc->replaceColor1);
		// glUniform4f(rc->boundShader->locations.replaceColor1, colorVec.r, colorVec.g, colorVec.b, colorVec.a);
		// colorVec = NewVec4FromColor(rc->replaceColor2);
		// glUniform4f(rc->boundShader->locations.replaceColor2, colorVec.r, colorVec.g, colorVec.b, colorVec.a);
		// colorVec = NewVec4FromColor(rc->replaceColor3);
		// glUniform4f(rc->boundShader->locations.replaceColor3, colorVec.r, colorVec.g, colorVec.b, colorVec.a);
		// colorVec = NewVec4FromColor(rc->replaceColor4);
		// glUniform4f(rc->boundShader->locations.replaceColor4, colorVec.r, colorVec.g, colorVec.b, colorVec.a);
		
		glUniform4f(rc->boundShader->locations.sourceRectangle, rc->sourceRectangle.x, rc->sourceRectangle.y, rc->sourceRectangle.width, rc->sourceRectangle.height);
		glUniform4f(rc->boundShader->locations.altSourceRectangle, rc->altSourceRectangle.x, rc->altSourceRectangle.y, rc->altSourceRectangle.width, rc->altSourceRectangle.height);
		glUniform4f(rc->boundShader->locations.maskRectangle, rc->maskRectangle.x, rc->maskRectangle.y, rc->maskRectangle.width, rc->maskRectangle.height);
		glUniform2f(rc->boundShader->locations.shiftVec, rc->shiftVec.x, rc->shiftVec.y);
		glUniform1i(rc->boundShader->locations.gradientEnabled, rc->gradientEnabled ? 1 : 0);
		glUniform1f(rc->boundShader->locations.circleRadius, rc->circleRadius);
		glUniform1f(rc->boundShader->locations.circleInnerRadius, rc->circleInnerRadius);
		glUniform1f(rc->boundShader->locations.saturation, rc->saturation);
		glUniform1f(rc->boundShader->locations.brightness, rc->brightness);
		glUniform1f(rc->boundShader->locations.time, rc->time);
		glUniform1f(rc->boundShader->locations.value0, rc->value0);
		glUniform1f(rc->boundShader->locations.value1, rc->value1);
		glUniform1f(rc->boundShader->locations.value2, rc->value2);
		glUniform1f(rc->boundShader->locations.value3, rc->value3);
		glUniform1f(rc->boundShader->locations.value4, rc->value4);
		glUniform2f(rc->boundShader->locations.mousePos, rc->mousePos.x, rc->mousePos.y);
		glUniform2f(rc->boundShader->locations.screenSize, rc->screenSize.x, rc->screenSize.y);
	}
}

void RcSetWorldMatrix(const mat4& worldMatrix)
{
	rc->worldMatrix = worldMatrix;
	if (rc->boundShader != nullptr)
	{
		glUniformMatrix4fv(rc->boundShader->locations.worldMatrix, 1, GL_FALSE, &worldMatrix.values[0][0]);
	}
}
void RcSetViewMatrix(const mat4& viewMatrix)
{
	rc->viewMatrix = viewMatrix;
	if (rc->boundShader != nullptr)
	{
		glUniformMatrix4fv(rc->boundShader->locations.viewMatrix, 1, GL_FALSE, &viewMatrix.values[0][0]);
	}
}
void RcSetProjectionMatrix(const mat4& projectionMatrix)
{
	rc->projectionMatrix = projectionMatrix;
	if (rc->boundShader != nullptr)
	{
		glUniformMatrix4fv(rc->boundShader->locations.projectionMatrix, 1, GL_FALSE, &projectionMatrix.values[0][0]);
	}
}

void RcSetViewport(rec viewport, bool round = true)
{
	r32 scalar = SinR32((ProgramTime%10000)/10000.0f * Pi32*2);
	rc->viewport = viewport;
	if (round)
	{
		rc->viewport.topLeft = Vec2Round(rc->viewport.topLeft);
		rc->viewport.size = Vec2Round(rc->viewport.size);
	}
	
	bool flipYAxis = (rc->boundFrameBuffer == nullptr);
	reci openglRec = NewReci(
		(i32)rc->viewport.x,
		0,
		(i32)(rc->viewport.width),
		(i32)(rc->viewport.height)
	);
	if (rc->boundFrameBuffer != nullptr)
	{
		openglRec.y = (i32)(rc->viewport.y);
	}
	else
	{
		openglRec.y = (i32)(RenderScreenSize.height - (rc->viewport.y + rc->viewport.height));	
	}
	glViewport(openglRec.x, openglRec.y, openglRec.width, openglRec.height);
	
	mat4 projMatrix = Mat4_Identity;
	projMatrix = Mat4Scale(NewVec3(2.0f/rc->viewport.width, (flipYAxis ? -1 : 1) * 2.0f/rc->viewport.height, 1.0f));
	projMatrix = Mat4Multiply(projMatrix, Mat4Translate(NewVec3(-rc->viewport.width/2.0f, -rc->viewport.height/2.0f, 0.0f)));
	projMatrix = Mat4Multiply(projMatrix, Mat4Translate(NewVec3(-rc->viewport.x, -rc->viewport.y, 0.0f)));
	RcSetProjectionMatrix(projMatrix);
}

void RcBindFrameBuffer(const FrameBuffer_t* frameBuffer)
{
	rc->boundFrameBuffer = frameBuffer;
	
	if (frameBuffer == nullptr)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
	else
	{
		glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer->id);
	}
	RcSetViewport(rc->viewport);
}
void RcBindBuffer(const VertexBuffer_t* vertBufferPntr)
{
	if (rc->boundBuffer == vertBufferPntr) { return; }
	rc->boundBuffer = vertBufferPntr;
	
	if (vertBufferPntr == nullptr || rc->boundShader == nullptr)
	{
		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	else
	{
		glBindVertexArray(rc->boundShader->vertexArray);
		glBindBuffer(GL_ARRAY_BUFFER, vertBufferPntr->id);
		if (rc->boundShader->usesSheetFrameVerts)
		{
			glVertexAttribPointer(rc->boundShader->locations.positionAttrib,      3,                    GL_FLOAT, GL_FALSE, sizeof(SheetFrameVertex_t), (void*)0);
			glVertexAttribPointer(rc->boundShader->locations.colorAttrib,         4,                    GL_FLOAT, GL_FALSE, sizeof(SheetFrameVertex_t), (void*)sizeof(v3));
			glVertexAttribPointer(rc->boundShader->locations.texCoordAttrib,      2,                    GL_FLOAT, GL_FALSE, sizeof(SheetFrameVertex_t), (void*)(sizeof(v3)+sizeof(v4)));
			for (u32 rIndex = 0; rIndex < NUM_REPLACE_COLORS-4; rIndex++)
			{
				glVertexAttribPointer(rc->boundShader->locations.replaceColorAttribs[rIndex], 4, GL_FLOAT, GL_FALSE, sizeof(SheetFrameVertex_t), (void*)(sizeof(v3)+sizeof(v4)+sizeof(v2) + (sizeof(v4)*rIndex)));
			}
		}
		else
		{
			glVertexAttribPointer(rc->boundShader->locations.positionAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex_t), (void*)0);
			glVertexAttribPointer(rc->boundShader->locations.colorAttrib,    4, GL_FLOAT, GL_FALSE, sizeof(Vertex_t), (void*)sizeof(v3));
			glVertexAttribPointer(rc->boundShader->locations.texCoordAttrib, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex_t), (void*)(sizeof(v3)+sizeof(v4)));
		}
	}
}
void RcBindTexture(const Texture_t* texturePntr)
{
	if (rc->boundTexture == texturePntr) { return; }
	rc->boundTexture = texturePntr;
	
	RcDebugIncrement(numTextureBinds);
	
	glActiveTexture(GL_TEXTURE0);
	if (texturePntr == nullptr)
	{
		glBindTexture(GL_TEXTURE_2D, rc->dotTexture.id);
	}
	else
	{
		glBindTexture(GL_TEXTURE_2D, rc->boundTexture->id);
		glUniform1i(rc->boundShader->locations.texture, 0);
		glUniform2f(rc->boundShader->locations.textureSize, (r32)rc->boundTexture->width, (r32)rc->boundTexture->height);
	}
}
void RcBindAltTexture(const Texture_t* texturePntr)
{
	rc->boundAltTexture = texturePntr;
	
	glActiveTexture(GL_TEXTURE1);
	if (texturePntr == nullptr)
	{
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	else
	{
		glBindTexture(GL_TEXTURE_2D, rc->boundAltTexture->id);
		glUniform1i(rc->boundShader->locations.altTexture, 1);
		glUniform2f(rc->boundShader->locations.altTextureSize, (r32)rc->boundAltTexture->width, (r32)rc->boundAltTexture->height);
	}
}
void RcBindFont(const Font_t* fontPntr, r32 fontScale = 1.0f, bool strPosSnapEnabled = true)
{
	rc->boundFont = fontPntr;
	rc->fontScale = fontScale;
	rc->strPosSnapEnabled = strPosSnapEnabled;
}

void RcDrawBufferTriangles(u32 numVertices = 0)
{
	RcDebugIncrement(numDrawCalls);
	Assert(rc->boundBuffer != nullptr);
	Assert(rc->boundShader != nullptr);
	Assert(rc->boundBuffer->isSheetVertices == rc->boundShader->usesSheetFrameVerts);
	if (numVertices == 0) { numVertices = rc->boundBuffer->numVertices; }
	glDrawArrays(GL_TRIANGLES, 0, numVertices);
}

void RcSetColor(Color_t color)
{
	rc->primaryColor = color;
	if (rc->boundShader != nullptr)
	{
		glUniform4f(rc->boundShader->locations.primaryColor, color.r / 255.f, color.g / 255.f, color.b / 255.f, color.a / 255.f);
	}
}
void RcSetSecondaryColor(Color_t color)
{
	rc->secondaryColor = color;
	if (rc->boundShader != nullptr)
	{
		glUniform4f(rc->boundShader->locations.secondaryColor, color.r / 255.f, color.g / 255.f, color.b / 255.f, color.a / 255.f);
	}
}

void RcSetReplaceColor(u8 index, Color_t color)
{
	Assert(index < NUM_REPLACE_COLORS);
	r32 red   = ((r32)color.r / 255.0f);
	r32 green = ((r32)color.g / 255.0f);
	r32 blue  = ((r32)color.b / 255.0f);
	r32 alpha = ((r32)color.a / 255.0f);
	if (rc->replaceColors[index].x != red || rc->replaceColors[index].y != green || rc->replaceColors[index].z != blue || rc->replaceColors[index].w != alpha)
	{
		rc->replaceColors[index].x = red;
		rc->replaceColors[index].y = green;
		rc->replaceColors[index].z = blue;
		rc->replaceColors[index].w = alpha;
		if (rc->boundShader != nullptr)
		{
			glUniform4fv(rc->boundShader->locations.replaceColors, NUM_REPLACE_COLORS, &rc->replaceColors[0].values[0]);
		}
	}
}
void RcSetReplaceColorDark(Color_t color)
{
	RcSetReplaceColor(4, color);
}
void RcSetReplaceColorPrimary(Color_t color)
{
	RcSetReplaceColor(8, color);
}
void RcSetReplaceColorSecondary(Color_t color)
{
	RcSetReplaceColor(9, color);
}
void RcSetReplaceColorLight(Color_t color)
{
	RcSetReplaceColor(11, color);
}
void RcSetReplaceColors(PalColor_t color)
{
	bool colorsChanged = false;
	for (u32 cIndex = 0; cIndex < NUM_REPLACE_COLORS; cIndex++)
	{
		r32 red   = ((r32)color.colors[cIndex].r / 255.0f);
		r32 green = ((r32)color.colors[cIndex].g / 255.0f);
		r32 blue  = ((r32)color.colors[cIndex].b / 255.0f);
		r32 alpha = ((r32)color.colors[cIndex].a / 255.0f);
		if (rc->replaceColors[cIndex].x != red || rc->replaceColors[cIndex].y != green || rc->replaceColors[cIndex].z != blue || rc->replaceColors[cIndex].w != alpha)
		{
			rc->replaceColors[cIndex].x = red;
			rc->replaceColors[cIndex].y = green;
			rc->replaceColors[cIndex].z = blue;
			rc->replaceColors[cIndex].w = alpha;
			colorsChanged = true;
		}
	}
	if (rc->boundShader != nullptr && colorsChanged)
	{
		glUniform4fv(rc->boundShader->locations.replaceColors, NUM_REPLACE_COLORS, &rc->replaceColors[0].values[0]);
	}
}
void RcSetAllReplaceColors(Color_t color)
{
	bool colorsChanged = false;
	v4 newColor = NewVec4FromColor(color);
	for (u32 cIndex = 0; cIndex < NUM_REPLACE_COLORS; cIndex++)
	{
		if (rc->replaceColors[cIndex] != newColor) { rc->replaceColors[cIndex] = newColor; colorsChanged = true; }
	}
	if (rc->boundShader != nullptr && colorsChanged)
	{
		glUniform4fv(rc->boundShader->locations.replaceColors, NUM_REPLACE_COLORS, &rc->replaceColors[0].values[0]);
	}
}
void RcDefaultReplaceColors()
{
	rc->replaceColors[0]  = NewVec4FromColor(ReplacePink1);
	rc->replaceColors[1]  = NewVec4FromColor(ReplacePink2);
	rc->replaceColors[2]  = NewVec4FromColor(ReplacePink3);
	rc->replaceColors[3]  = NewVec4FromColor(ReplacePink4);
	rc->replaceColors[4]  = NewVec4FromColor(ReplacePink5);
	rc->replaceColors[5]  = NewVec4FromColor(ReplacePink6);
	rc->replaceColors[6]  = NewVec4FromColor(ReplacePink7);
	rc->replaceColors[7]  = NewVec4FromColor(ReplacePink8);
	rc->replaceColors[8]  = NewVec4FromColor(ReplacePink9);
	rc->replaceColors[9]  = NewVec4FromColor(ReplacePink10);
	rc->replaceColors[10] = NewVec4FromColor(ReplacePink11);
	rc->replaceColors[11] = NewVec4FromColor(ReplacePink12);
	rc->replaceColors[12] = NewVec4FromColor(ReplacePink13);
	rc->replaceColors[13] = NewVec4FromColor(ReplacePink14);
	rc->replaceColors[14] = NewVec4FromColor(ReplacePink15);
	rc->replaceColors[15] = NewVec4FromColor(ReplacePink16);
	if (rc->boundShader != nullptr)
	{
		glUniform4fv(rc->boundShader->locations.replaceColors, NUM_REPLACE_COLORS, &rc->replaceColors[0].values[0]);
	}
}

void RcSetSourceRectangle(rec sourceRectangle, bool round = true)
{
	rc->sourceRectangle = sourceRectangle;
	if (round)
	{
		rc->sourceRectangle.topLeft = Vec2Round(rc->sourceRectangle.topLeft);
		rc->sourceRectangle.size = Vec2Round(rc->sourceRectangle.size);
	}
	if (rc->boundShader != nullptr)
	{
		glUniform4f(rc->boundShader->locations.sourceRectangle, rc->sourceRectangle.x, rc->sourceRectangle.y, rc->sourceRectangle.width, rc->sourceRectangle.height);
	}
}
void RcDefaultSourceRectangle()
{
	if (rc->boundTexture != nullptr)
	{
		RcSetSourceRectangle(NewRec(0, 0, (r32)rc->boundTexture->width, (r32)rc->boundTexture->height));
	}
	else
	{
		RcSetSourceRectangle(NewRec(0, 0, 0, 0));
	}
}

void RcSetAltSourceRectangle(rec sourceRectangle)
{
	rc->altSourceRectangle = sourceRectangle;
	if (rc->boundShader != nullptr)
	{
		glUniform4f(rc->boundShader->locations.altSourceRectangle, rc->altSourceRectangle.x, rc->altSourceRectangle.y, rc->altSourceRectangle.width, rc->altSourceRectangle.height);
	}
}
void RcDefaultAltSourceRectangle()
{
	if (rc->boundAltTexture != nullptr)
	{
		RcSetSourceRectangle(NewRec(0, 0, (r32)rc->boundAltTexture->width, (r32)rc->boundAltTexture->height));
	}
	else
	{
		RcSetSourceRectangle(NewRec(0, 0, 0, 0));
	}
}

void RcSetMaskRectangle(rec maskRectangle)
{
	rc->maskRectangle = maskRectangle;
	if (rc->boundShader != nullptr)
	{
		glUniform4f(rc->boundShader->locations.maskRectangle, maskRectangle.x, maskRectangle.y, maskRectangle.width, maskRectangle.height);
	}
}
void RcSetMaskSheetFrame(const SpriteSheet_t* spriteSheet, v2i frame)
{
	rec maskRec = NewRec(NewVec2(Vec2iMultiply(spriteSheet->frameSize, frame) + spriteSheet->padding), NewVec2(spriteSheet->innerFrameSize));
	RcSetMaskRectangle(maskRec);
}
void RcDisableMaskRectangle()
{
	RcSetMaskRectangle(NewRec(0, 0, 0, 0));
}

void RcSetShiftVec(v2 shiftVec)
{
	rc->shiftVec = shiftVec;
	if (rc->boundShader != nullptr)
	{
		glUniform2f(rc->boundShader->locations.shiftVec, shiftVec.x, shiftVec.y);
	}
}

void RcSetGradientEnabled(bool gradientEnabled)
{
	rc->gradientEnabled = gradientEnabled;
	if (rc->boundShader != nullptr)
	{
		glUniform1i(rc->boundShader->locations.gradientEnabled, gradientEnabled ? 1 : 0);
	}
}

void RcSetCircleRadius(r32 radius, r32 innerRadius = 0.0f)
{
	rc->circleRadius = radius;
	rc->circleInnerRadius = innerRadius;
	if (rc->boundShader != nullptr)
	{
		glUniform1f(rc->boundShader->locations.circleRadius, radius);
		glUniform1f(rc->boundShader->locations.circleInnerRadius, innerRadius);
	}
}

void RcSetSaturation(r32 saturation)
{
	rc->saturation = saturation;
	if (rc->boundShader != nullptr)
	{
		glUniform1f(rc->boundShader->locations.saturation, saturation);
	}
}

void RcSetBrightness(r32 brightness)
{
	rc->brightness = brightness;
	if (rc->boundShader != nullptr)
	{
		glUniform1f(rc->boundShader->locations.brightness, brightness);
	}
}

void RcSetTime(r32 time)
{
	rc->time = time;
	if (rc->boundShader != nullptr)
	{
		glUniform1f(rc->boundShader->locations.time, time);
	}
}

void RcSetDepth(r32 depth)
{
	rc->depth = depth;
}

void RcSetShaderValue0(r32 value)
{
	rc->value0 = value;
	if (rc->boundShader != nullptr)
	{
		glUniform1f(rc->boundShader->locations.value0, value);
	}
}
void RcSetShaderValue1(r32 value)
{
	rc->value1 = value;
	if (rc->boundShader != nullptr)
	{
		glUniform1f(rc->boundShader->locations.value1, value);
	}
}
void RcSetShaderValue2(r32 value)
{
	rc->value2 = value;
	if (rc->boundShader != nullptr)
	{
		glUniform1f(rc->boundShader->locations.value2, value);
	}
}
void RcSetShaderValue3(r32 value)
{
	rc->value3 = value;
	if (rc->boundShader != nullptr)
	{
		glUniform1f(rc->boundShader->locations.value3, value);
	}
}
void RcSetShaderValue4(r32 value)
{
	rc->value4 = value;
	if (rc->boundShader != nullptr)
	{
		glUniform1f(rc->boundShader->locations.value4, value);
	}
}
void RcSetShaderValue(u8 index, r32 value)
{
	switch (index)
	{
		case 0: RcSetShaderValue0(value); break;
		case 1: RcSetShaderValue1(value); break;
		case 2: RcSetShaderValue2(value); break;
		case 3: RcSetShaderValue3(value); break;
		case 4: RcSetShaderValue4(value); break;
		default: PrintLine_E("Invalid shader index passed to RcSetShaderValue: %u", index); break;
	}
}

void RcSetMousePos(v2 mousePos)
{
	rc->mousePos = mousePos;
	if (rc->boundShader != nullptr)
	{
		glUniform2f(rc->boundShader->locations.mousePos, mousePos.x, mousePos.y);
	}
}
void RcSetScreenSize(v2 screenSize)
{
	rc->screenSize = screenSize;
	if (rc->boundShader != nullptr)
	{
		glUniform2f(rc->boundShader->locations.screenSize, screenSize.x, screenSize.y);
	}
}

// +--------------------------------------------------------------+
// |                      Batching Functions                      |
// +--------------------------------------------------------------+
bool RcStartFontBatch(const Texture_t* batchFontBake, MemoryArena_t* vertsMemArena, u32 numGlyphsExpected = FONT_BATCH_BASE_CHUNK_SIZE)
{
	Assert(batchFontBake != nullptr);
	Assert(vertsMemArena != nullptr);
	if (rc->numBatchingFonts >= MAX_BATCHING_FONTS) { return false; }
	rc->batchingFontBakes[rc->numBatchingFonts] = batchFontBake;
	if (rc->batchingFontVerts[rc->numBatchingFonts].items == nullptr)
	{
		CreateDynamicArray(&rc->batchingFontVerts[rc->numBatchingFonts], vertsMemArena, sizeof(FontBatchVertex_t), FONT_BATCH_CHUNK_SIZE, numGlyphsExpected);
	}
	rc->numBatchingFonts++;
	return true;
}
bool RcBatchGlyph(const Texture_t* fontBake, v2 position, Color_t color, rec sourceRec, r32 scale)
{
	for (u32 bIndex = 0; bIndex < rc->numBatchingFonts; bIndex++)
	{
		const Texture_t* batchFontBake = rc->batchingFontBakes[bIndex];
		DynArray_t* vertsArray = &rc->batchingFontVerts[bIndex];
		if (batchFontBake == fontBake)
		{
			DynArrayExpand(vertsArray, vertsArray->length+1);
			FontBatchVertex_t* newVertex = DynArrayAdd(vertsArray, FontBatchVertex_t);
			Assert(newVertex != nullptr);
			ClearPointer(newVertex);
			newVertex->position = NewVec3(position.x, position.y, rc->depth);
			newVertex->color = NewVec4FromColor(color);
			newVertex->sourceRec = sourceRec;
			newVertex->scale = scale;
			return true;
		}
	}
	return false;
}
void RcFinishFontBatches(const Shader_t* shader)
{
	const Shader_t* oldShader = rc->boundShader;
	for (u32 bIndex = 0; bIndex < rc->numBatchingFonts; bIndex++)
	{
		const Texture_t* batchFontBake = rc->batchingFontBakes[bIndex];
		DynArray_t* vertsArray = &rc->batchingFontVerts[bIndex];
		if (vertsArray->length > 0)
		{
			TempPushMark();
			u32 numVerts = vertsArray->length * 6;
			Vertex_t* verts = PushArray(TempArena, Vertex_t, numVerts);
			if (verts != nullptr)
			{
				for (u32 vIndex = 0; vIndex < vertsArray->length; vIndex++)
				{
					FontBatchVertex_t* fontVert = DynArrayGet(vertsArray, FontBatchVertex_t, vIndex);
					Assert(fontVert != nullptr);
					Vertex_t* vertsPntr = &verts[6*vIndex];
					rec sourceRec = fontVert->sourceRec;
					r32 scale = fontVert->scale;
					
					//top left
					vertsPntr[0].position.x = fontVert->position.x;
					vertsPntr[0].position.y = fontVert->position.y;
					vertsPntr[0].position.z = fontVert->position.z;
					vertsPntr[0].color = fontVert->color;
					vertsPntr[0].texCoord.x = sourceRec.x;
					vertsPntr[0].texCoord.y = sourceRec.y;
					//top right
					vertsPntr[1].position.x = fontVert->position.x + sourceRec.width*scale;
					vertsPntr[1].position.y = fontVert->position.y;
					vertsPntr[1].position.z = fontVert->position.z;
					vertsPntr[1].color = fontVert->color;
					vertsPntr[1].texCoord.x = sourceRec.x + sourceRec.width;
					vertsPntr[1].texCoord.y = sourceRec.y;
					//bottom left
					vertsPntr[2].position.x = fontVert->position.x;
					vertsPntr[2].position.y = fontVert->position.y + sourceRec.height*scale;
					vertsPntr[2].position.z = fontVert->position.z;
					vertsPntr[2].color = fontVert->color;
					vertsPntr[2].texCoord.x = sourceRec.x;
					vertsPntr[2].texCoord.y = sourceRec.y + sourceRec.height;
					
					//bottom left
					vertsPntr[3].position.x = fontVert->position.x;
					vertsPntr[3].position.y = fontVert->position.y + sourceRec.height*scale;
					vertsPntr[3].position.z = fontVert->position.z;
					vertsPntr[3].color = fontVert->color;
					vertsPntr[3].texCoord.x = sourceRec.x;
					vertsPntr[3].texCoord.y = sourceRec.y + sourceRec.height;
					//top right
					vertsPntr[4].position.x = fontVert->position.x + sourceRec.width*scale;
					vertsPntr[4].position.y = fontVert->position.y;
					vertsPntr[4].position.z = fontVert->position.z;
					vertsPntr[4].color = fontVert->color;
					vertsPntr[4].texCoord.x = sourceRec.x + sourceRec.width;
					vertsPntr[4].texCoord.y = sourceRec.y;
					//bottom right
					vertsPntr[5].position.x = fontVert->position.x + sourceRec.width*scale;
					vertsPntr[5].position.y = fontVert->position.y + sourceRec.height*scale;
					vertsPntr[5].position.z = fontVert->position.z;
					vertsPntr[5].color = fontVert->color;
					vertsPntr[5].texCoord.x = sourceRec.x + sourceRec.width;
					vertsPntr[5].texCoord.y = sourceRec.y + sourceRec.height;
				}
				
				VertexBuffer_t* vertBuffer = &rc->batchFontVertBuffers[bIndex];
				DynamicVertexBufferExpand(vertBuffer, numVerts, FONT_BATCH_BASE_CHUNK_SIZE);
				DynamicVertexBufferFill(vertBuffer, verts, numVerts);
				
				TempPopMark();
				
				RcBindShader(shader);
				RcBindTexture(batchFontBake);
				RcDefaultSourceRectangle();
				RcSetColor(White);
				RcSetWorldMatrix(Mat4_Identity);
				RcBindBuffer(vertBuffer);
				RcDrawBufferTriangles(numVerts);
			}
			else 
			{
				TempPopMark();
				PrintLine_E("Failed to allocated vertices for font bake of %u glyphs", vertsArray->length);
				Assert(false);
			}
			
			RcDebugIncrementBy(numBatchedGlyphs, vertsArray->length);
		}
		
		// DestroyDynamicArray(vertsArray);
		vertsArray->length = 0;
		rc->batchingFontBakes[bIndex] = nullptr;
	}
	rc->numBatchingFonts = 0;
	RcBindShader(oldShader);
}

bool RcStartSpriteSheetBatch(const SpriteSheet_t* batchSpriteSheet, MemoryArena_t* vertsMemArena, u32 numFramesExpected = SHEET_BATCH_BASE_CHUNK_SIZE)
{
	Assert(batchSpriteSheet != nullptr);
	Assert(vertsMemArena != nullptr);
	if (rc->numBatchingSheets >= MAX_BATCHING_FONTS) { return false; }
	rc->batchingSheets[rc->numBatchingSheets] = batchSpriteSheet;
	if (rc->batchingSheetVerts[rc->numBatchingSheets].items == nullptr)
	{
		CreateDynamicArray(&rc->batchingSheetVerts[rc->numBatchingSheets], vertsMemArena, sizeof(SheetBatchVertex_t), SHEET_BATCH_CHUNK_SIZE, numFramesExpected);
	}
	rc->numBatchingSheets++;
	return true;
}
bool RcBatchSheetFrame(const SpriteSheet_t* spriteSheet, rec drawRec, Color_t color, v2i frame, Dir2_t rotation, bool flippedX, bool flippedY)
{
	for (u32 sIndex = 0; sIndex < rc->numBatchingSheets; sIndex++)
	{
		const SpriteSheet_t* batchSpriteSheet = rc->batchingSheets[sIndex];
		DynArray_t* vertsArray = &rc->batchingSheetVerts[sIndex];
		if (batchSpriteSheet == spriteSheet)
		{
			DynArrayExpand(vertsArray, vertsArray->length+1);
			SheetBatchVertex_t* newVertex = DynArrayAdd(vertsArray, SheetBatchVertex_t);
			Assert(newVertex != nullptr);
			ClearPointer(newVertex);
			newVertex->position = NewVec3(drawRec.x, drawRec.y, rc->depth);
			newVertex->size = drawRec.size;
			newVertex->color = NewVec4FromColor(color);
			MyMemCopy(&newVertex->replaceColors[0], &rc->replaceColors[0], sizeof(v4) * ArrayCount(newVertex->replaceColors));
			newVertex->frame = frame;
			newVertex->rotationAndFlags = (u8)rotation;
			if (flippedX) { FlagSet(newVertex->rotationAndFlags, 0x10); }
			if (flippedY) { FlagSet(newVertex->rotationAndFlags, 0x20); }
			return true;
		}
	}
	return false;
}
void RcFinishSheetBatches(const Shader_t* shader)
{
	const Shader_t* oldShader = rc->boundShader;
	for (u32 sIndex = 0; sIndex < rc->numBatchingSheets; sIndex++)
	{
		const SpriteSheet_t* batchSpriteSheet = rc->batchingSheets[sIndex];
		DynArray_t* vertsArray = &rc->batchingSheetVerts[sIndex];
		if (vertsArray->length > 0)
		{
			TempPushMark();
			u32 numVerts = vertsArray->length * 6;
			SheetFrameVertex_t* verts = PushArray(TempArena, SheetFrameVertex_t, numVerts);
			if (verts != nullptr)
			{
				for (u32 vIndex = 0; vIndex < vertsArray->length; vIndex++)
				{
					SheetBatchVertex_t* batchVert = DynArrayGet(vertsArray, SheetBatchVertex_t, vIndex);
					Assert(batchVert != nullptr);
					SheetFrameVertex_t* vertsPntr = &verts[6*vIndex];
					rec sourceRec = NewRec(NewVec2(Vec2iMultiply(batchSpriteSheet->frameSize, batchVert->frame) + batchSpriteSheet->padding), NewVec2(batchSpriteSheet->innerFrameSize));
					v3 topLeft = batchVert->position;
					v3 topRight = batchVert->position + NewVec3(batchVert->size.width, 0, 0);
					v3 bottomLeft = batchVert->position + NewVec3(0, batchVert->size.height, 0);
					v3 bottomRight = batchVert->position + NewVec3(batchVert->size.width, batchVert->size.height, 0);
					if (IsFlagSet(batchVert->rotationAndFlags, 0x10)) { sourceRec.x += sourceRec.width; sourceRec.width = -sourceRec.width; }
					if (IsFlagSet(batchVert->rotationAndFlags, 0x20)) { sourceRec.y += sourceRec.height; sourceRec.height = -sourceRec.height; }
					if ((batchVert->rotationAndFlags & 0x0F) == Dir2_Right) { v3 temp = topLeft; topLeft = bottomLeft; bottomLeft = bottomRight; bottomRight = topRight; topRight = temp; }
					else if ((batchVert->rotationAndFlags & 0x0F) == Dir2_Left) { v3 temp = topLeft; topLeft = topRight; topRight = bottomRight; bottomRight = bottomLeft; bottomLeft = temp; }
					else if ((batchVert->rotationAndFlags & 0x0F) == Dir2_Up) { v3 temp = topLeft; topLeft = bottomRight; bottomRight = temp; temp = topRight; topRight = bottomLeft; bottomLeft = temp; }
					
					//top left
					vertsPntr[0].position = topLeft;
					vertsPntr[0].color = batchVert->color;
					vertsPntr[0].texCoord.x = sourceRec.x;
					vertsPntr[0].texCoord.y = sourceRec.y;
					MyMemCopy(&vertsPntr[0].replaceColors[0], &batchVert->replaceColors[0], sizeof(v4) * ArrayCount(vertsPntr[0].replaceColors));
					//top right
					vertsPntr[1].position = topRight;
					vertsPntr[1].color = batchVert->color;
					vertsPntr[1].texCoord.x = sourceRec.x + sourceRec.width;
					vertsPntr[1].texCoord.y = sourceRec.y;
					MyMemCopy(&vertsPntr[1].replaceColors[0], &batchVert->replaceColors[0], sizeof(v4) * ArrayCount(vertsPntr[1].replaceColors));
					//bottom left
					vertsPntr[2].position = bottomLeft;
					vertsPntr[2].color = batchVert->color;
					vertsPntr[2].texCoord.x = sourceRec.x;
					vertsPntr[2].texCoord.y = sourceRec.y + sourceRec.height;
					MyMemCopy(&vertsPntr[2].replaceColors[0], &batchVert->replaceColors[0], sizeof(v4) * ArrayCount(vertsPntr[2].replaceColors));
					
					//bottom left
					vertsPntr[3].position = bottomLeft;
					vertsPntr[3].color = batchVert->color;
					vertsPntr[3].texCoord.x = sourceRec.x;
					vertsPntr[3].texCoord.y = sourceRec.y + sourceRec.height;
					MyMemCopy(&vertsPntr[3].replaceColors[0], &batchVert->replaceColors[0], sizeof(v4) * ArrayCount(vertsPntr[3].replaceColors));
					//top right
					vertsPntr[4].position = topRight;
					vertsPntr[4].color = batchVert->color;
					vertsPntr[4].texCoord.x = sourceRec.x + sourceRec.width;
					vertsPntr[4].texCoord.y = sourceRec.y;
					MyMemCopy(&vertsPntr[4].replaceColors[0], &batchVert->replaceColors[0], sizeof(v4) * ArrayCount(vertsPntr[4].replaceColors));
					//bottom right
					vertsPntr[5].position = bottomRight;
					vertsPntr[5].color = batchVert->color;
					vertsPntr[5].texCoord.x = sourceRec.x + sourceRec.width;
					vertsPntr[5].texCoord.y = sourceRec.y + sourceRec.height;
					MyMemCopy(&vertsPntr[5].replaceColors[0], &batchVert->replaceColors[0], sizeof(v4) * ArrayCount(vertsPntr[5].replaceColors));
				}
				
				VertexBuffer_t* vertBuffer = &rc->batchSheetVertBuffers[sIndex];
				DynamicVertexBufferExpand(vertBuffer, numVerts, SHEET_BATCH_BASE_CHUNK_SIZE, true);
				DynamicVertexBufferFillSheet(vertBuffer, verts, numVerts);
				
				TempPopMark();
				
				RcBindShader(shader);
				RcBindTexture(&batchSpriteSheet->texture);
				RcDefaultSourceRectangle();
				RcSetColor(White);
				RcSetWorldMatrix(Mat4_Identity);
				RcBindBuffer(vertBuffer);
				RcDrawBufferTriangles(numVerts);
			}
			else 
			{
				TempPopMark();
				PrintLine_E("Failed to allocated vertices for sheet bake of %u glyphs", vertsArray->length);
				Assert(false);
			}
			
			RcDebugIncrementBy(numSheetFramesBatched, vertsArray->length);
		}
		
		// DestroyDynamicArray(vertsArray);
		vertsArray->length = 0;
		rc->batchingSheets[sIndex] = nullptr;
	}
	rc->numBatchingSheets = 0;
	RcBindShader(oldShader);
}

// +--------------------------------------------------------------+
// |                        Begin Function                        |
// +--------------------------------------------------------------+
void RcBegin(const Shader_t* defaultShader, rec defaultViewport, const Font_t* defaultFont, FrameBuffer_t* defaultFramebuffer = nullptr, bool altBlendMode = false)
{
	RcBindFrameBuffer(defaultFramebuffer);
	RcSetViewport(defaultViewport);
	RcBindFont(defaultFont);
	RcBindBuffer(nullptr);
	RcBindTexture(nullptr);
	RcBindAltTexture(nullptr);
	RcSetDepth(1.0f);
	
	// glBlendEquation(GL_FUNC_ADD);
	if (altBlendMode)
	{
		glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
	}
	else
	{
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}
	glEnable(GL_BLEND);
	
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GEQUAL, 0.1f);
	
	rc->worldMatrix        = Mat4_Identity;
	rc->viewMatrix         = Mat4_Identity;
	// rc->projectionMatrix   = Mat4_Identity; //set by RcSetViewport
	rc->sourceRectangle    = NewRec(0, 0, 1, 1);
	rc->altSourceRectangle = NewRec(0, 0, 1, 1);
	rc->maskRectangle      = NewRec(0, 0, 0, 0);
	rc->shiftVec           = Vec2_Zero;
	rc->primaryColor       = White;
	rc->secondaryColor     = White;
	rc->replaceColors[0]    = NewVec4FromColor(ReplacePink1);
	rc->replaceColors[1]    = NewVec4FromColor(ReplacePink2);
	rc->replaceColors[2]    = NewVec4FromColor(ReplacePink3);
	rc->replaceColors[3]    = NewVec4FromColor(ReplacePink4);
	rc->replaceColors[4]    = NewVec4FromColor(ReplacePink5);
	rc->replaceColors[5]    = NewVec4FromColor(ReplacePink6);
	rc->replaceColors[6]    = NewVec4FromColor(ReplacePink7);
	rc->replaceColors[7]    = NewVec4FromColor(ReplacePink8);
	rc->replaceColors[8]    = NewVec4FromColor(ReplacePink9);
	rc->replaceColors[9]    = NewVec4FromColor(ReplacePink10);
	rc->replaceColors[10]   = NewVec4FromColor(ReplacePink11);
	rc->replaceColors[11]   = NewVec4FromColor(ReplacePink12);
	rc->replaceColors[12]   = NewVec4FromColor(ReplacePink13);
	rc->replaceColors[13]   = NewVec4FromColor(ReplacePink14);
	rc->replaceColors[14]   = NewVec4FromColor(ReplacePink15);
	rc->replaceColors[15]   = NewVec4FromColor(ReplacePink16);
	rc->gradientEnabled    = false;
	rc->circleRadius       = 0.0f;
	rc->circleInnerRadius  = 0.0f;
	rc->saturation         = 1.0f;
	rc->brightness         = 1.0f;
	rc->time               = 0.0f;
	rc->value0             = 0.0f;
	rc->value1             = 0.0f;
	rc->value2             = 0.0f;
	rc->value3             = 0.0f;
	rc->value4             = 0.0f;
	rc->mousePos           = RenderMousePos;
	rc->screenSize         = RenderScreenSize;
	RcBindShader(defaultShader, true);
}


// +--------------------------------------------------------------+
// |                        Other Actions                         |
// +--------------------------------------------------------------+
void RcStartStencilDrawing()
{
	glEnable(GL_STENCIL_TEST);
	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE); //don't draw to the color buffer
	glDepthMask(GL_FALSE); //don't draw to the depth buffer
	glStencilMask(0xFF); //
	glStencilFunc(GL_NEVER, 1, 0xFF);
	glStencilOp(GL_REPLACE, GL_KEEP, GL_KEEP);
}
void RcUseStencil(bool inverseMask = false)
{
	glEnable(GL_STENCIL_TEST);
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	glDepthMask(GL_TRUE);
	glStencilMask(0x00);
	if (inverseMask)
	{
		glStencilFunc(GL_GREATER, 1, 0xFF);
	}
	else
	{
		glStencilFunc(GL_EQUAL, 1, 0xFF);
	}
}
void RcDisableStencil()
{
	glDisable(GL_STENCIL_TEST);
}

void RcClearColorBuffer(Color_t clearColor)
{
	v4 colorVec = NewVec4FromColor(clearColor);
	glClearColor(colorVec.x, colorVec.y, colorVec.z, colorVec.w);
	glClear(GL_COLOR_BUFFER_BIT);
}
void RcClearDepthBuffer(r32 clearDepth)
{
	glClearDepth(clearDepth);
	glClear(GL_DEPTH_BUFFER_BIT);
}
void RcClear(Color_t clearColor, r32 clearDepth)
{
	v4 colorVec = NewVec4FromColor(clearColor);
	glClearColor(colorVec.x, colorVec.y, colorVec.z, colorVec.w);
	glClearDepth(clearDepth);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
}
void RcClearStencilBuffer(u32 stencilValue)
{
	glClearStencil(stencilValue);
	glClear(GL_STENCIL_BUFFER_BIT);
}

r32 RcLineHeight(r32 scale = 1.0f)
{
	if (rc->boundFont == nullptr) { return 0; }
	return rc->boundFont->lineHeight * scale * rc->fontScale;
}
r32 RcMaxExtendUp(r32 scale = 1.0f)
{
	if (rc->boundFont == nullptr) { return 0; }
	return rc->boundFont->maxExtendUp * scale * rc->fontScale;
}
r32 RcMaxExtendDown(r32 scale = 1.0f)
{
	if (rc->boundFont == nullptr) { return 0; }
	return rc->boundFont->maxExtendDown * scale * rc->fontScale;
}
r32 RcCenterOffset(r32 scale = 1.0f)
{
	if (rc->boundFont == nullptr) { return 0; }
	return (-rc->boundFont->lineHeight/2 + rc->boundFont->maxExtendUp) * scale * rc->fontScale;
}
v2 RcMeasureString(const char* strPntr, u32 strLength, r32 scale = 1.0f)
{
	if (rc->boundFont == nullptr) { return Vec2_Zero; }
	return MeasureString(rc->boundFont, strPntr, strLength) * scale * rc->fontScale;
}
v2 RcMeasureStringNt(const char* nullTermString, r32 scale = 1.0f)
{
	if (rc->boundFont == nullptr) { return Vec2_Zero; }
	return MeasureString(rc->boundFont, nullTermString) * scale * rc->fontScale;
}
v2 RcMeasureFormattedString(const char* strPntr, u32 strLength, r32 maxWidth, bool preserveWords = true, r32 scale = 1.0f)
{
	if (rc->boundFont == nullptr) { return Vec2_Zero; }
	return MeasureFormattedString(rc->boundFont, strPntr, strLength, maxWidth / (scale * rc->fontScale), preserveWords) * scale * rc->fontScale;
}
v2 RcMeasureFormattedStringNt(const char* nullTermString, r32 maxWidth, bool preserveWords = true, r32 scale = 1.0f)
{
	if (rc->boundFont == nullptr) { return Vec2_Zero; }
	return MeasureFormattedString(rc->boundFont, nullTermString, MyStrLength32(nullTermString), maxWidth / (scale * rc->fontScale), preserveWords) * scale * rc->fontScale;
}
