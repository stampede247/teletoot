/*
File:   app_text_box.cpp
Author: Taylor Robbins
Date:   11\04\2017
Description: 
	** Handles interaction and rendering with Text Boxes 
*/
	
u32 InsertStringInPlace(char* str, u32 strLength, u32 maxLength, u32 insertIndex, const char* insertStr, u32 insertStrLength)
{
	Assert(str != nullptr);
	Assert(insertStr != nullptr);
	Assert(insertIndex <= strLength);
	
	u32 fromIndex = strLength;
	u32 toIndex = strLength + insertStrLength;
	while (true)
	{
		Assert(toIndex < maxLength);
		
		str[toIndex] = str[fromIndex]; //NOTE: This will also write the \0
		
		if (fromIndex == insertIndex)
		{
			for (u32 cIndex = insertStrLength; cIndex > 0; cIndex--)
			{
				Assert(toIndex > 0);
				str[toIndex-1] = insertStr[cIndex-1];
				toIndex--;
			}
		}
		
		if (toIndex == 0) { break; }
		toIndex--;
		fromIndex--;
	}
	
	Assert(toIndex == 0);
	Assert(fromIndex == 0);
	return strLength + insertStrLength;
}

u32 RemoveStringRegionInPlace(char* str, u32 strLength, u32 regionStartIndex, u32 regionEndIndex)
{
	Assert(str != nullptr);
	Assert(regionStartIndex <= strLength);
	Assert(regionEndIndex <= strLength);
	if (regionStartIndex == regionEndIndex) { return strLength; } //nothing to remove
	
	u32 regionMin = (u32)MinI32(regionStartIndex, regionEndIndex);
	u32 regionMax = (u32)MaxI32(regionStartIndex, regionEndIndex);
	
	u32 fromIndex = 0;
	u32 toIndex = 0;
	while (fromIndex <= strLength)
	{
		if (fromIndex >= regionMin && fromIndex < regionMax)
		{
			//Don't insert the character
		}
		else
		{
			str[toIndex] = str[fromIndex]; //NOTE: This will also write the \0
			toIndex++;
		}
		
		fromIndex++;
	}
	
	return toIndex-1;
}

TextBox_t NewTextBoxStatic(rec drawRec, const char* chars, u32 numChars, u32 memorySize, char* memoryPntr, const Font_t* font)
{
	Assert(memoryPntr != nullptr);
	Assert(memorySize > 0);
	
	TextBox_t result = {};
	result.drawRec = drawRec;
	result.font = font;
	result.fontScale = 1.0f;
	
	result.leftPadding = 5;
	
	result.colors.background1 = DynamicColor_iha(Grey8, White, White);
	result.colors.background2 = DynamicColor_iha(Grey8, White, White);
	result.colors.text        = DynamicColor(Black);
	result.colors.cursor1     = DynamicColor(Black);
	result.colors.cursor2     = DynamicColor(ColorTransparent(Black, 0.5f));
	result.colors.border      = DynamicColor_ia(Black, Black);
	result.colors.selection1  = DynamicColor_ia(ColorTransparent(NewColor(0xFFB4B4B4), 0.5f), ColorTransparent(PureBlue, 0.5f));
	result.colors.selection2  = DynamicColor_ia(ColorTransparent(NewColor(0xFFB4B4B4), 0.5f), ColorTransparent(PureBlue, 0.3f));
	
	result.chars = memoryPntr;
	result.maxNumChars = memorySize;
	
	if (chars != nullptr && numChars > 0)
	{
		Assert(numChars < result.maxNumChars);
		
		MyMemCopy(result.chars, chars, numChars);
		result.chars[numChars] = '\0';
		result.numChars = numChars;
		
		result.cursorBegin = numChars;
		result.cursorEnd = numChars;
		
		//TODO: Set the drawOffset so we can see the cursor position
	}
	else
	{
		result.numChars = 0;
		result.cursorBegin = 0;
		result.cursorEnd = 0;
	}
	
	return result;
}

TextBox_t NewTextBox(rec drawRec, const char* chars, u32 numChars, u32 maxNumChars, MemoryArena_t* arenaPntr, const Font_t* font)
{
	return NewTextBoxStatic(drawRec, chars, numChars, maxNumChars, PushArray(arenaPntr, char, maxNumChars), font);
}

void DeleteTextBox(MemoryArena_t* allocArena, TextBox_t* tb)
{
	Assert(tb != nullptr);
	Assert(allocArena != nullptr);
	
	if (tb->chars != nullptr)
	{
		ArenaPop(allocArena, tb->chars);
		tb->chars = nullptr;
	}
}	

void TextBoxRellocate(TextBox_t* tb, rec drawRec)
{
	Assert(tb != nullptr);
	
	tb->drawRec = drawRec;
	
	//TODO: Set the drawOffset so we can still see the cursor
}

void TextBoxClear(TextBox_t* tb)
{
	Assert(tb != nullptr);
	
	tb->numChars = 0;
	tb->chars[0] = '\0';
	tb->cursorEnd = 0;
	tb->cursorBegin = tb->cursorEnd;
}

void TextBoxSet(TextBox_t* tb, const char* newChars, u32 numChars)
{
	Assert(tb != nullptr);
	Assert(newChars != nullptr);
	Assert(numChars < tb->maxNumChars);
	
	MyMemCopy(tb->chars, newChars, numChars);
	tb->chars[numChars] = '\0';
	tb->numChars = numChars;
	tb->cursorEnd = numChars;
	tb->cursorBegin = tb->cursorEnd;
}

void TextBoxUpdate(TextBox_t* tb, bool selected, bool mouseOverUi)
{
	Assert(tb != nullptr);
	
	u32 cursorMin = (u32)MinI32(tb->cursorBegin, tb->cursorEnd);
	u32 cursorMax = (u32)MaxI32(tb->cursorBegin, tb->cursorEnd);
	
	if (selected)
	{
		// +==============================+
		// |          Left Arrow          |
		// +==============================+
		if (ButtonRepeated(Button_Left))
		{
			HandleButton(Button_Left);
			if (ButtonDownNoHandling(Button_Shift))
			{
				if (tb->cursorEnd > 0)
				{
					if (ButtonDownNoHandling(Button_Control))
					{
						tb->cursorEnd = FindWordBound(tb->chars, tb->numChars, tb->cursorEnd, false);
					}
					else
					{
						tb->cursorEnd--;
					}
				}
			}
			else
			{
				if (tb->cursorBegin != tb->cursorEnd)
				{
					tb->cursorBegin = cursorMin;
					tb->cursorEnd = cursorMin;
				}
				else if (tb->cursorEnd > 0)
				{
					if (ButtonDownNoHandling(Button_Control))
					{
						tb->cursorEnd = FindWordBound(tb->chars, tb->numChars, tb->cursorEnd, false);
					}
					else
					{
						tb->cursorEnd--;
					}
					tb->cursorBegin = tb->cursorEnd;
				}
			}
		}
		
		// +==============================+
		// |         Right Arrow          |
		// +==============================+
		if (ButtonRepeated(Button_Right))
		{
			HandleButton(Button_Right);
			if (ButtonDownNoHandling(Button_Shift))
			{
				if (tb->cursorEnd < tb->numChars)
				{
					if (ButtonDownNoHandling(Button_Control))
					{
						tb->cursorEnd = FindWordBound(tb->chars, tb->numChars, tb->cursorEnd, true);
					}
					else
					{
						tb->cursorEnd++;
					}
				}
			}
			else
			{
				if (tb->cursorBegin != tb->cursorEnd)
				{
					tb->cursorBegin = cursorMax;
					tb->cursorEnd = cursorMax;
				}
				else if (tb->cursorEnd < tb->numChars)
				{
					if (ButtonDownNoHandling(Button_Control))
					{
						tb->cursorEnd = FindWordBound(tb->chars, tb->numChars, tb->cursorEnd, true);
					}
					else
					{
						tb->cursorEnd++;
					}
					tb->cursorBegin = tb->cursorEnd;
				}
			}
		}
		
		// +==============================+
		// |       Character Input        |
		// +==============================+
		if (appInput->textInputLength > 0)
		{
			for (u32 cIndex = 0; cIndex < appInput->textInputLength; cIndex++)
			{
				char newChar = appInput->textInput[cIndex];
				Buttons_t charButton = GetButtonForChar(newChar);
				if (charButton == Buttons_NumButtons || !app->buttonHandled[charButton])
				{
					if (charButton != Buttons_NumButtons) { HandleButton(charButton); }
					
					if (tb->cursorBegin != tb->cursorEnd)
					{
						//Remove the selected text before writing the characters
						
						tb->numChars = RemoveStringRegionInPlace(tb->chars, tb->numChars, tb->cursorBegin, tb->cursorEnd);
						tb->cursorEnd = (u32)MinI32(tb->cursorBegin, tb->cursorEnd);
						tb->cursorBegin = tb->cursorEnd;
						
					}
					
					if (tb->numChars+1 < tb->maxNumChars)
					{
						tb->numChars = InsertStringInPlace(tb->chars, tb->numChars, tb->maxNumChars, tb->cursorEnd, &newChar, 1);
						tb->cursorEnd++;
						tb->cursorBegin++;
					}
					else
					{
						Notify_W("Input text box is full");
					}
				}
			}
		}
		
		// +==============================+
		// |          Backspace           |
		// +==============================+
		if (ButtonRepeated(Button_Backspace) || ButtonRepeated(Button_Delete))
		{
			if (tb->cursorBegin == tb->cursorEnd)
			{
				if (ButtonRepeated(Button_Backspace) && tb->cursorEnd > 0)
				{
					HandleButton(Button_Backspace);
					u32 wordEndIndex = tb->cursorEnd-1;
					if (ButtonDownNoHandling(Button_Control))
					{
						wordEndIndex = FindWordBound(tb->chars, tb->numChars, tb->cursorEnd, false);
					}
					
					tb->numChars = RemoveStringRegionInPlace(tb->chars, tb->numChars, tb->cursorEnd, wordEndIndex);
					tb->cursorEnd = wordEndIndex;
					tb->cursorBegin = tb->cursorEnd;
				}
				else if (ButtonRepeated(Button_Delete) && tb->cursorEnd < tb->numChars)
				{
					HandleButton(Button_Delete);
					u32 wordEndIndex = tb->cursorEnd+1;
					if (ButtonDownNoHandling(Button_Control))
					{
						wordEndIndex = FindWordBound(tb->chars, tb->numChars, tb->cursorEnd, true);
					}
					
					tb->numChars = RemoveStringRegionInPlace(tb->chars, tb->numChars, tb->cursorEnd, wordEndIndex);
				}
			}
			else
			{
				HandleButton(Button_Backspace);
				HandleButton(Button_Delete);
				tb->numChars = RemoveStringRegionInPlace(tb->chars, tb->numChars, tb->cursorBegin, tb->cursorEnd);
				tb->cursorEnd = (u32)MinI32(tb->cursorBegin, tb->cursorEnd);
				tb->cursorBegin = tb->cursorEnd;
			}
		}
		
		// +==============================+
		// |         Home and End         |
		// +==============================+
		if (ButtonPressed(Button_Home))
		{
			HandleButton(Button_Home);
			if (ButtonDownNoHandling(Button_Shift))
			{
				tb->cursorEnd = 0;
			}
			else
			{
				tb->cursorEnd = 0;
				tb->cursorBegin = tb->cursorEnd;
			}
		}
		if (ButtonPressed(Button_End))
		{
			HandleButton(Button_End);
			if (ButtonDownNoHandling(Button_Shift))
			{
				tb->cursorEnd = tb->numChars;
			}
			else
			{
				tb->cursorEnd = tb->numChars;
				tb->cursorBegin = tb->cursorEnd;
			}
		}
		
		// +==============================+
		// |       Mouse Selection        |
		// +==============================+
		if (!mouseOverUi && appInput->mouseInsideWindow && IsInsideRec(tb->drawRec, RenderMouseStartPos))
		{
			v2 relativePos = NewVec2(RenderMousePos.x - (tb->drawRec.x + tb->leftPadding), 0);
			i32 mouseIndex = GetStringIndexForLocation(tb->font, tb->chars, tb->numChars, relativePos / tb->fontScale);
			if (mouseIndex < 0) { mouseIndex = 0; }
			if ((u32)mouseIndex > tb->numChars) { mouseIndex = (i32)tb->numChars; }
			
			if (ButtonPressed(MouseButton_Left))
			{
				HandleButton(MouseButton_Left);
				tb->cursorBegin = (u32)mouseIndex;
			}
			if (ButtonDown(MouseButton_Left))
			{
				HandleButton(MouseButton_Left);
				tb->cursorEnd = (u32)mouseIndex;
			}
			if (ButtonReleased(MouseButton_Left))
			{
				HandleButton(MouseButton_Left);
			}
		}
		
		// +==============================+
		// |     Paste From Clipboard     |
		// +==============================+
		if (ButtonDownNoHandling(Button_Control) && ButtonPressed(Button_V))
		{
			HandleButton(Button_V);
			TempPushMark();
			
			u32 clipboardDataSize = 0;
			char* clipbardData = (char*)platform->CopyFromClipboard(TempArena, &clipboardDataSize);
			
			if (clipbardData != nullptr)
			{
				char* sanatized = SanatizeStringAdvanced(clipbardData, clipboardDataSize, TempArena, true, false, true);
				u32 sanatizedLength = MyStrLength32(sanatized);
				
				for (u32 cIndex = 0; cIndex < sanatizedLength; cIndex++)
				{
					char newChar = sanatized[cIndex];
					if (tb->cursorBegin != tb->cursorEnd)
					{
						//Remove the selected text before writing the characters
						
						tb->numChars = RemoveStringRegionInPlace(tb->chars, tb->numChars, tb->cursorBegin, tb->cursorEnd);
						tb->cursorEnd = (u32)MinI32(tb->cursorBegin, tb->cursorEnd);
						tb->cursorBegin = tb->cursorEnd;
					}
					
					if (tb->numChars+1 < tb->maxNumChars)
					{
						tb->numChars = InsertStringInPlace(tb->chars, tb->numChars, tb->maxNumChars, tb->cursorEnd, &newChar, 1);
						tb->cursorEnd++;
						tb->cursorBegin++;
					}
					else
					{
						Notify_W("Input text box is full");
					}
				}
			}
			
			TempPopMark();
		}
		
		// +==============================+
		// |      Copy to Clipboard       |
		// +==============================+
		if (ButtonDownNoHandling(Button_Control) && ButtonPressed(Button_C))
		{
			HandleButton(Button_C);
			if (tb->cursorBegin != tb->cursorEnd)
			{
				u32 selectionLength = cursorMax-cursorMin;
				char* tempString = TempString(selectionLength+1);
				MyMemCopy(tempString, &tb->chars[cursorMin], selectionLength);
				tempString[selectionLength] = '\0';
				platform->CopyToClipboard(tempString, selectionLength+1);
				PrintLine_D("Copied %u bytes to clipboard", selectionLength);
			}
		}
		
		
		HandleButton(Button_Left);
		HandleButton(Button_Right);
		HandleButton(Button_Up);
		HandleButton(Button_Down);
		HandleButton(Button_Backspace);
		HandleButton(Button_Delete);
		HandleButton(Button_Home);
		HandleButton(Button_End);
		HandleButton(Button_Enter);
		for (char c = ' '; c <= '~'; c++)
		{
			Buttons_t charBtn = GetButtonForChar(c);
			if (charBtn < Buttons_NumButtons) { HandleButton(charBtn); }
		}
	}
}

void TextBoxRender(TextBox_t* tb, bool selected)
{
	Assert(tb != nullptr);
	
	const Font_t* oldFont = rc->boundFont;
	r32 oldFontScale = rc->fontScale;
	RcBindFont(tb->font, tb->fontScale);
	
	r32 colorLerp = (SinR32((ProgramTime/1000.0f)*6.0f) + 1.0f) / 2.0f;
	ColorState_t colorState = selected ? ColorState_Active : ColorState_Idle;
	if (IsInsideRec(tb->drawRec, RenderMousePos) && appInput->mouseInsideWindow)
	{
		colorState = selected ? ColorState_ActiveHover : ColorState_Hover;
		if (ButtonDownNoHandling(MouseButton_Left) && IsInsideRec(tb->drawRec, RenderMouseStartPos))
		{
			colorState = ColorState_Pressed;
		}
	}
	
	u32 cursorMin = (u32)MinI32(tb->cursorBegin, tb->cursorEnd);
	u32 cursorMax = (u32)MaxI32(tb->cursorBegin, tb->cursorEnd);
	
	v2 textSize = RcMeasureString(tb->chars, tb->numChars);
	v2 textPos = tb->drawRec.topLeft + NewVec2(tb->leftPadding, tb->drawRec.height/2.0f - textSize.y/2.0f + RcMaxExtendUp());
	textPos = Vec2Round(textPos);
	
	r32 cursorOffset   = RoundR32(RcMeasureString(tb->chars, tb->cursorEnd).x);
	r32 selectionStart = RcMeasureString(tb->chars, cursorMin).x;
	r32 selectionWidth = RcMeasureString(&tb->chars[cursorMin], cursorMax-cursorMin).x;
	
	// +==============================+
	// |     Draw the Background      |
	// +==============================+
	RcDrawGradient(tb->drawRec, tb->colors.background1[colorState], tb->colors.background2[colorState], Dir2_Down);
	RcDrawButton(tb->drawRec, NoColor, tb->colors.border[colorState], 1);
	
	// +==============================+
	// |      Draw the Selection      |
	// +==============================+
	{
		Rectangle_t selectionRec = NewRec(
			textPos.x + selectionStart,
			textPos.y - RcMaxExtendUp(),
			selectionWidth, RcLineHeight()
		);
		Color_t selectionColor = ColorLerp(tb->colors.selection1[colorState], tb->colors.selection2[colorState], colorLerp);
		RcDrawRectangle(selectionRec, selectionColor);
	}
	
	// +==============================+
	// |        Draw the Text         |
	// +==============================+
	RcDrawString(tb->chars, tb->numChars, textPos, tb->colors.text[colorState]);
	
	// +==============================+
	// |       Draw the Cursor        |
	// +==============================+
	if (selected)
	{
		Rectangle_t cursorRec = NewRec(
			textPos.x + cursorOffset,
			textPos.y - RcMaxExtendUp(),
			1, RcLineHeight()
		);
		
		Color_t cursorColor = ColorLerp(tb->colors.cursor1[colorState], tb->colors.cursor2[colorState], colorLerp);
		RcDrawRectangle(cursorRec, cursorColor);
	}
	
	RcBindFont(oldFont, oldFontScale);
}


