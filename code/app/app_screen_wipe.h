/*
File:   app_screen_wipe.h
Author: Taylor Robbins
Date:   08\28\2018
*/

#ifndef _APP_SCREEN_WIPE_H
#define _APP_SCREEN_WIPE_H

typedef enum
{
	ScreenWipeStencil_Fade,
	ScreenWipeStencil_LeftToRight,
	ScreenWipeStencil_RightToLeft,
	ScreenWipeStencil_TopDown,
	ScreenWipeStencil_BottomUp,
	ScreenWipeStencil_Swirl,
	ScreenWipeStencil_VerticalSlats,
	ScreenWipeStencil_HorizontalSlats,
	ScreenWipeStencil_Fizzle,
} ScreenWipeStencil_t;

const char* GetScreenWipeStencilStr(ScreenWipeStencil_t stencilType)
{
	switch (stencilType)
	{
		case ScreenWipeStencil_Fade:            return "Fade";
		case ScreenWipeStencil_LeftToRight:     return "LeftToRight";
		case ScreenWipeStencil_RightToLeft:     return "RightToLeft";
		case ScreenWipeStencil_TopDown:         return "TopDown";
		case ScreenWipeStencil_BottomUp:        return "BottomUp";
		case ScreenWipeStencil_Swirl:           return "Swirl";
		case ScreenWipeStencil_VerticalSlats:   return "VerticalSlats";
		case ScreenWipeStencil_HorizontalSlats: return "HorizontalSlats";
		case ScreenWipeStencil_Fizzle:          return "Fizzle";
		default: return "Unknown";
	}
}

typedef enum
{
	ScreenWipe_Simple,
	ScreenWipe_BlackWithSpinner,
	ScreenWipe_Psychedelic,
} ScreenWipeType_t;

const char* GetScreenWipeTypeStr(ScreenWipeType_t screenWipeType)
{
	switch (screenWipeType)
	{
		case ScreenWipe_Simple:           return "Simple";
		case ScreenWipe_BlackWithSpinner: return "BlackWithSpinner";
		case ScreenWipe_Psychedelic:      return "Psychedelic";
		default: return "Unknown";
	}
}

typedef enum
{
	ScreenWipeSpinnerChoice_Water = 0,
	ScreenWipeSpinnerChoice_Lava,
	ScreenWipeSpinnerChoice_Spikes,
	ScreenWipeSpinnerChoice_Teleporter,
	ScreenWipeSpinnerChoice_DiamondSparkling,
	ScreenWipeSpinnerChoice_RedDiamondSparkling,
	// ScreenWipeSpinnerChoice_GroundCrumbling, //TODO: Implement these animation choices
	// ScreenWipeSpinnerChoice_SlimeWebBreaking,
	// ScreenWipeSpinnerChoice_WoodBoxFloating,
	// ScreenWipeSpinnerChoice_WoodBarrelFloating,
	// ScreenWipeSpinnerChoice_MetalBoxFloating,
	// ScreenWipeSpinnerChoice_MetalBarrelFloating,
	// ScreenWipeSpinnerChoice_IceFloating,
	// ScreenWipeSpinnerChoice_IceDissolving,
	// ScreenWipeSpinnerChoice_SlimeFloating,
	// ScreenWipeSpinnerChoice_SlimeDissolving,
	
	
	ScreenWipeSpinnerChoice_PlayerWalking,
	ScreenWipeSpinnerChoice_PlayerPushing,
	ScreenWipeSpinnerChoice_PlayerPulling,
	ScreenWipeSpinnerChoice_PlayerDrowning,
	ScreenWipeSpinnerChoice_PlayerBurning,
	ScreenWipeSpinnerChoice_PlayerFalling,
	ScreenWipeSpinnerChoice_PlayerExploding,
	ScreenWipeSpinnerChoice_PlayerTeleporting,
	ScreenWipeSpinnerChoice_PlayerDancing,
	
	ScreenWipeSpinnerChoice_MouseWalking,
	// ScreenWipeSpinnerChoice_MouseDrowning, //TODO: Create these animations!
	ScreenWipeSpinnerChoice_MouseBurning,
	// ScreenWipeSpinnerChoice_MouseFalling, //TODO: Create these animations!
	ScreenWipeSpinnerChoice_MouseExploding,
	
	ScreenWipeSpinnerChoice_SkeletonWalking,
	ScreenWipeSpinnerChoice_SkeletonDrowning,
	ScreenWipeSpinnerChoice_SkeletonBurning,
	ScreenWipeSpinnerChoice_SkeletonFalling,
	ScreenWipeSpinnerChoice_SkeletonExploding,
	
	ScreenWipeSpinnerChoice_BatWalking,
	ScreenWipeSpinnerChoice_BatExploding,
	
	ScreenWipeSpinnerChoice_NumOptions,
} ScreenWipeSpinnerChoice_t;

const char* GetScreenWipeSpinnerChoiceStr(ScreenWipeSpinnerChoice_t screenWipeSpinnerChoice)
{
	switch (screenWipeSpinnerChoice)
	{
		case ScreenWipeSpinnerChoice_Teleporter:          return "Teleporter";
		case ScreenWipeSpinnerChoice_Water:               return "Water";
		case ScreenWipeSpinnerChoice_Lava:                return "Lava";
		case ScreenWipeSpinnerChoice_Spikes:              return "Spikes";
		case ScreenWipeSpinnerChoice_DiamondSparkling:    return "DiamondSparkling";
		case ScreenWipeSpinnerChoice_RedDiamondSparkling: return "RedDiamondSparkling";
		// case ScreenWipeSpinnerChoice_GroundCrumbling:     return "GroundCrumbling"; //TODO: Uncomment us when the choices are implemented
		// case ScreenWipeSpinnerChoice_SlimeWebBreaking:    return "SlimeWebBreaking"; //TODO: Uncomment us when the choices are implemented
		// case ScreenWipeSpinnerChoice_WoodBoxFloating:     return "WoodBoxFloating"; //TODO: Uncomment us when the choices are implemented
		// case ScreenWipeSpinnerChoice_WoodBarrelFloating:  return "WoodBarrelFloating"; //TODO: Uncomment us when the choices are implemented
		// case ScreenWipeSpinnerChoice_MetalBoxFloating:    return "MetalBoxFloating"; //TODO: Uncomment us when the choices are implemented
		// case ScreenWipeSpinnerChoice_MetalBarrelFloating: return "MetalBarrelFloating"; //TODO: Uncomment us when the choices are implemented
		// case ScreenWipeSpinnerChoice_IceFloating:         return "IceFloating"; //TODO: Uncomment us when the choices are implemented
		// case ScreenWipeSpinnerChoice_IceDissolving:       return "IceDissolving"; //TODO: Uncomment us when the choices are implemented
		// case ScreenWipeSpinnerChoice_SlimeFloating:       return "SlimeFloating"; //TODO: Uncomment us when the choices are implemented
		// case ScreenWipeSpinnerChoice_SlimeDissolving:     return "SlimeDissolving"; //TODO: Uncomment us when the choices are implemented
		
		case ScreenWipeSpinnerChoice_PlayerWalking:       return "PlayerWalking";
		case ScreenWipeSpinnerChoice_PlayerPushing:       return "PlayerPushing";
		case ScreenWipeSpinnerChoice_PlayerPulling:       return "PlayerPulling";
		case ScreenWipeSpinnerChoice_PlayerDrowning:      return "PlayerDrowning";
		case ScreenWipeSpinnerChoice_PlayerBurning:       return "PlayerBurning";
		case ScreenWipeSpinnerChoice_PlayerFalling:       return "PlayerFalling";
		case ScreenWipeSpinnerChoice_PlayerExploding:     return "PlayerExploding";
		case ScreenWipeSpinnerChoice_PlayerTeleporting:   return "PlayerTeleporting";
		case ScreenWipeSpinnerChoice_PlayerDancing:       return "PlayerDancing";
		
		case ScreenWipeSpinnerChoice_MouseWalking:        return "MouseWalking";
		// case ScreenWipeSpinnerChoice_MouseDrowning:       return "MouseDrowning"; //TODO: Uncomment us when the animations are added
		case ScreenWipeSpinnerChoice_MouseBurning:        return "MouseBurning";
		// case ScreenWipeSpinnerChoice_MouseFalling:        return "MouseFalling"; //TODO: Uncomment us when the animations are added
		case ScreenWipeSpinnerChoice_MouseExploding:      return "MouseExploding";
		
		case ScreenWipeSpinnerChoice_SkeletonWalking:     return "SkeletonWalking";
		case ScreenWipeSpinnerChoice_SkeletonDrowning:    return "SkeletonDrowning";
		case ScreenWipeSpinnerChoice_SkeletonBurning:     return "SkeletonBurning";
		case ScreenWipeSpinnerChoice_SkeletonFalling:     return "SkeletonFalling";
		case ScreenWipeSpinnerChoice_SkeletonExploding:   return "SkeletonExploding";
		
		case ScreenWipeSpinnerChoice_BatWalking:          return "BatWalking";
		case ScreenWipeSpinnerChoice_BatExploding:        return "BatExploding";
		
		default: return "Unknown";
	}
}

#endif //  _APP_SCREEN_WIPE_H
