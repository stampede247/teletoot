/*
File:   app_performance.h
Author: Taylor Robbins
Date:   03\26\2020
*/

#ifndef _APP_PERFORMANCE_H
#define _APP_PERFORMANCE_H

#if PERFORMANCE_CHECKS
#define PERF_MARK(name) Assert(perfIndex < ArrayCount(perfTimes)); perfTimes[perfIndex] = platform->GetPerfTime(); perfNames[perfIndex] = (name); perfIndex++
#define PERF_START(numTimes) u32 perfIndex = 0; PerfTime_t perfTimes[(numTimes)+1]; const char* perfNames[(numTimes)+1]; ClearArray(perfTimes); ClearArray(perfNames); PERF_MARK("Start")
#else
#define PERF_MARK(name)
#define PERF_START(numTimes)
#endif

#endif //  _APP_PERFORMANCE_H
