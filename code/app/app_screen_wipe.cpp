/*
File:   app_screen_wipe.cpp
Author: Taylor Robbins
Date:   08\28\2018
Description: 
	** Holds the functions that render the screen wipe animations used when transitioning between some states 
*/

bool StartScreenWipe(ScreenWipeType_t type = ScreenWipe_BlackWithSpinner, ScreenWipeStencil_t stencilType = ScreenWipeStencil_Fade, bool forceReset = false)
{
	if (app->screenWipeActive == false || forceReset)
	{
		PrintLine_D("Screen wipe %s-%s started", GetScreenWipeTypeStr(type), GetScreenWipeStencilStr(stencilType));
		
		app->screenWipeActive = true;
		app->screenWipeProgress = 0.0f;
		app->screenWipeType = type;
		app->screenWipeStencilType = stencilType;
		if (app->screenWipeType == ScreenWipe_BlackWithSpinner)
		{
			app->screenWipeSpinnerChoice = (ScreenWipeSpinnerChoice_t)RandU32(0, ScreenWipeSpinnerChoice_NumOptions);
			app->screenWipeSpinnerRotation = Dir2_Down;//TODO: Make a random direction function
		}
		return true;
	}
	else
	{
		return false;
	}
}

void UpdateScreenWipe()
{
	r32 wipeSpeed = WIPE_SPEED;
	if (app->screenWipeStencilType == ScreenWipeStencil_Swirl) { wipeSpeed = SWIRL_WIPE_SPEED; }
	if (app->screenWipeStencilType == ScreenWipeStencil_VerticalSlats) { wipeSpeed = SLATS_WIPE_SPEED; }
	if (app->screenWipeStencilType == ScreenWipeStencil_HorizontalSlats) { wipeSpeed = SLATS_WIPE_SPEED; }
	
	if (app->screenWipeActive && app->screenWipeProgress < 1.0f)
	{
		app->screenWipeProgress += (ElapsedMs / wipeSpeed);
		if (app->screenWipeProgress >= 1.0f) { app->screenWipeProgress = 1.0f; }
	}
	if (!app->screenWipeActive && app->screenWipeProgress > 0.0f)
	{
		app->screenWipeProgress -= (ElapsedMs / wipeSpeed);
		if (app->screenWipeProgress <= 0.0f) { app->screenWipeProgress = 0.0f; }
	}
}

r32 ScreenWipeCreateStencil(ScreenWipeStencil_t stencilType, rec wipeRec, r32 animProgress, bool progressingForward)
{
	r32 paintAlpha = 1.0f;
	RcClearStencilBuffer(0);
	
	if (stencilType == ScreenWipeStencil_Swirl) { animProgress = Ease(EasingStyle_QuadraticIn, animProgress); }
	else if (stencilType == ScreenWipeStencil_HorizontalSlats) { animProgress = Ease(EasingStyle_Linear, animProgress); }
	else if (stencilType == ScreenWipeStencil_VerticalSlats) { animProgress = Ease(EasingStyle_Linear, animProgress); }
	else
	{
		animProgress = Ease(progressingForward ? EasingStyle_QuadraticOut : EasingStyle_QuadraticIn, animProgress);
	}
	
	switch (stencilType)
	{
		// +==============================+
		// |    ScreenWipeStencil_Fade    |
		// +==============================+
		case ScreenWipeStencil_Fade:
		{
			RcDrawRectangle(wipeRec, White);
			paintAlpha = animProgress;
		} break;
		
		// +===============================+
		// | ScreenWipeStencil_LeftToRight |
		// +===============================+
		case ScreenWipeStencil_LeftToRight:
		{
			RcDrawRectangle(NewRec(wipeRec.x, wipeRec.y, wipeRec.width*animProgress, wipeRec.height), White);
		} break;
		
		// +===============================+
		// | ScreenWipeStencil_RightToLeft |
		// +===============================+
		case ScreenWipeStencil_RightToLeft:
		{
			RcDrawRectangle(NewRec(wipeRec.x + wipeRec.width * (1-animProgress), wipeRec.y, wipeRec.width * animProgress, wipeRec.height), White);
		} break;
		
		// +==============================+
		// |  ScreenWipeStencil_TopDown   |
		// +==============================+
		case ScreenWipeStencil_TopDown:
		{
			RcDrawRectangle(NewRec(wipeRec.x, wipeRec.y, wipeRec.width, wipeRec.height * animProgress), White);
		} break;
		
		// +==============================+
		// |  ScreenWipeStencil_BottomUp  |
		// +==============================+
		case ScreenWipeStencil_BottomUp:
		{
			RcDrawRectangle(NewRec(wipeRec.x, wipeRec.y + wipeRec.height * (1-animProgress), wipeRec.width, wipeRec.height * animProgress), White);
		} break;
		
		// +==============================+
		// |   ScreenWipeStencil_Swirl    |
		// +==============================+
		case ScreenWipeStencil_Swirl:
		{
			RcBindShader(GetShader(wipeShader));
			RcSetTime(animProgress);
			
			RcDrawRectangle(wipeRec, White);
			RcDrawRectangle(NewRec(wipeRec.topLeft + wipeRec.size, -wipeRec.size), White);
			
			RcBindShader(GetShader(mainShader));
		} break;
		
		// +==================================+
		// | ScreenWipeStencil_VerticalSlats  |
		// +==================================+
		case ScreenWipeStencil_VerticalSlats:
		{
			u32 numSlats = (u32)CeilR32i(wipeRec.width / 300);
			for (u32 sIndex = 0; sIndex < numSlats; sIndex++)
			{
				rec slatRec = wipeRec;
				slatRec.width /= (r32)numSlats;
				slatRec.x += slatRec.width * sIndex;
				r32 slatAnim = animProgress * ((r32)numSlats);
				u32 slatIndex = sIndex;
				if (!progressingForward) { slatIndex = numSlats-1 - sIndex; }
				slatAnim -= ((r32)slatIndex*3/4);
				if (slatAnim >= 1.0f) { slatAnim = 1.0f; }
				if (slatAnim > 0.0f)
				{
					if (slatIndex%2 == 0) { slatRec.height *= slatAnim; }
					else { slatRec.y += slatRec.height * (1-slatAnim); slatRec.height *= slatAnim; }
					RcDrawRectangle(slatRec, White);
				}
			}
		} break;
		
		// +====================================+
		// | ScreenWipeStencil_HorizontalSlats  |
		// +====================================+
		case ScreenWipeStencil_HorizontalSlats:
		{
			u32 numSlats = (u32)CeilR32i(wipeRec.height / 300);
			for (u32 sIndex = 0; sIndex < numSlats; sIndex++)
			{
				rec slatRec = wipeRec;
				slatRec.height /= (r32)numSlats;
				slatRec.y += slatRec.height * sIndex;
				r32 slatAnim = animProgress * ((r32)numSlats);
				u32 slatIndex = sIndex;
				if (!progressingForward) { slatIndex = numSlats-1 - sIndex; }
				slatAnim -= ((r32)slatIndex*3/4);
				if (slatAnim >= 1.0f) { slatAnim = 1.0f; }
				if (slatAnim > 0.0f)
				{
					if (slatIndex%2 == 0) { slatRec.width *= slatAnim; }
					else { slatRec.x += slatRec.width * (1-slatAnim); slatRec.width *= slatAnim; }
					RcDrawRectangle(slatRec, White);
				}
			}
		} break;
		
		// +==============================+
		// |   ScreenWipeStencil_Fizzle   |
		// +==============================+
		case ScreenWipeStencil_Fizzle:
		{
			//TODO: Implement me!
		} break;
	};
	
	return paintAlpha;
}

void ScreenWipePaint(ScreenWipeType_t wipeType, rec wipeRec, r32 animProgress, bool progressingForward, r32 wipeAlpha, ScreenWipeSpinnerChoice_t spinnerChoice, Dir2_t spinnerRotation)
{
	Color_t fadeWhite = ColorTransparent(White, wipeAlpha);
	Color_t fadeBlack = ColorTransparent(Black, wipeAlpha);
	
	switch (wipeType)
	{
		// +==============================+
		// |      ScreenWipe_Simple       |
		// +==============================+
		case ScreenWipe_Simple:
		{
			//TODO: Implement me!
		} break;
		
		// +==============================+
		// | ScreenWipe_BlackWithSpinner  |
		// +==============================+
		case ScreenWipe_BlackWithSpinner:
		{
			RcBindShader(GetShader(tileShader));
			RcDefaultReplaceColors();
			RcSetReplaceColorPrimary(NewColor(0, 0, 0));
			RcSetReplaceColorSecondary(NewColor(50, 50, 50));
			// RcBindTexture(GetTexture(gameUiBackTexture));
			RcDrawTexturedRec(wipeRec, fadeWhite, NewRec(Vec2_Zero, wipeRec.size/8));
			
			r32 logoScale = wipeRec.width / (r32)GetTextureWidth(logoSprite);
			rec logoRec = NewRec(0, 0, wipeRec.width, GetTextureHeight(logoSprite)*logoScale);
			RcDefaultReplaceColors();
			RcBindTexture(GetTexture(logoSprite));
			RcDrawTexturedRec(logoRec, fadeWhite);
			
			RcBindFont(GetFont(pixelFont), 4);
			Color_t textColor = ColorMultiplyAlpha(fadeWhite, (SinR32((ProgramTime/1000.0f)*6.0f) + 1.0f) / 2.0f);
			RcDrawStringNt("Loading...", NewVec2(10, wipeRec.height - 10 - RcMaxExtendDown()), textColor);
			
			rec spinnerRec = NewRec(0, 0, 64, 64);
			spinnerRec.topLeft = wipeRec.topLeft + wipeRec.size - spinnerRec.size - NewVec2(10);
			v2i animFrame = NewVec2i(1, 0);
			Color_t spinnerColor = fadeWhite;
			SpriteSheet_t* spinnerSheet = GetSheet(entitiesSheet);
			u8 rotIndex = 0;
			if (spinnerRotation == Dir2_Up)    { rotIndex = 1; }
			if (spinnerRotation == Dir2_Left)  { rotIndex = 2; }
			if (spinnerRotation == Dir2_Right) { rotIndex = 3; }
			bool drawSpinnerRotated = false;
			EntityInfo_t tempInfo = {};
			tempInfo.rotation = Dir2_Down;
			// tempInfo.variant = ;//TODO: Add a random variant to the screenWipe variables
			
			RcDrawSheetFrame(spinnerSheet, animFrame, spinnerRec, spinnerColor, drawSpinnerRotated ? spinnerRotation : Dir2_Down);
			RcDefaultReplaceColors();
		} break;
		
		// +==============================+
		// |    ScreenWipe_Psychedelic    |
		// +==============================+
		case ScreenWipe_Psychedelic:
		{
			//TODO: Implement me!
		} break;
	}
}

void ApplyScreenWipe(rec wipeRec)
{
	// +==============================+
	// |        Apply Stencil         |
	// +==============================+
	if (app->screenWipeStencilType == ScreenWipeStencil_Swirl && app->screenWipeProgress >= 0.9f)
	{
		r32 fadeIn = (app->screenWipeProgress-0.9f) / 0.1f;
		RcDrawRectangle(wipeRec, ColorTransparent(Black, fadeIn));
	}
	RcStartStencilDrawing();
	r32 wipeAlpha = ScreenWipeCreateStencil(app->screenWipeStencilType, wipeRec, app->screenWipeProgress, app->screenWipeActive);
	
	// +==============================+
	// |        Paint Stencil         |
	// +==============================+
	RcUseStencil();
	ScreenWipePaint(app->screenWipeType, wipeRec, app->screenWipeProgress, app->screenWipeActive, wipeAlpha, app->screenWipeSpinnerChoice, app->screenWipeSpinnerRotation);
	RcDisableStencil();
}

