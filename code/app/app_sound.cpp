/*
File:   app_sound.cpp
Author: Taylor Robbins
Date:   11\11\2017
Description: 
	** Handles playing, handling, and stopping sounds using OpenAL
*/

LoadSongCallback_DEFINITION(AppSoundSongLoadCallback);

Sound_t* GetSoundPntrById(ALuint soundId)
{
	for (u32 rIndex = 0; rIndex < ArrayCount(soundsList->sounds); rIndex++)
	{
		SoundResource_t* sndResource = &soundsList->sounds[rIndex];
		if (soundId == sndResource->sound.id)
		{
			return &sndResource->sound;
		}
	}
	for (u32 mIndex = 0; mIndex < NUM_RESOURCE_MUSICS; mIndex++)
	{
		Resource_t* musicResource = GetResourceByType(ResourceType_Music, mIndex);
		Assert(musicResource != nullptr);
		if (IsResourceLoaded(musicResource) && soundId == musicResource->music.id)
		{
			return &musicResource->music;
		}
	}
	return nullptr;
}

//TODO: Implement these functions so they work with the custom audio layer
#if AUDIO_ENABLED && !USE_CUSTOM_AUDIO
SoundInstance_t* StartSndInstance(const Sound_t* srcPntr, bool looping = false, r32 volume = 1.0f, r32 pitch = 1.0f, u32 sampleIndex = 0)
{
	Assert(srcPntr != nullptr);
	Assert(srcPntr->isValid);
	SoundInstance_t* result = nullptr;
	
	for (u32 sIndex = 0; sIndex < MAX_SOUND_INSTANCES; sIndex++)
	{
		if (app->soundInstances[sIndex].playing == false)
		{
			result = &app->soundInstances[sIndex];
			break;
		}
	}
	
	if (result == nullptr)
	{
		WriteLine_E("Sound instance buffer is full!");
		return nullptr;
	}
	
	ClearPointer(result);
	
	alGenSources(1, &result->id);
	PrintOpenAlError("alGenSources");
	alSourcef (result->id, AL_PITCH, pitch);
	PrintOpenAlError("alSourcef(AL_PITCH)");
	alSourcef (result->id, AL_GAIN, volume);
	PrintOpenAlError("alSourcef(AL_GAIN)");
	alSource3f(result->id, AL_POSITION, 0, 0, 0);
	PrintOpenAlError("alSourcef(AL_POSITION)");
	alSource3f(result->id, AL_VELOCITY, 0, 0, 0);
	PrintOpenAlError("alSourcef(AL_VELOCITY)");
	alSourcei (result->id, AL_LOOPING, looping ? AL_TRUE : AL_FALSE);
	PrintOpenAlError("alSourcef(AL_LOOPING)");
	alSourcei (result->id, AL_BUFFER, srcPntr->id);
	PrintOpenAlError("alSourcef(AL_BUFFER)");
	if (sampleIndex > 0)
	{
		ALint alSampleIndex = (ALint)sampleIndex;
		alSourcei(result->id, AL_SAMPLE_OFFSET, alSampleIndex);
		PrintOpenAlError("alSourcef(AL_SAMPLE_OFFSET)");
	}
	
	alSourcePlay(result->id);
	PrintOpenAlError("alSourcePlay");
	
	result->playing = true;
	result->isLoopingSound = false;
	result->soundId = srcPntr->id;
	result->startTime = ProgramTime;
	return result;
}
BufferedSoundInstance_t* StartBufferedSndInstance(const Sound_t* buffers, u32 numBuffers, bool looping = false, r32 volume = 1.0f, r32 pitch = 1.0f)
{
	Assert(buffers != nullptr);
	Assert(numBuffers > 0);
	BufferedSoundInstance_t* result = nullptr;
	
	for (u32 sIndex = 0; sIndex < MAX_SOUND_INSTANCES; sIndex++)
	{
		if (app->bufferedSoundInstances[sIndex].playing == false)
		{
			result = &app->bufferedSoundInstances[sIndex];
			break;
		}
	}
	
	if (result == nullptr)
	{
		WriteLine_E("Buffered sound instance buffer is full!");
		return nullptr;
	}
	
	ClearPointer(result);
	result->allocArena = mainHeap;
	
	alGenSources(1, &result->id);
	PrintOpenAlError("alGenSources");
	alSourcef (result->id, AL_PITCH, pitch);
	PrintOpenAlError("alSourcef(AL_PITCH)");
	alSourcef (result->id, AL_GAIN, volume);
	PrintOpenAlError("alSourcef(AL_GAIN)");
	alSource3f(result->id, AL_POSITION, 0, 0, 0);
	PrintOpenAlError("alSourcef(AL_POSITION)");
	alSource3f(result->id, AL_VELOCITY, 0, 0, 0);
	PrintOpenAlError("alSourcef(AL_VELOCITY)");
	alSourcei (result->id, AL_LOOPING, looping ? AL_TRUE : AL_FALSE);
	PrintOpenAlError("alSourcef(AL_LOOPING)");
	
	result->numBuffers = numBuffers;
	result->bufferIds = PushArray(result->allocArena, ALuint, numBuffers);
	Assert(result->bufferIds != nullptr);
	for (u32 bIndex = 0; bIndex < numBuffers; bIndex++)
	{
		const Sound_t* buffer = &buffers[bIndex];
		Assert(buffer->isValid);
		alSourceQueueBuffers(result->id, 1, &buffer->id);
		PrintOpenAlError("alSourceQueueBuffers()");
		result->bufferIds[bIndex] = buffer->id;
	}
	
	alSourcePlay(result->id);
	PrintOpenAlError("alSourcePlay");
	
	result->playing = true;
	return result;
}

void StopSndInstance(SoundInstance_t* instance)
{
	Assert(instance != nullptr);
	Assert(instance >= &app->soundInstances[0]);
	Assert(instance <= &app->soundInstances[MAX_SOUND_INSTANCES-1]);
	
	if (alIsSource(instance->id))
	{
		alSourceStop(instance->id);
		PrintOpenAlError("alSourceStop");
		alDeleteSources(1, &instance->id);
		PrintOpenAlError("alDeleteSources");
	}
	
	ClearPointer(instance);
}
void StopBufferedSndInstance(BufferedSoundInstance_t* instance)
{
	Assert(instance != nullptr);
	Assert(instance >= &app->bufferedSoundInstances[0]);
	Assert(instance <= &app->bufferedSoundInstances[MAX_SOUND_INSTANCES-1]);
	
	if (alIsSource(instance->id))
	{
		alSourceStop(instance->id);
		PrintOpenAlError("alSourceStop");
		alDeleteSources(1, &instance->id);
		PrintOpenAlError("alDeleteSources");
	}
	
	if (instance->bufferIds != nullptr)
	{
		Assert(instance->allocArena != nullptr);
		ArenaPop(instance->allocArena, instance->bufferIds);
	}
	
	ClearPointer(instance);
}

void SetSndInstanceVolume(SoundInstance_t* instance, r32 volume)
{
	Assert(instance != nullptr);
	
	if (alIsSource(instance->id))
	{
		alSourcef(instance->id, AL_GAIN, volume);
		PrintOpenAlError("AlSourcef(AL_GAIN)");
	}
}
void SetSndInstancePitch(SoundInstance_t* instance, r32 pitch)
{
	Assert(instance != nullptr);
	
	if (alIsSource(instance->id))
	{
		alSourcef(instance->id, AL_PITCH, pitch);
		PrintOpenAlError("AlSourcef(AL_PITCH)");
	}
}

r32 GetSndInstancePlayOffset(SoundInstance_t* instance)
{
	ALfloat alSecOffset = 0;
	alGetSourcef(instance->id, AL_SEC_OFFSET, &alSecOffset);
	return (r32)(alSecOffset * 1000.0f);
	
}

void StopSoundsBySrc(const Sound_t* srcPntr)
{
	Assert(srcPntr != nullptr);
	for (u32 sIndex = 0; sIndex < MAX_SOUND_INSTANCES; sIndex++)
	{
		SoundInstance_t* instance = &app->soundInstances[sIndex];
		if (instance->playing && instance->soundId == srcPntr->id)
		{
			StopSndInstance(instance);
		}
	}
}
void StopSoundsByResource(const SoundResource_t* sndResource)
{
	Assert(sndResource != nullptr);
	for (u32 sIndex = 0; sIndex < MAX_SOUND_INSTANCES; sIndex++)
	{
		SoundInstance_t* instance = &app->soundInstances[sIndex];
		if (instance->playing && instance->soundId == sndResource->sound.id)
		{
			StopSndInstance(instance);
		}
	}
}

void StopAllLoopingSounds()
{
	for (u32 sIndex = 0; sIndex < MAX_SOUND_INSTANCES; sIndex++)
	{
		SoundInstance_t* instance = &app->soundInstances[sIndex];
		if (instance->playing && instance->isLoopingSound)
		{
			StopSndInstance(instance);
		}
	}
}

SoundInstance_t* GetSndInstanceBySrc(const Sound_t* srcPntr, u32 index) //pre-declared in app_func_defs.h
{
	Assert(srcPntr != nullptr);
	u32 foundIndex = 0;
	for (u32 sIndex = 0; sIndex < MAX_SOUND_INSTANCES; sIndex++)
	{
		SoundInstance_t* instance = &app->soundInstances[sIndex];
		if (instance->playing && instance->soundId == srcPntr->id)
		{
			if (foundIndex >= index) { return instance; }
			foundIndex++;
		}
	}
	return nullptr;
}

// +--------------------------------------------------------------+
// |                    Sound Effect Functions                    |
// +--------------------------------------------------------------+
SoundInstance_t* SoundEffect_(const SoundResource_t* sndResource, v2i tilePos, bool singleton, bool looping, r32 pitch = 1.0f, bool global = false)
{
	if (sett->masterSoundEnabled && sett->soundEffectsEnabled)
	{
		Assert(sndResource != nullptr);
		r32 distance = 0;
		r32 newDistVolume = (distance < AUDIO_FADE_DISTANCE) ? (1 - (distance / AUDIO_FADE_DISTANCE)) : 0.0f;
		r32 newVolume = sett->masterVolume * sett->soundEffectVolume * sndResource->volume * newDistVolume;
		if (newVolume <= 0) { return nullptr; }
		
		const Sound_t* source = &sndResource->sound;
		if (!sndResource->sound.isValid)
		{
			#if PLAY_MISSING_SOUNDS
			source = (sndResource->looping ? &soundsList->missingSoundLooping : &soundsList->missingSound);
			#else
			// PrintLine_I("Sound \"%s\" is missing", sndResource->readableName);
			return nullptr;
			#endif
		}
		
		if (singleton)
		{
			u32 sIndex = 0;
			while (SoundInstance_t* currentSnd = GetSndInstanceBySrc(source, sIndex++))
			{
				if (TimeSince(currentSnd->startTime) <= SND_EFFECT_COMBINE_TIME)
				{
					//The sound is already playing, no point in playing it again
					if (currentSnd->distVolume < newDistVolume)
					{
						alSourcef(currentSnd->id, AL_GAIN, newVolume);
						PrintOpenAlError("AlSourcef(AL_GAIN)");
						currentSnd->distVolume = newDistVolume;
						currentSnd->tilePos = tilePos;
					}
					return currentSnd;
				}
			}
		}
		if (sndResource->looping)
		{
			for (u32 sIndex = 0; sIndex < MAX_SOUND_INSTANCES; sIndex++)
			{
				SoundInstance_t* sndInstance = &app->soundInstances[sIndex];
				if (sndInstance->playing && sndInstance->soundId == source->id && sndInstance->isLoopingSound)
				{
					sndInstance->loopContinueFlag = true;
					if (sndInstance->distVolume < newDistVolume)
					{
						alSourcef(sndInstance->id, AL_GAIN, newVolume);
						PrintOpenAlError("AlSourcef(AL_GAIN)");
						sndInstance->distVolume = newDistVolume;
						sndInstance->tilePos = tilePos;
					}
					return sndInstance;
				}
			}
		}
		SoundInstance_t* result = StartSndInstance(source, sndResource->looping, newVolume, pitch);
		if (result == nullptr) { return nullptr; }
		result->distVolume = newDistVolume;
		result->tilePos = tilePos;
		if (sndResource->looping)
		{
			result->isLoopingSound = sndResource->looping;
			result->loopContinueFlag = true;
		}
		return result;
	}
	else { return nullptr; }
}

#define SoundEffect(sndName, tilePos)                    SoundEffect_(&soundsList->sndName, tilePos, false, false)
#define SoundEffectPitch(sndName, pitch, tilePos)        SoundEffect_(&soundsList->sndName, tilePos, false, false, pitch)
#define SoundEffectGlobal(sndName, pitch)                SoundEffect_(&soundsList->sndName, Vec2i_Zero, false, false, pitch, true)

#define SoundEffectSingle(sndName, tilePos)              SoundEffect_(&soundsList->sndName, tilePos, true, false)
#define SoundEffectSinglePitch(sndName, pitch, tilePos)  SoundEffect_(&soundsList->sndName, tilePos, true, false, pitch)
#define SoundEffectSingleGlobal(sndName, pitch)          SoundEffect_(&soundsList->sndName, Vec2i_Zero, false, false, pitch, true)

#define SoundEffectLooping(sndName, tilePos)             SoundEffect_(&soundsList->sndName, tilePos, false, true)
#define SoundEffectLoopingPitch(sndName, pitch, tilePos) SoundEffect_(&soundsList->sndName, tilePos, false, true, pitch)
#define SoundEffectLoopingGlobal(sndName, pitch)         SoundEffect_(&soundsList->sndName, Vec2i_Zero, false, true, pitch, true)

// +--------------------------------------------------------------+
// |                       Music Functions                        |
// +--------------------------------------------------------------+
r32 GetMusicTransitionAmount()
{
	if (app->musicTransStart == 0) { return 1.0f; }
	if (ProgramTime < app->musicTransStart) { return 0.0f; }
	if (ProgramTime - app->musicTransStart >= app->musicTransTime) { return 1.0f; }
	
	u64 transTime = ProgramTime - app->musicTransStart;
	r32 result = (r32)transTime / (r32)app->musicTransTime;
	return result;
}

void GetMusicTransitionVolumes(r32* volume1, r32* volume2)
{
	*volume1 = 1.0f;
	*volume2 = 1.0f;
	
	r32 amount = GetMusicTransitionAmount();
	if (amount >= 1.0f)
	{
		*volume1 = 0.0f; *volume2 = 1.0f;
		return;
	}
	
	switch (app->transitionType)
	{
		case SoundTransition_Immediate:
		{
			*volume1 = 1.0f; *volume2 = 0.0f;
		} break;
		
		case SoundTransition_FadeOut:
		{
			*volume1 = (1.0f - amount); *volume2 = 0.0f;
		} break;
		
		case SoundTransition_FadeIn:
		{
			*volume1 = 0.0f; *volume2 = amount;
		} break;
		
		case SoundTransition_FadeOutFadeIn:
		{
			if (amount < 0.5f) { *volume1 = (1.0f - amount*2); *volume2 = 0.0f; }
			else               { *volume1 = 0.0f; *volume2 = (amount - 0.5f) * 2; }
		} break;
		
		case SoundTransition_CrossFade:
		{
			*volume1 = (1.0f - amount); *volume2 = amount;
		} break;
	};
}

void QueueMusicChangeEx(Sound_t* newSong, SoundTransition_t transitionType, u64 transitionTime, bool repeating, u32 startOffset = 0)
{
	if (newSong != nullptr && !newSong->isValid)
	{
		if (!newSong->isLoading)
		{
			Resource_t* musicResource = GetResourceByVoidPntr(newSong);
			Assert(musicResource != nullptr);
			Assert(musicResource->type == ResourceType_Music);
			Sound_t* musicPntr = &musicResource->music;
			const char* resourceFilePath = GetMusicFilePath(musicResource->subIndex);
			Assert(resourceFilePath != nullptr);
			musicPntr->isLoading = true;
			bool queuedUp = QueueSongLoad(resourceFilePath, AppSoundSongLoadCallback);
			if (queuedUp)
			{
				PrintLine_I("Queued up song load for resource music[%u]: \"%s\"", musicResource->subIndex, resourceFilePath);
			}
			else
			{
				musicPntr->isLoading = false;
				PrintLine_E("Failed to queue up song load for resource music[%u]: \"%s\"", musicResource->subIndex, resourceFilePath);
			}
		}
		else { WriteLine_W("Song is already loading when another transition is requested"); }
		app->delayedMusicQueue.filled = true;
		app->delayedMusicQueue.newSong = newSong;
		app->delayedMusicQueue.transitionType = transitionType;
		app->delayedMusicQueue.repeating = repeating;
		app->delayedMusicQueue.transitionTime = transitionTime;
		app->delayedMusicQueue.startOffset = startOffset;
		return;
	}
	
	r32 currentTransitionTime = GetMusicTransitionAmount();
	if (currentTransitionTime < 1.0f)
	{
		WriteLine_W("Last music transition was cut short");
		if (app->lastMusic != nullptr) { StopSndInstance(app->lastMusic); }
		if (app->currentMusic != nullptr) { SetSndInstanceVolume(app->currentMusic, sett->masterVolume * sett->musicVolume * ((sett->masterSoundEnabled && sett->musicEnabled) ? 1.0f : 0.0f)); }
	}
	
	app->transitionType = transitionType;
	app->musicTransTime = transitionTime;
	app->musicTransStart = ProgramTime;
	app->lastMusic = app->currentMusic;
	app->currentMusic = nullptr;
	
	if (newSong != nullptr)
	{
		if (!newSong->isValid)
		{
			Resource_t* songResource = GetResourceByVoidPntr(newSong);
			Assert(songResource != nullptr);
			Assert(songResource->type == ResourceType_Music);
			LoadResource(songResource);
		}
		if (newSong->isValid)
		{
			u32 maxStartOffset = 1;
			if (newSong->sampleCount > 0)
			{
				Assert(newSong->channelCount > 0);
				maxStartOffset = newSong->sampleCount / newSong->channelCount;
			}
			PrintLine_D("Starting at %u/%u", startOffset, maxStartOffset);
			app->currentMusic = StartSndInstance(newSong, repeating, 0.0f, 1.0f, (startOffset%maxStartOffset));
		}
	}
	
	r32 volume1, volume2;
	GetMusicTransitionVolumes(&volume1, &volume2);
	if (app->lastMusic    != nullptr) { SetSndInstanceVolume(app->lastMusic,    volume1 * sett->masterVolume * sett->musicVolume * ((sett->masterSoundEnabled && sett->musicEnabled) ? 1.0f : 0.0f)); }
	if (app->currentMusic != nullptr) { SetSndInstanceVolume(app->currentMusic, volume2 * sett->masterVolume * sett->musicVolume * ((sett->masterSoundEnabled && sett->musicEnabled) ? 1.0f : 0.0f)); }
}
void QueueMusicChange(Sound_t* newSong)
{
	QueueMusicChangeEx(newSong, SoundTransition_CrossFade, DEF_MUSIC_TRANS_TIME, true);
}

void SoftQueueMusicChangeEx(Sound_t* newSong, SoundTransition_t transitionType, u64 transitionTime, bool repeating, u32 startOffset = 0)
{
	if (app->currentMusic == nullptr && newSong == nullptr) { return; } //already playing nothing
	if (app->currentMusic != nullptr && newSong != nullptr && app->currentMusic->soundId == newSong->id) { return; } //already playing that song
	QueueMusicChangeEx(newSong, transitionType, transitionTime, repeating, startOffset);
}
void SoftQueueMusicChange(Sound_t* newSong)
{
	SoftQueueMusicChangeEx(newSong, SoundTransition_CrossFade, DEF_MUSIC_TRANS_TIME, true);
}

// +--------------------------------------------------------------+
// |                       Update Function                        |
// +--------------------------------------------------------------+
void UpdateSndInstances()
{
	for (u32 sIndex = 0; sIndex < MAX_SOUND_INSTANCES; sIndex++)
	{
		SoundInstance_t* instance = &app->soundInstances[sIndex];
		BufferedSoundInstance_t* bufferedInstance = &app->bufferedSoundInstances[sIndex];
		
		if (instance->playing)
		{
			ALenum audioState = 0;
			alGetSourcei(instance->id, AL_SOURCE_STATE, &audioState);
			Resource_t* soundResource = GetResourceBySoundId(instance->soundId);
			if (soundResource != nullptr) { UpdateResourceAccessTime(soundResource); }
			bool errorOccurred = PrintOpenAlError("alGetSourcei(AL_SOURCE_STATE)");
			if (audioState != AL_PLAYING || errorOccurred)
			{
				StopSndInstance(instance);
				if (app->currentMusic == instance) { app->currentMusic = nullptr; }
				if (app->lastMusic == instance) { app->lastMusic = nullptr; }
			}
		}
		if (bufferedInstance->playing)
		{
			ALenum audioState = 0;
			alGetSourcei(bufferedInstance->id, AL_SOURCE_STATE, &audioState);
			// Resource_t* soundResource = GetResourceBySoundId(bufferedInstance->soundId);
			// if (soundResource != nullptr) { UpdateResourceAccessTime(soundResource); }
			bool errorOccurred = PrintOpenAlError("alGetSourcei(AL_SOURCE_STATE)");
			if (audioState != AL_PLAYING || errorOccurred)
			{
				StopBufferedSndInstance(bufferedInstance);
			}
		}
	}
	
	r32 transitionAmount = GetMusicTransitionAmount();
	if (transitionAmount >= 1.0f && app->lastMusic != nullptr)
	{
		WriteLine_D("Finished transition");
		StopSndInstance(app->lastMusic);
		app->lastMusic = nullptr;
	}
	r32 volume1, volume2;
	GetMusicTransitionVolumes(&volume1, &volume2);
	if (app->lastMusic != nullptr)
	{
		SetSndInstanceVolume(app->lastMusic,    volume1 * sett->masterVolume * sett->musicVolume * ((sett->masterSoundEnabled && sett->musicEnabled) ? 1.0f : 0.0f));
		Resource_t* soundResource = GetResourceBySoundId(app->lastMusic->soundId);
		if (soundResource != nullptr) { UpdateResourceAccessTime(soundResource); }
	}
	if (app->currentMusic != nullptr)
	{
		SetSndInstanceVolume(app->currentMusic, volume2 * sett->masterVolume * sett->musicVolume * ((sett->masterSoundEnabled && sett->musicEnabled) ? 1.0f : 0.0f));
		Resource_t* soundResource = GetResourceBySoundId(app->currentMusic->soundId);
		if (soundResource != nullptr) { UpdateResourceAccessTime(soundResource); }
	}
	
	// for (u32 mIndex = 0; mIndex < NUM_RESOURCE_MUSICS; mIndex++)
	// {
	// 	Resource_t* musicResource = GetResourceByType(ResourceType_Music, mIndex);
	// 	Assert(musicResource != nullptr);
	// 	Assert(musicResource->type == ResourceType_Music);
	// 	if (IsResourceLoaded(musicResource))
	// 	{
	// 		bool isInUse = false;
	// 		if (app->currentMusic != nullptr && app->currentMusic->soundId == musicResource->music.id) { isInUse = true; }
	// 		if (app->lastMusic != nullptr && app->lastMusic->soundId == musicResource->music.id) { isInUse = true; }
	// 		if (!isInUse)
	// 		{
	// 			PrintLine_D("Unloading unused music \"%s\"", GetFileNamePart(musicResource->filePath));
	// 			UnloadResource(musicResource);
	// 		}
	// 	}
	// }
}

void PrepareLoopingSoundsForUpdate()
{
	for (u32 sIndex = 0; sIndex < MAX_SOUND_INSTANCES; sIndex++)
	{
		SoundInstance_t* instance = &app->soundInstances[sIndex];
		if (instance->playing && instance->isLoopingSound)
		{
			instance->loopContinueFlag = false;
			instance->distVolume = 0.0f;
		}
	}
}

void CancelLoopingSoundsAfterUpdate()
{
	for (u32 sIndex = 0; sIndex < MAX_SOUND_INSTANCES; sIndex++)
	{
		SoundInstance_t* instance = &app->soundInstances[sIndex];
		if (instance->playing && instance->isLoopingSound)
		{
			if (!instance->loopContinueFlag)
			{
				StopSndInstance(instance);
			}
		}
	}
}

// +--------------------------------------------------------------+
// |                      Song Load Callback                      |
// +--------------------------------------------------------------+
LoadSongCallback_DEFINITION(AppSoundSongLoadCallback)
{
	Assert(filePath != nullptr);
	Assert(sampleBuffer != nullptr);
	Assert(soundInfo != nullptr);
	Resource_t* musicResource = GetResourceByFilePathNt(filePath);
	Assert(musicResource != nullptr);
	Assert(musicResource->type == ResourceType_Music);
	Sound_t* musicPntr = &musicResource->music;
	Assert(!musicPntr->isValid);
	Assert(musicPntr->isLoading);
	musicPntr->isLoading = false;
	if (success)
	{
		Assert(sampleBuffer->samples != nullptr);
		musicPntr->format = soundInfo->format;
		musicPntr->frequency = soundInfo->frequency;
		musicPntr->channelCount = soundInfo->channelCount;
		
		alGenBuffers(1, &musicPntr->id);
		PrintOpenAlError("alGenBuffers");
		alBufferData(musicPntr->id, musicPntr->format, sampleBuffer->samples, sampleBuffer->numSamples * sizeof(i16), musicPntr->frequency);
		musicPntr->sampleCount = sampleBuffer->numSamples;
		ALCenum error = alGetError();
		if (error != AL_NO_ERROR)
		{
			PrintLine_D("OpenAL Error after alBufferData: %s", GetOpenAlErrorStr(error));
			alDeleteBuffers(1, &musicPntr->id);
			PrintOpenAlError("alDeleteBuffers");
		}
		else
		{
			musicPntr->isValid = true;
			if (musicResource->filePath != nullptr) { ArenaPop(staticHeap, musicResource->filePath); }
			musicResource->filePath = ArenaNtString(staticHeap, filePath);
			musicResource->lastLoadTime = ProgramTime;
			musicResource->wasPreloaded = false;
			UpdateResourceAccessTime(musicResource);
		}
		
		if (app->delayedMusicQueue.filled && app->delayedMusicQueue.newSong == musicPntr)
		{
			WriteLine_I("Delayed music queue completed successfully!");
			QueueMusicChangeEx(app->delayedMusicQueue.newSong, app->delayedMusicQueue.transitionType, app->delayedMusicQueue.transitionTime, app->delayedMusicQueue.repeating, app->delayedMusicQueue.startOffset);
			app->delayedMusicQueue.filled = false;
		}
	}
}

#else //!AUDIO_ENABLED or USE_CUSTOM_AUDIO
SoundInstance_t* SoundEffect_(GameInstance_t* instance, const SoundResource_t* sndResource, v2i tilePos, bool singleton, bool looping, r32 pitch = 1.0f, bool global = false) { return NULL; }
void QueueMusicChangeEx(Sound_t* newSong, SoundTransition_t transitionType, u64 transitionTime, bool repeating) {}
void SoftQueueMusicChangeEx(Sound_t* newSong, SoundTransition_t transitionType, u64 transitionTime, bool repeating) {}
SoundInstance_t* GetSndInstanceBySrc(const Sound_t* srcPntr, u32 index) { return NULL; }
void StopSndInstance(SoundInstance_t* instance) {}

#define SoundEffect(instance, sndName, tilePos)                    SoundEffect_(instance, &soundsList->sndName, tilePos, false, false)
#define SoundEffectPitch(instance, sndName, pitch, tilePos)        SoundEffect_(instance, &soundsList->sndName, tilePos, false, false, pitch)
#define SoundEffectGlobal(instance, sndName, pitch)                SoundEffect_(instance, &soundsList->sndName, Vec2i_Zero, false, false, pitch, true)

#define SoundEffectSingle(instance, sndName, tilePos)              SoundEffect_(instance, &soundsList->sndName, tilePos, true, false)
#define SoundEffectSinglePitch(instance, sndName, pitch, tilePos)  SoundEffect_(instance, &soundsList->sndName, tilePos, true, false, pitch)
#define SoundEffectSingleGlobal(instance, sndName, pitch)          SoundEffect_(instance, &soundsList->sndName, Vec2i_Zero, false, false, pitch, true)

#define SoundEffectLooping(instance, sndName, tilePos)             SoundEffect_(instance, &soundsList->sndName, tilePos, false, true)
#define SoundEffectLoopingPitch(instance, sndName, pitch, tilePos) SoundEffect_(instance, &soundsList->sndName, tilePos, false, true, pitch)
#define SoundEffectLoopingGlobal(instance, sndName, pitch)         SoundEffect_(instance, &soundsList->sndName, Vec2i_Zero, false, true, pitch, true)

//#define QueueMusicChangeEx(newSong, transitionType, transitionTime)     //nothing
#define QueueMusicChange(newSong)                                       //nothing
//#define SoftQueueMusicChangeEx(newSong, transitionType, transitionTime) //nothing
#define SoftQueueMusicChange(newSong)                                   //nothing

void StopSoundsBySrc(const Sound_t* srcPntr){}
void UpdateSndInstances(){}
void PrepareLoopingSoundsForUpdate(){}
void CancelLoopingSoundsAfterUpdate(){}
void StopAllLoopingSounds(){}
void AppSoundSongLoadCallback(const char* filePath, bool success, const SampleBuffer_t* sampleBuffer, const Sound_t* soundInfo){}
//LoadSongCallback_DEFINITION(AppSoundSongLoadCallback);

#endif
