/*
File:   app_notifications.cpp
Author: Taylor Robbins
Date:   01\29\2020
Description: 
	** Handles showing the user notifications of various bugs or messages
	** that need to be displayed but that don't crash the game
*/

void InitNotifications()
{
	CreateDynamicArray(&app->notifications, mainHeap, sizeof(AppNotification_t));
}

//TODO: Make this thread safe!
AppNotification_t* PushNotification(const char* filePath, u32 lineNumber, const char* funcName, const char* message, Color_t textColor, r32 duration)
{
	Assert(message != nullptr);
	Assert(duration >= 0);
	AppNotification_t* newNotification = nullptr;
	if (sett->notificationPopupsEnabled)
	{
		if (app->notifications.length >= MAX_NOTIFICATIONS_ON_SCREEN)
		{
			AppNotification_t* lastNotification = DynArrayGet(&app->notifications, AppNotification_t, app->notifications.length-1);
			Assert(lastNotification != nullptr);
			if (lastNotification->message != nullptr) { ArenaPop(mainHeap, lastNotification->message); }
			DynArrayRemove(&app->notifications, app->notifications.length-1);
		}
		newNotification = DynArrayInsert(&app->notifications, AppNotification_t, 0);
		Assert(newNotification != nullptr);
		ClearPointer(newNotification);
		newNotification->message = ArenaNtString(mainHeap, message);
		newNotification->messageColor = textColor;
		newNotification->age = 0;
		newNotification->lifeSpan = duration;
		app->newNotificationAnim = 0.0f;
	}
	AppDebugOutput(DbgFlag_Inverted, filePath, lineNumber, funcName, DbgLevel_Notify, true, message);
	SoundEffectSingle(notificationSound, Vec2i_Zero);
	return newNotification;
}
AppNotification_t* PushNotificationPrint(const char* filePath, u32 lineNumber, const char* funcName, Color_t textColor, r32 duration, const char* formatStr, ...)
{
	TempPushMark();
	TempPrintVa(message, messageLength, formatStr);
	AppNotification_t* newNotification = PushNotification(filePath, lineNumber, funcName, message, textColor, duration);
	TempPopMark();
	return newNotification;
}

void NotificationsUpdateRecs()
{
	r32 yOffset = 0;
	RcBindFont(GetFont(debugFont));
	for (u32 nIndex = 0; nIndex < app->notifications.length; nIndex++)
	{
		AppNotification_t* notification = DynArrayGet(&app->notifications, AppNotification_t, nIndex);
		Assert(notification != nullptr);
		r32 animAmount = 1.0f;
		if (notification->age < NOTIFICATION_ANIM_TIME) { animAmount = notification->age / NOTIFICATION_ANIM_TIME; }
		else if (notification->age >= notification->lifeSpan - NOTIFICATION_ANIM_TIME)
		{
			animAmount = 1.0f - (notification->age - (notification->lifeSpan - NOTIFICATION_ANIM_TIME)) / NOTIFICATION_ANIM_TIME;
		}
		v2 messageSize = RcMeasureFormattedStringNt(notification->message, MAX_NOTIFICATION_WIDTH);
		notification->drawRec.size = messageSize + NewVec2(NOTIFICATION_PADDING*2, NOTIFICATION_PADDING*2);
		notification->drawRec.x = RenderScreenSize.width - (notification->drawRec.width * EaseQuadraticOut(animAmount));
		notification->drawRec.y = yOffset;
		if (nIndex > 0 && app->newNotificationAnim < 1.0f)
		{
			AppNotification_t* firstNofication = DynArrayGet(&app->notifications, AppNotification_t, 0);
			Assert(firstNofication != nullptr);
			notification->drawRec.y -= (firstNofication->drawRec.height + NOTIFICATION_PADDING) * EaseQuadraticOut(1.0f - app->newNotificationAnim);
		}
		yOffset += notification->drawRec.height + NOTIFICATION_PADDING;
	}
}
void NotificationsCaptureMouse()
{
	NotificationsUpdateRecs();
	for (u32 nIndex = 0; nIndex < app->notifications.length; nIndex++)
	{
		AppNotification_t* notification = DynArrayGet(&app->notifications, AppNotification_t, nIndex);
		Assert(notification != nullptr);
		if (IsMouseInside(notification->drawRec)) { CaptureMousePrint("Notification%u", nIndex); }
	}
}

void NotificationsUpdate()
{
	r32 perceivedElapsedMs = ClampR32(ElapsedMs, 0, 1000.0f/30.0f);
	
	// +==============================+
	// |   Update Notification Anim   |
	// +==============================+
	if (app->newNotificationAnim < 1.0f)
	{
		app->newNotificationAnim += perceivedElapsedMs / NOTIFICATION_ANIM_TIME;
		if (app->newNotificationAnim >= 1.0f) { app->newNotificationAnim = 1.0f; }
	}
	
	// +==============================+
	// |   Update Notification Age    |
	// +==============================+
	for (u32 nIndex = 0; nIndex < app->notifications.length; )
	{
		AppNotification_t* notification = DynArrayGet(&app->notifications, AppNotification_t, nIndex);
		Assert(notification != nullptr);
		notification->age += perceivedElapsedMs;
		if (notification->age >= notification->lifeSpan)
		{
			if (notification->message != nullptr) { ArenaPop(mainHeap, notification->message); }
			DynArrayRemove(&app->notifications, nIndex);
			//NOTE: Don't increment nIndex
		}
		else { nIndex++; }
	}
}

void NotificationsRender()
{
	NotificationsUpdateRecs();
	
	// +==============================+
	// |      Draw Notifications      |
	// +==============================+
	RcBindFont(GetFont(debugFont));
	for (u32 nIndex = 0; nIndex < app->notifications.length; nIndex++)
	{
		AppNotification_t* notification = DynArrayGet(&app->notifications, AppNotification_t, nIndex);
		Assert(notification != nullptr);
		v2 messagePos = NewVec2(
			notification->drawRec.x + NOTIFICATION_PADDING,
			notification->drawRec.y + NOTIFICATION_PADDING + RcMaxExtendUp()
		);
		
		RcDrawRectangle(notification->drawRec, ColorTransparent(MonokaiBack, 0.80f));
		RcDrawFormattedString(notification->message, messagePos, MAX_NOTIFICATION_WIDTH, notification->messageColor);
	}
}
