/*
File:   main_menu.cpp
Author: Taylor Robbins
Date:   07\10\2020
Description: 
	** The main menu contains the buttons for choosing various game modes, options for changing save slots and settings,
	** buttons to see the changelog and credits and various other things
*/


#include "main_menu/main_menu_helpers.cpp"

// +--------------------------------------------------------------+
// |                     Initialize Main Menu                     |
// +--------------------------------------------------------------+
void InitializeMainMenu()
{
	Assert(mainMenu != nullptr);
	
	mainMenu->initialized = true;
}

// +--------------------------------------------------------------+
// |                       Start Main Menu                        |
// +--------------------------------------------------------------+
bool StartMainMenu(AppState_t oldAppState, TransInfoType_t transInfoType, void* transInfoPntr)
{
	Assert(mainMenu != nullptr);
	Assert(mainMenu->initialized == true);
	SoftQueueMusicChange(nullptr);
	
	return true;
}

// +--------------------------------------------------------------+
// |                    Deinitialize Main Menu                    |
// +--------------------------------------------------------------+
void DeinitializeMainMenu()
{
	Assert(mainMenu != nullptr);
	Assert(mainMenu->initialized == true);
	
	mainMenu->initialized = false;
	ClearPointer(mainMenu);
}

// +--------------------------------------------------------------+
// |                       Does Cover Below                       |
// +--------------------------------------------------------------+
bool DoesMainMenuCoverBelow(AppState_t belowState)
{
	return true;
}

// +--------------------------------------------------------------+
// |                       Update Main Menu                       |
// +--------------------------------------------------------------+
void UpdateMainMenu(AppMenu_t appMenuAbove)
{
	Assert(mainMenu != nullptr);
	Assert(mainMenu->initialized == true);
	
	r32 timeScale = (ElapsedMs / (1000.0f / 60.0f));
	
	// +==============================+
	// |       Handle Wipe Done       |
	// +==============================+
	if (mainMenu->activatedWipe && app->screenWipeProgress >= 1.0f)
	{
		mainMenu->activatedWipe = false;
		app->screenWipeActive = false;
		if (mainMenu->wipeDoneTransInfoType != TransInfoType_Nothing)
		{
			if (mainMenu->pushWipeDoneState) { PushAppStateExt(mainMenu->wipeDoneState, mainMenu->wipeDoneTransInfoType, mainMenu->wipeDoneTransInfoPntr); }
			else { ChangeAppStateExt(mainMenu->wipeDoneState, mainMenu->wipeDoneTransInfoType, mainMenu->wipeDoneTransInfoPntr); }
		}
		else
		{
			if (mainMenu->pushWipeDoneState) { PushAppState(mainMenu->wipeDoneState); }
			else { ChangeAppState(mainMenu->wipeDoneState); }
		}
		return;
	}
	
	// +==============================+
	// |       Tilde Shortcuts        |
	// +==============================+
	if (app->developerModeEnabled && ButtonPressed(Button_Tilde) && !mainMenu->activatedWipe)
	{
		HandleButton(Button_Tilde);
		WriteLine_I("Going to Mess AppState");
		StartScreenWipe(ScreenWipe_BlackWithSpinner, ScreenWipeStencil_BottomUp);
		mainMenu->activatedWipe = true;
		mainMenu->wipeDoneState = AppState_Mess;
		mainMenu->pushWipeDoneState = true;
		mainMenu->wipeDoneTransInfoType = TransInfoType_Nothing;
	}
}

// +--------------------------------------------------------------+
// |                       Render Main Menu                       |
// +--------------------------------------------------------------+
void RenderMainMenu(FrameBuffer_t* renderBuffer, AppState_t appStateAbove, AppMenu_t appMenuAbove)
{
	Assert(mainMenu != nullptr);
	Assert(mainMenu->initialized == true);
	
	RcBegin(GetShader(tileShader), NewRec(Vec2_Zero, RenderScreenSize), GetFont(pixelFont), renderBuffer);
	RcClear(ColorCornflowerBlue, 1.0f);
	
	
}

