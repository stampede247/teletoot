/*
File:   platformInterface.h
Author: Taylor Robbins
Date:   11\04\2017
Description:
	** The plat_interface file is the only file (besides external libraries) that is
	** included by both the application and the platform layer. It defines the backbone
	** of structures that are used to communicate between the two layers. It also provides
	** a space for some platform specific header files to be included in order to provide
	** structures and definitions for the application that change when running on different
	** operating systems.
*/

#ifndef _PLAT_INTERFACE_H
#define _PLAT_INTERFACE_H

#define MEMORY_ARENA_META_INFO_ENABLED (false && DEBUG)

//NOTE: mylib.h defines the WIN32_COMPILATION, OSX_COMPILATION, and LINUX_COMPILATION defines
//      It also automatically #includes <windows.h>
#include "mylib/mylib.h"

#if WINDOWS_COMPILATION
	#if USE_CUSTOM_AUDIO
		#define INITGUID
		#include <dsound.h>
		#include <Dbt.h> //DBT_CONFIGCHANGED and other wParam values for WM_DEVICECHANGE
		#include <mmdeviceapi.h>
		#include "win32/win32_audio.h"
	#else //!USE_CUSTOM_AUDIO
		#include <openal/al.h>
		#include <openal/alc.h>
	#endif
	
	#include "win32/win32_performance.h"
	#include "win32/win32_threads.h"
	#include "win32/win32_tasks.h"
	#include "win32/win32_files.h"
	#include "win32/win32_fonts.h"
	#include "win32/win32_http.h"
	
	#define EXPORT __declspec(dllexport)
	#define IMPORT __declspec(dllimport)
	
	#define GLFW_EXPOSE_NATIVE_WIN32
#endif

#if OSX_COMPILATION
	#include <pthread.h>
	#include <semaphore.h>
	#include "osx/osx_performance.h"
	#include "osx/osx_helpers.h"
	#include "osx/osx_fonts.h"
	#include "osx/osx_threads.h"
	#include "osx/osx_tasks.h"
	#include "win32/win32_http.h"
	
	#if USE_CUSTOM_AUDIO
		//TODO: Add include files for custom audio layer
	#else
		#ifdef HAVE_AL_AL_H
		#include <AL/al.h>
		#elif defined(__APPLE__)
		#include <OpenAL/al.h>
		#else
		#include <al.h>
		#endif
		#ifdef HAVE_AL_ALC_H
		#include <AL/alc.h>
		#elif defined(__APPLE__)
		#include <OpenAL/alc.h>
		#else
		#include <alc.h>
		#endif
	#endif
	
	#define EXPORT extern "C" __attribute__((visibility("default")))
	#define IMPORT
#endif

#if LINUX_COMPILATION
	#include "linux/linux_helpers.h"
	
	#if USE_CUSTOM_AUDIO
		//TODO: Add include files for custom audio layer
	#else
		#include <AL/al.h>
		#include <AL/alc.h>
	#endif
	
	#define EXPORT extern "C" __attribute__((visibility("default")))
	#define IMPORT
#endif

#include "GL/glew.h"
#include "GLFW/glfw3.h"
#if OSX_COMPILATION
#define GLFW_EXPOSE_NATIVE_NSGL
#define GLFW_EXPOSE_NATIVE_COCOA
#include "GLFW/glfw3native.h"
#endif

#if STEAM_BUILD
#include "steam/steam_api.h"
#endif

#include "plat/plat_structs.h"

#define GL_GPU_MEM_INFO_TOTAL_AVAILABLE_MEM_NVX 0x9048
#define GL_GPU_MEM_INFO_CURRENT_AVAILABLE_MEM_NVX 0x9049

// +--------------------------------------------------------------+
// |                   Platform Layer Functions                   |
// +--------------------------------------------------------------+
#define GetPerfTime_DEFINITION(functionName) PerfTime_t functionName()
typedef GetPerfTime_DEFINITION(GetPerfTime_f);
#define GetPerfTimeDiff_DEFINITION(functionName) r64 functionName(const PerfTime_t* tStart, const PerfTime_t* tEnd)
typedef GetPerfTimeDiff_DEFINITION(GetPerfTimeDiff_f);

#define CompleteRelativePath_DEFINITION(functionName) char* functionName(MemoryArena_t* memArena, const char* path, u32 pathLength)
typedef CompleteRelativePath_DEFINITION(CompleteRelativePath_f);
#define DoesFolderExist_DEFINITION(functionName) bool functionName(const char* folderPath)
typedef DoesFolderExist_DEFINITION(DoesFolderExist_f);
#define CreateFolder_DEFINITION(functionName) bool functionName(const char* folderPath)
typedef CreateFolder_DEFINITION(CreateFolder_f);
#define DoesFileExist_DEFINITION(functionName) bool functionName(const char* filePath)
typedef DoesFileExist_DEFINITION(DoesFileExist_f);
#define FreeFileMemory_DEFINITION(functionName)  void functionName(FileInfo_t* fileInfo)
typedef FreeFileMemory_DEFINITION(FreeFileMemory_f);
#define ReadEntireFile_DEFINITION(functionName)  FileInfo_t functionName(const char* filePath)
typedef ReadEntireFile_DEFINITION(ReadEntireFile_f);
#define WriteEntireFile_DEFINITION(functionName) bool functionName(const char* filePath, const void* memory, u32 memorySize)
typedef WriteEntireFile_DEFINITION(WriteEntireFile_f);
#define DeleteFile_DEFINITION(functionName) bool functionName(const char* filePath)
typedef DeleteFile_DEFINITION(DeleteFile_f);
#define OpenFile_DEFINITION(functionName) bool functionName(const char* filePath, bool openForWriting, OpenFile_t* openFileOut)
typedef OpenFile_DEFINITION(OpenFile_f);
#define WriteToFile_DEFINITION(functionName) bool functionName(OpenFile_t* filePntr, const void* newData, u32 newDataSize)
typedef WriteToFile_DEFINITION(WriteToFile_f);
#define SeekToOffsetInFile_DEFINITION(functionName) bool functionName(OpenFile_t* filePntr, u64 offset)
typedef SeekToOffsetInFile_DEFINITION(SeekToOffsetInFile_f);
#define MoveFileCursor_DEFINITION(functionName) bool functionName(OpenFile_t* filePntr, i64 moveAmount)
typedef MoveFileCursor_DEFINITION(MoveFileCursor_f);
#define ReadFromFile_DEFINITION(functionName) bool functionName(OpenFile_t* filePntr, void* bytesOut, u32 numBytes, bool moveCursor)
typedef ReadFromFile_DEFINITION(ReadFromFile_f);
#define CloseFile_DEFINITION(functionName) void functionName(OpenFile_t* filePntr)
typedef CloseFile_DEFINITION(CloseFile_f);
#define MoveFile_DEFINITION(functionName) bool functionName(const char* srcFilePath, const char* destFilePath, bool allowOverwrite, bool keepOriginal)
typedef MoveFile_DEFINITION(MoveFile_f);
#define LaunchFile_DEFINITION(functionName) bool functionName(const char* filePath)
typedef LaunchFile_DEFINITION(LaunchFile_f);
#define GetNumFilesInDirectory_DEFINITION(functionName) u32 functionName(const char* folderPath)
typedef GetNumFilesInDirectory_DEFINITION(GetNumFilesInDirectory_f);
#define GetFileInDirectory_DEFINITION(functionName) char* functionName(MemoryArena_t* arenaPntr, const char* folderPath, u32 index)
typedef GetFileInDirectory_DEFINITION(GetFileInDirectory_f);
#define ShowSourceFile_DEFINITION(functionName) bool functionName(const char* filePath, u32 lineNumber)
typedef ShowSourceFile_DEFINITION(ShowSourceFile_f);
#define ShowFile_DEFINITION(functionName) bool functionName(const char* filePath)
typedef ShowFile_DEFINITION(ShowFile_f);
#define GetSpecialFolderPath_DEFINITION(functionName) char* functionName(MemoryArena_t* arenaPntr, SpecialFolder_t folder, const char* applicationName)
typedef GetSpecialFolderPath_DEFINITION(GetSpecialFolderPath_f);

#define DebugOutput_DEFINITION(functionName) void functionName(const char* filePath, u32 lineNumber, const char* functionStr, DbgLevel_t dbgLevel, bool newLine, const char* message)
typedef DebugOutput_DEFINITION(DebugOutput_f);
#define DebugPrintVa_DEFINITION(functionName) void functionName(const char* filePath, u32 lineNumber, const char* functionStr, DbgLevel_t dbgLevel, bool newLine, const char* formatString, va_list args, u32 printSize)
typedef DebugPrintVa_DEFINITION(DebugPrintVa_f);
#define DebugPrint_DEFINITION(functionName) void functionName(const char* filePath, u32 lineNumber, const char* functionStr, DbgLevel_t dbgLevel, bool newLine, const char* formatString, ...)
typedef DebugPrint_DEFINITION(DebugPrint_f);

#define CopyToClipboard_DEFINITION(functionName) void functionName(const void* dataPntr, u32 dataSize)
typedef CopyToClipboard_DEFINITION(CopyToClipboard_f);
#define CopyFromClipboard_DEFINITION(functionName) void* functionName(MemoryArena_t* arenaPntr, u32* dataLengthOut)
typedef CopyFromClipboard_DEFINITION(CopyFromClipboard_f);

#define WatchFile_DEFINITION(functionName) bool functionName(const char* filePath)
typedef WatchFile_DEFINITION(WatchFile_f);
#define UnwatchFile_DEFINITION(functionName) bool functionName(const char* filePath)
typedef UnwatchFile_DEFINITION(UnwatchFile_f);
#define IsWatchingFile_DEFINITION(functionName) bool functionName(const char* filePath)
typedef IsWatchingFile_DEFINITION(IsWatchingFile_f);

#define AllocateMemory_DEFINITION(functionName) void* functionName(u32 numBytes)
typedef AllocateMemory_DEFINITION(AllocateMemory_f);
#define ReallocateMemory_DEFINITION(functionName) void* functionName(void* memPntr, u32 newSize)
typedef ReallocateMemory_DEFINITION(ReallocateMemory_f);
#define FreeMemory_DEFINITION(functionName) void functionName(void* memPntr)
typedef FreeMemory_DEFINITION(FreeMemory_f);

#define UpdateLoadingBar_DEFINITION(functionName) void functionName(r32 percentDone)
typedef UpdateLoadingBar_DEFINITION(UpdateLoadingBar_f);
#define WasWindowCloseRequested_DEFINITION(functionName) bool functionName()
typedef WasWindowCloseRequested_DEFINITION(WasWindowCloseRequested_f);

#define OpenEmailPrompt_DEFINITION(functionName) bool functionName(const char* recipientStr, const char* subjectStr, const char* bodyStr)
typedef OpenEmailPrompt_DEFINITION(OpenEmailPrompt_f);

#define HttpRequestStart_DEFINITION(functionName) bool functionName(const HttpRequest_t* request)
typedef HttpRequestStart_DEFINITION(HttpRequestStart_f);
#define HttpRequestInProgress_DEFINITION(functionName) bool functionName()
typedef HttpRequestInProgress_DEFINITION(HttpRequestInProgress_f);

#define OpenPlatformFont_DEFINITION(functionName) bool functionName(const char* fontName, i32 fontSize, bool bold, bool italic, PlatformFont_t* fontOut)
typedef OpenPlatformFont_DEFINITION(OpenPlatformFont_f);
#define ClosePlatformFont_DEFINITION(functionName) void functionName(PlatformFont_t* font)
typedef ClosePlatformFont_DEFINITION(ClosePlatformFont_f);

#define CreateMutex_DEFINITION(functionName) void functionName(Mutex_t* mutex)
typedef CreateMutex_DEFINITION(CreateMutex_f);
#define LockMutex_DEFINITION(functionName)   bool functionName(Mutex_t* mutex, u32 timeoutMs)
typedef LockMutex_DEFINITION(LockMutex_f);
#define UnlockMutex_DEFINITION(functionName) void functionName(Mutex_t* mutex)
typedef UnlockMutex_DEFINITION(UnlockMutex_f);
#define GetThisThreadId_DEFINITION(functionName) ThreadId_t functionName()
typedef GetThisThreadId_DEFINITION(GetThisThreadId_f);
#define IsMainThread_DEFINITION(functionName) bool functionName(ThreadId_t threadId)
typedef IsMainThread_DEFINITION(IsMainThread_f);
#define SleepForMs_DEFINITION(functionName) void functionName(u32 numMs)
typedef SleepForMs_DEFINITION(SleepForMs_f);

#define QueueThreadedTask_DEFINITION(functionName) u32 functionName(const ThreadedTask_t* task)
typedef QueueThreadedTask_DEFINITION(QueueThreadedTask_f);
#define GetThreadInfo_DEFINITION(functionName) TaskThreadInfo_t* functionName(ThreadId_t threadId)
typedef GetThreadInfo_DEFINITION(GetThreadInfo_f);

// +--------------------------------------------------------------+
// |                          Structures                          |
// +--------------------------------------------------------------+
struct StartupInfo_t
{
	PlatformType_t platformType;
	Version_t version;
	GlfwVersion_t glfwVersion;
	u32 numMonitors;
	u32 primaryMonitorIndex;
	const MonitorInfo_t* monitors;
	#if AUDIO_ENABLED && USE_CUSTOM_AUDIO
	u32 numAudioDevices;
	u32 primaryAudioDeviceIndex;
	const AudioDevice_t* audioDevices;
	#endif
	RealTime_t systemTime; //world time (UTC timezone)
	RealTime_t localTime; //world time (Local timezone)
	u32 numProgramArguments;
	char** programArguments;
	
	MemoryArena_t* platformMainHeap;
	MemoryArena_t* platformTempArena;
	MemoryArena_t* platformStdHeap;
	
	FreeFileMemory_f*  FreeFileMemory;
	ReadEntireFile_f*  ReadEntireFile;
	WriteEntireFile_f* WriteEntireFile;
	GetSpecialFolderPath_f* GetSpecialFolderPath;
	DoesFileExist_f* DoesFileExist;
};

#define MAX_STARTUP_OP_PATH_LENGTH 256
struct StartupOptions_t
{
	v2i windowSize;
	v2i minWindowSize;
	v2i maxWindowSize;
	v2i windowPosition;
	bool fullscreen;
	u32 fullscreenMonitorIndex;
	bool autoIconify;
	bool topmostWindow;
	bool decoratedWindow;
	bool resizableWindow;
	bool forceAspectRatio;
	v2i aspectRatio;
	u32 antialiasingNumSamples;
	u32 temporaryMemorySize;
	u32 permanantMemorySize;
	MouseMode_t mouseMode;
	#if AUDIO_ENABLED && USE_CUSTOM_AUDIO
	u32 audioDeviceIndex;
	#endif
	
	Color_t loadingColor;
	char iconFilePaths[4][MAX_STARTUP_OP_PATH_LENGTH]; //16, 24, 32, and 64 pixel icons
	char loadingImagePath[MAX_STARTUP_OP_PATH_LENGTH];
};

struct PlatformInfo_t
{
	PlatformType_t platformType;
	Version_t version;
	GlfwVersion_t glfwVersion;
	OpenglVersion_t openglVersion;
	
	bool steamInitialized;
	u32 steamAppId;
	
	GLFWwindow* window;
	
	#if AUDIO_ENABLED
		#if USE_CUSTOM_AUDIO
		AudioInfo_t audioInfo;
		u32 numAudioDevices;
		u32 primaryAudioDeviceIndex;
		const AudioDevice_t* audioDevices;
		#else
		ALCdevice* alDevice;
		ALCcontext* alContext;
		#endif
	#endif
	
	u32 numProgramArguments;
	char** programArguments;
	
	u32 numMonitors;
	u32 primaryMonitorIndex;
	const MonitorInfo_t* monitors;
	
	#if DEBUG
	const void* appAllocationsPntr;
	#endif
	
	GetPerfTime_f*     GetPerfTime;
	GetPerfTimeDiff_f* GetPerfTimeDiff;
	
	CompleteRelativePath_f* CompleteRelativePath;
	DoesFolderExist_f*      DoesFolderExist;
	CreateFolder_f*         CreateFolder;
	
	DoesFileExist_f*   DoesFileExist;
	FreeFileMemory_f*  FreeFileMemory;
	ReadEntireFile_f*  ReadEntireFile;
	WriteEntireFile_f* WriteEntireFile;
	DeleteFile_f*      DeleteFile;
	
	OpenFile_f*           OpenFile;
	WriteToFile_f*        WriteToFile;
	SeekToOffsetInFile_f* SeekToOffsetInFile;
	MoveFileCursor_f*     MoveFileCursor;
	ReadFromFile_f*       ReadFromFile;
	CloseFile_f*          CloseFile;
	MoveFile_f*           MoveFile;
	LaunchFile_f*         LaunchFile;
	
	GetNumFilesInDirectory_f* GetNumFilesInDirectory;
	GetFileInDirectory_f*     GetFileInDirectory;
	
	ShowSourceFile_f* ShowSourceFile; // Debug Feature
	ShowFile_f*       ShowFile; // Debug Feature
	
	GetSpecialFolderPath_f* GetSpecialFolderPath;
	
	DebugOutput_f*    DebugOutput;
	DebugPrintVa_f*   DebugPrintVa;
	DebugPrint_f*     DebugPrint;
	
	CopyToClipboard_f*   CopyToClipboard;
	CopyFromClipboard_f* CopyFromClipboard;
	
	WatchFile_f*      WatchFile;
	UnwatchFile_f*    UnwatchFile;
	IsWatchingFile_f* IsWatchingFile;
	
	AllocateMemory_f*   AllocateMemory;
	ReallocateMemory_f* ReallocateMemory;
	FreeMemory_f*       FreeMemory;
	
	UpdateLoadingBar_f*        UpdateLoadingBar; // Could be dummy function 
	WasWindowCloseRequested_f* WasWindowCloseRequested;
	
	OpenEmailPrompt_f* OpenEmailPrompt; // Could be dummy function 
	
	HttpRequestStart_f*      HttpRequestStart; // Could be dummy function 
	HttpRequestInProgress_f* HttpRequestInProgress; // Could be dummy function 
	
	OpenPlatformFont_f*  OpenPlatformFont;
	ClosePlatformFont_f* ClosePlatformFont;
	
	CreateMutex_f*     CreateMutex;
	LockMutex_f*       LockMutex;
	UnlockMutex_f*     UnlockMutex;
	GetThisThreadId_f* GetThisThreadId;
	IsMainThread_f*    IsMainThread;
	SleepForMs_f*      SleepForMs;
	
	QueueThreadedTask_f* QueueThreadedTask;
	GetThreadInfo_f*     GetThreadInfo;
};

struct AppMemory_t
{
	u32 permanantSize;
	u32 transientSize;
	
	void* permanantPntr;
	void* transientPntr;
};

#define MAX_NUM_GAMEPADS        GLFW_JOYSTICK_LAST
#define MAX_NUM_INPUT_EVENTS    32
#define MAX_INPUT_TEXT_LENGTH   64

struct AppInput_t
{
	bool mouseInsideWindow;
	bool mouseInsideChanged;
	v2 mousePos;
	bool mouseMoved;
	v2 mouseStartPos[3];
	r32 mouseMaxDist[3];
	v2 scrollDelta;
	v2 scrollValue;
	bool scrollChanged;
	
	v2i windowPosition;
	bool windowMoved;
	v2i screenSize;
	bool windowResized;
	i32 windowBorderSizeLeft;
	i32 windowBorderSizeTop;
	i32 windowBorderSizeRight;
	i32 windowBorderSizeBottom;
	
	bool isFullscreen;
	bool fullscreenChanged;
	bool windowHasFocus;
	bool windowFocusChanged;
	bool windowIsMinimized;
	bool windowMinimizedChanged;
	bool windowInteractionHappened;
	
	u32 currentMonitorIndex;
	bool currentMonitorChanged;
	u32 currentDisplayModeIndex;
	bool currentDisplayModeChanged;
	r32 targetFramerate;
	bool targetFramerateChanged;
	bool doingManualUpdateLoopWait;
	
	u32 numAvailableTaskThreads;
	u32 numActiveThreadedTasks;
	u32 numQueuedThreadedTasks;
	u32 numTotalThreadedTasks;
	
	r64 programTimeF; //ms (since start of program)
	u64 programTime; //ms (since start of program)
	r64 elapsedMs; //ms
	r64 avgElapsedMs; //ms
	r64 framerate; //frames/sec
	r64 avgFramerate; //frames/sec
	r64 timeDelta; //stays near 1.0f if framerate is close to targetFramerate
	r64 lastUpdateElapsedMs; //ms
	r32 timeSpentWaitingLastFrame; //ms
	
	RealTime_t systemTime; //world time (UTC timezone)
	RealTime_t localTime; //world time (Local timezone)
	
	u8 modifiers;
	ButtonState_t buttons[Buttons_NumButtons];
	
	GamepadState_t gamepads[MAX_NUM_GAMEPADS];
	
	u32 numInputEvents;
	InputEvent_t inputEvents[MAX_NUM_INPUT_EVENTS];
	
	u8 textInputLength;
	char textInput[MAX_INPUT_TEXT_LENGTH];
	
	u32 numDebugLines;
	const PlatDebugLine_t* debugLines;
	
	u32 numDroppedFiles;
	const char** droppedFilePaths;
	
	#if AUDIO_ENABLED && USE_CUSTOM_AUDIO
	bool audioDevicesChanged;
	bool primaryAudioDeviceChanged;
	bool audioReset;
	#endif
};

#define MAX_WINDOW_TITLE_LENGTH 64

struct AppOutput_t
{
	bool recenterMouse;
	bool showMouse;
	bool closeWindow;
	
	CursorType_t cursorType;
	MouseMode_t mouseMode;
	
	char windowTitle[MAX_WINDOW_TITLE_LENGTH];
	
	bool makeFullscreen;
	u32 fullscreenMonitorIndex;
	u32 fullscreenDisplayModeIndex;
	
	bool makeWindowed;
	v2i windowedPosition;
	v2i windowedResolution;
	
	bool changeResolution;
	v2i newResolution;
	bool changeDisplayMode;
	u32 newDisplayModeIndex;
	
	bool changeAutoIconify;
	bool autoIconify;
	
	bool shouldShowErrorAndClose;
	char* errorMessage;
};


//+================================================================+
//|                  Game Function Definitions                     |
//+================================================================+

#define AppGetVersion_DEFINITION(functionName)                   Version_t functionName(bool* resetApplication)
typedef AppGetVersion_DEFINITION(AppGetVersion_f);

#define AppGetStartupOptions_DEFINITION(functionName)            void functionName(const StartupInfo_t* StartupInfo, StartupOptions_t* StartupOptions)
typedef AppGetStartupOptions_DEFINITION(AppGetStartupOptions_f);

#define AppInitialize_DEFINITION(functionName)                   void functionName(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory, const AppInput_t* AppInput)
typedef AppInitialize_DEFINITION(AppInitialize_f);

#define AppReloaded_DEFINITION(functionName)                     void functionName(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory)
typedef AppReloaded_DEFINITION(AppReloaded_f);

#define AppTaskFinished_DEFINITION(functionName)                 void functionName(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory, ThreadedTask_t* task)
typedef AppTaskFinished_DEFINITION(AppTaskFinished_f);

#define AppUpdate_DEFINITION(functionName)                       void functionName(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory, const AppInput_t* AppInput, AppOutput_t* AppOutput)
typedef AppUpdate_DEFINITION(AppUpdate_f);

#define AppPerformTask_DEFINITION(functionName)                  void functionName(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory, ThreadedTask_t* task, const TaskThreadInfo_t* threadInfo)
typedef AppPerformTask_DEFINITION(AppPerformTask_f);

#define AppClosing_DEFINITION(functionName)                      void functionName(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory)
typedef AppClosing_DEFINITION(AppClosing_f);

#if AUDIO_ENABLED && USE_CUSTOM_AUDIO
#define AppGetAudioOutput_DEFINITION(functionName)               void functionName(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory, AudioOutput_t* AudioOutput)
typedef AppGetAudioOutput_DEFINITION(AppGetAudioOutput_f);
#endif

#endif //  _PLAT_INTERFACE_H
