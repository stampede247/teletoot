/*
File:   plat_structs.h
Author: Taylor Robbins
Date:   06\05\2019
Description: 
	** Contains the miscellaneous common structures used by both the application and the platform layer

#included by plat_interface.h
*/

struct Version_t
{
	u32 major;
	u32 minor;
	u32 build;
};

struct GlfwVersion_t
{
	i32 major;
	i32 minor;
	i32 revision;
};

struct OpenglVersion_t
{
	u32 major;
	u32 minor;
};

typedef enum
{
	Platform_Windows = 0,
	Platform_Linux,
	Platform_OSX,
	Platform_NumTypes,
} PlatformType_t;

typedef enum
{
	Cursor_Default = 0,
	Cursor_Text,
	Cursor_Pointer,
	Cursor_ResizeHorizontal,
	Cursor_ResizeVertical,
	Cursor_NumTypes,
} CursorType_t;

typedef enum
{
	MouseMode_Default = 0,
	MouseMode_Hidden,
	MouseMode_Delta,
	MouseMode_NumModes,
} MouseMode_t;

struct MonitorDisplayMode_t
{
	const GLFWvidmode* glfwMode;
	v2i resolution;
	u32 bitDepth;
	r32 refreshRate;
};

struct MonitorInfo_t
{
	GLFWmonitor* glfwMonitor;
	char* name;
	v2i offset;
	v2i originalOffset;
	v2i physicalSize;
	v2 contentScale;
	reci workArea;
	u32 numModes;
	u32 currentMode;
	u32 originalMode;
	MonitorDisplayMode_t* modes;
};

typedef enum
{
	SpecialFolder_Saves = 0,
	SpecialFolder_Settings,
	SpecialFolder_Screenshots,
	SpecialFolder_CustomLevels,
	SpecialFolder_Share,
	SpecialFolder_NumFolders,
} SpecialFolder_t;

typedef enum
{
	//NOTE: We put the MouseButtons at the bottom so we can use them
	//		when referencing AppInput mouseStartPos and mouseMaxDist
	MouseButton_Left = 0,
	MouseButton_Right,
	MouseButton_Middle,
	
	Button_A,
	Button_B,
	Button_C,
	Button_D,
	Button_E,
	Button_F,
	Button_G,
	Button_H,
	Button_I,
	Button_J,
	Button_K,
	Button_L,
	Button_M,
	Button_N,
	Button_O,
	Button_P,
	Button_Q,
	Button_R,
	Button_S,
	Button_T,
	Button_U,
	Button_V,
	Button_W,
	Button_X,
	Button_Y,
	Button_Z,
	
	Button_0,
	Button_1,
	Button_2,
	Button_3,
	Button_4,
	Button_5,
	Button_6,
	Button_7,
	Button_8,
	Button_9,
	
	Button_Num0,
	Button_Num1,
	Button_Num2,
	Button_Num3,
	Button_Num4,
	Button_Num5,
	Button_Num6,
	Button_Num7,
	Button_Num8,
	Button_Num9,
	
	Button_NumPeriod,
	Button_NumDivide,
	Button_NumMultiply,
	Button_NumSubtract,
	Button_NumAdd,
	
	Button_F1,
	Button_F2,
	Button_F3,
	Button_F4,
	Button_F5,
	Button_F6,
	Button_F7,
	Button_F8,
	Button_F9,
	Button_F10,
	Button_F11,
	Button_F12,
	
	Button_Enter,
	Button_Backspace,
	Button_Escape,
	Button_Insert,
	Button_Delete,
	Button_Home,
	Button_End,
	Button_PageUp,
	Button_PageDown,
	Button_Tab,
	Button_CapsLock,
	Button_NumLock,
	
	Button_Control,
	Button_Alt,
	Button_Shift,
	
	Button_Right,
	Button_Left,
	Button_Up,
	Button_Down,
	
	Button_Plus,
	Button_Minus,
	Button_Pipe,
	Button_OpenBracket,
	Button_CloseBracket,
	Button_Colon,
	Button_Quote,
	Button_Comma,
	Button_Period,
	Button_QuestionMark,
	Button_Tilde,
	Button_Space,
	
	Buttons_NumButtons,
} Buttons_t;

typedef enum
{
	Gamepad_A,
	Gamepad_B,
	Gamepad_X,
	Gamepad_Y,
	
	Gamepad_Right,
	Gamepad_Left,
	Gamepad_Up,
	Gamepad_Down,
	
	Gamepad_LeftBumper,
	Gamepad_RightBumper,
	Gamepad_LeftTrigger,
	Gamepad_RightTrigger,
	Gamepad_LeftStick,
	Gamepad_RightStick,
	
	Gamepad_Start,
	Gamepad_Back,
	Gamepad_Home,
	
	Gamepad_lsRight,
	Gamepad_lsLeft,
	Gamepad_lsUp,
	Gamepad_lsDown,
	Gamepad_rsRight,
	Gamepad_rsLeft,
	Gamepad_rsUp,
	Gamepad_rsDown,
	
	Gamepad_NumButtons,
} GamepadButtons_t;

inline const char* GetButtonName(Buttons_t button)
{
	switch (button)
	{
		case Button_A:            return "A";
		case Button_B:            return "B";
		case Button_C:            return "C";
		case Button_D:            return "D";
		case Button_E:            return "E";
		case Button_F:            return "F";
		case Button_G:            return "G";
		case Button_H:            return "H";
		case Button_I:            return "I";
		case Button_J:            return "J";
		case Button_K:            return "K";
		case Button_L:            return "L";
		case Button_M:            return "M";
		case Button_N:            return "N";
		case Button_O:            return "O";
		case Button_P:            return "P";
		case Button_Q:            return "Q";
		case Button_R:            return "R";
		case Button_S:            return "S";
		case Button_T:            return "T";
		case Button_U:            return "U";
		case Button_V:            return "V";
		case Button_W:            return "W";
		case Button_X:            return "X";
		case Button_Y:            return "Y";
		case Button_Z:            return "Z";
		case Button_0:            return "0";
		case Button_1:            return "1";
		case Button_2:            return "2";
		case Button_3:            return "3";
		case Button_4:            return "4";
		case Button_5:            return "5";
		case Button_6:            return "6";
		case Button_7:            return "7";
		case Button_8:            return "8";
		case Button_9:            return "9";
		case Button_Num0:         return "Num 0";
		case Button_Num1:         return "Num 1";
		case Button_Num2:         return "Num 2";
		case Button_Num3:         return "Num 3";
		case Button_Num4:         return "Num 4";
		case Button_Num5:         return "Num 5";
		case Button_Num6:         return "Num 6";
		case Button_Num7:         return "Num 7";
		case Button_Num8:         return "Num 8";
		case Button_Num9:         return "Num 9";
		case Button_NumPeriod:    return "Num Period";
		case Button_NumDivide:    return "Num Divide";
		case Button_NumMultiply:  return "Num Multiply";
		case Button_NumSubtract:  return "Num Subtract";
		case Button_NumAdd:       return "Num Add";
		case Button_F1:           return "F1";
		case Button_F2:           return "F2";
		case Button_F3:           return "F3";
		case Button_F4:           return "F4";
		case Button_F5:           return "F5";
		case Button_F6:           return "F6";
		case Button_F7:           return "F7";
		case Button_F8:           return "F8";
		case Button_F9:           return "F9";
		case Button_F10:          return "F10";
		case Button_F11:          return "F11";
		case Button_F12:          return "F12";
		case Button_Enter:        return "Enter";
		case Button_Backspace:    return "Backspace";
		case Button_Escape:       return "Escape";
		case Button_Insert:       return "Insert";
		case Button_Delete:       return "Delete";
		case Button_Home:         return "Home";
		case Button_End:          return "End";
		case Button_PageUp:       return "PageUp";
		case Button_PageDown:     return "PageDown";
		case Button_Tab:          return "Tab";
		case Button_CapsLock:     return "Caps Lock";
		case Button_NumLock:      return "Num Lock";
		case Button_Control:      return "Control";
		case Button_Alt:          return "Alt";
		case Button_Shift:        return "Shift";
		case Button_Right:        return "Right";
		case Button_Left:         return "Left";
		case Button_Up:           return "Up";
		case Button_Down:         return "Down";
		case Button_Plus:         return "Plus";
		case Button_Minus:        return "Minus";
		case Button_Pipe:         return "Pipe";
		case Button_OpenBracket:  return "Open Bracket";
		case Button_CloseBracket: return "Close Bracket";
		case Button_Colon:        return "Colon";
		case Button_Quote:        return "Quote";
		case Button_Comma:        return "Comma";
		case Button_Period:       return "Period";
		case Button_QuestionMark: return "Question Mark";
		case Button_Tilde:        return "Tilde";
		case Button_Space:        return "Space";
		case MouseButton_Left:    return "Mouse Left";
		case MouseButton_Right:   return "Mouse Right";
		case MouseButton_Middle:  return "Mouse Middle";
		case Buttons_NumButtons:  return "None";
		default:                  return "Unknown";
	};
}

inline const char* GetGamepadButtonName(GamepadButtons_t button)
{
	switch (button)
	{
		case Gamepad_A:            return "A";
		case Gamepad_B:            return "B";
		case Gamepad_X:            return "X";
		case Gamepad_Y:            return "Y";
		case Gamepad_Right:        return "Right";
		case Gamepad_Left:         return "Left";
		case Gamepad_Up:           return "Up";
		case Gamepad_Down:         return "Down";
		case Gamepad_LeftBumper:   return "Left Bumper";
		case Gamepad_RightBumper:  return "Right Bumper";
		case Gamepad_LeftTrigger:  return "Left Trigger";
		case Gamepad_RightTrigger: return "Right Trigger";
		case Gamepad_LeftStick:    return "Left Stick";
		case Gamepad_RightStick:   return "Right Stick";
		case Gamepad_Start:        return "Start";
		case Gamepad_Back:         return "Back";
		case Gamepad_Home:         return "Home";
		case Gamepad_lsRight:      return "Left Stick Right";
		case Gamepad_lsLeft:       return "Left Stick Left";
		case Gamepad_lsUp:         return "Left Stick Up";
		case Gamepad_lsDown:       return "Left Stick Down";
		case Gamepad_rsRight:      return "Right Stick Right";
		case Gamepad_rsLeft:       return "Right Stick Left";
		case Gamepad_rsUp:         return "Right Stick Up";
		case Gamepad_rsDown:       return "Right Stick Down";
		default:                  return "Unknown";
	};
}

inline Buttons_t GetButtonForChar(char c)
{
	switch (c)
	{
		case 'a':  case 'A':  return Button_A;
		case 'b':  case 'B':  return Button_B;
		case 'c':  case 'C':  return Button_C;
		case 'd':  case 'D':  return Button_D;
		case 'e':  case 'E':  return Button_E;
		case 'f':  case 'F':  return Button_F;
		case 'g':  case 'G':  return Button_G;
		case 'h':  case 'H':  return Button_H;
		case 'i':  case 'I':  return Button_I;
		case 'j':  case 'J':  return Button_J;
		case 'k':  case 'K':  return Button_K;
		case 'l':  case 'L':  return Button_L;
		case 'm':  case 'M':  return Button_M;
		case 'n':  case 'N':  return Button_N;
		case 'o':  case 'O':  return Button_O;
		case 'p':  case 'P':  return Button_P;
		case 'q':  case 'Q':  return Button_Q;
		case 'r':  case 'R':  return Button_R;
		case 's':  case 'S':  return Button_S;
		case 't':  case 'T':  return Button_T;
		case 'u':  case 'U':  return Button_U;
		case 'v':  case 'V':  return Button_V;
		case 'w':  case 'W':  return Button_W;
		case 'x':  case 'X':  return Button_X;
		case 'y':  case 'Y':  return Button_Y;
		case 'z':  case 'Z':  return Button_Z;
		case '0':  case ')':  return Button_0;
		case '1':  case '!':  return Button_1;
		case '2':  case '@':  return Button_2;
		case '3':  case '#':  return Button_3;
		case '4':  case '$':  return Button_4;
		case '5':  case '%':  return Button_5;
		case '6':  case '^':  return Button_6;
		case '7':  case '&':  return Button_7;
		case '8':  case '*':  return Button_8;
		case '9':  case '(':  return Button_9;
		case '`':  case '~':  return Button_Tilde;
		case '-':  case '_':  return Button_Minus;
		case '+':  case '=':  return Button_Plus;
		case '{':  case '[':  return Button_OpenBracket;
		case ']':  case '}':  return Button_CloseBracket;
		case '\\': case '|':  return Button_Pipe;
		case ';':  case ':':  return Button_Colon;
		case '\'': case '"':  return Button_Quote;
		case ',':  case '<':  return Button_Comma;
		case '.':  case '>':  return Button_Period;
		case '/':  case '?':  return Button_QuestionMark;
		case ' ':             return Button_Space;
		case '\t':            return Button_Tab;
		case '\b':            return Button_Backspace;
		case '\n': case '\r': return Button_Enter;
		default: return Buttons_NumButtons;
	}
}

enum
{
	Modifier_Ctrl     = 0x01,
	Modifier_Alt      = 0x02,
	Modifier_Shift    = 0x04,
	Modifier_CapsLock = 0x08,
	
	Modifier_None     = 0x00,
	Modifier_All      = 0x0F,
	
	Modifier_CtrlAlt = (Modifier_Ctrl|Modifier_Alt),
	Modifier_CtrlShift = (Modifier_Ctrl|Modifier_Shift),
	Modifier_AltShift = (Modifier_Alt|Modifier_Shift),
	Modifier_CtrlAltShift = (Modifier_Ctrl|Modifier_Alt|Modifier_Shift),
};

struct ButtonState_t
{
	u32  transCount;//Transition count since last frame
	u32 repeatCount;//Num of times a button repeat event was received
	bool isDown;//Whether or not the button ended down on this frame
	r32 lastTransTime;//Number of milliseconds since the button last transitioned
};

#define GAMEPAD_STICK_DEAD_ZONE 0.25f
#define GAMEPAD_STICK_DIRECTION_DEAD_ZONE 0.6f
struct GamepadState_t
{
	char* name;
	bool isPlaystation;
	
	bool wasConnected;
	bool isConnected;
	
	ButtonState_t buttons[Gamepad_NumButtons];
	
	r32 leftTrigger;
	r32 rightTrigger;
	v2 leftStick;
	v2 rightStick;
	v2 leftStickRaw;
	v2 rightStickRaw;
	v2 dPad;
};

typedef enum
{
	InputEventType_ButtonTransition = 0,
	InputEventType_GamepadTransition,
	InputEventType_MouseMoved,
	InputEventType_MouseScrolled,
	InputEventType_FileChanged,
	InputEventType_AudioDevicesChanged,
	InputEventType_PrimaryAudioDeviceChanged,
	InputEventType_AudioReset,
	InputEventType_AudioShutdown,
	InputEventType_NumTypes,
} InputEventType_t;

typedef enum
{
	BtnTransFlag_Pressed  = 0x01,
	BtnTransFlag_Repeat   = 0x02,
	BtnTransFlag_Char     = 0x04,
	BtnTransFlag_Ctrl     = 0x08,
	BtnTransFlag_Alt      = 0x10,
	BtnTransFlag_Shift    = 0x20,
	BtnTransFlag_Super    = 0x40,
	BtnTransFlag_CapsLock = 0x80,
	
	BtnTransFlag_Modifiers = (BtnTransFlag_Ctrl|BtnTransFlag_Alt|BtnTransFlag_Shift|BtnTransFlag_Super)
} BtnTransFlag_t;

typedef enum
{
	GamepadTransFlag_Pressed = 0x01,
	GamepadTransFlag_Repeat  = 0x02,
} GamepadTransFlag_t;

//Only some of the data in this stucture matters at a time (depending on the type)
struct InputEvent_t
{
	InputEventType_t type;
	u8 flags;
	
	Buttons_t button;
	char character;
	
	GamepadButtons_t gamepadBtn;
	u32 gamepadIndex;
	
	v2 mousePos;
	v2 oldMousePos;
	
	v2 scrollDelta;
	
	char* filePath;
};

char GetButtonChar(Buttons_t button, bool shift = false, bool capsLock = false)
{
	bool capitalize = (shift ^ capsLock);
	char result = '\0';
	switch (button)
	{
		case Button_A:            result = (capitalize ? 'A' : 'a'); break;
		case Button_B:            result = (capitalize ? 'B' : 'b'); break;
		case Button_C:            result = (capitalize ? 'C' : 'c'); break;
		case Button_D:            result = (capitalize ? 'D' : 'd'); break;
		case Button_E:            result = (capitalize ? 'E' : 'e'); break;
		case Button_F:            result = (capitalize ? 'F' : 'f'); break;
		case Button_G:            result = (capitalize ? 'G' : 'g'); break;
		case Button_H:            result = (capitalize ? 'H' : 'h'); break;
		case Button_I:            result = (capitalize ? 'I' : 'i'); break;
		case Button_J:            result = (capitalize ? 'J' : 'j'); break;
		case Button_K:            result = (capitalize ? 'K' : 'k'); break;
		case Button_L:            result = (capitalize ? 'L' : 'l'); break;
		case Button_M:            result = (capitalize ? 'M' : 'm'); break;
		case Button_N:            result = (capitalize ? 'N' : 'n'); break;
		case Button_O:            result = (capitalize ? 'O' : 'o'); break;
		case Button_P:            result = (capitalize ? 'P' : 'p'); break;
		case Button_Q:            result = (capitalize ? 'Q' : 'q'); break;
		case Button_R:            result = (capitalize ? 'R' : 'r'); break;
		case Button_S:            result = (capitalize ? 'S' : 's'); break;
		case Button_T:            result = (capitalize ? 'T' : 't'); break;
		case Button_U:            result = (capitalize ? 'U' : 'u'); break;
		case Button_V:            result = (capitalize ? 'V' : 'v'); break;
		case Button_W:            result = (capitalize ? 'W' : 'w'); break;
		case Button_X:            result = (capitalize ? 'X' : 'x'); break;
		case Button_Y:            result = (capitalize ? 'Y' : 'y'); break;
		case Button_Z:            result = (capitalize ? 'Z' : 'z'); break;
		case Button_1:            result = (shift ? '!' : '1'); break;
		case Button_2:            result = (shift ? '@' : '2'); break;
		case Button_3:            result = (shift ? '#' : '3'); break;
		case Button_4:            result = (shift ? '$' : '4'); break;
		case Button_5:            result = (shift ? '%' : '5'); break;
		case Button_6:            result = (shift ? '^' : '6'); break;
		case Button_7:            result = (shift ? '&' : '7'); break;
		case Button_8:            result = (shift ? '*' : '8'); break;
		case Button_9:            result = (shift ? '(' : '9'); break;
		case Button_0:            result = (shift ? ')' : '0'); break;
		case Button_Num1:         result = '1'; break;
		case Button_Num2:         result = '2'; break;
		case Button_Num3:         result = '3'; break;
		case Button_Num4:         result = '4'; break;
		case Button_Num5:         result = '5'; break;
		case Button_Num6:         result = '6'; break;
		case Button_Num7:         result = '7'; break;
		case Button_Num8:         result = '8'; break;
		case Button_Num9:         result = '9'; break;
		case Button_Num0:         result = '0'; break;
		case Button_NumPeriod:    result = '.'; break;
		case Button_NumDivide:    result = '/'; break;
		case Button_NumMultiply:  result = '*'; break;
		case Button_NumSubtract:  result = '-'; break;
		case Button_NumAdd:       result = '+'; break;
		case Button_Enter:        result = '\n'; break;
		case Button_Backspace:    result = '\b'; break;
		case Button_Tab:          result = '\t'; break;
		case Button_Plus:         result = (shift ? '+' : '='); break;
		case Button_Minus:        result = (shift ? '_' : '-'); break;
		case Button_Pipe:         result = (shift ? '|' : '\\'); break;
		case Button_OpenBracket:  result = (shift ? '{' : '['); break;
		case Button_CloseBracket: result = (shift ? '}' : ']'); break;
		case Button_Colon:        result = (shift ? ':' : ';'); break;
		case Button_Quote:        result = (shift ? '"' : '\''); break;
		case Button_Comma:        result = (shift ? '<' : ','); break;
		case Button_Period:       result = (shift ? '>' : '.'); break;
		case Button_QuestionMark: result = (shift ? '?' : '/'); break;
		case Button_Tilde:        result = (shift ? '~' : '`'); break;
		case Button_Space:        result = ' '; break;
	};
	return result;
}

const char* GetSpecialFolderStr(SpecialFolder_t specialFolder)
{
	switch (specialFolder)
	{
		case SpecialFolder_Saves:        return "Saves";
		case SpecialFolder_Settings:     return "Settings";
		case SpecialFolder_Screenshots:  return "Screenshots";
		case SpecialFolder_CustomLevels: return "Custom Levels";
		case SpecialFolder_Share:        return "Share";
		default: return "Unknown";
	};
}

struct PlatDebugLine_t
{
	DbgLevel_t level;
	u32 lineNumber;
	
	u32 functionNameLength;
	char* functionNamePntr;
	u32 fileNameLength;
	char* fileNamePntr;
	u32 lineLength;
	char* linePntr;
};

#define MAX_TASK_THREADS 8
