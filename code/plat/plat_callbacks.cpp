/*
File:   plat_callbacks.cpp
Author: Taylor Robbins
Date:   11\04\2017
Description: 
	** Contains all the callback functions that we register with GLFW 
*/

void GlfwErrorCallback(i32 errorCode, const char* description)
{
	PrintLine_E("GlfwErrorCallback: %X \"%s\"", errorCode, description);
	if (platform.glfwLastErrorStr != nullptr) { free(platform.glfwLastErrorStr); platform.glfwLastErrorStr = nullptr; }
	platform.glfwLastErrorCode = errorCode;
	if (description != nullptr)
	{
		u32 descriptionLength = MyStrLength32(description);
		platform.glfwLastErrorStr = (char*)malloc(descriptionLength+1);
		MyMemCopy(platform.glfwLastErrorStr, description, descriptionLength);
		platform.glfwLastErrorStr[descriptionLength] = '\0';
	}
}

void GlfwWindowCloseCallback(GLFWwindow* window)
{
	WriteLine_W("Window Closing");
	//TODO: Should we dissallow the close in some scenarios?
	// glfwSetWindowShouldClose(window, false);
}

void GlfwWindowSizeCallback(GLFWwindow* window, i32 screenWidth, i32 screenHeight)
{
	Assert(platform.currentInput != nullptr);
	platform.screenSize = NewVec2i(screenWidth, screenHeight);
	if (platform.currentInput->screenSize != platform.screenSize)
	{
		platform.currentInput->screenSize = platform.screenSize;
		platform.currentInput->windowResized = true;
		platform.doingManualUpdateLoopWait = false;
	}
	platform.currentInput->windowInteractionHappened = true;
	// WriteLine_D("WindowSizeCallback");
}

void GlfwWindowMoveCallback(GLFWwindow* window, i32 posX, i32 posY)
{
	Assert(platform.currentInput != nullptr);
	platform.windowPosition = NewVec2i(posX, posY);
	if (platform.currentInput->windowPosition != platform.windowPosition)
	{
		platform.currentInput->windowPosition = platform.windowPosition;
		platform.currentInput->windowMoved = true;
	}
	platform.currentInput->windowInteractionHappened = true;
	// WriteLine_D("WindowMoveCallback");
}

void GlfwWindowMinimizeCallback(GLFWwindow* window, i32 isMinimized)
{
	Assert(platform.currentInput != nullptr);
	bool newMinimizedState = (isMinimized > 0);
	if (platform.currentInput->windowIsMinimized != newMinimizedState)
	{
		platform.currentInput->windowIsMinimized = newMinimizedState;
		platform.currentInput->windowMinimizedChanged = true;
	}
}

void GlfwWindowFocusCallback(GLFWwindow* window, i32 isFocused)
{
	Assert(platform.currentInput != nullptr);
	bool newFocusState = (isFocused > 0);
	if (platform.currentInput->windowHasFocus != newFocusState)
	{
		platform.currentInput->windowHasFocus = (isFocused > 0);
		platform.currentInput->windowFocusChanged = true;
	}
}

void GlfwKeyPressedCallback(GLFWwindow* window, i32 key, i32 scanCode, i32 action, i32 modifiers)
{
	Assert(platform.currentInput != nullptr);
	Plat_HandleKeyEvent(window, platform.currentInput, key, action, modifiers);
}

void GlfwCharPressedCallback(GLFWwindow* window, u32 codepoint)
{
	// PrintLine_D("Text Input: 0x%X", codepoint);
	Assert(platform.currentInput != nullptr);
	if (platform.currentInput->textInputLength < ArrayCount(platform.currentInput->textInput))
	{
		platform.currentInput->textInput[platform.currentInput->textInputLength] = (u8)codepoint;
		platform.currentInput->textInputLength++;
	}
}

void GlfwCursorPosCallback(GLFWwindow* window, r64 mouseX, r64 mouseY)
{
	Assert(platform.currentInput != nullptr);
	
	v2 newMousePos = NewVec2((r32)mouseX, (r32)mouseY);
	#if DOUBLE_MOUSE_POS
	newMousePos = newMousePos * 2;
	#endif
	
	if (platform.currentInput->mousePos != newMousePos)
	{
		platform.currentInput->mousePos = newMousePos;
		platform.currentInput->mouseMoved = true;
		
		if (platform.currentInput->buttons[MouseButton_Left].isDown)
		{
			r32 distance = Vec2Length(newMousePos - platform.currentInput->mouseStartPos[MouseButton_Left]);
			if (distance > platform.currentInput->mouseMaxDist[MouseButton_Left])
			{
				platform.currentInput->mouseMaxDist[MouseButton_Left] = distance;
			}
		}
		if (platform.currentInput->buttons[MouseButton_Right].isDown)
		{
			r32 distance = Vec2Length(newMousePos - platform.currentInput->mouseStartPos[MouseButton_Right]);
			if (distance > platform.currentInput->mouseMaxDist[MouseButton_Right])
			{
				platform.currentInput->mouseMaxDist[MouseButton_Right] = distance;
			}
		}
		if (platform.currentInput->buttons[MouseButton_Middle].isDown)
		{
			r32 distance = Vec2Length(newMousePos - platform.currentInput->mouseStartPos[MouseButton_Middle]);
			if (distance > platform.currentInput->mouseMaxDist[MouseButton_Middle])
			{
				platform.currentInput->mouseMaxDist[MouseButton_Middle] = distance;
			}
		}
	}
}

void GlfwMousePressCallback(GLFWwindow* window, i32 button, i32 action, i32 modifiers)
{
	Assert(platform.currentInput != nullptr);
	Plat_HandleMouseEvent(window, platform.currentInput, button, action, modifiers);
}

void GlfwMouseScrollCallback(GLFWwindow* window, r64 deltaX, r64 deltaY)
{
	Assert(platform.currentInput != nullptr);
	platform.currentInput->scrollValue += NewVec2((r32)deltaX, (r32)deltaY);
	platform.currentInput->scrollDelta += NewVec2((r32)deltaX, (r32)deltaY);
	if (deltaX != 0 || deltaY != 0)
	{
		platform.currentInput->scrollChanged = true;
	}
}

void GlfwCursorEnteredCallback(GLFWwindow* window, i32 entered)
{
	Assert(platform.currentInput != nullptr);
	bool newEnteredState = (entered > 0);
	if (platform.currentInput->mouseInsideWindow != newEnteredState)
	{
		platform.currentInput->mouseInsideWindow = newEnteredState;
		platform.currentInput->mouseInsideChanged = true;
	}
}

void GlfwFileDropCallback(GLFWwindow* window, int count, const char** filePaths)
{
	Assert(window != nullptr);
	Assert(count > 0);
	Assert(filePaths != nullptr);
	for (u32 fIndex = 0; fIndex < (u32)count; fIndex++)
	{
		const char* filePath = filePaths[fIndex];
		Assert(filePath != nullptr);
		char* storedPath = ArenaNtString(&platform.mainHeap, filePath);
		Assert(storedPath != nullptr);
		char** arraySpace = DynArrayAdd(&platform.droppedFilePaths, char*);
		Assert(arraySpace != nullptr);
		*arraySpace = storedPath;
	}
}

#if WINDOWS_COMPILATION
void GlfwSystemEventCallback(GLFWwindow* window, unsigned int uMsg, __int64 wParam, __int64 lParam)
{
	if (IsThisMainThread())
	{
		#if 0
		if (platform.startupFinished &&
			uMsg != WM_SETCURSOR && uMsg != WM_MOUSEACTIVATE && uMsg != WM_MOUSEMOVE && uMsg != WM_MOUSEWHEEL &&
			uMsg != WM_NCMOUSEMOVE && uMsg != WM_MOUSELEAVE && uMsg != WM_NCHITTEST && uMsg != WM_PAINT &&
			uMsg != WM_KEYDOWN && uMsg != WM_KEYUP && uMsg != WM_ACTIVATE && uMsg != WM_ACTIVATEAPP &&
			uMsg != WM_KILLFOCUS && uMsg != WM_SETFOCUS && uMsg != WM_IME_NOTIFY && uMsg != WM_IME_SETCONTEXT &&
			uMsg != WM_WINDOWPOSCHANGING && uMsg != WM_WINDOWPOSCHANGED && uMsg != WM_MOVE && uMsg != WM_MOVING &&
			uMsg != WM_GETMINMAXINFO && uMsg != WM_EXITSIZEMOVE && uMsg != WM_ERASEBKGND && uMsg != WM_NCPAINT &&
			uMsg != WM_NCACTIVATE && uMsg != WM_LBUTTONDOWN && uMsg != WM_LBUTTONUP && uMsg != WM_CAPTURECHANGED &&
			uMsg != WM_NCMOUSELEAVE && uMsg != WM_ENTERSIZEMOVE && uMsg != WM_SIZE && uMsg != WM_NCLBUTTONDOWN &&
			uMsg != WM_NCCALCSIZE && uMsg != WM_CHAR)
		{
			PrintLine_D("Got event %s (%u) %lld %lld", Win32_GetWindowsMsgStr(uMsg), uMsg, wParam, lParam);
		}
		#endif
		switch (uMsg)
		{
			case WM_DEVICECHANGE:
			{
				#if 0//AUDIO_ENABLED && USE_CUSTOM_AUDIO
				Assert_(IsThisMainThread());
				// PrintLine_D("Got event %s (%u) %lld %lld", Win32_GetWindowsMsgStr(uMsg), uMsg, wParam, lParam);
				if (wParam == DBT_CONFIGCHANGED || wParam == DBT_DEVICEARRIVAL || wParam == DBT_DEVICEREMOVECOMPLETE || wParam == DBT_DEVNODES_CHANGED)
				{
					Win32_HandleAudioDevicesChanged(&platform.info.audioInfo);
				}
				#endif
			} break;
			
			case WM_PAINT:
			case WM_MOVE:
			case WM_SIZE:
			{
				if (platform.startupFinished)
				{
					DoUpdateLoopIteration(true);
				}
			} break;
		}
	}
}
#endif

void GlfwDebugOutputCallback(const char* filePath, u32 lineNumber, const char* funcName, bool isError, bool newLine, const char* message)
{
	_Win32_DebugOutput(DbgFlag_ExternalLib, filePath, lineNumber, funcName, isError ? DbgLevel_Error : DbgLevel_Info, newLine, message);
}
