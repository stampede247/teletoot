/*
File:   game.h
Author: Taylor Robbins
Date:   11\17\2017
*/

#ifndef _GAME_H
#define _GAME_H

#include "game_defines.h"

#include "game_structs.h"
#include "game_diagonal_directions.h"
#include "game_corners.h"

#include "game/game_tile.h"
#include "game/game_entity.h"
#include "game/game_level.h"
#include "game/game_entity_lookup.h"

#include "game/game_morse.h"
#include "game/game_toot_level.h"
#include "game/game_view.h"

struct GameData_t
{
	bool initialized;
	bool activatedWipe;
	AppState_t wipeDoneState;
	
	MorseInput_t currentMorse;
	r32 hudScale;
	rec hudRec;
	rec hudPrimaryBtnRec;
	rec hudSecondaryBtnRec;
	rec hudPrimaryFurnaceRec;
	rec hudSecondaryFurnaceRec;
	rec hudEnterBtnRec;
	rec hudInputRec;
	rec hudStackRec;
	rec hudConductorRec;
	r32 conductorAnimTime;
	rec hudShovelerRec;
	TootLevel_t level;
	View_t view;
	
	bool levelComplete;
	r32 completionAnimTime;
	r32 completionMenuOpenAnim;
	r32 completionMenuScale;
	rec completionMenuRec;
	rec nextLevelBtnRec;
	
	PlayingMorse_t playingMorse;
	
	u32 currentLevelNumber;
	char* overrideLevelPath;
	
	bool showingMessage;
	char* messageStr;
	r32 messageOpenAnim;
	r32 messageScale;
	rec messageRec;
	rec messageOkayBtn;
	
	r32 derailedMessageAnim;
	rec derailedMessageRec;
	
	bool showingWinGame;
	rec winGameArtRec;
	
	r32 gameTimer;
};

#endif //  _GAME_H
