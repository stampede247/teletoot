/*
File:   game_train.h
Author: Taylor Robbins
Date:   07\10\2020
*/

#ifndef _GAME_TRAIN_H
#define _GAME_TRAIN_H

struct Train_t
{
	v2i tilePos;
	Dir2_t direction;
	r32 chugSpeed;
	r32 chugTime;
	bool isPrimaryChug;
	r32 movePercent;
	Dir2_t fromDir;
	v2i fromTilePos;
	bool usedThisPlume;
	bool plumeIsPrimary;
	bool plumeIsSuccess;
	bool plumeIsFailure;
	bool usedLastPlume;
	bool plumeWasPrimary;
	bool plumeWasSuccess;
	bool plumeWasFailure;
	bool isDead;
	bool reachedEnd;
	r32 deadAnimPercent;
	bool shovelerThrowing;
	bool shovelerPrimary;
	r32 shovelerIdleTimer;
	r32 leftFurnaceAnim;
	r32 rightFurnaceAnim;
	v2 drawPos;
};

typedef enum
{
	TrackType_End = 0x00,
	TrackType_Straight,
	TrackType_Corner,
	TrackType_Cross,
	TrackType_BridgeStraight,
	TrackType_BridgeCorner,
	TrackType_BridgeCross,
	TrackType_NumTypes,
} TrackType_t;

struct Track_t
{
	TrackType_t type;
	v2i tilePos;
	Dir2_t rotation;
	r32 creationAnim;
};

typedef enum
{
	GroundType_Error = 0x00,
	GroundType_Grass,
	GroundType_Water,
	GroundType_Forest,
	GroundType_Sand,
	GroundType_NumTypes,
} GroundType_t;

struct Ground_t
{
	GroundType_t type;
	u8 connections;
};

typedef enum
{
	DecorType_Ground = 0x00,
	DecorType_Mountain,
	DecorType_Trees,
	DecorType_EndGoal,
	DecorType_Sheep,
	DecorType_Rock,
	DecorType_House,
	DecorType_NumTypes,
} DecorType_t;

struct Decoration_t
{
	DecorType_t type;
	v2i tilePos;
	bool isScattered;
	v2 scatterVec;
	r32 scatterAnimTime;
	r32 rotation;
};

struct Particle_t
{
	SpriteSheet_t* sheet;
	r32 lifeSpan;
	Color_t startColor;
	Color_t endColor;
	PalColor_t palColorStart;
	PalColor_t palColorEnd;
	v2 position;
	v2 velocity;
	v2 acceleration;
	r32 age;
};

struct TootLevel_t
{
	MemoryArena_t* allocArena;
	Train_t train;
	union
	{
		v2i size;
		struct { i32 width, height; };
	};
	Ground_t* grounds;
	DynArray_t tracks;
	DynArray_t decorations;
	v2i placingPos;
	Dir2_t placingDir;
	bool trackComplete;
	
	r32 failedTrackAnimTime;
	Track_t failedTrack;
	v2i failedBlockPos;
	
	u8 songNumber;
	bool hideUndo;
	bool hideCornerLeft;
	bool hideCornerRight;
	bool hideHorn;
	bool hideBridgeStraight;
	bool hideBridgeLeft;
	bool hideBridgeRight;
};

#endif //  _GAME_TRAIN_H
