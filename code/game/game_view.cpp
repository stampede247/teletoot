/*
File:   game_view.cpp
Author: Taylor Robbins
Date:   07\11\2020
Description: 
	** Handles moving the view the follow the train around the level
*/

void InitView(View_t* view, v2 position)
{
	Assert(view != nullptr);
	ClearPointer(view);
	view->position = position;
	view->positionGoto = position;
}

void UpdateView(View_t* view, TootLevel_t* level, r32 levelScale)
{
	Assert(view != nullptr);
	if (Vec2Length(view->positionGoto - view->position) >= 0.1f)
	{
		view->position += (view->positionGoto - view->position) / VIEW_FOLLOW_LAG;
	}
	else
	{
		view->position = view->positionGoto;
	}
	if (view->position.x < 0 + RenderScreenSize.width/2) { view->position.x = 0 + RenderScreenSize.width/2; }
	if (view->position.x > level->width*32.0f*levelScale - RenderScreenSize.width/2) { view->position.x = level->width*32.0f*levelScale - RenderScreenSize.width/2; }
	if (view->position.y < 0 + RenderScreenSize.height/2) { view->position.y = 0 + RenderScreenSize.height/2; }
	if (view->position.y > level->height*32.0f*levelScale - RenderScreenSize.height/2) { view->position.y = level->height*32.0f*levelScale - RenderScreenSize.height/2; }
}
