/*
File:   game_view.h
Author: Taylor Robbins
Date:   07\11\2020
*/

#ifndef _GAME_VIEW_H
#define _GAME_VIEW_H

struct View_t
{
	v2 position;
	v2 positionGoto;
};

#endif //  _GAME_VIEW_H
