/*
File:   game.cpp
Author: Taylor Robbins
Date:   11\17\2017
Description: 
	** Holds all the main gameplay code 
*/

#include "game/game_func_defs.h"
#include "game/game_helpers.cpp"

#include "game/game_level.cpp"
#include "game/game_level_design_serialization_text.cpp"
#include "game/game_entity_lookup.cpp"

#include "game/game_morse.cpp"
#include "game/game_toot_level.cpp"
#include "game/game_view.cpp"
#include "game/game_toot_level_update.cpp"
#include "game/game_toot_level_render.cpp"

#include "game/game_functions.cpp"

// +--------------------------------------------------------------+
// |                       Initialize Game                        |
// +--------------------------------------------------------------+
void InitializeGame()
{
	Assert(game != nullptr);
	
	CreateMorseInput(&game->currentMorse);
	bool loadLevelSuccess = GameLoadLevel();
	Assert(loadLevelSuccess);
	
	game->initialized = true;
}

// +--------------------------------------------------------------+
// |                          Start Game                          |
// +--------------------------------------------------------------+
bool StartGame(AppState_t oldAppState, TransInfoType_t transInfoType, void* transInfoPntr)
{
	Assert(game->initialized);
	SoftQueueMusicChange(nullptr);
	
	if (game->currentLevelNumber == 0)
	{
		GameShowMessage(FIRST_MESSAGE);
	}
	
	return true;
}

// +--------------------------------------------------------------+
// |                      Deinitialize Game                       |
// +--------------------------------------------------------------+
void DeinitializeGame()
{
	Assert(game != nullptr);
	Assert(game->initialized == true);
	StopAllLoopingSounds();
	SoftQueueMusicChange(GetMusic(stayOnTrack1));
	
	DestroyMorseInput(&game->currentMorse);
	
	game->initialized = false;
	ClearPointer(game);
}

// +--------------------------------------------------------------+
// |                       Does Cover Below                       |
// +--------------------------------------------------------------+
bool DoesGameCoverBelow(AppState_t belowState)
{
	return true;
}

// +--------------------------------------------------------------+
// |                         Game Update                          |
// +--------------------------------------------------------------+
void UpdateGame(AppMenu_t appMenuAbove)
{
	Assert(game != nullptr);
	Assert(game->initialized);
	
	r32 timeScale = (ElapsedMs / (1000.0f / 60.0f));
	
	if (!game->showingWinGame)
	{
		game->gameTimer += ElapsedMs;
	}
	
	GameUiUpdateRecs();
	GameUiCaptureMouse();
	
	if (game->currentMorse.pieces.length > 0 && !game->showingMessage && !game->showingWinGame)
	{
		game->currentMorse.duration += ElapsedMs;
	}
	
	if (!game->showingMessage && !game->showingWinGame)
	{
		UpdateTrain(&game->level, &game->level.train);
		UpdateLevel(&game->level);
	}
	
	#if DEVELOPER
	if (ButtonDown(Button_Up))
	{
		HandleButton(Button_Up);
		if (game->level.train.chugSpeed > 16)
		{
			game->level.train.chugSpeed -= 1;
			r32 speedPercent = game->level.train.chugSpeed / DEFAULT_CHUG_SPEED;
			SetSndInstancePitch(app->currentMusic, 1.0f + (1.0f - speedPercent));
		}
	}
	if (ButtonDown(Button_Down))
	{
		HandleButton(Button_Down);
		if (game->level.train.chugSpeed < DEFAULT_CHUG_SPEED*2)
		{
			game->level.train.chugSpeed += 1;
			r32 speedPercent = game->level.train.chugSpeed / DEFAULT_CHUG_SPEED;
			SetSndInstancePitch(app->currentMusic, 1.0f + (1.0f - speedPercent));
		}
	}
	#endif
	
	// +==============================+
	// |     Handle Reset Button      |
	// +==============================+
	if (ButtonPressed(Button_R) && !game->showingMessage)
	{
		HandleButton(Button_R);
		if (game->showingWinGame)
		{
			game->currentLevelNumber = 0;
			game->showingWinGame = false;
			game->gameTimer = 0;
		}
		GameLoadLevel();
	}
	
	// +==============================+
	// |  Update Conductor Anim Time  |
	// +==============================+
	if (game->conductorAnimTime >= 0.0f)
	{
		game->conductorAnimTime -= TIME_SCALED_ANIM(CONDUCTOR_ANIM_TIME, timeScale);
		if (game->conductorAnimTime <= 0.0f) { game->conductorAnimTime = 0.0f; }
	}
	
	// +==============================+
	// |      View Follows Train      |
	// +==============================+
	if (!game->level.train.isDead)
	{
		game->view.positionGoto = game->level.train.drawPos + NewVec2(100, 200);
		if (appInput->mouseInsideWindow)
		{
			game->view.positionGoto += (RenderMousePos - RenderScreenSize/2) / 2;
		}
	}
	
	if (/*!game->showingMessage && */!game->showingWinGame) { UpdateView(&game->view, &game->level, LEVEL_RENDER_SCALE); }
	
	// +==============================+
	// |    Update Completion Anim    |
	// +==============================+
	if (game->level.train.reachedEnd && !game->levelComplete)
	{
		game->levelComplete = true;
		game->completionAnimTime = 0.0f;
		SoundEffectGlobal(devonWooHooSound, 1.0f);
	}
	if (game->levelComplete && game->completionAnimTime < 1.0f)
	{
		game->completionAnimTime += TIME_SCALED_ANIM(COMPLETION_ANIM_TIME, timeScale);
		if (game->completionAnimTime >= 1.0f)
		{
			game->completionAnimTime = 1.0f;
			SoundEffectGlobal(clappingSound, 1.0f);
		}
	}
	if (game->levelComplete && game->completionAnimTime >= 1.0f && game->completionMenuOpenAnim < 1.0f)
	{
		game->completionMenuOpenAnim += TIME_SCALED_ANIM(COMPLETION_MENU_OPEN_TIME, timeScale);
		if (game->completionMenuOpenAnim >= 1.0f) { game->completionAnimTime = 1.0f; }
	}
	
	// +==============================+
	// |   Handle Next Level Button   |
	// +==============================+
	if (game->completionMenuOpenAnim > 0.0f || ButtonPressed(Button_PageUp))
	{
		if (IsMouseOver("CompletionMenuNextLevelBtn")) { appOutput->cursorType = Cursor_Pointer; }
		if (HandleMouseClickedOn(MouseButton_Left, "CompletionMenuNextLevelBtn") || ButtonReleased(Button_Z) || ButtonReleased(Button_X) || ButtonReleased(Button_Enter) || ButtonPressed(Button_PageUp))
		{
			HandleButton(Button_Z);
			HandleButton(Button_X);
			HandleButton(Button_Enter);
			HandleButton(Button_PageUp);
			game->showingMessage = false;
			char* oldOverridePath = game->overrideLevelPath;
			game->overrideLevelPath = nullptr;
			game->currentLevelNumber++;
			bool noLevelFound = false;
			bool loadedNextLevel = GameLoadLevel(&noLevelFound);
			if (loadedNextLevel)
			{
				if (oldOverridePath != nullptr) { ArenaPop(mainHeap, oldOverridePath); }
			}
			else if (noLevelFound)
			{
				game->showingWinGame = true;
				SoundEffectGlobal(kyleWooHooSound, 1.0f);
			}
			else
			{
				NotifyPrint_E("Failed to go to next level! (level%u.lvl)", game->currentLevelNumber);
				game->currentLevelNumber--;
				game->overrideLevelPath = oldOverridePath;
			}
		}
	}
	
	// +==============================+
	// |      Handle Okay Button      |
	// +==============================+
	if (game->showingMessage)
	{
		if (IsMouseOver("MessageWindowOkayBtn")) { appOutput->cursorType = Cursor_Pointer; }
		if (HandleMouseClickedOn(MouseButton_Left, "MessageWindowOkayBtn") || ButtonReleased(Button_Z) || ButtonReleased(Button_X) || ButtonReleased(Button_Enter))
		{
			HandleButton(Button_Z);
			HandleButton(Button_X);
			HandleButton(Button_Enter);
			game->showingMessage = false;
			if (game->currentLevelNumber == 0 && StrCompareIgnoreCase(game->messageStr, FIRST_MESSAGE, 6))
			{
				GameShowMessage(SECOND_MESSAGE);
			}
		}
		HandleButton(Button_Z);
		HandleButton(Button_X);
		HandleButton(Button_Enter);
	}
	
	// +==============================+
	// |     Update Message Anim      |
	// +==============================+
	if (game->showingMessage && game->messageOpenAnim < 1.0f)
	{
		game->messageOpenAnim += TIME_SCALED_ANIM(MESSAGE_OPEN_ANIM_TIME, timeScale);
		if (game->messageOpenAnim >= 1.0f) { game->messageOpenAnim = 1.0f; }
	}
	if (!game->showingMessage && game->messageOpenAnim > 0.0f)
	{
		game->messageOpenAnim -= TIME_SCALED_ANIM(MESSAGE_CLOSE_ANIM_TIME, timeScale);
		if (game->messageOpenAnim <= 0.0f) { game->messageOpenAnim = 0.0f; }
	}
	
	if (game->level.train.isDead && game->derailedMessageAnim < 1.0f)
	{
		game->derailedMessageAnim += TIME_SCALED_ANIM(DERAILED_MESSAGE_TIME, timeScale);
		if (game->derailedMessageAnim >= 1.0f)
		{
			SoundEffectGlobal(derailedSound, 1.0f);
			game->derailedMessageAnim = 1.0f;
		}
	}
	if (!game->level.train.isDead && game->derailedMessageAnim > 0.0f)
	{
		game->derailedMessageAnim = 0.0f;
	}
	
	UpdatePlayingMorse(&game->playingMorse);
}

// +--------------------------------------------------------------+
// |                         Game Render                          |
// +--------------------------------------------------------------+
void RenderGame(FrameBuffer_t* renderBuffer, AppState_t appStateAbove, AppMenu_t appMenuAbove)
{
	GameUiUpdateRecs();
	
	RcBegin(GetShader(tileShader), NewRec(Vec2_Zero, RenderScreenSize), GetFont(pixelFont), renderBuffer);
	RcClear(Black, 1.0f);
	RcSetDepth(1.0f);
	mat4 viewMatrix = Mat4_Identity;
	Mat4Transform(viewMatrix, Mat4Translate2(-game->view.position.x + RenderScreenSize.width/2, -game->view.position.y + RenderScreenSize.height/2));
	RcSetViewMatrix(viewMatrix);
	
	RenderTootLevel(&game->level, Vec2_Zero, LEVEL_RENDER_SCALE);
	
	// +==============================+
	// |       UI Render Setup        |
	// +==============================+
	RcBindShader(GetShader(tileShader));
	RcSetViewport(NewRec(Vec2_Zero, RenderScreenSize));
	RcSetViewMatrix(Mat4_Identity);
	RcSetDepth(0.0f);
	
	// +==============================+
	// |           Draw HUD           |
	// +==============================+
	{
		v2i shovelerFrame = NewVec2i(1, 0);
		if (!game->level.train.shovelerPrimary) { shovelerFrame.x = 3; }
		if (game->level.train.shovelerIdleTimer <= 0) { shovelerFrame.x = 0; }
		else if (game->level.train.shovelerThrowing) { shovelerFrame.x += 1; }
		v2i conductorFrame = NewVec2i(0, 0);
		if (game->conductorAnimTime > 0.0f)
		{
			conductorFrame.x += ClampI32((i32)(game->conductorAnimTime * 4), 0, 4);
		}
		v2i stackFrame = NewVec2i(0, 0);
		stackFrame.x += (i32)(game->level.train.chugTime/game->level.train.chugSpeed * 6);
		SpriteSheet_t* plumeSheet = GetSheet(hudPlumeAnimSheet);
		v2i plumeFrame = stackFrame;
		bool drawPlume = true; //game->level.train.usedLastPlume;
		PalColor_t plumeColor = plt->plume;
		rec plumeRec = game->hudStackRec;
		plumeRec.y -= game->hudStackRec.height*3.0f/4.0f;
		if (game->level.train.usedLastPlume)
		{
			if (game->level.train.plumeWasSuccess)
			{
				// plumeSheet = GetSheet(plumeSuccessAnimSheet);
				plumeColor = plt->plumeSuccess;
			}
			else if (game->level.train.plumeWasFailure)
			{
				// plumeSheet = GetSheet(plumeSuccessAnimSheet);
				plumeColor = plt->plume;
			}
			else
			{
				plumeColor = game->level.train.plumeWasPrimary ? plt->furnaceRed : plt->furnaceBlue;
			}
		}
		PalColor_t furnaceColors = plt->furnaceBoth;
		PalColor_t leftFurnaceFireColor = plt->fireYellow;
		PalColor_t rightFurnaceFireColor = plt->fireBlue;
		v2i leftFurnaceFrame = NewVec2i(0 + (i32)((ProgramTime%700)/100), 0);
		v2i rightFurnaceFrame = NewVec2i(0 + (i32)((ProgramTime%700)/100), 0);
		if (game->level.train.leftFurnaceAnim > 0.7f) { leftFurnaceFrame = NewVec2i(7, 0); }
		else if (game->level.train.leftFurnaceAnim > 0.3f) { leftFurnaceFrame = NewVec2i(8, 0); }
		else if (game->level.train.leftFurnaceAnim > 0.0f) { leftFurnaceFrame = NewVec2i(7, 0); }
		if (game->level.train.rightFurnaceAnim > 0.7f) { rightFurnaceFrame = NewVec2i(7, 0); }
		else if (game->level.train.rightFurnaceAnim > 0.3f) { rightFurnaceFrame = NewVec2i(8, 0); }
		else if (game->level.train.rightFurnaceAnim > 0.0f) { rightFurnaceFrame = NewVec2i(7, 0); }
		if (!game->level.train.isDead && !game->level.train.reachedEnd)
		{
			if (IsMouseOver("HudPrimaryBtn") || ButtonDownNoHandling(Button_Z))
			{
				furnaceColors.dark = furnaceColors.primary4;
				furnaceColors.primary3 = furnaceColors.primary2;
				furnaceColors.primary = furnaceColors.secondary;
			}
			if (IsMouseOver("HudSecondaryBtn") || ButtonDownNoHandling(Button_X))
			{
				furnaceColors.secondary2 = furnaceColors.light;
				furnaceColors.light2 = furnaceColors.light3;
				furnaceColors.light4 = furnaceColors.light5;
			}
		}
		RcBindTexture(GetTexture(hudBackSprite));
		RcSetReplaceColors(furnaceColors);
		RcDrawTexturedRec(game->hudRec, White);
		RcSetReplaceColors(leftFurnaceFireColor);
		RcDrawSheetFrame(GetSheet(furnaceFireSheet), leftFurnaceFrame, game->hudPrimaryFurnaceRec, White);
		RcSetReplaceColors(rightFurnaceFireColor);
		RcDrawSheetFrame(GetSheet(furnaceFireSheet), rightFurnaceFrame, game->hudSecondaryFurnaceRec, White);
		RcDefaultReplaceColors();
		RcDrawSheetFrame(GetSheet(shovelerSheet), shovelerFrame, game->hudShovelerRec, White);
		RcDrawSheetFrame(GetSheet(conductorSheet), conductorFrame, game->hudConductorRec, White);
		RcDrawSheetFrame(GetSheet(hudStackAnimSheet), stackFrame, game->hudStackRec, White);
		RcSetReplaceColors(plumeColor);
		RcDrawSheetFrame(plumeSheet, plumeFrame, plumeRec, White);
		DrawMorseInput(&game->currentMorse, game->hudInputRec, game->hudScale/2, false, true);
		RcBindTexture(GetTexture(hudBackButtonsSprite));
		RcSetReplaceColors(furnaceColors);
		RcDrawTexturedRec(game->hudRec, White);
		// RcPrintStringAligned(game->hudShovelerRec.topLeft + game->hudShovelerRec.size/2, White, 1.0f, Alignment_Center, "%.1f", game->level.train.shovelerIdleTimer);
	}
	
	// +==============================+
	// |       Draw Code Cards        |
	// +==============================+
	r32 cardsScale = 2.0f;
	RcBindFont(GetFont(pixelFont), cardsScale);
	rec cardRec = NewRec(Vec2_Zero, GetTextureSizef(cardBackSprite) * cardsScale);
	for (u32 cIndex = 0; cIndex < NUM_CODE_CARDS; cIndex++)
	{
		const char* codeStr = "";
		const char* codeName = "Unknown";
		v2i iconFrame = Vec2i_Zero;
		Dir2_t iconRotation = game->level.placingDir;
		bool shouldHide = false;
		switch (cIndex)
		{
			case 0: codeStr = UNDO_CODE;                      codeName = "Undo";             iconFrame = NewVec2i(0, 0); shouldHide = game->level.hideUndo;  iconRotation = Dir2_Down; break;
			case 1: codeStr = TRACK_STRAIGHT_CODE;            codeName = "Straight";         iconFrame = NewVec2i(1, 0); shouldHide = false;  break;
			case 2: codeStr = TRACK_CORNER_LEFT_CODE;         codeName = "Turn Left";        iconFrame = NewVec2i(3, 0); shouldHide = game->level.hideCornerLeft;  iconRotation = Dir2Opposite(iconRotation); break;
			case 3: codeStr = TRACK_CORNER_RIGHT_CODE;        codeName = "Turn Right";       iconFrame = NewVec2i(2, 0); shouldHide = game->level.hideCornerRight;  iconRotation = Dir2Opposite(iconRotation); break;
			case 4: codeStr = TRAIN_HORN_BLOW_CODE;           codeName = "Horn";             iconFrame = NewVec2i(7, 0); shouldHide = game->level.hideHorn;  iconRotation = Dir2_Down; break;
			case 5: codeStr = TRACK_BRIDGE_STRAIGHT_CODE;     codeName = "Bridge Straight";  iconFrame = NewVec2i(4, 0); shouldHide = game->level.hideBridgeStraight;  break;
			case 6: codeStr = TRACK_BRIDGE_CORNER_LEFT_CODE;  codeName = "Bridge Left";      iconFrame = NewVec2i(6, 0); shouldHide = game->level.hideBridgeLeft;  iconRotation = Dir2Opposite(iconRotation); break;
			case 7: codeStr = TRACK_BRIDGE_CORNER_RIGHT_CODE; codeName = "Bridge Right";     iconFrame = NewVec2i(5, 0); shouldHide = game->level.hideBridgeRight;  iconRotation = Dir2Opposite(iconRotation); break;
		}
		if (!shouldHide)
		{
			v2 nameStrSize = RcMeasureStringNt(codeName, 1.0f);
			v2 nameStrPos = Vec2Round(NewVec2(cardRec.x + cardRec.width/2 - nameStrSize.width/2, cardRec.y + 4*cardsScale + RcMaxExtendUp()));
			v2 iconSize = GetSheetFrameSizef(trackIcons) * cardsScale;
			rec iconRec = NewRec(cardRec.x + cardRec.width/2 - iconSize.width/2, nameStrPos.y + RcMaxExtendDown(), iconSize.width, iconSize.height);
			v2 codeIconSize = GetTextureSizef(primarySprite);
			u32 codeLength = MyStrLength32(codeStr);
			v2 codeSize = Vec2_Zero;
			codeSize.width = (codeIconSize.width * codeLength) + (1 * cardsScale * (codeLength-1));
			codeSize.height = codeIconSize.height;
			RcBindTexture(GetTexture(cardBackSprite));
			RcDrawTexturedRec(cardRec, White);
			RcDrawSheetFrame(GetSheet(trackIcons), iconFrame, iconRec, White, iconRotation);
			rec codeIconRec = NewRec(cardRec.x + cardRec.width/2 -codeSize.width/2, cardRec.y + cardRec.height - codeSize.height - 4*cardsScale, codeIconSize.width, codeIconSize.height);
			for (u32 pIndex = 0; pIndex < MyStrLength32(codeStr); pIndex++)
			{
				if (codeStr[pIndex] == 'z' || codeStr[pIndex] == 'Z') { RcBindTexture(GetTexture(primarySprite)); }
				else { RcBindTexture(GetTexture(secondarySprite)); }
				RcDrawTexturedRec(codeIconRec, White);
				codeIconRec.x += codeIconRec.width + 1*cardsScale;
			}
			RcDrawStringNt(codeName, nameStrPos, White);
			cardRec.y += cardRec.height;
		}
	}
	
	// +==============================+
	// |     Draw Completion Menu     |
	// +==============================+
	if (game->completionMenuOpenAnim > 0.0f)
	{
		RcBindTexture(GetTexture(completionBackSprite));
		RcDrawTexturedRec(game->completionMenuRec, White);
		
		v2i buttonFrame = NewVec2i(0, 0);
		if (IsMouseClickingOn(MouseButton_Left, "CompletionMenuNextLevelBtn") || ButtonDownNoHandling(Button_Z) || ButtonDownNoHandling(Button_X) || ButtonDownNoHandling(Button_Enter))
		{
			buttonFrame.y += 2;
		}
		else if (IsMouseOver("CompletionMenuNextLevelBtn"))
		{
			buttonFrame.y += 1;
		}
		
		RcDrawSheetFrame(GetSheet(buttonsSheet), buttonFrame, game->nextLevelBtnRec, White);
	}
	
	// +==============================+
	// |         Draw Message         |
	// +==============================+
	RcBindFont(GetFont(pixelFont), game->messageScale);
	if (game->messageOpenAnim > 0.0f)
	{
		RcBindTexture(GetTexture(cardBackSprite));
		RcDrawTexturedRec(game->messageRec, White);
		
		v2 messagePos = NewVec2(game->messageRec.x + 15*game->messageScale, game->messageRec.y + 15*game->messageScale + RcMaxExtendUp());
		v2i buttonFrame = NewVec2i(1, 0);
		if (IsMouseClickingOn(MouseButton_Left, "MessageWindowOkayBtn") || ButtonDownNoHandling(Button_Z) || ButtonDownNoHandling(Button_X) || ButtonDownNoHandling(Button_Enter))
		{
			buttonFrame.y += 2;
		}
		else if (IsMouseOver("MessageWindowOkayBtn"))
		{
			buttonFrame.y += 1;
		}
		
		RcDrawSheetFrame(GetSheet(buttonsSheet), buttonFrame, game->messageOkayBtn, White);
		RcDrawFormattedString(game->messageStr, MyStrLength32(game->messageStr), messagePos, MESSAGE_WINDOW_WIDTH, White, 1.0f, Alignment_Left, true);
	}
	
	// +==============================+
	// |    Draw Derailed Message     |
	// +==============================+
	if (game->level.train.isDead && game->derailedMessageAnim >= 0.7f)
	{
		r32 alpha = (game->derailedMessageAnim - 0.7f) / 0.3f;
		RcDrawRectangle(NewRec(Vec2_Zero, RenderScreenSize), ColorTransparent(White, 0.5f * alpha));
		
		RcBindTexture(GetTexture(derailedSprite));
		RcDrawTexturedRec(game->derailedMessageRec, ColorTransparent(alpha));
		
		if (game->derailedMessageAnim >= 1.0f)
		{
			RcBindFont(GetFont(pixelFont), 8.0f);
			v2 resetStrSize = RcMeasureStringNt(RESET_MESSAGE);
			v2 resetStrPos = NewVec2(game->derailedMessageRec.x + game->derailedMessageRec.width/2 - resetStrSize.width/2, game->derailedMessageRec.y + game->derailedMessageRec.height - RcMaxExtendDown());
			rec backRec = NewRec(resetStrPos.x, resetStrPos.y - RcMaxExtendUp(), resetStrSize.width, resetStrSize.height);
			RcDrawRectangle(RecInflate(backRec, 15), ColorTransparent(Black, 0.5f));
			RcDrawStringNt(RESET_MESSAGE, resetStrPos, White);
		}
	}
	
	// +==============================+
	// |       Draw Win Message       |
	// +==============================+
	if (game->showingWinGame)
	{
		RcDrawRectangle(NewRec(Vec2_Zero, RenderScreenSize), ColorTransparent(Black, 0.5f));
		RcBindTexture(GetTexture(youWinSprite));
		RcDrawTexturedRec(game->winGameArtRec, White);
		
		RcBindFont(GetFont(debugFont), 2.0f);
		v2 textPos = NewVec2(RenderScreenSize.width/2, RenderScreenSize.height - RcMaxExtendDown());
		char* timeText = TempPrint("Completion Time: %s", FormattedMilliseconds((u64)FloorR32i(game->gameTimer)));
		RcDrawStringNt(timeText, textPos + NewVec2(0, 2), Black, 1.0f, Alignment_Center);
		RcDrawStringNt(timeText, textPos, White, 1.0f, Alignment_Center);
	}
	
	// +================================+
	// |       Draw Debug Overlay       |
	// +================================+
	if (app->showDebugOverlay)
	{
		rec overlayRec = NewRec(10, 10, (r32)RenderScreenSize.x - 10*2, (r32)RenderScreenSize.y - 10*2);
		RcDrawButton(overlayRec, ColorTransparent(Black, 0.5f), ColorTransparent(White, 0.5f));
		
		RcBindFont(GetFont(debugFont));
		v2 textPos = NewVec2(overlayRec.x + 5, overlayRec.y + 5 + RcMaxExtendUp());
		
		// RcPrintString(textPos, White, 1.0f, "AppData Size: %u/%u (%.3f%%)",
		// 	sizeof(AppData_t), AppMemory->permanantSize,
		// 	(r32)sizeof(AppData_t) / (r32)AppMemory->permanantSize * 100.0f);
		// textPos.y += RcLineHeight();
		
		RcPrintString(textPos, White, 1.0f, "Main Heap: %s/%s (%.3f%%)",
			FormattedSizeStr(mainHeap->used), FormattedSizeStr(mainHeap->size),
			(r32)mainHeap->used / (r32)mainHeap->size * 100.0f);
		textPos.y += RcLineHeight();
		
		RcPrintString(textPos, White, 1.0f, "Temp Init: %s/%s (%.3f%%)",
			FormattedSizeStr(app->appInitTempHighWaterMark), FormattedSizeStr(TempArena->size),
			(r32)app->appInitTempHighWaterMark / (r32)TempArena->size * 100.0f);
		textPos.y += RcLineHeight();
		
		RcPrintString(textPos, White, 1.0f, "Temp Update: %s/%s (%.3f%%) %u marks",
			FormattedSizeStr(ArenaGetHighWaterMark(TempArena)), FormattedSizeStr(TempArena->size),
			(r32)ArenaGetHighWaterMark(TempArena) / (r32)TempArena->size * 100.0f, ArenaNumMarks(TempArena));
		textPos.y += RcLineHeight();
		
		#if DEBUG
		const DynArray_t* appAllocations = (const DynArray_t*)platform->appAllocationsPntr;
		RcPrintString(textPos, White, 1.0f, "App Allocations: %u", appAllocations->length);
		textPos.y += RcLineHeight();
		#if 0
		for (u32 aIndex = 0; aIndex < appAllocations->length; aIndex++)
		{
			u8* allocationStructPntr = (u8*)DynArrayGet_(appAllocations, aIndex);
			u32 allocationSize = *((u32*)(allocationStructPntr + 0));
			void* allocationBase = *((void**)(allocationStructPntr + sizeof(u32)));
			
			RcPrintString(textPos, White, 1.0f, "  [%u]: %s %p", aIndex, FormattedSizeStr(allocationSize), allocationBase);
			textPos.y += RcLineHeight();
		}
		#endif
		#endif
		
		RcPrintString(textPos, White, 1.0f, "Program Time: %lu", ProgramTime);
		textPos.y += RcLineHeight();
		
		RcPrintString(textPos, White, 1.0f, "Time Delta: %.2f", appInput->timeDelta);
		textPos.y += RcLineHeight();
		
		RcPrintString(textPos, White, 1.0f, "System Timestamp: %ld", appInput->systemTime.timestamp);
		textPos.y += RcLineHeight();
		
		RcPrintString(textPos, White, 1.0f, "System Time: %s", FormattedTimeStr(appInput->systemTime));
		textPos.y += RcLineHeight();
		
		RcPrintString(textPos, White, 1.0f, "Local Timestamp: %ld", LocalTime.timestamp);
		textPos.y += RcLineHeight();
		
		RcPrintString(textPos, White, 1.0f, "Local Time: %s", FormattedTimeStr(LocalTime));
		textPos.y += RcLineHeight();
		
		textPos.y += RcLineHeight();
		
		RcPrintString(textPos, White, 1.0f, "Framerate: %.0lf", appInput->avgFramerate);
		textPos.y += RcLineHeight();
		
		RcPrintString(textPos, White, 1.0f, "Elapsed Time: %.1fms", appInput->avgElapsedMs);
		textPos.y += RcLineHeight();
		
		RcPrintString(textPos, White, 1.0f, "Chug Speed: %.1fms (%.1f%%, %.1fbpm)", game->level.train.chugSpeed, (game->level.train.chugSpeed / DEFAULT_CHUG_SPEED) * 100, (60.0f*1000.0f) / game->level.train.chugSpeed);
		textPos.y += RcLineHeight();
		
		if (app->currentMusic != nullptr)
		{
			rec chugVisualRec = NewRec(textPos.x, textPos.y - RcMaxExtendUp() + RcLineHeight(), 0, 20);
			chugVisualRec.width = RenderScreenSize.width - 10 - chugVisualRec.x;
			
			r32 playOffset = GetSndInstancePlayOffset(app->currentMusic);
			Sound_t* sourceSound = GetSoundPntrById(app->currentMusic->soundId);
			if (sourceSound != nullptr)
			{
				r32 songLengthMs = ((r32)sourceSound->sampleCount / (r32)(sourceSound->frequency * sourceSound->channelCount)) * 1000.0f;
				v2 lengthStrPos = NewVec2(chugVisualRec.x + chugVisualRec.width, chugVisualRec.y - 1 - RcMaxExtendDown());
				RcPrintStringAligned(lengthStrPos, White, 1.0f, Alignment_Right, "%.3f / %.3f", playOffset/1000.0f, songLengthMs/1000.0f);
				for (r32 offset = CHUG_BEAT_OFFSET; offset < songLengthMs; offset += DEFAULT_CHUG_SPEED)
				{
					rec tickRec = NewRec(chugVisualRec.x+1, chugVisualRec.y, 1, chugVisualRec.height);
					tickRec.x += RoundR32((chugVisualRec.width-2) * (offset / songLengthMs));
					RcDrawRectangle(tickRec, PalBlueLighter);
				}
				rec playCursorRec = NewRec(chugVisualRec.x, chugVisualRec.y, 2, chugVisualRec.height);
				playCursorRec.x += RoundR32((chugVisualRec.width-2) * (playOffset / songLengthMs));
				RcDrawRectangle(playCursorRec, White);
				
				r32 musicChugOffset = ModR32(playOffset - CHUG_BEAT_OFFSET, DEFAULT_CHUG_SPEED);
				r32 minError = MinR32(AbsR32(musicChugOffset - game->level.train.chugTime), AbsR32(musicChugOffset + DEFAULT_CHUG_SPEED - game->level.train.chugTime));
				{
					rec errorRec = playCursorRec;
					errorRec.width = minError;
					RcDrawRectangle(errorRec, ColorTransparent(PalRed, 0.5f));
				}
			}
			RcDrawButton(chugVisualRec, Transparent, White, 1);
			textPos.y += chugVisualRec.y + 10;
		}
		
		//Sound Instance List
		textPos = NewVec2(overlayRec.x + 5 + 500, overlayRec.y + 5 + RcMaxExtendUp());
		
		u32 numActiveInstances = 0;
		for (u32 sIndex = 0; sIndex < MAX_SOUND_INSTANCES; sIndex++)
		{
			SoundInstance_t* instance = &app->soundInstances[sIndex];
			if (instance->playing)
			{
				numActiveInstances++;
			}
		}
		//TODO: Re-implement this display when the custom audio layer is working
		#if AUDIO_ENABLED && !USE_CUSTOM_AUDIO
		RcPrintString(textPos, White, 1.0f, "%u instance%s", numActiveInstances, (numActiveInstances == 1) ? "" : "s");
		textPos.y += RcLineHeight();
		for (u32 sIndex = 0; sIndex < MAX_SOUND_INSTANCES; sIndex++)
		{
			SoundInstance_t* instance = &app->soundInstances[sIndex];
			if (instance->playing)
			{
				const char* sndName = nullptr;
				for (u32 rIndex = 0; rIndex < ArrayCount(soundsList->sounds); rIndex++)
				{
					SoundResource_t* sndResource = &soundsList->sounds[rIndex];
					if (instance->soundId == sndResource->sound.id)
					{
						sndName = sndResource->readableName;
						break;
					}
				}
				for (u32 mIndex = 0; mIndex < NUM_RESOURCE_MUSICS; mIndex++)
				{
					Resource_t* musicResource = GetResourceByType(ResourceType_Music, mIndex);
					Assert(musicResource != nullptr);
					if (IsResourceLoaded(musicResource) && instance->soundId == musicResource->music.id)
					{
						Assert(musicResource->readableName != nullptr);
						sndName = musicResource->readableName;
						break;
					}
				}
				if (sndName == nullptr) { sndName = TempPrint("%u", instance->soundId); }
				RcPrintString(textPos, White, 1.0f, "[%u] %s %s %.1f%%", sIndex, sndName, FormattedMilliseconds(TimeSince(instance->startTime)), instance->distVolume * 100);
				textPos.y += RcLineHeight();
			}
		}
		#endif
	}
}

