/*
File:   game_toot_level_render.cpp
Author: Taylor Robbins
Date:   07\10\2020
Description: 
	** Handles drawing the TootLevel_t
*/

void RenderGround(TootLevel_t* level, Ground_t* ground, v2i tilePos, rec drawRec)
{
	Assert(level != nullptr);
	Assert(ground != nullptr);
	
	u8 numExtraFrames = 0;
	v2i extraFrames[4];
	
	switch (ground->type)
	{
		case GroundType_Grass:
		{
			v2i frame = NewVec2i(0 + (tilePos.x%3), 2 + (tilePos.y%3));
			RcDrawSheetFrame(GetSheet(groundSheet), frame, drawRec, White, Dir2_Down, false, false);
		} break;
		case GroundType_Water:
		{
			v2i frame1 = NewVec2i(0 + (i32)((ProgramTime%2000)/250), 1);
			v2i frame2 = NewVec2i(0 + (i32)(7 - ((ProgramTime+330)%2000)/250), 1);
			RcDrawSheetFrame(GetSheet(groundSheet), frame1, drawRec, White, Dir2_Down, false, false);
			RcDrawSheetFrame(GetSheet(groundSheet), frame2, drawRec, ColorTransparent(PalBlueLighter, 0.3f), Dir2_Left, true, false);
			
			v2i groundBaseFrame = NewVec2i(3, 2);
			v2i connectionFrame = GetConnectedFrame(ground->connections, &numExtraFrames, &extraFrames[0]);
			RcDrawSheetFrame(GetSheet(groundSheet), groundBaseFrame + connectionFrame, drawRec, White, Dir2_Down, false, false);
			for (u32 eIndex = 0; eIndex < numExtraFrames; eIndex++)
			{
				RcDrawSheetFrame(GetSheet(groundSheet), groundBaseFrame + extraFrames[eIndex], drawRec, White, Dir2_Down, false, false);
			}
			// RcPrintStringAligned(drawRec.topLeft + drawRec.size/2, Black, 1.0f, Alignment_Center, "0x%02X", ground->connections);
			
		} break;
		case GroundType_Sand:
		{
			v2i frame = NewVec2i(1, 0);
			RcDrawSheetFrame(GetSheet(groundSheet), frame, drawRec, White, Dir2_Down, false, false);
			
			v2i groundBaseFrame = NewVec2i(7, 2);
			v2i connectionFrame = GetConnectedFrame(ground->connections, &numExtraFrames, &extraFrames[0]);
			RcDrawSheetFrame(GetSheet(groundSheet), groundBaseFrame + connectionFrame, drawRec, White, Dir2_Down, false, false);
			for (u32 eIndex = 0; eIndex < numExtraFrames; eIndex++)
			{
				RcDrawSheetFrame(GetSheet(groundSheet), groundBaseFrame + extraFrames[eIndex], drawRec, White, Dir2_Down, false, false);
			}
		} break;
		default:
		{
			RcDrawRectangle(drawRec, PalRed);
		} break;
	}
}

void RenderTrack(TootLevel_t* level, Track_t* track, rec drawRec, bool isFailure = false)
{
	Assert(level != nullptr);
	Assert(track != nullptr);
	
	r32 drawScale = drawRec.width / 32.0f;
	Color_t drawColor = isFailure ? ColorTransparent(PalRed, 0.5f) : White;
	v2 drawCenter = drawRec.topLeft + drawRec.size/2;
	switch (track->type)
	{
		case TrackType_Cross:
		{
			RcBindTexture(GetTexture(trackCrossSprite));
			RcDrawTexturedRotatedRec(drawRec, drawColor, track->rotation, false, false);
		} break;
		case TrackType_BridgeCross:
		{
			RcBindTexture(GetTexture(trackBridgeCrossSprite));
			RcDrawTexturedRotatedRec(drawRec, drawColor, track->rotation, false, false);
		} break;
		case TrackType_End:
		{
			v2 trackSize = GetTextureSizef(trackEndSprite) * drawScale;
			bool flippedLighting = false;
			if (track->rotation == Dir2_Up || track->rotation == Dir2_Left) { flippedLighting = true; }
			if (track->rotation == Dir2_Left || track->rotation == Dir2_Right) { r32 temp = trackSize.width; trackSize.width = trackSize.height; trackSize.height = temp; }
			rec trackRec = NewRecCentered(drawRec.topLeft + drawRec.size/2, trackSize);
			RcBindTexture(GetTexture(trackEndSprite));
			RcDrawTexturedRotatedRec(trackRec, drawColor, track->rotation, flippedLighting, false);
		} break;
		case TrackType_Straight:
		{
			v2 trackSize = GetTextureSizef(trackStraightSprite) * drawScale;
			bool flippedLighting = false;
			if (track->rotation == Dir2_Up || track->rotation == Dir2_Left) { flippedLighting = true; }
			if (track->rotation == Dir2_Left || track->rotation == Dir2_Right) { r32 temp = trackSize.width; trackSize.width = trackSize.height; trackSize.height = temp; }
			rec trackRec = NewRecCentered(drawRec.topLeft + drawRec.size/2, trackSize);
			RcBindTexture(GetTexture(trackStraightSprite));
			RcDrawTexturedRotatedRec(trackRec, drawColor, track->rotation, flippedLighting, false);
		} break;
		case TrackType_Corner:
		{
			v2 trackSize = GetTextureSizef(trackCornerSprite) * drawScale;
			bool flippedLighting = false;
			// if (track->rotation == Dir2_Up || track->rotation == Dir2_Left) { flippedLighting = true; }
			v2 drawOffset = Vec2_Zero;
			if (track->rotation == Dir2_Down)
			{
				drawOffset.x = -drawRec.width;
			}
			else if (track->rotation == Dir2_Left)
			{
				drawOffset.x = -drawRec.width;
				drawOffset.y = -drawRec.height;
			}
			else if (track->rotation == Dir2_Up)
			{
				drawOffset.y = -drawRec.height;
			}
			else if (track->rotation == Dir2_Right)
			{
				drawOffset = Vec2_Zero;
			}
			rec trackRec = NewRec(drawRec.topLeft + drawOffset, trackSize);
			drawCenter = trackRec.topLeft + trackRec.size/2;
			Texture_t* cornerTexture = GetTexture(trackCornerSprite);
			if (track->rotation == Dir2_Left || track->rotation == Dir2_Up) { cornerTexture = GetTexture(trackCornerSprite2); }
			RcBindTexture(cornerTexture);
			RcDrawTexturedRotatedRec(trackRec, drawColor, track->rotation, flippedLighting, false);
		} break;
		case TrackType_BridgeStraight:
		{
			v2 trackSize = GetTextureSizef(trackBridgeStraightSprite) * drawScale;
			bool flippedLighting = false;
			if (track->rotation == Dir2_Up || track->rotation == Dir2_Left) { flippedLighting = true; }
			if (track->rotation == Dir2_Left || track->rotation == Dir2_Right) { r32 temp = trackSize.width; trackSize.width = trackSize.height; trackSize.height = temp; }
			rec trackRec = NewRecCentered(drawRec.topLeft + drawRec.size/2, trackSize);
			RcBindTexture(GetTexture(trackBridgeStraightSprite));
			RcDrawTexturedRotatedRec(trackRec, drawColor, track->rotation, flippedLighting, false);
		} break;
		case TrackType_BridgeCorner:
		{
			v2 trackSize = GetTextureSizef(trackBridgeCornerSprite) * drawScale;
			bool flippedLighting = false;
			// if (track->rotation == Dir2_Up || track->rotation == Dir2_Left) { flippedLighting = true; }
			v2 drawOffset = Vec2_Zero;
			if (track->rotation == Dir2_Down)
			{
				drawOffset.x = -drawRec.width;
			}
			else if (track->rotation == Dir2_Left)
			{
				drawOffset.x = -drawRec.width;
				drawOffset.y = -drawRec.height;
			}
			else if (track->rotation == Dir2_Up)
			{
				drawOffset.y = -drawRec.height;
			}
			else if (track->rotation == Dir2_Right)
			{
				drawOffset = Vec2_Zero;
			}
			rec trackRec = NewRec(drawRec.topLeft + drawOffset, trackSize);
			drawCenter = trackRec.topLeft + trackRec.size/2;
			Texture_t* cornerTexture = GetTexture(trackBridgeCornerSprite);
			if (track->rotation == Dir2_Left || track->rotation == Dir2_Up) { cornerTexture = GetTexture(trackBridgeCornerSprite2); }
			RcBindTexture(cornerTexture);
			RcDrawTexturedRotatedRec(trackRec, drawColor, track->rotation, flippedLighting, false);
		} break;
	}
	
	if (!isFailure && track->creationAnim < 1.0f)
	{
		rec poofRec = NewRecCentered(drawCenter, GetSheetFrameSizef(plumeSuccessAnimSheet) * drawScale);
		v2i poofFrame = NewVec2i(0 + (i32)(track->creationAnim*6), 0);
		PalColor_t poofColor = plt->plumeSuccess;
		RcSetReplaceColors(poofColor);
		RcDrawSheetFrame(GetSheet(plumeSuccessAnimSheet), poofFrame, poofRec, White);
	}
}

void RenderDecoration(TootLevel_t* level, Decoration_t* decor, rec drawRec)
{
	Assert(level != nullptr);
	Assert(decor != nullptr);
	
	switch (decor->type)
	{
		case DecorType_Trees:
		{
			v2i frame = NewVec2i(1, 0);
			RcDrawSheetFrame(GetSheet(entitiesSheet), frame, drawRec, White);
		} break;
		case DecorType_Rock:
		{
			v2i frame = NewVec2i(0, 2);
			RcDrawSheetFrame(GetSheet(entitiesSheet), frame, drawRec, White);
		} break;
		case DecorType_House:
		{
			v2i frame = NewVec2i(3, 2);
			RcDrawSheetFrame(GetSheet(entitiesSheet), frame, drawRec, White);
		} break;
		case DecorType_EndGoal:
		{
			v2i frame = NewVec2i(0 + (i32)((ProgramTime%1000)/250), 0);
			drawRec.size = drawRec.size * 2;
			RcDrawSheetFrame(GetSheet(endTownSheet), frame, drawRec, White);
		} break;
		case DecorType_Sheep:
		{
			if (decor->isScattered && decor->scatterAnimTime >= 1.0f) { break; }
			v2i frame = NewVec2i(0 + (i32)((ProgramTime%2000)/200), 4);
			obb2 drawBox = NewObb2(drawRec);
			drawBox.rotation = decor->rotation;
			Color_t drawColor = White;
			if (decor->isScattered)
			{
				i32 frameNum = (i32)((ProgramTime%400)/100);
				if (frameNum == 3) { frameNum = 1; }
				frame = NewVec2i(0 + frameNum, 4);
				drawBox.rotation = RandR32(0, Pi32*2);
				drawBox.center += decor->scatterVec * decor->scatterAnimTime;
				if (decor->scatterAnimTime >= 0.7f)
				{
					drawColor.a = (u8)(((decor->scatterAnimTime-0.7f)*0.3f) * 255);
				}
			}
			RcDrawSheetFrameObb2(GetSheet(entitiesSheet), frame, drawBox, drawColor);
		} break;
	}
}

void RenderTrain(TootLevel_t* level, Train_t* train, rec drawRec, v2 drawOffset, r32 drawScale)
{
	Assert(level != nullptr);
	Assert(train != nullptr);
	if (train->isDead && train->deadAnimPercent >= 1.0f) { return; }
	
	if (!train->isDead || train->deadAnimPercent < 0.70f)
	{
		v2i frame = Vec2i_Zero;
		frame.x += (i32)((ProgramTime%(60*17))/60);
		obb2 drawBox = NewObb2((NewVec2(train->tilePos) + Vec2_Half) * 32.0f * drawScale + drawOffset, GetSheetFrameSizef(trainSheet) * drawScale, GetDir2RotationFrom(Dir2_Down, train->direction));
		if (train->tilePos != train->fromTilePos)
		{
			v2 fromPos = (NewVec2(train->fromTilePos) + Vec2_Half) * 32.0f * drawScale;
			fromPos += drawOffset;
			if (train->fromDir != train->direction)
			{
				//TODO: Animate him around a corner
				drawBox.rotation = AngleLerp(GetDir2RotationFrom(Dir2_Down, train->fromDir), drawBox.rotation, train->movePercent);
				v2 perpRightVec = Vec2PerpRight(NewVec2(train->fromDir));
				r32 arcRadius = 32.0f * 1.0f * drawScale;
				v2 arcCenter = Vec2_Zero;
				r32 arcStart = 0;
				r32 arcEnd = 0;
				if (Vec2Dot(NewVec2(train->tilePos - train->fromTilePos), perpRightVec) >= 0)
				{
					arcCenter = fromPos + (arcRadius * perpRightVec);
					arcStart = GetDir2RotationFrom(Dir2_Down, train->fromDir) + 0;
					arcEnd = GetDir2RotationFrom(Dir2_Down, train->fromDir) + (1.0f/2.0f) * Pi32;
				}
				else
				{
					arcCenter = fromPos + (arcRadius * -perpRightVec);
					arcStart = GetDir2RotationFrom(Dir2_Down, train->fromDir) + Pi32;
					arcEnd = GetDir2RotationFrom(Dir2_Down, train->fromDir) + (1.0f/2.0f) * Pi32;
				}
				r32 currentAngle = LerpR32(arcStart, arcEnd, train->movePercent);
				drawBox.center = arcCenter + arcRadius * NewVec2(CosR32(currentAngle), SinR32(currentAngle));
				
				// RcDrawRectangle(NewRecCentered(arcCenter, Vec2_One*5*drawScale), PalPink);
			}
			else
			{
				drawBox.center = Vec2Lerp(fromPos, drawBox.center, train->movePercent);
			}
		}
		train->drawPos = drawBox.center;
		RcDrawSheetFrameObb2(GetSheet(trainSheet), frame, drawBox, White);
		
		v2i smokeFrame = NewVec2i((i32)((ProgramTime%900)/100), 0);
		RcDrawSheetFrameObb2(GetSheet(trainSmokeSheet), smokeFrame, drawBox, White);
	}
	
	if (train->isDead)
	{
		v2i explosionFrame = NewVec2i(0 + (i32)(train->deadAnimPercent*5), 0);
		rec explosionRec = NewRecCentered(drawRec.topLeft + drawRec.size/2, GetSheetFrameSizef(explosionSheet) * (drawScale+1));
		RcDrawSheetFrame(GetSheet(explosionSheet), explosionFrame, explosionRec, White);
	}
}

void RenderTootLevel(TootLevel_t* level, v2 drawPos, r32 drawScale)
{
	for (i32 yPos = 0; yPos < level->height; yPos++)
	{
		for (i32 xPos = 0; xPos < level->width; xPos++)
		{
			v2i tilePos = NewVec2i(xPos, yPos);
			Ground_t* ground =  GetGroundAt(level, tilePos);
			if (ground->connections == 0x00)
			{
				ground->connections = GetGroundConnections(level, ground, tilePos);
			}
			rec drawRec = NewRec(xPos*32.0f, yPos*32.0f, 32.0f, 32.0f);
			drawRec.topLeft = drawRec.topLeft * drawScale;
			drawRec.size = drawRec.size * drawScale;
			drawRec.topLeft += drawPos;
			RenderGround(level, ground, tilePos, drawRec);
			// if (ground->type == GroundType_Water)
			{
				RcDrawRectangle(drawRec, ColorTransparent(Black, (((xPos + yPos)%2) == 0) ? 0.15f : 0.0f));
			}
		}
	}
	
	for (u32 tIndex = 0; tIndex < level->tracks.length; tIndex++)
	{
		Track_t* track = DynArrayGet(&level->tracks, Track_t, tIndex);
		Assert(track != nullptr);
		rec drawRec = NewRec(track->tilePos.x*32.0f, track->tilePos.y*32.0f, 32.0f, 32.0f);
		drawRec.topLeft = drawRec.topLeft * drawScale;
		drawRec.size = drawRec.size * drawScale;
		drawRec.topLeft += drawPos;
		RenderTrack(level, track, drawRec);
	}
	
	for (u32 dIndex = 0; dIndex < level->decorations.length; dIndex++)
	{
		Decoration_t* decor = DynArrayGet(&level->decorations, Decoration_t, dIndex);
		Assert(decor != nullptr);
		rec drawRec = NewRec(decor->tilePos.x*32.0f, decor->tilePos.y*32.0f, 32.0f, 32.0f);
		drawRec.topLeft = drawRec.topLeft * drawScale;
		drawRec.size = drawRec.size * drawScale;
		drawRec.topLeft += drawPos;
		RenderDecoration(level, decor, drawRec);
	}
	
	v2 trainPos = Vec2Multiply(NewVec2(level->train.tilePos) + Vec2_Half, NewVec2(32, 32));
	v2 trainSize = GetSheetFrameSizef(trainSheet);
	// if (level->train.direction == Dir2_Left || level->train.direction == Dir2_Right) { r32 temp = trainSize.width; trainSize.width = trainSize.height; trainSize.height = temp; }
	rec trainRec = NewRecCentered(trainPos, trainSize);
	trainRec.topLeft = trainRec.topLeft * drawScale;
	trainRec.size = trainRec.size * drawScale;
	trainRec.topLeft += drawPos;
	RenderTrain(level, &level->train, trainRec, drawPos, drawScale);
	
	if (level->failedTrackAnimTime > 0.0f)
	{
		rec failedTrackRec = NewRec(level->failedTrack.tilePos.x*32.0f, level->failedTrack.tilePos.y*32.0f, 32.0f, 32.0f);
		failedTrackRec.topLeft = failedTrackRec.topLeft * drawScale;
		failedTrackRec.size = failedTrackRec.size * drawScale;
		failedTrackRec.topLeft += drawPos;
		RenderTrack(level, &level->failedTrack, failedTrackRec, true);
		rec blockedSpotRec = NewRec(level->failedBlockPos.x*32.0f, level->failedBlockPos.y*32.0f, 32.0f, 32.0f);
		blockedSpotRec.topLeft = blockedSpotRec.topLeft * drawScale;
		blockedSpotRec.size = blockedSpotRec.size * drawScale;
		blockedSpotRec.topLeft += drawPos;
		RcBindTexture(GetTexture(blockSpotSprite));
		RcDrawTexturedRec(blockedSpotRec, White);
	}
	
	if (!level->trackComplete)
	{
		rec buildSpotRec = NewRec(level->placingPos.x*32.0f, level->placingPos.y*32.0f, 32.0f, 32.0f);
		buildSpotRec.topLeft = buildSpotRec.topLeft * drawScale;
		buildSpotRec.size = buildSpotRec.size * drawScale;
		buildSpotRec.topLeft += drawPos;
		RcBindTexture(GetTexture(buildSpotSprite));
		RcDrawTexturedRotatedRec(buildSpotRec, White, level->placingDir, false, false);
	}
}