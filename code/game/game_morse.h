/*
File:   game_morse.h
Author: Taylor Robbins
Date:   07\10\2020
*/

#ifndef _GAME_MORSE_H
#define _GAME_MORSE_H

struct MorsePiece_t
{
	bool isPrimary;
	r32 time;
};

struct MorseInput_t
{
	r32 duration;
	DynArray_t pieces;
};

struct PlayingMorse_t
{
	bool isDone;
	MorseInput_t morse;
	r32 time;
};

#endif //  _GAME_MORSE_H
