/*
File:   game_helpers.cpp
Author: Taylor Robbins
Date:   02\01\2019
Description: 
	** Holds functions that can be utilized by anything related to the Game AppState
*/

void GameControllerDisconnected(u32 controllerIndex) //pre-declared in app_func_defs.h
{
	bool disconnectMattered = false;
	
	//TODO: Anything we need to do?
	
	if (!disconnectMattered)
	{
		PrintLine_I("Controller[%u] disconnected. But we don't need to pause", controllerIndex);
	}
}

void GameShowMessage(const char* message)
{
	if (game->messageStr != nullptr)
	{
		ArenaPop(mainHeap, game->messageStr);
		game->messageOpenAnim = 0.0f;
	}
	game->messageStr = ArenaNtString(mainHeap, message);
	Assert(game->messageStr != nullptr);
	game->showingMessage = true;
	PrintLine_D("Showing message: \"%s\"", game->messageStr);
}

void GameUiUpdateRecs()
{
	game->hudScale = 4.0f; //TODO: Choose this is a more dynamic way?
	game->hudRec = NewRec(0, 0, GetTextureWidth(hudBackSprite) * game->hudScale, GetTextureHeight(hudBackSprite) * game->hudScale);
	game->hudRec.x = RenderScreenSize.width/2 - game->hudRec.width/2;
	game->hudRec.y = RenderScreenSize.height - game->hudRec.height;
	game->hudRec.topLeft = Vec2Round(game->hudRec.topLeft);
	reci enterBtnPixelRec = NewReci(63, 17, 32, 43);
	game->hudEnterBtnRec.size = NewVec2(enterBtnPixelRec.width * game->hudScale, enterBtnPixelRec.height * game->hudScale);
	game->hudEnterBtnRec.topLeft = game->hudRec.topLeft + NewVec2(enterBtnPixelRec.x * game->hudScale, enterBtnPixelRec.y * game->hudScale);
	reci primaryBtnPixelRec = NewReci(8, 17, 26, 40);
	game->hudPrimaryBtnRec.size = NewVec2(primaryBtnPixelRec.width * game->hudScale, primaryBtnPixelRec.height * game->hudScale);
	game->hudPrimaryBtnRec.topLeft = game->hudRec.topLeft + NewVec2(primaryBtnPixelRec.x * game->hudScale, primaryBtnPixelRec.y * game->hudScale);
	reci secondaryBtnPixelRec = NewReci(37, 17, 24, 40);
	game->hudSecondaryBtnRec.size = NewVec2(secondaryBtnPixelRec.width * game->hudScale, secondaryBtnPixelRec.height * game->hudScale);
	game->hudSecondaryBtnRec.topLeft = game->hudRec.topLeft + NewVec2(secondaryBtnPixelRec.x * game->hudScale, secondaryBtnPixelRec.y * game->hudScale);
	reci inputPixelRec = NewReci(0, 52, 69, 12);
	game->hudInputRec.size = NewVec2(inputPixelRec.width * game->hudScale, inputPixelRec.height * game->hudScale);
	game->hudInputRec.topLeft = game->hudRec.topLeft + NewVec2(inputPixelRec.x * game->hudScale, inputPixelRec.y * game->hudScale);
	reci hudStackPixelRec = NewReci(29-9, 7-11, 14 + 9*2, 10 + 11*2);
	game->hudStackRec.size = NewVec2(hudStackPixelRec.width * game->hudScale, hudStackPixelRec.height * game->hudScale);
	game->hudStackRec.topLeft = game->hudRec.topLeft + NewVec2(hudStackPixelRec.x * game->hudScale, hudStackPixelRec.y * game->hudScale);
	reci hudConductorPixelRec = NewReci(64, 18, 23, 41);
	game->hudConductorRec.size = NewVec2(hudConductorPixelRec.width * game->hudScale, hudConductorPixelRec.height * game->hudScale);
	game->hudConductorRec.topLeft = game->hudRec.topLeft + NewVec2(hudConductorPixelRec.x * game->hudScale, hudConductorPixelRec.y * game->hudScale);
	reci hudShovelerPixelRec = NewReci(17, 29, 32, 32);
	game->hudShovelerRec.size = NewVec2(hudShovelerPixelRec.width * game->hudScale, hudShovelerPixelRec.height * game->hudScale);
	game->hudShovelerRec.topLeft = game->hudRec.topLeft + NewVec2(hudShovelerPixelRec.x * game->hudScale, hudShovelerPixelRec.y * game->hudScale);
	reci primaryFurnacePixelRec = NewReci(8, 20, 26, 37);
	game->hudPrimaryFurnaceRec.size = NewVec2(primaryFurnacePixelRec.width * game->hudScale, primaryFurnacePixelRec.height * game->hudScale);
	game->hudPrimaryFurnaceRec.topLeft = game->hudRec.topLeft + NewVec2(primaryFurnacePixelRec.x * game->hudScale, primaryFurnacePixelRec.y * game->hudScale);
	reci secondaryFurnacePixelRec = NewReci(36, 20, 26, 37);
	game->hudSecondaryFurnaceRec.size = NewVec2(secondaryFurnacePixelRec.width * game->hudScale, secondaryFurnacePixelRec.height * game->hudScale);
	game->hudSecondaryFurnaceRec.topLeft = game->hudRec.topLeft + NewVec2(secondaryFurnacePixelRec.x * game->hudScale, secondaryFurnacePixelRec.y * game->hudScale);
	
	game->completionMenuScale = 4.0f;
	game->completionMenuRec.size = GetTextureSizef(completionBackSprite) * game->completionMenuScale;
	game->completionMenuRec.x = RenderScreenSize.width/2 - game->completionMenuRec.width/2;
	game->completionMenuRec.y = RenderScreenSize.height/2 - game->completionMenuRec.height/2;
	game->completionMenuRec.x -= (game->completionMenuRec.x + game->completionMenuRec.width) * EaseQuadraticInOut(1-game->completionMenuOpenAnim);
	game->completionMenuRec.topLeft = Vec2Round(game->completionMenuRec.topLeft);
	
	game->nextLevelBtnRec.size = GetSheetFrameSizef(buttonsSheet) * game->completionMenuScale/2;
	game->nextLevelBtnRec.x = game->completionMenuRec.x + game->completionMenuRec.width*(3.0f/8.0f) - game->nextLevelBtnRec.width/2;
	game->nextLevelBtnRec.y = game->completionMenuRec.y + game->completionMenuRec.height - game->nextLevelBtnRec.height*(3.0f/4.0f);
	game->nextLevelBtnRec.topLeft = Vec2Round(game->nextLevelBtnRec.topLeft);
	
	if (game->messageStr != nullptr)
	{
		game->messageScale = 2.0f;
		RcBindFont(GetFont(pixelFont), game->messageScale);
		v2 messageSize = RcMeasureFormattedStringNt(game->messageStr, MESSAGE_WINDOW_WIDTH);
		game->messageRec.width = MESSAGE_WINDOW_WIDTH + 20*2*game->messageScale;
		game->messageRec.height = messageSize.height + 25*2*game->messageScale;
		game->messageRec.x = RenderScreenSize.width/2 - game->messageRec.width/2;
		game->messageRec.y = RenderScreenSize.height/2 - game->messageRec.height/2;
		game->messageRec.y -= (game->messageRec.y + game->messageRec.height) * EaseQuadraticInOut(1-game->messageOpenAnim);
		game->messageRec.topLeft = Vec2Round(game->messageRec.topLeft);
		
		game->messageOkayBtn.size = GetSheetFrameSizef(buttonsSheet) * game->messageScale;
		game->messageOkayBtn.x = game->messageRec.x + game->messageRec.width*(4.0f/8.0f) - game->messageOkayBtn.width/2;
		game->messageOkayBtn.y = game->messageRec.y + game->messageRec.height - game->messageOkayBtn.height*(3.0f/4.0f);
		game->messageOkayBtn.topLeft = Vec2Round(game->messageOkayBtn.topLeft);
	}
	
	r32 derailedScale = 1.0f;
	game->derailedMessageRec.size = GetTextureSizef(derailedSprite);
	while (game->derailedMessageRec.width * (derailedScale+1) <= RenderScreenSize.width*(3.0f/4.0f) && game->derailedMessageRec.height * (derailedScale+1) <= RenderScreenSize.height) { derailedScale += 1.0f; }
	game->derailedMessageRec.size = game->derailedMessageRec.size * derailedScale;
	game->derailedMessageRec.x = RenderScreenSize.width/2 - game->derailedMessageRec.width/2;
	game->derailedMessageRec.y = RenderScreenSize.height/2 - game->derailedMessageRec.height/2;
	game->derailedMessageRec.topLeft = Vec2Round(game->derailedMessageRec.topLeft);
	
	r32 youWinScale = 1.0f;
	game->winGameArtRec.size = GetTextureSizef(youWinSprite);
	while (game->winGameArtRec.width * (youWinScale+1) <= RenderScreenSize.width && game->winGameArtRec.height * (youWinScale+1) <= RenderScreenSize.height) { youWinScale += 1.0f; }
	game->winGameArtRec.size = game->winGameArtRec.size * youWinScale;
	game->winGameArtRec.x = RenderScreenSize.width/2 - game->winGameArtRec.width/2;
	game->winGameArtRec.y = RenderScreenSize.height/2 - game->winGameArtRec.height/2;
	game->winGameArtRec.topLeft = Vec2Round(game->winGameArtRec.topLeft);
}

void GameUiCaptureMouse()
{
	if (IsMouseInside(game->messageOkayBtn)) { CaptureMouse("MessageWindowOkayBtn"); }
	if (IsMouseInside(game->messageRec)) { CaptureMouse("MessageWindow"); }
	if (IsMouseInside(game->nextLevelBtnRec)) { CaptureMouse("CompletionMenuNextLevelBtn"); }
	if (IsMouseInside(game->completionMenuRec)) { CaptureMouse("CompletionMenu"); }
	if (IsMouseInside(game->hudPrimaryBtnRec)) { CaptureMouse("HudPrimaryBtn"); }
	if (IsMouseInside(game->hudSecondaryBtnRec)) { CaptureMouse("HudSecondaryBtn"); }
	if (IsMouseInside(game->hudEnterBtnRec)) { CaptureMouse("HudEnterBtn"); }
	if (IsMouseInside(game->hudRec)) { CaptureMouse("Hud"); }
}

void DrawMorseInput(MorseInput_t* morse, rec drawRec, r32 scale, bool drawCardBack, bool drawInputBack)
{
	// RcDrawRectangle(drawRec, PalBackgroundLighter);
	if (drawCardBack)
	{
		RcBindTexture(GetTexture(cardBackSprite));
		RcDrawTexturedRec(drawRec, White);
	}
	
	v2 codeSize = Vec2_Zero;
	for (u32 pIndex = 0; pIndex < morse->pieces.length; pIndex++)
	{
		MorsePiece_t* piece = DynArrayGet(&morse->pieces, MorsePiece_t, pIndex);
		Texture_t* pieceTexture = GetTexture(primarySprite);
		if (!piece->isPrimary) { pieceTexture = GetTexture(secondarySprite); }
		v2 pieceSize = NewVec2(pieceTexture->size) * scale;
		if (pIndex > 0) { codeSize.width += 3 * scale; }
		codeSize.width += pieceSize.width;
		if (codeSize.height < pieceSize.height) { codeSize.height = pieceSize.height; }
	}
	
	if (drawInputBack)
	{
		rec codeRec = NewRec(drawRec.x + drawRec.width/2 - codeSize.width/2, drawRec.y + drawRec.height/2 - codeSize.height/2, codeSize.width, codeSize.height);
		RcDrawRectangle(RecInflate(codeRec, 2*scale), ColorTransparent(Black, 0.75f));
	}
	
	v2 piecePos = NewVec2(drawRec.x + drawRec.width/2 - codeSize.width/2, drawRec.y + drawRec.height/2 - codeSize.height/2);
	piecePos = Vec2Round(piecePos);
	for (u32 pIndex = 0; pIndex < morse->pieces.length; pIndex++)
	{
		MorsePiece_t* piece = DynArrayGet(&morse->pieces, MorsePiece_t, pIndex);
		Texture_t* pieceTexture = GetTexture(primarySprite);
		if (!piece->isPrimary) { pieceTexture = GetTexture(secondarySprite); }
		v2 pieceSize = NewVec2(pieceTexture->size) * scale;
		rec pieceRec = NewRec(piecePos, pieceSize);
		pieceRec.y += codeSize.height/2 - pieceRec.height/2;
		pieceRec.topLeft = Vec2Round(pieceRec.topLeft);
		RcBindTexture(pieceTexture);
		RcDrawTexturedRec(pieceRec, White);
		// RcDrawRectangle(NewRec(piecePos, pieceSize), Black);
		piecePos.x += 3 * scale + pieceSize.width;
	}
}
