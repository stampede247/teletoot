/*
File:   game_toot_level.cpp
Author: Taylor Robbins
Date:   07\10\2020
Description: 
	** Handles the TootLevel_t structure and all of things contained inside of it
*/

void DestroyTootLevel(TootLevel_t* level)
{
	Assert(level != nullptr);
	DestroyDynamicArray(&level->tracks);
	DestroyDynamicArray(&level->decorations);
	if (level->grounds != nullptr)
	{
		Assert(level->allocArena != nullptr);
		ArenaPop(level->allocArena, level->grounds);
	}
	ClearPointer(level);
}

void CreateTootLevel(MemoryArena_t* memArena, TootLevel_t* level, v2i size, GroundType_t groundType, v2i trainPos, Dir2_t trainDir, r32 chugSpeed, u32 numTracks = 0, u32 numDecorations = 0)
{
	Assert(level != nullptr);
	
	ClearPointer(level);
	level->size = size;
	level->allocArena = memArena;
	
	CreateDynamicArray(&level->tracks, memArena, sizeof(Track_t), 16, numTracks);
	CreateDynamicArray(&level->decorations, memArena, sizeof(Decoration_t), 16, numDecorations);
	
	level->grounds = PushArray(memArena, Ground_t, level->width * level->height);
	Assert(level->grounds != nullptr);
	MyMemSet(level->grounds, 0x00, sizeof(Ground_t) * level->width * level->height);
	for (i32 xPos = 0; xPos < level->width; xPos++)
	{
		for (i32 yPos = 0; yPos < level->height; yPos++)
		{
			Ground_t* ground = &level->grounds[xPos + (yPos * level->width)];
			ground->type = groundType;
		}
	}
	Assert(ArenaValidate(mainHeap));
	
	level->train.tilePos = trainPos;
	level->train.direction = trainDir;
	level->train.chugSpeed = DEFAULT_CHUG_SPEED;
	level->train.chugTime = CHUG_BEAT_OFFSET;
	level->train.movePercent = 1.0f;
	level->train.fromTilePos = trainPos;
	level->train.fromDir = trainDir;
}

Decoration_t* AllocDecoration(TootLevel_t* level, DecorType_t type, v2i tilePos)
{
	Assert(level != nullptr);
	Decoration_t* result = DynArrayAdd(&level->decorations, Decoration_t);
	Assert(result != nullptr);
	ClearPointer(result);
	result->type = type;
	result->tilePos = tilePos;
	return result;
}
Track_t* AllocTrack(TootLevel_t* level, TrackType_t type, v2i tilePos, Dir2_t rotation)
{
	Assert(level != nullptr);
	Track_t* result = DynArrayAdd(&level->tracks, Track_t);
	Assert(result != nullptr);
	ClearPointer(result);
	result->type = type;
	result->tilePos = tilePos;
	result->rotation = rotation;
	result->creationAnim = 0.0f;
	return result;
}

Ground_t* GetGroundAt(TootLevel_t* level, v2i tilePos)
{
	Assert(level != nullptr);
	if (tilePos.x < 0 || tilePos.y < 0 || tilePos.x >= level->width || tilePos.y >= level->height) { return nullptr; }
	if (level->grounds == nullptr) { return nullptr; }
	return &level->grounds[tilePos.x + (tilePos.y * level->width)];
}
const Ground_t* GetGroundAt(const TootLevel_t* level, v2i tilePos) //const variant
{
	Assert(level != nullptr);
	if (tilePos.x < 0 || tilePos.y < 0 || tilePos.x >= level->width || tilePos.y >= level->height) { return nullptr; }
	if (level->grounds == nullptr) { return nullptr; }
	return &level->grounds[tilePos.x + (tilePos.y * level->width)];
}

void CreateTootLevel(MemoryArena_t* memArena, TootLevel_t* level, const Level_t* pcqLevel)
{
	Assert(pcqLevel != nullptr);
	
	const Entity_t* trainEntity = nullptr;
	u32 numDecorations = 0;
	u32 numTracks = 0;
	
	{
		const LoopEntities(entityPntr, pcqLevel)
		{
			if (entityPntr->type == Entity_Monster && entityPntr->info.variant == MonsterVariant_Spider)
			{
				trainEntity = entityPntr;
			}
			else if (entityPntr->type == Entity_Rail)
			{
				if (entityPntr->info.variant == RailVariant_Straight) { numTracks++; }
				if (entityPntr->info.variant == RailVariant_DeadEnd) { numTracks++; }
				if (entityPntr->info.variant == RailVariant_Corner) { numTracks++; }
				if (entityPntr->info.variant == RailVariant_AllDirections) { numTracks++; }
				if (entityPntr->info.variant == RailVariant_Box) { numTracks++; }
			}
			else if (entityPntr->type == Entity_Passalong)
			{
				if (entityPntr->info.variant == PassalongVariant_Straight) { numTracks++; }
				if (entityPntr->info.variant == PassalongVariant_Corner) { numTracks++; }
			}
			else if (entityPntr->type == Entity_SolidDecoration)
			{
				if (entityPntr->info.variant == SolidDecor_BubbleVent) { numDecorations++; }
				if (entityPntr->info.variant == SolidDecor_DancingBush) { numDecorations++; }
			}
			else if (entityPntr->type == Entity_FragileDecoration)
			{
				if (entityPntr->info.variant == FragileDecor_Rocks) { numDecorations++; }
			}
			else if (entityPntr->type == Entity_Bag)
			{
				if (entityPntr->info.variant == BagVariant_Sheep) { numDecorations++; }
			}
			else if (entityPntr->type == Entity_Exit) { numDecorations++; }
		}
	}
	Assert(trainEntity != nullptr);
	
	r32 chugSpeed = DEFAULT_CHUG_SPEED; //TODO: Decide this based off the trainEntity
	CreateTootLevel(memArena, level, pcqLevel->size, GroundType_Grass, trainEntity->tilePos, trainEntity->info.rotation, chugSpeed, numDecorations, numTracks);
	
	for (i32 xPos = 0; xPos < level->width; xPos++)
	{
		for (i32 yPos = 0; yPos < level->height; yPos++)
		{
			v2i tilePos = NewVec2i(xPos, yPos);
			Ground_t* ground = GetGroundAt(level, tilePos);
			Assert(ground != nullptr);
			const Tile_t* tile = GetTileAt(pcqLevel, tilePos);
			Assert(tile != nullptr);
			bool weKnowThisTile = false;
			if (tile->type == Tile_Ground)
			{
				if (tile->variant == GroundVariant_Grass || tile->variant == GroundVariant_GrassBordered)
				{
					ground->type = GroundType_Grass;
					weKnowThisTile = true;
				}
				if (tile->variant == GroundVariant_Dirt)
				{
					ground->type = GroundType_Sand;
					weKnowThisTile = true;
				}
			}
			else if (tile->type == Tile_Water)
			{
				ground->type = GroundType_Water;
				weKnowThisTile = true;
			}
			
			if (!weKnowThisTile)
			{
				ground->type = GroundType_Error;
			}
		}
	}
	
	Assert(ArenaValidate(mainHeap));
	
	level->hideUndo           = true;
	level->hideCornerLeft     = true;
	level->hideCornerRight    = true;
	level->hideHorn           = true;
	level->hideBridgeStraight = true;
	level->hideBridgeLeft     = true;
	level->hideBridgeRight    = true;
	const LoopEntities(entity, pcqLevel)
	{
		if (entity->type == Entity_Rail)
		{
			if (entity->info.variant == RailVariant_Straight)
			{
				Track_t* newTrack = AllocTrack(level, TrackType_Straight, entity->tilePos, entity->info.rotation);
				Assert(newTrack != nullptr);
			}
			if (entity->info.variant == RailVariant_DeadEnd)
			{
				Track_t* newTrack = AllocTrack(level, TrackType_End, entity->tilePos, entity->info.rotation);
				Assert(newTrack != nullptr);
			}
			if (entity->info.variant == RailVariant_Corner)
			{
				Track_t* newTrack = AllocTrack(level, TrackType_Corner, entity->tilePos, entity->info.rotation);
				Assert(newTrack != nullptr);
			}
			if (entity->info.variant == RailVariant_AllDirections)
			{
				Track_t* newTrack = AllocTrack(level, TrackType_Cross, entity->tilePos, Dir2_Down);
				Assert(newTrack != nullptr);
			}
			if (entity->info.variant == RailVariant_Box)
			{
				Track_t* newTrack = AllocTrack(level, TrackType_BridgeCross, entity->tilePos, Dir2_Down);
				Assert(newTrack != nullptr);
			}
		}
		else if (entity->type == Entity_Passalong)
		{
			if (entity->info.variant == PassalongVariant_Straight)
			{
				Track_t* newTrack = AllocTrack(level, TrackType_BridgeStraight, entity->tilePos, Dir2CounterClockwise(entity->info.rotation));
				Assert(newTrack != nullptr);
			}
			if (entity->info.variant == PassalongVariant_Corner)
			{
				Track_t* newTrack = AllocTrack(level, TrackType_BridgeCorner, entity->tilePos, Dir2CounterClockwise(entity->info.rotation));
				Assert(newTrack != nullptr);
			}
			if (entity->info.variant == PassalongVariant_Single)
			{
				level->placingPos = entity->tilePos;
				level->placingDir = entity->info.rotation;
			}
		}
		else if (entity->type == Entity_SolidDecoration)
		{
			if (entity->info.variant == SolidDecor_BubbleVent)
			{
				Decoration_t* newDecor = AllocDecoration(level, DecorType_Mountain, entity->tilePos);
				Assert(newDecor != nullptr);
			}
			if (entity->info.variant == SolidDecor_DancingBush)
			{
				Decoration_t* newDecor = AllocDecoration(level, DecorType_Trees, entity->tilePos);
				Assert(newDecor != nullptr);
			}
		}
		else if (entity->type == Entity_FragileDecoration)
		{
			if (entity->info.variant == FragileDecor_Rocks)
			{
				Decoration_t* newDecor = AllocDecoration(level, DecorType_Rock, entity->tilePos);
				Assert(newDecor != nullptr);
			}
		}
		else if (entity->type == Entity_Bag)
		{
			if (entity->info.variant == BagVariant_Sheep)
			{
				Decoration_t* newDecor = AllocDecoration(level, DecorType_Sheep, entity->tilePos);
				Assert(newDecor != nullptr);
			}
		}
		else if (entity->type == Entity_Exit)
		{
			Decoration_t* newDecor = AllocDecoration(level, DecorType_EndGoal, entity->tilePos);
			Assert(newDecor != nullptr);
		}
		else if (entity->type == Entity_Diamond)
		{
			if (entity->info.variant == DiamondVariant_Default)
			{
				level->songNumber = 0x01;
				PrintLine_D("Should play song 2");
			}
			else if (entity->info.variant == DiamondVariant_GoldBar)
			{
				level->songNumber = 0x02;
				PrintLine_D("Should play song 3");
			}
			else if (entity->info.variant == DiamondVariant_Red)
			{
				level->songNumber = 0x03;
				PrintLine_D("Should play song 4");
			}
		}
		else if (entity->type == Entity_Key)
		{
			if (entity->info.color == 0x01) { level->hideUndo           = false; } //red
			if (entity->info.color == 0x02) { level->hideCornerLeft     = false; } //cyan
			if (entity->info.color == 0x03) { level->hideCornerRight    = false; } //black
			if (entity->info.color == 0x04) { level->hideHorn           = false; } //white
			if (entity->info.color == 0x05) { level->hideBridgeStraight = false; } //yellow
			if (entity->info.color == 0x06) { level->hideBridgeLeft     = false; } //orange
			if (entity->info.color == 0x07) { level->hideBridgeRight    = false; } //blue
		}
	}
}

u8 GetGroundConnections(const TootLevel_t* level, const Ground_t* ground, v2i tilePos)
{
	u8 result = 0x00;
	const Ground_t* leftGround  = GetGroundAt(level, tilePos + Vec2i_Left);
	const Ground_t* rightGround = GetGroundAt(level, tilePos + Vec2i_Right);
	const Ground_t* upGround    = GetGroundAt(level, tilePos + Vec2i_Up);
	const Ground_t* downGround  = GetGroundAt(level, tilePos + Vec2i_Down);
	if (leftGround  == nullptr || (leftGround  != nullptr && leftGround->type  == ground->type)) { FlagSet(result, Dir2_Left);  }
	if (rightGround == nullptr || (rightGround != nullptr && rightGround->type == ground->type)) { FlagSet(result, Dir2_Right); }
	if (upGround    == nullptr || (upGround    != nullptr && upGround->type    == ground->type)) { FlagSet(result, Dir2_Up);    }
	if (downGround  == nullptr || (downGround  != nullptr && downGround->type  == ground->type)) { FlagSet(result, Dir2_Down);  }
	const Ground_t* upLeftGround      = GetGroundAt(level, tilePos + Vec2i_Up   + Vec2i_Left);
	const Ground_t* upRightGround     = GetGroundAt(level, tilePos + Vec2i_Up   + Vec2i_Right);
	const Ground_t* downLeftGround    = GetGroundAt(level, tilePos + Vec2i_Down + Vec2i_Left);
	const Ground_t* downRightGround   = GetGroundAt(level, tilePos + Vec2i_Down + Vec2i_Right);
	if (upLeftGround    == nullptr || (upLeftGround    != nullptr && upLeftGround->type    == ground->type)) { FlagSet(result, Dir2Ex_TopLeft);     }
	if (upRightGround   == nullptr || (upRightGround   != nullptr && upRightGround->type   == ground->type)) { FlagSet(result, Dir2Ex_TopRight);    }
	if (downLeftGround  == nullptr || (downLeftGround  != nullptr && downLeftGround->type  == ground->type)) { FlagSet(result, Dir2Ex_BottomLeft);  }
	if (downRightGround == nullptr || (downRightGround != nullptr && downRightGround->type == ground->type)) { FlagSet(result, Dir2Ex_BottomRight); }
	return result;
}

v2i GetConnectedFrame(u8 connections, u8* numExtraFramesOut = nullptr, v2i* extraFramesOut = nullptr)
{
	v2i result = NewVec2i(3, 3);
	
	bool connectUp        = IsFlagSet(connections, Dir2_Up);
	bool connectDown      = IsFlagSet(connections, Dir2_Down);
	bool connectLeft      = IsFlagSet(connections, Dir2_Left);
	bool connectRight     = IsFlagSet(connections, Dir2_Right);
	bool connectUpLeft    = IsFlagSet(connections, Dir2Ex_TopLeft);
	bool connectUpRight   = IsFlagSet(connections, Dir2Ex_TopRight);
	bool connectDownLeft  = IsFlagSet(connections, Dir2Ex_BottomLeft);
	bool connectDownRight = IsFlagSet(connections, Dir2Ex_BottomRight);
	
	if ( connectUp &&  connectDown &&  connectLeft &&  connectRight) { result = NewVec2i(1, 1); } //udlr
	if (!connectUp && !connectDown && !connectLeft && !connectRight) { result = NewVec2i(3, 3); } //none
	
	if (!connectUp && !connectDown &&  connectLeft &&  connectRight) { result = NewVec2i(1, 3); } //lr
	if ( connectUp &&  connectDown && !connectLeft && !connectRight) { result = NewVec2i(3, 1); } //ud
	
	if ( connectUp && !connectDown &&  connectLeft && !connectRight) { result = NewVec2i(2, 2); } //ul
	if ( connectUp && !connectDown && !connectLeft &&  connectRight) { result = NewVec2i(0, 2); } //ur
	if (!connectUp &&  connectDown &&  connectLeft && !connectRight) { result = NewVec2i(2, 0); } //dl
	if (!connectUp &&  connectDown && !connectLeft &&  connectRight) { result = NewVec2i(0, 0); } //dr
	
	if (!connectUp &&  connectDown &&  connectLeft &&  connectRight) { result = NewVec2i(1, 0); } //dlr
	if ( connectUp && !connectDown &&  connectLeft &&  connectRight) { result = NewVec2i(1, 2); } //ulr
	if ( connectUp &&  connectDown && !connectLeft &&  connectRight) { result = NewVec2i(0, 1); } //udr
	if ( connectUp &&  connectDown &&  connectLeft && !connectRight) { result = NewVec2i(2, 1); } //udl
	
	if ( connectUp && !connectDown && !connectLeft && !connectRight) { result = NewVec2i(3, 2); } //u
	if (!connectUp &&  connectDown && !connectLeft && !connectRight) { result = NewVec2i(3, 0); } //d
	if (!connectUp && !connectDown &&  connectLeft && !connectRight) { result = NewVec2i(2, 3); } //l
	if (!connectUp && !connectDown && !connectLeft &&  connectRight) { result = NewVec2i(0, 3); } //r
	
	if (extraFramesOut != nullptr)
	{
		u8 numExtraFrames = 0;
		if (!connectUpRight   && connectUp   && connectRight) { extraFramesOut[numExtraFrames++] = NewVec2i(0, 4); }
		if (!connectUpLeft    && connectUp   && connectLeft)  { extraFramesOut[numExtraFrames++] = NewVec2i(1, 4); }
		if (!connectDownLeft  && connectDown && connectLeft)  { extraFramesOut[numExtraFrames++] = NewVec2i(2, 4); }
		if (!connectDownRight && connectDown && connectRight) { extraFramesOut[numExtraFrames++] = NewVec2i(3, 4); }
		if (numExtraFramesOut != nullptr) { *numExtraFramesOut = numExtraFrames; }
	}
	
	return result;
}

bool IsLevelTilePosBlocked(const TootLevel_t* level, v2i tilePos)
{
	Assert(level != nullptr);
	for (u32 dIndex = 0; dIndex < level->decorations.length; dIndex++)
	{
		Decoration_t* decoration = DynArrayGet(&level->decorations, Decoration_t, dIndex);
		Assert(decoration != nullptr);
		if (decoration->tilePos == tilePos && !decoration->isScattered)
		{
			return true;
		}
		if (decoration->type == DecorType_EndGoal)
		{
			if (decoration->tilePos == tilePos + Vec2i_Up)  { return true; }
			if (decoration->tilePos == tilePos + Vec2i_Left)  { return true; }
			if (decoration->tilePos == tilePos + Vec2i_Up + Vec2i_Left)  { return true; }
		}
	}
	for (u32 tIndex = 0; tIndex < level->tracks.length; tIndex++)
	{
		Track_t* track = DynArrayGet(&level->tracks, Track_t, tIndex);
		Assert(track != nullptr);
		if (track->tilePos == tilePos) { return true; }
		else if (track->type == TrackType_Corner || track->type == TrackType_BridgeCorner)
		{
			if (track->tilePos + NewVec2i(track->rotation) == tilePos) { return true; }
			if (track->tilePos + NewVec2i(Dir2Clockwise(track->rotation)) == tilePos) { return true; }
		}
	}
	return false;
}
