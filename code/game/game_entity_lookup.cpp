/*
File:   game_entity_lookup.cpp
Author: Taylor Robbins
Date:   02\19\2020
Description: 
	** Holds a bunch functions and macros that help us look for or loop over entities in a level in an efficient and concise manner
*/

bool DoesEntityMatchFilters(const Level_t* level, const EntityLookupInfo_t* info, const Entity_t* entity)
{
	Assert(info != nullptr);
	Assert(entity != nullptr);
	if (IsFlagSet(info->filterFlags, EntFltr_Id))                { if (entity->id                             != info->id)         { return false; } }
	if (IsFlagSet(info->filterFlags, EntFltr_Type))              { if (entity->type                           != info->type)       { return false; } }
	if (IsFlagSet(info->filterFlags, EntFltr_Variant))           { if (entity->info.variant                   != info->variant)    { return false; } }
	if (IsFlagSet(info->filterFlags, EntFltr_At))                { if (entity->tilePos                        != info->position)   { return false; } }
	if (IsFlagSet(info->filterFlags, EntFltr_Group))             { if (entity->info.group                     != info->group)      { return false; } }
	if (IsFlagSet(info->filterFlags, EntFltr_In))
	{
		reci collisionRec = GetEntityCollisionRec(level, entity);
		if (!IsTilePosInReci(collisionRec, info->position)) { return false; }
	}
	if (IsFlagSet(info->filterFlags, EntFltr_RegionOver))
	{
		if (entity->type != Entity_Region) { return false; }
		if (entity->info.variant != RegionVariant_Default && entity->info.variant != RegionVariant_Puzzle) { return false; }
		if (!IsTilePosInReci(GetEntityRegionRec(level, entity), info->position)) { return false; }
	}
	if (IsFlagSet(info->filterFlags, EntFltr_Expression))        { if (entity->type != Entity_Number && entity->type != Entity_EntityCounter && entity->type != Entity_Operator) { return false; } }
	return true;
}

Entity_t* LookupEntity(Level_t* level, EntityLookupInfo_t* info) //pre-declared in game_entity_lookup.h
{
	Assert(level != nullptr);
	Assert(info != nullptr);
	if (IsFlagSet(info->filterFlags, EntFltr_At))
	{
		if (info->entityIndex == 0)
		{
			if (info->position.x >= 0 && info->position.y >= 0 && info->position.x < level->width && info->position.y < level->height)
			{
				info->entityPntr = level->tileEntityPntrs[(info->position.y * level->width) + info->position.x];
			}
			else { info->entityPntr = nullptr; }
		}
		while (info->entityPntr != nullptr)
		{
			Entity_t* result = info->entityPntr;
			info->entityIndex++;
			info->entityPntr = info->entityPntr->next;
			if (DoesEntityMatchFilters(level, info, result))
			{
				info->foundIndex++;
				return result;
			}
		}
	}
	else
	{
		while (info->entityIndex < level->entities.length)
		{
			info->entityPntr = BktArrayGet(&level->entities, Entity_t, info->entityIndex);
			info->entityIndex++;
			if (IsEntityAlive(info->entityPntr) && DoesEntityMatchFilters(level, info, info->entityPntr))
			{
				info->foundIndex++;
				return info->entityPntr;
			}
		}
	}
	return nullptr;
}
const Entity_t* LookupEntity(const Level_t* level, EntityLookupInfo_t* info) //pre-declared in game_entity_lookup.h
{
	return (const Entity_t*)LookupEntity((Level_t*)level, info);
}

bool IsEntityByTypeAt(const Level_t* level, EntityType_t entityType, v2i tilePos) //pre-declared in game_entity_lookup.h
{
	Assert(level != nullptr);
	EntityLookupInfo_t info = {};
	info.filterFlags = (EntFltr_Type|EntFltr_At);
	info.type = entityType;
	info.position = tilePos;
	return (LookupEntity(level, &info) != nullptr);
}

bool IsExpressionEntityAt(const Level_t* level, v2i tilePos) //pre-declared in game_entity_lookup.h
{
	Assert(level != nullptr);
	EntityLookupInfo_t info = {};
	info.filterFlags = (EntFltr_At|EntFltr_Expression);
	info.position = tilePos;
	info.isOfFilter = true;
	return (LookupEntity(level, &info) != nullptr);
}

bool DoesEntityIdExist(const Level_t* level, u32 id) //pre-declared in game_entity_lookup.h
{
	EntityLookupInfo_t info = {};
	info.filterFlags = (EntFltr_Id);
	info.id = id;
	return (LookupEntity(level, &info) != nullptr);
}

u32 CountEntitiesByType(const Level_t* level, EntityType_t type) //pre-declared in game_entity_lookup.h
{
	u32 result = 0;
	const LoopEntitiesByType(entity, level, type) { result++; }
	return result;
}
u32 CountEntitiesAt(const Level_t* level, v2i tilePos) //pre-declared in game_entity_lookup.h
{
	u32 result = 0;
	const LoopEntitiesAt(entity, level, tilePos) { result++; }
	return result;
}
