/*
File:   game_level.h
Author: Taylor Robbins
Date:   11\05\2017
*/

#ifndef _GAME_LEVEL_H
#define _GAME_LEVEL_H

typedef enum
{
	RoomViewMode_ChunksStrict = 0x00,
	RoomViewMode_ChunksDefault,
	RoomViewMode_ChunksSmooth,
	RoomViewMode_FreeRoamStrict,
	RoomViewMode_FreeRoamDefault,
	RoomViewMode_FreeRoamSmooth,
	RoomViewMode_NumModes,
} RoomViewMode_t; 

struct Room_t
{
	u32 id;
	u8 viewMode;
	reci chunkRec;
};

//NOTE: heavily serialized. Make sure you add/remove things to the serializations if you add/remove things here
struct Level_t
{
	char title[LEVEL_TITLE_MAX_LENGTH];
	u32 id;
	
	MemoryArena_t* allocArena;
	
	union
	{
		v2i size;
		struct { i32 width, height; };
	};
	v2i chunkSize;
	v2i chunkOffset;
	
	u32 nextRoomId;
	u32 numRooms;
	Room_t* rooms;
	
	u32 nextRoofGroupNumber;
	
	Tile_t* tiles;
	Entity_t** tileEntityPntrs;
	
	u32 nextEntityId;
	BktArray_t entities;
};

const char* GetViewModeStr(u8 viewMode)
{
	switch (viewMode)
	{
		case RoomViewMode_ChunksStrict:    return "Chunks Strict";
		case RoomViewMode_ChunksDefault:   return "Chunks Default";
		case RoomViewMode_ChunksSmooth:    return "Chunks Smooth";
		case RoomViewMode_FreeRoamStrict:  return "Free Roam Strict";
		case RoomViewMode_FreeRoamDefault: return "Free Roam Default";
		case RoomViewMode_FreeRoamSmooth:  return "Free Roam Smooth";
		default: return "Unknown";
	}
}

#endif //  _GAME_LEVEL_H
