/*
File:   game_level.cpp
Author: Taylor Robbins
Date:   11\05\2017
Description: 
	** Handles creation, copying, and general interaction with Level_t structures
*/


// +--------------------------------------------------------------+
// |                   Initialization Functions                   |
// +--------------------------------------------------------------+
void InitializeTile(Tile_t* tile, TileType_t type, u8 variant = 0x00, u8 color = 0x00, bool hasWall = false, u8 wallVariant = 0x00, u8 wallColor = 0x00, bool hasRoof = false, u8 roofVariant = 0x00, u8 roofColor = 0x00)
{
	ClearPointer(tile);
	tile->type = type;
	tile->variant = variant;
	tile->color = color;
	tile->hasWall = hasWall;
	tile->wallVariant = wallVariant;
	tile->wallColor = wallColor;
	tile->hasRoof = hasRoof;
	tile->roofVariant = roofVariant;
	tile->roofColor = roofColor;
	tile->roofGroupNumber = 0;
}
void InitializeTileWall(Tile_t* tile, u8 wallVariant = 0x00, u8 wallColor = 0x00)
{
	ClearPointer(tile);
	tile->type = Tile_Ground;
	tile->variant = 0x00;
	tile->color = 0x00;
	tile->hasWall = true;
	tile->wallVariant = wallVariant;
	tile->wallColor = wallColor;
	tile->hasRoof = false;
	tile->roofGroupNumber = 0;
}

void InitializeEntity(Entity_t* entity, EntityType_t type, v2i tilePos, u8 variant = 0x00)
{
	Assert(entity != nullptr);
	ClearPointer(entity);
	
	entity->flags = EntFlag_IsAlive;
	entity->tilePos = tilePos;
	entity->tilePosLast = tilePos;
	entity->type = type;
	entity->info.variant = variant;
	entity->info.color = 0x00;
	entity->info.rotation = Dir2_Down;
	// entity->drawRec = NewRec((r32)tilePos.x, (r32)tilePos.y, 1.0f, 1.0f);
}

u32 NumLevelTemplateBytesRequiredForSize(v2i size, u32* rowWidthOut = nullptr)
{
	Assert(size.width >= 0 && size.height >= 0);
	u32 rowWidth = size.x / 8;
	if ((size.x % 8) != 0) { rowWidth += 1; }
	if (rowWidthOut != nullptr) { *rowWidthOut = rowWidth; }
	return rowWidth * (u32)size.height;
}

void InitializeLevel(MemoryArena_t* arenaPntr, Level_t* level, v2i size, u32 numEntities, const char* title)
{
	Assert(arenaPntr != nullptr);
	Assert(level != nullptr);
	ClearPointer(level);
	
	level->id = 0;
	level->size = size;
	level->allocArena = arenaPntr;
	strncpy(level->title, title, LEVEL_TITLE_MAX_LENGTH-1);
	level->title[LEVEL_TITLE_MAX_LENGTH-1] = '\0';
	
	level->chunkSize = size;
	level->chunkOffset = Vec2i_Zero;
	level->nextRoomId = 1;
	level->numRooms = 0;
	level->rooms = nullptr;
	level->nextRoofGroupNumber = 1;
	
	level->tiles = PushArray(arenaPntr, Tile_t, level->width * level->height);
	for (i32 xPos = 0; xPos < level->width; xPos++)
	{
		for (i32 yPos = 0; yPos < level->height; yPos++)
		{
			InitializeTile(&level->tiles[yPos*level->width + xPos], Tile_Ground, GroundVariant_Default, 0x00);
		}
	}
	
	level->tileEntityPntrs = PushArray(arenaPntr, Entity_t*, level->width * level->height);
	for (i32 xPos = 0; xPos < level->width; xPos++)
	{
		for (i32 yPos = 0; yPos < level->height; yPos++)
		{
			level->tileEntityPntrs[yPos*level->width + xPos] = nullptr;
		}
	}
	
	level->nextEntityId = 1;
	CreateBucketArray(&level->entities, arenaPntr, sizeof(Entity_t), ENTITY_ALLOC_SIZE, numEntities);
}

void SetLevelTitle(Level_t* level, const char* title)
{
	Assert(level != nullptr);
	
	strncpy(level->title, title, LEVEL_TITLE_MAX_LENGTH-1);
	level->title[LEVEL_TITLE_MAX_LENGTH-1] = '\0';
}

bool AreTilesEqual(const Tile_t* tile1, const Tile_t* tile2)
{
	Assert(tile1 != nullptr);
	Assert(tile2 != nullptr);
	if (tile1->type != tile2->type) { return false; }
	if (tile1->variant != tile2->variant) { return false; }
	if (tile1->color != tile2->color) { return false; }
	if (tile1->hasWall != tile2->hasWall) { return false; }
	if (tile1->hasWall && tile1->wallVariant != tile2->wallVariant) { return false; }
	if (tile1->hasWall && tile1->wallColor != tile2->wallColor) { return false; }
	if (tile1->hasRoof != tile2->hasRoof) { return false; }
	if (tile1->hasRoof && tile1->roofVariant != tile2->roofVariant) { return false; }
	if (tile1->hasRoof && tile1->roofColor != tile2->roofColor) { return false; }
	return true;
}

bool IsEntityInLevel(const Level_t* level, const Entity_t* entity)
{
	if (level == nullptr) { return false; }
	if (entity == nullptr) { return false; }
	return IsPntrInBktArray(&level->entities, entity);
}

// +--------------------------------------------------------------+
// |                  Get Entity/Tile Functions                   |
// +--------------------------------------------------------------+
Tile_t* GetTileAt(Level_t* level, v2i tilePos)
{
	Assert(level != nullptr);
	Assert(level->tiles != nullptr);
	
	if (tilePos.x < 0 || tilePos.y < 0 || tilePos.x >= level->width || tilePos.y >= level->height)
	{
		return nullptr;
	}
	
	return &level->tiles[(tilePos.y * level->width) + tilePos.x];
}
const Tile_t* GetTileAt(const Level_t* level, v2i tilePos) //const version
{
	return (const Tile_t*)GetTileAt((Level_t*)level, tilePos);
}
Tile_t* GetTileAtExt(Level_t* level, v2i tilePos)
{
	Assert(level != nullptr);
	Assert(level->tiles != nullptr);
	
	if (tilePos.x < 0) { tilePos.x = 0; }
	if (tilePos.x >= level->width) { tilePos.x = level->width-1; }
	if (tilePos.y < 0) { tilePos.y = 0; }
	if (tilePos.y >= level->height) { tilePos.y = level->height-1; }
	
	return GetTileAt(level, tilePos);
}
const Tile_t* GetTileAtExt(const Level_t* level, v2i tilePos) //const version
{
	return (const Tile_t*)GetTileAtExt((Level_t*)level, tilePos);
}

reci GetLevelChunkLimits(const Level_t* level)
{
	reci result = NewReci(0, 0, (level->width - level->chunkOffset.x) / level->chunkSize.width, (level->height - level->chunkOffset.y) / level->chunkSize.height);
	if (((level->width - level->chunkOffset.x) % level->chunkSize.width) > 0) { result.width++; }
	if (((level->height - level->chunkOffset.y) % level->chunkSize.height) > 0) { result.height++; }
	if (level->chunkOffset.x > 0) { result.x--; result.width++; }
	if (level->chunkOffset.y > 0) { result.y--; result.height++; }
	return result;
}

reci GetEntityRegionRec(const Level_t* level, const Entity_t* entity)
{
	Assert(level != nullptr);
	Assert(entity != nullptr);
	reci result = NewReci(entity->tilePos, Vec2i_One);
	if (entity->type == Entity_Region)
	{
		v2i size = NewVec2i(entity->info.region.width, entity->info.region.height);
		result = NewReci(entity->tilePos.x - (size.width/2), entity->tilePos.y - (size.height/2), size.width, size.height);
	}
	if (entity->type == Entity_RespawnZone)
	{
		v2i size = NewVec2i(entity->info.respawnZone.width, entity->info.respawnZone.height);
		result = NewReci(entity->tilePos.x - (size.width/2), entity->tilePos.y - (size.height/2), size.width, size.height);
	}
	return result;
}

reci GetEntityCollisionRec(const Level_t* level, const Entity_t* entity)
{
	Assert(entity != nullptr);
	reci result = NewReci(entity->tilePos, Vec2i_One);
	if (entity->type == Entity_Door)
	{
		result.size = NewVec2i(entity->info.door.width, entity->info.door.height);
	}
	return result;
}

v2i GetChunkPos(const Level_t* level, v2i tilePos) //pre-declared in game_func_defs.h
{
	// if (tilePos.x < 0 || tilePos.y < 0 || tilePos.x >= level->width || tilePos.y >= level->height) { return Vec2i_Zero; }
	v2i chunkPos = Vec2Floori(Vec2Divide(NewVec2(tilePos - level->chunkOffset), NewVec2(level->chunkSize)));
	return chunkPos;
}
Room_t* GetRoomForChunk(Level_t* level, v2i chunkPos) //pre-declared in game_func_defs.h
{
	for (u32 rIndex = 0; rIndex < level->numRooms; rIndex++)
	{
		Room_t* roomPntr = &level->rooms[rIndex];
		if (chunkPos.x >= roomPntr->chunkRec.x &&
			chunkPos.y >= roomPntr->chunkRec.y &&
			chunkPos.x < roomPntr->chunkRec.x + roomPntr->chunkRec.width &&
			chunkPos.y < roomPntr->chunkRec.y + roomPntr->chunkRec.height)
		{
			return roomPntr;
		}
	}
	return nullptr;
}
const Room_t* GetRoomForChunk(const Level_t* level, v2i chunkPos) //const version pre-declared in game_func_defs.h
{
	return (const Room_t*)GetRoomForChunk((Level_t*)level, chunkPos);
}
Room_t* GetRoomForPos(Level_t* level, v2i tilePos) //pre-declared in game_func_defs.h
{
	v2i chunkPos = GetChunkPos(level, tilePos);
	return GetRoomForChunk(level, chunkPos);
}
const Room_t* GetRoomForPos(const Level_t* level, v2i tilePos) //const version pre-declared in game_func_defs.h
{
	return (const Room_t*)GetRoomForPos((Level_t*)level, tilePos);
}

Room_t* GetRoomById(Level_t* level, u32 roomId)
{
	Assert(level != nullptr);
	for (u32 rIndex = 0; rIndex < level->numRooms; rIndex++)
	{
		Assert(level->rooms != nullptr);
		if (level->rooms[rIndex].id == roomId) { return &level->rooms[rIndex]; }
	}
	return nullptr;
}
const Room_t* GetRoomById(const Level_t* level, u32 roomId) //const version
{
	return (const Room_t*)GetRoomById((Level_t*)level, roomId);
}

reci GetChunkRec(const Level_t* level, v2i chunkPos) //pre-declared in app_func_defs.h
{
	return NewReci(level->chunkOffset + Vec2iMultiply(chunkPos, level->chunkSize), level->chunkSize);
}
reci GetRoomRec(const Level_t* level, u32 roomId)
{
	Assert(level != nullptr);
	const Room_t* room = GetRoomById(level, roomId);
	Assert(room != nullptr);
	return NewReci(level->chunkOffset + Vec2iMultiply(room->chunkRec.topLeft, level->chunkSize), Vec2iMultiply(room->chunkRec.size, level->chunkSize));
}

bool ShouldLevelChunkBeCounted(const Level_t* level, v2i chunkPos)
{
	bool result = true;
	reci chunkRec = GetChunkRec(level, chunkPos);
	const LoopEntitiesByType(canceller, level, Entity_RoomCanceller)
	{
		if (canceller->tilePos.x >= chunkRec.x && canceller->tilePos.x < chunkRec.x + chunkRec.width &&
			canceller->tilePos.y >= chunkRec.y && canceller->tilePos.y < chunkRec.y + chunkRec.height)
		{
			result = false;
			break;
		}
	}
	return result;
}

u64 GetLevelRandomSeed(const Level_t* level)
{
	u64 result = 0;
	const LoopAllEntities(entity, level)
	{
		result += (u64)entity->id;
	}
	return result;
}

// +--------------------------------------------------------------+
// |                 Level Manipulation Functions                 |
// +--------------------------------------------------------------+
Room_t* AddRoom(Level_t* level, u8 viewMode, reci chunkRec)
{
	Assert(level != nullptr);
	Assert(level->allocArena != nullptr);
	Assert(viewMode < RoomViewMode_NumModes);
	
	Room_t* newSpace = PushArray(level->allocArena, Room_t, level->numRooms+1);
	if (level->rooms != nullptr)
	{
		if (level->numRooms > 0)
		{
			MyMemCopy(newSpace, level->rooms, sizeof(Room_t) * level->numRooms);
		}
		if (ArenaCanPop(level->allocArena, level->rooms))
		{
			ArenaPop(level->allocArena, level->rooms);
		}
	}
	level->rooms = newSpace;
	
	Room_t* newRoom = &level->rooms[level->numRooms];
	level->numRooms++;
	newRoom->id = level->nextRoomId;
	level->nextRoomId++;
	
	newRoom->chunkRec = chunkRec;
	newRoom->viewMode = viewMode;
	return newRoom;
}
Room_t* AddExistingRoom(Level_t* level, const Room_t* roomToCopy)
{
	Assert(level != nullptr);
	Assert(level->allocArena != nullptr);
	Assert(roomToCopy != nullptr);
	Assert(roomToCopy->viewMode < RoomViewMode_NumModes);
	
	Room_t* newSpace = PushArray(level->allocArena, Room_t, level->numRooms+1);
	if (level->rooms != nullptr)
	{
		if (level->numRooms > 0)
		{
			MyMemCopy(newSpace, level->rooms, sizeof(Room_t) * level->numRooms);
		}
		if (ArenaCanPop(level->allocArena, level->rooms))
		{
			ArenaPop(level->allocArena, level->rooms);
		}
	}
	level->rooms = newSpace;
	
	Room_t* newRoom = &level->rooms[level->numRooms];
	if (GetRoomById(level, roomToCopy->id))
	{
		PrintLine_E("Id conflict when placing existing room. Changed id %u->%u", roomToCopy->id, level->nextRoomId);
		newRoom->id = level->nextRoomId;
		level->nextRoomId++;
	}
	else
	{
		newRoom->id = roomToCopy->id;
		if (level->nextRoomId <= roomToCopy->id)
		{
			level->nextRoomId = roomToCopy->id + 1;
		}
	}
	level->numRooms++;
	
	newRoom->viewMode = roomToCopy->viewMode;
	newRoom->chunkRec = roomToCopy->chunkRec;
	return newRoom;
}
void RemoveRoom(Level_t* level, u32 roomIndex)
{
	Assert(level != nullptr);
	Assert(roomIndex < level->numRooms);
	Assert(level->rooms != nullptr);
	
	//shift things down by one
	for (u32 rIndex = roomIndex; rIndex+1 < level->numRooms; rIndex++)
	{
		MyMemCopy(&level->rooms[rIndex], &level->rooms[rIndex+1], sizeof(Room_t));
	}
	level->numRooms--;
	if (level->numRooms == 0)
	{
		Assert(level->allocArena != nullptr);
		if (ArenaCanPop(level->allocArena, level->rooms))
		{
			ArenaPop(level->allocArena, level->rooms);
		}
		level->rooms = nullptr;
	}
}

void AddEntityToTileLookups(Level_t* level, Entity_t* entityPntr)
{
	Assert(level != nullptr);
	Assert(entityPntr != nullptr);
	Assert(IsEntityInLevel(level, entityPntr));
	Assert(IsEntityAlive(entityPntr));
	
	v2i newTilePos = entityPntr->tilePos;
	if (newTilePos.x < 0 || newTilePos.y < 0 || newTilePos.x >= level->width || newTilePos.y >= level->height) { return; }
	
	Entity_t* newLookupList = level->tileEntityPntrs[newTilePos.y * level->width + newTilePos.x];
	while (newLookupList != nullptr && newLookupList->next != nullptr) { newLookupList = newLookupList->next; } //walk to the end of the linked list
	
	if (newLookupList == nullptr)
	{
		Assert(level->tileEntityPntrs[newTilePos.y * level->width + newTilePos.x] == nullptr);
		level->tileEntityPntrs[newTilePos.y * level->width + newTilePos.x] = entityPntr;
	}
	else
	{
		newLookupList->next = entityPntr;
	}
	entityPntr->next = nullptr;
}
void RemoveEntityFromTileLookups(Level_t* level, Entity_t* entityPntr)
{
	Assert(level != nullptr);
	Assert(entityPntr != nullptr);
	Assert(IsEntityInLevel(level, entityPntr));
	
	//Remove from old linked list
	v2i oldTilePos = entityPntr->tilePos;
	if (oldTilePos.x >= 0 && oldTilePos.y >= 0 && oldTilePos.x < level->width && oldTilePos.y < level->height)
	{
		Entity_t* oldLookupList = level->tileEntityPntrs[oldTilePos.y * level->width + oldTilePos.x];
		Entity_t* previous = nullptr;
		while (oldLookupList != nullptr && oldLookupList != entityPntr) { previous = oldLookupList; oldLookupList = oldLookupList->next; }
		
		if (oldLookupList != nullptr)
		{
			Assert(oldLookupList == entityPntr);
			if (previous != nullptr)
			{
				previous->next = entityPntr->next;
			}
			else
			{
				Assert(level->tileEntityPntrs[oldTilePos.y * level->width + oldTilePos.x] == entityPntr);
				level->tileEntityPntrs[oldTilePos.y * level->width + oldTilePos.x] = entityPntr->next;
			}
		}
		else
		{
			//Couldn't find the entity in the list it should be in, our list must be corrupt or outdated. Or the entity was previously moved without calling updating the lists
			Assert(false);
		}
	}
	entityPntr->next = nullptr;
}
void RefreshEntityTileLookups(Level_t* level)
{
	Assert(level != nullptr);
	
	Assert(level->tileEntityPntrs != nullptr);
	for (i32 yPos = 0; yPos < level->height; yPos++)
	{
		for (i32 xPos = 0; xPos < level->width; xPos++)
		{
			level->tileEntityPntrs[yPos * level->width + xPos] = nullptr;
		}
	}
	
	LoopAllEntities(entityPntr, level) { AddEntityToTileLookups(level, entityPntr); }
}

void MoveEntity(Level_t* level, Entity_t* entityPntr, v2i newTilePos)
{
	Assert(level != nullptr);
	Assert(entityPntr != nullptr);
	Assert(IsEntityInLevel(level, entityPntr));
	Assert(IsEntityAlive(entityPntr));
	
	RemoveEntityFromTileLookups(level, entityPntr);
	entityPntr->tilePos = newTilePos;
	AddEntityToTileLookups(level, entityPntr);
}
void KillEntity(Level_t* level, Entity_t* entityPntr)
{
	Assert(level != nullptr);
	Assert(entityPntr != nullptr);
	Assert(IsEntityInLevel(level, entityPntr));
	Assert(IsEntityAlive(entityPntr));
	
	RemoveEntityFromTileLookups(level, entityPntr);
	FlagUnset(entityPntr->flags, EntFlag_IsAlive);
}

Entity_t* AllocEntity(Level_t* level, EntityType_t type, v2i tilePos, u8 variant = 0x00)
{
	Assert(level != nullptr);
	Assert(level->allocArena != nullptr);
	Entity_t* result = nullptr;
	
	for (u32 eIndex = 0; eIndex < level->entities.length; eIndex++)
	{
		Entity_t* entityPntr = BktArrayGet(&level->entities, Entity_t, eIndex);
		Assert(entityPntr != nullptr);
		if (!IsEntityAlive(entityPntr))
		{
			result = entityPntr;
			break;
		}
	}
	if (result == nullptr)
	{
		result = BktArrayAdd(&level->entities, Entity_t);
		Assert(result != nullptr);
	}
	
	Assert(result != nullptr);
	InitializeEntity(result, type, tilePos, variant);
	AddEntityToTileLookups(level, result);
	
	// +==============================+
	// |  Keep track of nextEntityId  |
	// +==============================+
	result->id = level->nextEntityId;
	level->nextEntityId++;
	
	// PrintLine_D("Placed entity %u. Next entity %u", result->id, level->nextEntityId);
	return result;
}

//NOTE: This is mainly used during deserialization
Entity_t* AllocExistingEntity(Level_t* level, const Entity_t* entityToCopy)
{
	Assert(level != nullptr);
	Assert(level->allocArena != nullptr);
	Entity_t* result = nullptr;
	
	bool idExists = DoesEntityIdExist(level, entityToCopy->id);
	
	for (u32 eIndex = 0; eIndex < level->entities.length; eIndex++)
	{
		Entity_t* entityPntr = BktArrayGet(&level->entities, Entity_t, eIndex);
		Assert(entityPntr != nullptr);
		if (!IsEntityAlive(entityPntr))
		{
			result = entityPntr;
			break;
		}
	}
	if (result == nullptr)
	{
		result = BktArrayAdd(&level->entities, Entity_t);
		Assert(result != nullptr);
	}
	
	Assert(result != nullptr);
	MyMemCopy(result, entityToCopy, sizeof(Entity_t));
	AddEntityToTileLookups(level, result);
	
	// +==============================+
	// |  Keep track of nextEntityId  |
	// +==============================+
	if (idExists)
	{
		PrintLine_E("Id conflict when placing existing entity. Changed id %u->%u", result->id, level->nextEntityId);
		result->id = level->nextEntityId;
		level->nextEntityId++;
	}
	else if (result->id >= level->nextEntityId)
	{
		level->nextEntityId = result->id + 1;
	}
	
	//Fix player direction from old maps
	if (result->type == Entity_Player && result->info.rotation == Dir2_None) { result->info.rotation = Dir2_Down; }
	
	// PrintLine_D("Placed entity %u. Next entity %u", result->id, level->nextEntityId);
	return result;
}

void CreateDummyLevel(MemoryArena_t* arenaPntr, Level_t* level, v2i size)
{
	Assert(arenaPntr != nullptr);
	Assert(level != nullptr);
	
	InitializeLevel(arenaPntr, level, size, 2, "New Level");
	
	for (i32 yPos = 0; yPos < level->height; yPos++)
	{
		for (i32 xPos = 0; xPos < level->width; xPos++)
		{
			u32 tIndex = (yPos*level->width) + xPos;
			Tile_t* tilePntr = &level->tiles[tIndex];
			
			if (xPos == 0 || yPos == 0 || xPos == level->width-1 || yPos == level->height-1)
			{
				InitializeTileWall(tilePntr);
			}
			else
			{
				InitializeTile(tilePntr, Tile_Ground);
			}
		}
	}
	
	AllocEntity(level, Entity_Exit, size - Vec2i_One*2);
	AllocEntity(level, Entity_Player, Vec2i_One);
}

void CopyLevel(MemoryArena_t* arenaPntr, Level_t* dest, const Level_t* source)
{
	Assert(arenaPntr != nullptr);
	Assert(dest != nullptr);
	Assert(source != nullptr);
	
	MyMemCopy(dest, source, sizeof(Level_t));
	dest->allocArena = arenaPntr;
	dest->rooms = nullptr;
	dest->tiles = nullptr;
	ClearStruct(dest->entities);
	
	if (source->numRooms > 0)
	{
		dest->rooms = PushArray(arenaPntr, Room_t, source->numRooms);
		MyMemCopy(dest->rooms, source->rooms, sizeof(Room_t) * source->numRooms);
	}
	dest->numRooms = source->numRooms;
	
	dest->tiles = PushArray(arenaPntr, Tile_t, dest->width * dest->height);
	MyMemCopy(dest->tiles, source->tiles, sizeof(Tile_t) * dest->width * dest->height);
	
	CreateBucketArray(&dest->entities, arenaPntr, sizeof(Entity_t), ENTITY_ALLOC_SIZE, source->entities.length);
	for (u32 eIndex = 0; eIndex < source->entities.length; eIndex++)
	{
		const Entity_t* srcEntity = BktArrayGet(&source->entities, Entity_t, eIndex);
		Assert(srcEntity != nullptr);
		Entity_t* destEntity = BktArrayAdd(&dest->entities, Entity_t);
		Assert(destEntity != nullptr);
		MyMemCopy(destEntity, srcEntity, sizeof(Entity_t));
		//TODO: Fix up entity pointers if we have any
	}
	
	dest->tileEntityPntrs = PushArray(arenaPntr, Entity_t*, dest->width * dest->height);
	RefreshEntityTileLookups(dest);
}

void DeleteLevel(Level_t* level)
{
	Assert(level != nullptr);
	
	if (level->rooms != nullptr)
	{
		Assert(level->allocArena != nullptr);
		ArenaPop(level->allocArena, level->rooms);
		level->rooms = nullptr;
		level->numRooms = 0;
	}
	if (level->tiles != nullptr)
	{
		Assert(level->allocArena != nullptr);
		ArenaPop(level->allocArena, level->tiles);
		level->tiles = nullptr;
	}
	if (level->tileEntityPntrs != nullptr)
	{
		Assert(level->allocArena != nullptr);
		ArenaPop(level->allocArena, level->tileEntityPntrs);
		level->tileEntityPntrs = nullptr;
	}
	DestroyBucketArray(&level->entities);
}

void ResizeLevel(MemoryArena_t* arenaPntr, const Level_t* inLevel, Level_t* outLevel, v2i newSize, v2i offset)
{
	u32 numEntities = 0;
	const LoopAllEntities(countEntity, inLevel) { numEntities++; }
	
	InitializeLevel(arenaPntr, outLevel, newSize, numEntities, inLevel->title);
	outLevel->chunkSize = inLevel->chunkSize;
	outLevel->chunkOffset = inLevel->chunkOffset + offset;
	outLevel->id = inLevel->id;
	
	if (inLevel->numRooms > 0)
	{
		outLevel->rooms = PushArray(arenaPntr, Room_t, inLevel->numRooms);
		MyMemCopy(outLevel->rooms, inLevel->rooms, sizeof(Room_t) * inLevel->numRooms);
	}
	outLevel->numRooms = inLevel->numRooms;
	
	//Reset the chunkOffset to canonical position (offsetting rooms as we do, so they don't move)
	while (outLevel->chunkOffset.x >= outLevel->chunkSize.width)
	{
		outLevel->chunkOffset.x -= outLevel->chunkSize.width;
		for (u32 rIndex = 0; rIndex < outLevel->numRooms; rIndex++) { outLevel->rooms[rIndex].chunkRec.x += 1; }
	}
	while (outLevel->chunkOffset.y >= outLevel->chunkSize.height)
	{
		outLevel->chunkOffset.y -= outLevel->chunkSize.height;
		for (u32 rIndex = 0; rIndex < outLevel->numRooms; rIndex++) { outLevel->rooms[rIndex].chunkRec.y += 1; }
	}
	while (outLevel->chunkOffset.x < 0)
	{
		outLevel->chunkOffset.x += outLevel->chunkSize.width;
		for (u32 rIndex = 0; rIndex < outLevel->numRooms; rIndex++) { outLevel->rooms[rIndex].chunkRec.x -= 1; }
	}
	while (outLevel->chunkOffset.y < 0)
	{
		outLevel->chunkOffset.y += outLevel->chunkSize.height;
		for (u32 rIndex = 0; rIndex < outLevel->numRooms; rIndex++) { outLevel->rooms[rIndex].chunkRec.y -= 1; }
	}
	
	//delete rooms that fell outside the level
	for (u32 rIndex = 0; rIndex < outLevel->numRooms; )
	{
		if (outLevel->chunkOffset.x + (outLevel->rooms[rIndex].chunkRec.x + outLevel->rooms[rIndex].chunkRec.width) * outLevel->chunkSize.width < 0 ||
			outLevel->chunkOffset.y + (outLevel->rooms[rIndex].chunkRec.y + outLevel->rooms[rIndex].chunkRec.height) * outLevel->chunkSize.height < 0 ||
			outLevel->chunkOffset.x + outLevel->rooms[rIndex].chunkRec.x * outLevel->chunkSize.width >= outLevel->width ||
			outLevel->chunkOffset.y + outLevel->rooms[rIndex].chunkRec.y * outLevel->chunkSize.height >= outLevel->height)
		{
			RemoveRoom(outLevel, rIndex);
		}
		else { rIndex++; }
	}
	
	// +==============================+
	// |       Copy Tiles Over        |
	// +==============================+
	for (i32 yPos = 0; yPos < outLevel->height; yPos++)
	{
		for (i32 xPos = 0; xPos < outLevel->width; xPos++)
		{
			v2i tilePos = NewVec2i(xPos, yPos);
			v2i sourcePos = tilePos - offset;
			
			Tile_t* destTile = GetTileAt(outLevel, tilePos);
			const Tile_t* sourceTile = GetTileAtExt(inLevel, sourcePos);
			
			if (sourceTile != nullptr)
			{
				MyMemCopy(destTile, sourceTile, sizeof(Tile_t));
				destTile->tileConnections = 0x00;
				destTile->wallConnections = 0x00;
				destTile->roofConnections = 0x00;
			}
			else
			{
				InitializeTileWall(destTile);
			}
		}
	}
	
	// +==============================+
	// |      Copy Entities Over      |
	// +==============================+
	const LoopAllEntities(inEntity, inLevel)
	{
		v2i newPos = inEntity->tilePos + offset;
		//NOTE: We don't copy the entity into the new level if it's outside the new bounds
		if (newPos.x >= 0 && newPos.y >= 0 && newPos.x < newSize.width && newPos.y < newSize.height)
		{
			Entity_t* newEntity = AllocExistingEntity(outLevel, inEntity);
			newEntity->tilePos += offset;
			newEntity->tilePosLast += offset;
			// newEntity->drawRec = NewRec((r32)newEntity->tilePos.x, (r32)newEntity->tilePos.y, 1.0f, 1.0f);
		}
	}
	
	RefreshEntityTileLookups(outLevel);
}

u32 GetLevelMemorySize(const Level_t* level)
{
	u32 result = 0;
	if (level->rooms != nullptr && level->numRooms > 0)
	{
		result += (sizeof(Room_t) * level->numRooms);
	}
	if (level->tiles != nullptr && level->width > 0 && level->height > 0)
	{
		result += (sizeof(Tile_t) * (u32)level->width * (u32)level->height);
	}
	//TODO: Include the size of entities BktArray, the moveSpaceDatas, entityMoveInfos, and tileEntityPntrs
	return result;
}

bool DoRoofsConnect(const Tile_t* tile1, const Tile_t* tile2)
{
	Assert(tile1 != nullptr);
	Assert(tile2 != nullptr);
	
	if (!tile1->hasRoof && !tile2->hasRoof) { return true; }
	if (!tile1->hasRoof || !tile2->hasRoof) { return false; }
	if (tile1->roofVariant == tile2->roofVariant) { return true; }
	
	return false;
}

bool IsV2iInArray(const v2i* arrayPntr, u32 arraySize, v2i target, u32* indexOut = nullptr)
{
	for (u32 vIndex = 0; vIndex < arraySize; vIndex++)
	{
		if (arrayPntr[vIndex] == target)
		{
			if (indexOut != nullptr) { *indexOut = vIndex; }
			return true;
		}
	}
	return false;
}

void FloodFillRoofGroup(Level_t* level, v2i startPos, u32 roofGroupNumber)
{
	u32 searchedCount = 0;
	u32 searchedMaxSize = level->width*level->height;
	v2i* searched = PushArray(TempArena, v2i, searchedMaxSize);
	u32 nextCount = 1;
	u32 nextMaxSize = level->width*level->height;
	v2i* next = PushArray(TempArena, v2i, nextMaxSize);
	next[0] = startPos;
	Tile_t* srcTile = GetTileAt(level, startPos);
	Assert(srcTile != nullptr);
	
	while (nextCount > 0)
	{
		v2i tilePos = next[0];
		Tile_t* nextTile = GetTileAt(level, tilePos);
		
		Assert(searchedCount < searchedMaxSize);
		searched[searchedCount] = tilePos;
		searchedCount++;
		
		for (u32 nIndex = 0; nIndex+1 < nextCount; nIndex++)
		{
			MyMemCopy(&next[nIndex], &next[nIndex+1], sizeof(v2i));
		}
		nextCount--;
		
		if (DoRoofsConnect(srcTile, nextTile))
		{
			nextTile->roofGroupNumber = roofGroupNumber;
			
			if (tilePos.x > 0)
			{
				v2i leftPos = tilePos + NewVec2i(Dir2_Left);
				if (!IsV2iInArray(searched, searchedCount, leftPos) && !IsV2iInArray(next, nextCount, leftPos))
				{
					Assert(nextCount < nextMaxSize);
					next[nextCount] = leftPos;
					nextCount++;
				}
			}
			if (tilePos.x+1 < level->width)
			{
				v2i rightPos = tilePos + NewVec2i(Dir2_Right);
				if (!IsV2iInArray(searched, searchedCount, rightPos) && !IsV2iInArray(next, nextCount, rightPos))
				{
					Assert(nextCount < nextMaxSize);
					next[nextCount] = rightPos;
					nextCount++;
				}
			}
			if (tilePos.y+1 < level->height)
			{
				v2i downPos = tilePos + NewVec2i(Dir2_Down);
				if (!IsV2iInArray(searched, searchedCount, downPos) && !IsV2iInArray(next, nextCount, downPos))
				{
					Assert(nextCount < nextMaxSize);
					next[nextCount] = downPos;
					nextCount++;
				}
			}
			if (tilePos.y > 0)
			{
				v2i upPos = tilePos + NewVec2i(Dir2_Up);
				if (!IsV2iInArray(searched, searchedCount, upPos) && !IsV2iInArray(next, nextCount, upPos))
				{
					Assert(nextCount < nextMaxSize);
					next[nextCount] = upPos;
					nextCount++;
				}
			}
		}
	}
}

void LevelFillRoofGroupNumbers(Level_t* level)
{
	Assert(level != nullptr);
	for (i32 yPos = 0; yPos < level->height; yPos++)
	{
		for (i32 xPos = 0; xPos < level->width; xPos++)
		{
			v2i tilePos = NewVec2i(xPos, yPos);
			Tile_t* tile = GetTileAt(level, tilePos);
			Assert(tile != nullptr);
			if (tile->roofGroupNumber == 0)
			{
				u32 newGroupNumber = level->nextRoofGroupNumber;
				level->nextRoofGroupNumber++;
				FloodFillRoofGroup(level, tilePos, newGroupNumber);
			}
		}
	}
}
void LevelUpdateRoofGroupsAround(Level_t* level, v2i tilePos)
{
	Assert(level != nullptr);
	Assert(tilePos.x >= 0 && tilePos.x < level->width);
	Assert(tilePos.y >= 0 && tilePos.y < level->height);
	Tile_t* startTile = GetTileAt(level, tilePos);
	Assert(startTile != nullptr);
	startTile->roofGroupNumber = 0;
	LevelFillRoofGroupNumbers(level);
}
void LevelGenerateAllRoofGroups(Level_t* level)
{
	Assert(level != nullptr);
	for (i32 yPos = 0; yPos < level->height; yPos++)
	{
		for (i32 xPos = 0; xPos < level->width; xPos++)
		{
			v2i tilePos = NewVec2i(xPos, yPos);
			Tile_t* tile = GetTileAt(level, tilePos);
			Assert(tile != nullptr);
			tile->roofGroupNumber = 0;
		}
	}
	level->nextRoofGroupNumber = 1;
	LevelFillRoofGroupNumbers(level);
}

void UpdateTileConnectionsAround(Level_t* level, v2i tilePos)
{
	for (i32 yPos = tilePos.y-1; yPos <= tilePos.y+1; yPos++)
	{
		for (i32 xPos = tilePos.x-1; xPos <= tilePos.x+1; xPos++)
		{
			Tile_t* tilePntr = GetTileAt(level, NewVec2i(xPos, yPos));
			if (tilePntr != nullptr) { tilePntr->tileConnections = 0x00; }
		}
	}
}

void UpdateRoofConnectionsAround(Level_t* level, v2i tilePos)
{
	for (i32 yPos = tilePos.y-1; yPos <= tilePos.y+1; yPos++)
	{
		for (i32 xPos = tilePos.x-1; xPos <= tilePos.x+1; xPos++)
		{
			Tile_t* tilePntr = GetTileAt(level, NewVec2i(xPos, yPos));
			if (tilePntr != nullptr) { tilePntr->roofConnections = 0x00; }
		}
	}
}

bool IsPlayerInSameChunk(const Level_t* level, v2i tilePos)
{
	v2i targetChunkPos = GetChunkPos(level, tilePos);
	const LoopEntitiesByType(player, level, Entity_Player)
	{
		v2i playerChunk = GetChunkPos(level, player->tilePos);
		if (playerChunk == targetChunkPos) { return true; }
	}
	return false;
}
bool IsPlayerInSameRoom(const Level_t* level, v2i tilePos)
{
	v2i targetChunk = GetChunkPos(level, tilePos);
	const Room_t* targetRoom = GetRoomForChunk(level, targetChunk);
	const LoopEntitiesByType(player, level, Entity_Player)
	{
		v2i playerChunk = GetChunkPos(level, player->tilePos);
		const Room_t* playerRoom = GetRoomForChunk(level, playerChunk);
		if (playerChunk == targetChunk || (targetRoom != nullptr && targetRoom == playerRoom)) { return true; }
	}
	return false;
}

reci GetActiveAreaRegion(const Level_t* level, v2i tilePos, ActiveArea_t area)
{
	switch (area)
	{
		case ActiveArea_Level: return NewReci(Vec2i_Zero, level->size);
		case ActiveArea_Chunk:
		{
			v2i chunkPos = GetChunkPos(level, tilePos);
			return GetChunkRec(level, chunkPos);
		}
		case ActiveArea_Room:
		{
			const Room_t* room = GetRoomForPos(level, tilePos);
			if (room != nullptr)
			{
				return GetRoomRec(level, room->id);
			}
			else
			{
				v2i chunkPos = GetChunkPos(level, tilePos);
				return GetChunkRec(level, chunkPos);
			}
		}
		default: Assert(false); return Reci_Zero;
	}
}

bool IsInSameAreaAs(const Level_t* level, v2i tilePos, v2i comparePos, ActiveArea_t area)
{
	switch (area)
	{
		case ActiveArea_Level: return true;
		case ActiveArea_Chunk:
		{
			v2i posChunk = GetChunkPos(level, tilePos);
			v2i compareChunk = GetChunkPos(level, comparePos);
			return (posChunk == compareChunk);
		}
		case ActiveArea_Room:
		{
			const Room_t* posRoom = GetRoomForPos(level, tilePos);
			const Room_t* compareRoom = GetRoomForPos(level, comparePos);
			if (posRoom != nullptr)
			{
				return (posRoom == compareRoom);
			}
			else
			{
				v2i posChunk = GetChunkPos(level, tilePos);
				v2i compareChunk = GetChunkPos(level, comparePos);
				return (posChunk == compareChunk);
			}
		}
		default: Assert(false); return false;
	}
}