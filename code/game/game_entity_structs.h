/*
File:   game_entity_structs.h
Author: Taylor Robbins
Date:   01\05\2018
Description:
	** Holds all the enumerations for enitity options as well as the EntityInfo_t structure itself
#included from game_entity.h
*/

#ifndef _APP_ENTITY_TYPES_H
#define _APP_ENTITY_TYPES_H

// +--------------------------------------------------------------+
// |                    EntityInfo_t Structure                    |
// +--------------------------------------------------------------+
struct EntityInfo_t
{
	u8 variant;
	Dir2_t rotation;
	u8 group;
	u8 color;
	union
	{
		u8 options[NUM_ENTITY_OPTIONS];
		// +==============================+
		// |        Entity_Player         |
		// +==============================+
		struct
		{
			u8 startWithSword:1;
			u8 coopCollision:1;
			u8 autoRespawn:1;
			u8 offscreenIndication:1;
			u8 ignoreActivateRelease:1;
			u8 unused:3;
			u8 portalMoveDir; //used for keeping track of player input consistency through portals
			u8 portalExitDir; //used for keeping track of player input consistency through portals
		} player;
		
		// +==============================+
		// |         Entity_Exit          |
		// +==============================+
		struct
		{
			u8 hideOffscreenIndication:1;
			u8 unused:7;
		} exit;
		
		// +==============================+
		// |        Entity_Diamond        |
		// +==============================+
		struct
		{
			u8 sparkle:1;
			u8 unused:7;
			u8 count;
		} diamond;
		
		// +==============================+
		// |          Entity_Key          |
		// +==============================+
		struct
		{
			u8 count;
		} key;
		
		// +==============================+
		// |         Entity_Door          |
		// +==============================+
		struct
		{
			u8 width;
			u8 height;
		} door;
		
		// +==============================+
		// |          Entity_Box          |
		// +==============================+
		struct
		{
			u8 ledEnabled:1;
			u8 lockable:1;
			u8 locked:1;
			u8 unused1:5;
			u8 trigger;
			u8 handles;
			u8 blocked;
			u8 gravity;
			u8 magnets;
			u8 repels;
		} box;
		
		// +==============================+
		// |     Entity_PressurePlate     |
		// +==============================+
		struct
		{
			u8 singlePress:1;
			u8 ununsed:7;
			u8 triggerType;
		} pressurePlate;
		
		// +==============================+
		// |        Entity_LedPad         |
		// +==============================+
		struct
		{
			u8 singleTrigger:1;
			u8 stayLit:1;
			u8 disappearWhenPermaLit:1;
			u8 stayOnWhenAllLit:1;
			u8 allowMovingEntities:1;
			u8 unused:3;
			u8 connectionGroup;
			u8 triggerType;
			u8 requiredType;
		} ledPad;
		
		// +==============================+
		// |        Entity_LedDoor        |
		// +==============================+
		struct
		{
			u8 fullyDisappear:1;
			u8 unused:7;
			u8 connectionGroup;
			u8 numRequired;
		} ledDoor;
		
		// +==============================+
		// |      Entity_DiamondDoor      |
		// +==============================+
		struct
		{
			u8 showUiCounter:1;
			u8 reappear:1;
			u8 singleTrigger:1;
			u8 ignoreHollowed:1;
			u8 unused:4;
			u8 numIgnored;
			u8 countArea;
			u8 diamondType;
			u8 triggerType;
		} diamondDoor;
		
		// +==============================+
		// |         Entity_Rail          |
		// +==============================+
		struct
		{
			u8 innerEdge:1;
			u8 outerEdge:1;
			u8 singleTrigger:1;
			u8 fullyDisappear:1;
			u8 unused:4;
			u8 triggerType;
			u8 corners;
		} rail;
		
		// +==============================+
		// |         Entity_Lever         |
		// +==============================+
		struct
		{
			u8 singlePull:1;
			u8 isActivated:1;
			u8 holdState:1;
			u8 globalTied:1;
			u8 hideHintBubble:1;
			u8 unused:3;
			u8 triggerType;
		} lever;
		
		// +==============================+
		// |         Entity_Peach         |
		// +==============================+
		struct
		{
			u8 powerups;
		} peach;
		
		// +==============================+
		// |        Entity_Pusher         |
		// +==============================+
		struct
		{
			u8 deactivated:1;
			u8 singleTrigger:1;
			u8 autoActivate:1;
			u8 autoDeactivate:1;
			u8 unused:4;
			u8 triggerType;
		} pusher;
		
		// +==============================+
		// |      Entity_Teleporter       |
		// +==============================+
		struct
		{
			u8 deactivated:1;
			u8 instantView:1;
			u8 singleTrigger:1;
			u8 fullyDisappear:1;
			u8 unused:4;
			u8 type;
			u8 connectionGroup;
			u8 triggerType;
		} teleporter;
		
		// +==============================+
		// |      Entity_ToggleWall       |
		// +==============================+
		struct
		{
			u8 deactivated:1;
			u8 fullyDisappear:1;
			u8 spawnParticles:1;
			u8 unused:5;
			u8 triggerType;
		} toggleWall;
		
		// +==============================+
		// |         Entity_Timer         |
		// +==============================+
		struct
		{
			u8 deactivated:1;
			u8 singleRecvTrigger:1;
			u8 autoDisable:1;
			u8 triggerResetsTime:1;
			u8 forwardRecvTrigger:1;
			u8 unused:3;
			u8 period;
			u8 offset;
			u8 activeArea;
			u8 recvGroup;
			u8 triggerType;
			u8 displayIndex;
		} timer;
		
		// +==============================+
		// |        Entity_Monster        |
		// +==============================+
		// +==============================+
		// |         Entity_Moth          |
		// +==============================+
		// +==============================+
		// |        Entity_Critter        |
		// +==============================+
		// +==============================+
		// |       Entity_Friendly        |
		// +==============================+
		struct
		{
			u8 onPlayerMove:1;
			u8 deactivated:1;
			u8 stayOnSameTile:1;
			u8 avoidHazardousTiles:1;
			u8 invulnerable:1;
			u8 unused:3;
			u8 activeArea;
			u8 aiType;
			u8 delay;
			u8 delayOffset;
			u8 triggerType;
		} monster;
		
		// +==============================+
		// |         Entity_Trap          |
		// +==============================+
		struct
		{
			u8 deactivated:1;
			u8 singleTrigger:1;
			u8 autoDeactivate:1;
			u8 autoActivate:1;
			u8 triggerOnStepOff:1;
			u8 unused:3;
			u8 triggerType;
		} trap;
		
		// +==============================+
		// |       Entity_SaveSpot        |
		// +==============================+
		struct
		{
			u8 deactivated:1;
			u8 singleTrigger:1;
			u8 unused:6;
			u8 triggerType;
		} saveSpot;
		
		// +==============================+
		// |      Entity_MonsterDoor      |
		// +==============================+
		struct
		{
			u8 showUiCounter:1;
			u8 reappear:1;
			u8 singleTrigger:1;
			u8 unused:5;
			u8 numIgnored;
			u8 monsterType;
			u8 countArea;
			u8 triggerType;
		} monsterDoor;
		
		// +==============================+
		// |         Entity_Torch         |
		// +==============================+
		struct
		{
			u8 deactivated:1;
			u8 unused:7;
			u8 triggerType;
			u8 lightRadius;
		} torch;
		
		// +==============================+
		// |        Entity_Sensor         |
		// +==============================+
		struct
		{
			u8 onEnter:1;
			u8 onLeave:1;
			u8 singleTime:1;
			u8 sameColor:1;
			u8 unused1:4;
			u8 detectType;
			u8 width;
			u8 height;
		} sensor;
		
		// +==============================+
		// |         Entity_Light         |
		// +==============================+
		struct
		{
			u8 deactivated:1;
			u8 unused:7;
			u8 triggerType;
			u8 lightRadius;
		} light;
		
		// +==============================+
		// |      Entity_TetrisDoor       |
		// +==============================+
		struct
		{
			u8 showUiCounter:1;
			u8 reappear:1;
			u8 singleTrigger:1;
			u8 unused:5;
			u8 numIgnored;
			u8 countArea;
			u8 triggerType;
		} tetrisDoor;
		
		// +==============================+
		// |        Entity_Piston         |
		// +==============================+
		struct
		{
			u8 extended:1;
			u8 autoRetract:1;
			u8 autoExtend:1;
			u8 singleTrigger:1;
			u8 longArm:1;
			u8 showLed:1;
			u8 noSound:1;
			u8 unused:1;
			u8 triggerType;
			u8 ledColor;
		} piston;
		
		// +==============================+
		// |        Entity_Spawner        |
		// +==============================+
		struct
		{
			u8 autoRespawn:1;
			u8 spawnTriggered:1;
			u8 spawnParticles:1;
			u8 silent:1;
			u8 unused1:4;
			u8 maxCount;
			u8 considerVariant:1;
			u8 considerColor:1;
			u8 considerGroup:1;
			u8 considerRotation:1;
			u8 unused2:4;
			u8 countArea;
		} spawner;
		
		// +==============================+
		// |       Entity_FireStand       |
		// +==============================+
		struct
		{
			u8 lit;
			u8 triggerType;
		} fireStand;
		
		// +==============================+
		// |         Entity_Chest         |
		// +==============================+
		struct
		{
			u8 open:1;
			u8 hidden:1;
			u8 singleTrigger:1;
			u8 unused:5;
			u8 triggerType;
		} chest;
		
		// +==============================+
		// |    Entity_MonsterTrigger     |
		// +==============================+
		struct
		{
			u8 number;
			u8 monsterType;
			u8 countArea;
			u8 triggerType;
		} monsterTrigger;
		
		// +==============================+
		// |      Entity_WallButton       |
		// +==============================+
		struct
		{
			u8 singlePress:1;
			u8 hideLed:1;
			u8 showTimer:1;
			u8 unused:5;
			u8 triggerType;
			u8 activateTime;
		} wallButton;
		
		// +==============================+
		// |       Entity_ChestTrap       |
		// +==============================+
		struct
		{
			u8 triggerType;
			u8 singleTrigger;
		} chestTrap;
		
		// +==============================+
		// |       Entity_Resetter        |
		// +==============================+
		struct
		{
			u8 saveGame:1;
			u8 autoDisappear:1;
			u8 noParticles:1;
			u8 noSound:1;
			u8 respawnPlayer:1;
			u8 unused:3;
			u8 resetCategories;
			u8 area;
		} resetter;
		
		// +==============================+
		// |      Entity_DeathTimer       |
		// +==============================+
		struct
		{
			u8 triggerType;
			u8 triggered:1;
			u8 onlyOnStep:1;
			u8 triggerResetsTime:1;
			u8 countSimultaneousSteps:1;
			u8 singleEntity:1;
			u8 noParticles:1;
			u8 noSound:1;
			u8 unused:1;
			u8 time;
			u8 altGroup;
			u8 altTriggerType;
			u8 trackType;
		} deathTimer;
		
		// +==============================+
		// |    Entity_PlayerTeleport     |
		// +==============================+
		struct
		{
			u8 singleTime:1;
			u8 instantView:1;
			u8 unused1:6;
		} playerTeleport;
		
		// +==============================+
		// |        Entity_Rotator        |
		// +==============================+
		struct
		{
			u8 triggerType;
		} rotator;
		
		// +==============================+
		// |     Entity_PreviewCamera     |
		// +==============================+
		struct
		{
			u8 width;
			u8 height;
		} previewCamera;
		
		// +==============================+
		// |      Entity_CrystalBall      |
		// +==============================+
		struct
		{
			u8 playerSurrogate:1;
			u8 singleTrigger:1;
			u8 invisible:1;
			u8 unused:5;
			u8 triggerType;
		} crystalBall;
		
		// +==============================+
		// |     Entity_CrystalCamera     |
		// +==============================+
		struct
		{
			u8 timeStopped:1;
			u8 unused:7;
			u8 radius;
			u8 offset;
			u8 inputMethod;
		} crystalCamera;
		
		// +==============================+
		// |      Entity_GlyphGroup1      |
		// +==============================+
		// +==============================+
		// |      Entity_GlyphGroup2      |
		// +==============================+
		// +==============================+
		// |      Entity_GlyphGroup3      |
		// +==============================+
		// +==============================+
		// |      Entity_GlyphGroup4      |
		// +==============================+
		struct
		{
			u8 emboss:1;
			u8 unused:7;
			u8 layer;
			i8 offsetX;
			i8 offsetY;
		} glyph;
		
		// +==============================+
		// |        Entity_Candle         |
		// +==============================+
		struct
		{
			u8 deactivated;
			u8 triggerType;
			u8 singlePress;
		} candle;
		
		// +==============================+
		// |    Entity_SolidDecoration    |
		// +==============================+
		// +==============================+
		// |   Entity_FragileDecoration   |
		// +==============================+
		// +==============================+
		// |     Entity_OverheadDecor     |
		// +==============================+
		struct
		{
			u8 animated:1;
			u8 unused:7;
			u8 innerVariant;
			u8 secondaryColor;
		} decor;
		
		// +==============================+
		// |         Entity_Text          |
		// +==============================+
		struct
		{
			u8 textId;
			u8 hidden:1;
			u8 emboss:1;
			u8 unused:6;
			i8 offsetX;
			i8 offsetY;
		} text;
		
		// +==============================+
		// |      Entity_Projectile       |
		// +==============================+
		struct
		{
			u8 diagonal;
		} projectile;
		
		// +==============================+
		// |       Entity_HitSwitch       |
		// +==============================+
		struct
		{
			u8 singleSwitch:1;
			u8 activated:1;
			u8 globalTied:1;
			u8 alwaysLookOn:1;
			u8 unused1:4;
			u8 triggerType;
		} hitSwitch;
		
		// +==============================+
		// |        Entity_Turret         |
		// +==============================+
		struct
		{
			u8 invisible:1;
			u8 singleTrigger:1;
			u8 activated:1;
			u8 autoDeletePortals:1;
			u8 whileInRoom:1;
			u8 unused:3;
			u8 shotInterval;
			u8 shotOffset;
			u8 cannons;
			u8 spread;
			u8 triggerType;
			u8 altGroup;
			u8 altTriggerType;
		} turret;
		
		// +==============================+
		// |      Entity_TurnTrigger      |
		// +==============================+
		struct
		{
			u8 type;
		} turnTrigger;
		
		// +==============================+
		// |        Entity_Portal         |
		// +==============================+
		struct
		{
			u8 closed:1;
			u8 newestPlaced:1;
			u8 singleTime:1;
			u8 autoDisable:1;
			u8 singleUse:1;
			u8 unused:3;
			u8 triggerType;
			u8 connectionGroup;
		} portal;
		
		// +==============================+
		// |      Entity_PathStones       |
		// +==============================+
		struct
		{
			u8 stones;
			u8 crumbled;
			u8 rotated;
			u8 emboss;
		} pathStones;
		
		// +==============================+
		// |       Entity_Threshold       |
		// +==============================+
		struct
		{
			u8 onCrossOut:1;
			u8 onCrossIn:1;
			u8 singleTime:1;
			u8 unused1:5;
			u8 width;
		} threshold;
		
		// +==============================+
		// |         Entity_Saver         |
		// +==============================+
		struct
		{
			u8 singleTime:1;
			u8 delayed:1;
			u8 unused1:6;
		} saver;
		
		// +==============================+
		// |  Entity_ChunkChangeTrigger   |
		// +==============================+
		struct
		{
			u8 singleTime:1;
			u8 onChunkChange:1;
			u8 onRoomChange:1;
			u8 unused1:5;
		} chunkChangeTrigger;
		
		// +==============================+
		// |     Entity_EntityCounter     |
		// +==============================+
		struct
		{
			u8 typeIsSoft:1;
			u8 considerVariant:1;
			u8 considerGroup:1;
			u8 considerRotation:1;
			u8 considerColor:1;
			u8 considerState:1;
			u8 considerAnimState:1;
			u8 unused1:1;
			u8 considerOption0:1;
			u8 considerOption1:1;
			u8 considerOption2:1;
			u8 considerOption3:1;
			u8 considerOption4:1;
			u8 considerOption5:1;
			u8 considerOption6:1;
			u8 considerOption7:1;
			u8 stateValue;
			u8 animStateValue;
			u8 countArea;
			u8 displayIndex;
		} entityCounter;
		
		// +==============================+
		// |        Entity_Number         |
		// +==============================+
		struct
		{
			u8 value;
			u8 triggerType;
			u8 singleTime:1;
			u8 unused1:7;
			u8 altGroup;
			u8 altTriggerType;
			u8 altSingleTime:1;
			u8 unused2:7;
			u8 displayIndex;
		} number;
		
		// +==============================+
		// |      Entity_Comparator       |
		// +==============================+
		struct
		{
			u8 onTrue:1;
			u8 onFalse:1;
			u8 onTrueConstant:1;
			u8 onFalseConstant:1;
			u8 singleTime:1;
			u8 unused1:3;
			u8 triggerPeriod;
			u8 triggerOffset;
		} comparator;
		
		// +==============================+
		// |         Entity_Giver         |
		// +==============================+
		struct
		{
			u8 singleItem:1;
			u8 singleTrigger:1;
			u8 unused1:6;
			u8 triggerType;
			u8 deactivated;
		} giver;
		
		// +==============================+
		// |         Entity_Taker         |
		// +==============================+
		struct
		{
			u8 iceSkates:1;
			u8 pusherShoes:1;
			u8 lavaBoots:1;
			u8 waterBoots:1;
			u8 spikeShoes:1;
			u8 fireShoes:1;
			u8 pegasusBoots:1;
			u8 jesterShoes:1;
			u8 strength:1;
			u8 fire:1;
			u8 lightning:1;
			u8 dig:1;
			u8 bow:1;
			u8 portalGun:1;
			u8 pushWand:1;
			u8 pullWand:1;
			u8 triggerType;
			u8 deactivated;
			u8 width;
			u8 lifterGloves:1;
			u8 light:1;
			u8 freezeRod:1;
			u8 unused:5;
		} taker;
		
		// +==============================+
		// |     Entity_SwordPedestal     |
		// +==============================+
		struct
		{
			u8 taken;
		} swordPedestal;
		
		// +==============================+
		// |        Entity_Region         |
		// +==============================+
		struct
		{
			u8 width;
			u8 height;
			u8 saveOnCompletion:1;
			u8 singleTrigger:1;
			u8 onlyPlayerPortals:1;
			u8 unused1:5;
		} region;
		
		// +==============================+
		// |       Entity_Disruptor       |
		// +==============================+
		struct
		{
			u8 disabled:1;
			u8 unused:7;
			u8 triggerType;
		} disruptor;
		
		// +==============================+
		// |     Entity_FriendshipPad     |
		// +==============================+
		struct
		{
			u8 connectionGroup;
		} friendshipPad;
		
		// +==============================+
		// |    Entity_FriendshipDoor     |
		// +==============================+
		struct
		{
			u8 connectionGroup;
			u8 color2;
			u8 saveOnOpen:1;
			u8 unused:7;
		} friendshipDoor;
		
		// +==============================+
		// |    Entity_PlayerSeparator    |
		// +==============================+
		struct
		{
			u8 disabled:1;
			u8 singleTime:1;
			u8 biDirectional:1;
			u8 unused:6;
			u8 triggerType;
		} playerSeparator;
		
		// +==============================+
		// |      Entity_RespawnZone      |
		// +==============================+
		struct
		{
			u8 width;
			u8 height;
		} respawnZone;
		
		// +==============================+
		// |     Entity_ProgressArrow     |
		// +==============================+
		struct
		{
			u8 width;
			u8 height;
		} progressArrow;
		
		// +==============================+
		// |     Entity_LevelOptions      |
		// +==============================+
		struct
		{
			u8 background;
			u8 border;
			u8 backgroundColor;
			u8 borderColor;
			u8 coopBorderColors:1;
			u8 unused:7;
		} levelOptions;
		
		// +==============================+
		// |      Entity_MusicQueue       |
		// +==============================+
		struct
		{
			u8 singleTime:1;
			u8 hardQueue:1;
			u8 repeating:1;
			u8 onlyIfQuiet:1;
			u8 preloadSong:1;
			u8 autoPlay:1;
			u8 unused1:2;
			u8 song;
			u8 transition;
		} musicQueue;
		
		// +==============================+
		// |      Entity_RequireDoor      |
		// +==============================+
		struct
		{
			u8 showUiCounter:1;
			u8 reappear:1;
			u8 displayNumbers:1;
			u8 fullyDisappear:1;
			u8 unused:4;
			u8 ledColor;
			u8 requiredNumber;
			u8 currentNumber;
			u8 mainTriggerType;
			u8 altGroup;
			u8 altTriggerType;
		} requireDoor;
		
		// +==============================+
		// |         Entity_Crack         |
		// +==============================+
		struct
		{
			u8 playerActivated:1;
			u8 monsterActivated:1;
			u8 boxActivated:1;
			u8 spawnNumbers:1;
			u8 unused:4;
			u8 stepCount;
			u8 triggerType;
		} crack;
		
		// +==============================+
		// |        Entity_Painter        |
		// +==============================+
		struct
		{
			u8 singleUse:1;
			u8 autoPaint:1;
			u8 hideIcon:1;
			u8 dontPaintPlayer:1;
			u8 unused:4;
			u8 secondaryColor;
			u8 triggerType;
			u8 nozzles;
		} painter;
		
		// +==============================+
		// |       Entity_PopupWall       |
		// +==============================+
		struct
		{
			u8 activated:1;
			u8 unused:7;
		} popupWall;
		
		// +==============================+
		// |         Entity_Laser         |
		// +==============================+
		struct
		{
			u8 deactivated:1;
			u8 singleTrigger:1;
			u8 unused:6;
			u8 triggerType;
		} laser;
		
		// +==============================+
		// |    Entity_LaserReceptacle    |
		// +==============================+
		struct
		{
			u8 singleTrigger:1;
			u8 unused:7;
			u8 receptacleSides;
			u8 triggerType;
			u8 numRequired;
		} laserReceptacle;
		
		// +==============================+
		// |       Entity_EffectPad       |
		// +==============================+
		struct
		{
			u8 singleEffect:1;
			u8 singleTrigger:1;
			u8 deactivated:1;
			u8 autoDisable:1;
			u8 autoEnable:1;
			u8 unused:3;
			u8 triggerType;
		} effectPad;
		
		// +==============================+
		// |      Entity_CustomGlyph      |
		// +==============================+
		struct
		{
			u8 bitmapRow0;
			u8 bitmapRow1;
			u8 bitmapRow2;
			u8 bitmapRow3;
			u8 bitmapRow4;
			u8 bitmapRow5;
			u8 bitmapRow6;
			u8 bitmapRow7;
		} customGlyph;
		
		// +==============================+
		// |        Entity_Display        |
		// +==============================+
		struct
		{
			u8 turnedOff:1;
			u8 singleTrigger:1;
			u8 fullyDisappear:1;
			u8 showingSuccess:1;
			u8 unused:4;
			u8 displayIndex;
			u8 triggerType;
		} display;
		
		// +==============================+
		// |     Entity_MusicDirector     |
		// +==============================+
		struct
		{
			u8 isActive:1;
			u8 unused:7;
			u8 activeSong;
			u8 passiveSong;
			u8 passiveGroup;
		} musicDirector;
		
		// +==============================+
		// |         Entity_Relay         |
		// +==============================+
		struct
		{
			u8 singleTrigger:1;
			u8 invertPolarity:1;
			u8 ingorePositive:1;
			u8 ingoreNegative:1;
			u8 unused:4;
			u8 outGroup;
			u8 delay;
			u8 triggers[5];
		} relay;
		
		// +==============================+
		// |      Entity_TriggerGate      |
		// +==============================+
		struct
		{
			u8 enabled:1;
			u8 singleTrigger:1;
			u8 singleSignal:1;
			u8 unused:5;
			u8 triggerType;
			u8 inGroup;
			u8 outGroup;
		} triggerGate;
		
		// +==============================+
		// |      Entity_ViewTarget       |
		// +==============================+
		struct
		{
			u8 focused:1;
			u8 unused:7;
			u8 triggerType;
		} viewTarget;
		
		// +==============================+
		// |      Entity_ColorField       |
		// +==============================+
		struct
		{
			u8 disabled:1;
			u8 unused:7;
			u8 shape;
		} colorField;
		
		// +==============================+
		// |     Entity_EventTrigger      |
		// +==============================+
		struct
		{
			u8 singleTrigger:1;
			u8 unused:7;
			u8 width;
			u8 height;
			u8 boxMoved:1;
			u8 bagGrabbed:1;
			u8 leverPulled:1;
			u8 monsterMoved:1;
			u8 itemPickup:1;
			u8 crackStepped:1;
			u8 swordSwung:1;
			u8 teleporterUsed:1;
			u8 pressurePlateActivated:1;
			u8 painterActivated:1;
			u8 effectPadActivated:1;
			u8 occlusionDispersed:1;
			u8 popupWallActivated:1;
			u8 chestOpened:1;
			u8 keyDoorOpened:1;
			u8 wallButtonPressed:1;
		} eventTrigger;
	};
};

// +--------------------------------------------------------------+
// |                         Enumerations                         |
// +--------------------------------------------------------------+
// +==============================+
// |        DeathReason_t         |
// +==============================+
typedef enum
{
	DeathReason_None = 0x00,
	DeathReason_Hole,
	DeathReason_SlimeWebHole,
	DeathReason_Lava,
	DeathReason_Water,
	DeathReason_ToggleWall,
	DeathReason_LedDoor,
	DeathReason_DeathTimer,
	DeathReason_FireTrap,
	DeathReason_FireBall,
	DeathReason_SpikeTrap,
	DeathReason_MeltEffectPad,
	DeathReason_Arrow,
	DeathReason_LightningShot,
	DeathReason_OtherProjectile,
	DeathReason_DeadlyLaser,
	DeathReason_HitSomething,
	DeathReason_HitEntity,
	DeathReason_HitDisruptor,
	DeathReason_HitEntityBelow,
	DeathReason_NewPortalOpened,
	DeathReason_DisruptorTriggered,
	DeathReason_RegionTriggered,
	DeathReason_TurretFired,
	DeathReason_EntityEntered,
	DeathReason_EntityLeft,
	DeathReason_SteppedOn,
	DeathReason_SwordSwing,
	DeathReason_TetrisComplete,
	DeathReason_Triggered,
	DeathReason_NumReasons,
} DeathReason_t;

// +==============================+
// |       DiamondVariant_t       |
// +==============================+
enum
{
	DiamondVariant_Default = 0x00,
	DiamondVariant_GoldBar,
	DiamondVariant_Red,
	DiamondVariant_NumVariants,
};

// +==============================+
// |        DiamondType_t         |
// +==============================+
typedef enum
{
	DiamondType_BlueDiamonds = 0x00,
	DiamondType_RedDiamonds,
	DiamondType_GoldBars,
	DiamondType_All,
	DiamondType_NumTypes,
} DiamondType_t;

// +==============================+
// |         BoxVariant_t         |
// +==============================+
enum
{
	BoxShape_Box     = 0x01,
	BoxShape_Barrel  = 0x02,
	BoxShape_Boulder = 0x03,
	BoxShape_NumShapes = 3,
	BoxShape_Mask = 0x0F,
	
	BoxMaterial_Wood   = 0x10,
	BoxMaterial_Metal  = 0x20,
	BoxMaterial_Slime  = 0x30,
	BoxMaterial_Ice    = 0x40,
	BoxMaterial_Void   = 0x50,
	BoxMaterial_Tetris = 0x60,
	BoxMaterial_NumMaterials = 6,
	BoxMaterial_Mask = 0xF0,
	
	BoxVariant_WoodBox       = (BoxMaterial_Wood   | BoxShape_Box),
	BoxVariant_MetalBox      = (BoxMaterial_Metal  | BoxShape_Box),
	BoxVariant_SlimeBox      = (BoxMaterial_Slime  | BoxShape_Box),
	BoxVariant_IceBox        = (BoxMaterial_Ice    | BoxShape_Box),
	BoxVariant_VoidBox       = (BoxMaterial_Void   | BoxShape_Box),
	BoxVariant_TetrisBox     = (BoxMaterial_Tetris | BoxShape_Box),
	BoxVariant_WoodBarrel    = (BoxMaterial_Wood   | BoxShape_Barrel),
	BoxVariant_MetalBarrel   = (BoxMaterial_Metal  | BoxShape_Barrel),
	BoxVariant_SlimeBarrel   = (BoxMaterial_Slime  | BoxShape_Barrel),
	BoxVariant_IceBarrel     = (BoxMaterial_Ice    | BoxShape_Barrel),
	BoxVariant_VoidBarrel    = (BoxMaterial_Void   | BoxShape_Barrel),
	BoxVariant_TetrisBarrel  = (BoxMaterial_Tetris | BoxShape_Barrel),
	BoxVariant_WoodBoulder   = (BoxMaterial_Wood   | BoxShape_Boulder),
	BoxVariant_MetalBoulder  = (BoxMaterial_Metal  | BoxShape_Boulder),
	BoxVariant_SlimeBoulder  = (BoxMaterial_Slime  | BoxShape_Boulder),
	BoxVariant_IceBoulder    = (BoxMaterial_Ice    | BoxShape_Boulder),
	BoxVariant_VoidBoulder   = (BoxMaterial_Void   | BoxShape_Boulder),
	BoxVariant_TetrisBoulder = (BoxMaterial_Tetris | BoxShape_Boulder),
	BoxVariant_NumVariants = 18,
};

// +==============================+
// |         BagVariant_t         |
// +==============================+
enum
{
	BagVariant_Normal = 0x00,
	BagVariant_Sheep,
	BagVariant_NumVariants,
};

// +==============================+
// |        MothVariant_t         |
// +==============================+
enum
{
	MothVariant_Moth = 0x00,
	MothVariant_Bird,
	MothVariant_DragonFly,
	MothVariant_Bee,
	MothVariant_Firefly,
	MothVariant_NumVariants,
};

// +==============================+
// |       CritterVariant_t       |
// +==============================+
enum
{
	CritterVariant_Mouse = 0x00,
	CritterVariant_Snake,
	CritterVariant_Spider,
	CritterVariant_NumVariants,
};

// +==============================+
// |    PressurePlateVariant_t    |
// +==============================+
enum
{
	PressurePlateVariant_Round = 0x00,
	PressurePlateVariant_Square,
	PressurePlateVariant_Stone,
	PressurePlateVariant_NumVariants,
};

// +==============================+
// |        LeverVariant_t        |
// +==============================+
enum
{
	LeverVariant_Default = 0x00,
	LeverVariant_Joystick,
	LeverVariant_NumVariants,
};

// +==============================+
// |         KeyVariant_t         |
// +==============================+
enum
{
	KeyVariant_Default = 0x00,
	KeyVariant_Skeleton,
	KeyVariant_NumVariants,
};

// +==============================+
// |        RailVariant_t         |
// +==============================+
enum
{
	RailVariant_AllDirections = 0x00,
	RailVariant_TJunction,
	RailVariant_Corner,
	RailVariant_Straight,
	RailVariant_DeadEnd,
	RailVariant_Box,
	RailVariant_NumVariants,
};

// +==============================+
// |      PusherEntVariant_t      |
// +==============================+
enum
{
	PusherEntVariant_Default = 0x00,
	PusherEntVariant_Arrows,
	PusherEntVariant_Conveyor,
	PusherEntVariant_NumVariants,
};

// +==============================+
// |      AnimationVariant_t      |
// +==============================+
enum
{
	AnimationVariant_CrackBreak = 0x00,
	AnimationVariant_IceCrackBreak,
	AnimationVariant_IceDissolve,
	AnimationVariant_SlimeDissolve,
	AnimationVariant_SlimeWeb,
	AnimationVariant_WaterSplash,
	AnimationVariant_LavaSplash,
	AnimationVariant_DirtCrumble,
	AnimationVariant_ResetterPlayerNeeded,
	AnimationVariant_NumVariants,
};

// +==============================+
// |        ShoeVariant_t         |
// +==============================+
enum
{
	ShoeVariant_IceSkates = 0x00,
	ShoeVariant_PusherShoes,
	ShoeVariant_LavaBoots,
	ShoeVariant_WaterBoots,
	ShoeVariant_SpikeShoes,
	ShoeVariant_FireShoes,
	ShoeVariant_PegasusBoots,
	ShoeVariant_JesterShoes,
	ShoeVariant_NumVariants,
};

// +==============================+
// |        GloveVariant_t        |
// +==============================+
enum
{
	GloveVariant_Strength = 0x00,
	GloveVariant_Fire,
	GloveVariant_Lightning,
	GloveVariant_Dig,
	GloveVariant_Bow,
	GloveVariant_PortalGun,
	GloveVariant_PushWand,
	GloveVariant_PullWand,
	GloveVariant_LifterGloves,
	GloveVariant_Light,
	GloveVariant_FreezeRod,
	GloveVariant_NumVariants,
};

// +==============================+
// |        BushVariant_t         |
// +==============================+
enum
{
	BushVariant_Default = 0x00,
	BushVariant_Box,
	BushVariant_Barrel,
	BushVariant_Boulder,
	BushVariant_NumVariants,
};

// +==============================+
// |        TorchVariant_t        |
// +==============================+
enum
{
	TorchVariant_Default = 0x00,
	TorchVariant_Small,
	TorchVariant_Animated,
	TorchVariant_Castle1,
	TorchVariant_Castle2,
	TorchVariant_Bubble1,
	TorchVariant_Bubble2,
	TorchVariant_NumVariants,
};

// +==============================+
// |        LightVariant_t        |
// +==============================+
enum
{
	LightVariant_Default = 0x00,
	LightVariant_Small,
	LightVariant_NumVariants,
};

// +==============================+
// |       SpawnerVariant_t       |
// +==============================+
enum
{
	SpawnerVariant_Pipe = 0x00,
	SpawnerVariant_Elevator,
	SpawnerVariant_Invisible,
	SpawnerVariant_NumVariants,
};

// +==============================+
// |         TurretShot_t         |
// +==============================+
enum
{
	TurretShot_None        = Dir2_None,
	TurretShot_Up          = Dir2_Up,
	TurretShot_Left        = Dir2_Left,
	TurretShot_Right       = Dir2_Right,
	TurretShot_UpLeft      = Dir2_Up | Dir2_Left,
	TurretShot_UpRight     = Dir2_Up | Dir2_Right,
	TurretShot_LeftRight   = Dir2_Left | Dir2_Right,
	TurretShot_All         = Dir2_Up | Dir2_Left | Dir2_Right,
	TurretShot_NumOptions = 0x08,
};

// +==============================+
// |     TurnTriggerVariant_t     |
// +==============================+
enum
{
	TurnTrigger_Specific           = 0x00,
	TurnTrigger_Clockwise          = 0x01,
	TurnTrigger_Counterclockwise   = 0x02,
	TurnTrigger_Opposite           = 0x03,
	TurnTrigger_NumOptions         = 0x04,
};

// +==============================+
// |     ProjectileVariant_t      |
// +==============================+
enum
{
	ProjectileVariant_Arrow = 0x00,
	ProjectileVariant_Lightning,
	ProjectileVariant_FireBall,
	ProjectileVariant_PortalGun,
	ProjectileVariant_Push,
	ProjectileVariant_Pull,
	ProjectileVariant_Freeze,
	ProjectileVariant_Shuriken,
	ProjectileVariant_Bubble,
	ProjectileVariant_NumVariants,
};

// +==============================+
// |        ChestVariant_t        |
// +==============================+
enum
{
	ChestVariant_Default = 0x00,
	ChestVariant_Spikes,
	ChestVariant_Wooden,
	ChestVariant_Crystal,
	ChestVariant_DungeonWooden,
	ChestVariant_NumVariants,
};

// +==============================+
// |     WallButtonVariant_t      |
// +==============================+
enum
{
	WallButtonVariant_Default = 0x00,
	WallButtonVariant_Target,
	WallButtonVariant_Outlet,
	WallButtonVariant_Torch,
	WallButtonVariant_NumVariants,
};

// +==============================+
// |       RotatorVariant_t       |
// +==============================+
enum
{
	RotatorVariant_Default = 0x00,
	RotatorVariant_Meta,
	RotatorVariant_NumVariants,
};

// +==============================+
// |       CandleVariant_t        |
// +==============================+
enum
{
	CandleVariant_Default = 0x00,
	CandleVariant_Fancy,
	CandleVariant_Candelabra,
	CandleVariant_Lantern1,
	CandleVariant_Small,
	CandleVariant_BoneCandle,
	CandleVariant_SkullCandle,
	CandleVariant_SlopCandle,
	CandleVariant_NumVariants,
};

// +==============================+
// |         SolidDecor_t         |
// +==============================+
enum
{
	SolidDecor_Crystal = 0x00,
	SolidDecor_Campfire,
	SolidDecor_ChairRight,
	SolidDecor_ChairLeft,
	SolidDecor_Column,
	SolidDecor_FruitBox,
	SolidDecor_Base,
	SolidDecor_SmallTree,
	SolidDecor_Mushroom,
	SolidDecor_Stool,
	SolidDecor_SpikyBush,
	SolidDecor_Stool2,
	SolidDecor_DancingBush,
	SolidDecor_Chest,
	SolidDecor_Bookshelf,
	SolidDecor_StatueKnight,
	SolidDecor_Sunflower,
	SolidDecor_CoffinBlink,
	SolidDecor_Well,
	SolidDecor_LargeMushroom,
	SolidDecor_CrateSmall,
	SolidDecor_CrateLarge,
	SolidDecor_EgyptianSeptre,
	SolidDecor_EgyptianStatue,
	SolidDecor_Banner1,
	SolidDecor_Banner2,
	SolidDecor_SmallPalmTree,
	SolidDecor_BonsaiTree,
	SolidDecor_PottedPlant1,
	SolidDecor_PottedPlant2,
	SolidDecor_PottedPlant3,
	SolidDecor_PottedPlant4,
	SolidDecor_Pyramid,
	SolidDecor_EgyptianEmblem,
	SolidDecor_EgyptianBeetle,
	SolidDecor_EgyptianFish,
	SolidDecor_EgyptianPot,
	SolidDecor_SmallTomb,
	SolidDecor_SmallShrine,
	SolidDecor_Balance,
	SolidDecor_EgyptianLantern1,
	SolidDecor_EgyptianLantern2,
	SolidDecor_TrashCanOpen,
	SolidDecor_TrashCanClosed,
	SolidDecor_TrashBag,
	SolidDecor_ArmedLog1,
	SolidDecor_ArmedLog2,
	SolidDecor_ArmedLog3,
	SolidDecor_ArmedLog4,
	SolidDecor_ArmedLog5,
	SolidDecor_Stump1,
	SolidDecor_Stump2,
	SolidDecor_Stump3,
	SolidDecor_Sign,
	SolidDecor_CastleGate1,
	SolidDecor_CastleGate2,
	SolidDecor_BubbleVent,
	SolidDecor_CandyTree1,
	SolidDecor_CandyTree2,
	SolidDecor_CandyTree3,
	SolidDecor_CloudCupcake,
	SolidDecor_HappyTree,
	SolidDecor_UnicornStatue,
	SolidDecor_CatStatue,
	SolidDecor_IceSpikes1,
	SolidDecor_IceSpikes2,
	SolidDecor_LargeSnowman,
	SolidDecor_SmallSnowmen1,
	SolidDecor_SmallSnowmen2,
	SolidDecor_SmallSnowmen3,
	SolidDecor_Tree1,
	SolidDecor_AlienPlantClosed,
	SolidDecor_AlienPlantOpen,
	SolidDecor_Pumpkin,
	SolidDecor_Tree2,
	SolidDecor_Tree3,
	SolidDecor_Tree4,
	SolidDecor_Tree5,
	SolidDecor_Tree6,
	SolidDecor_Tree7,
	SolidDecor_Tree8,
	SolidDecor_Tree9,
	SolidDecor_Tree10,
	SolidDecor_Tree11,
	SolidDecor_Tree12,
	SolidDecor_Tree13,
	SolidDecor_Tree14,
	SolidDecor_Tree15,
	SolidDecor_Tree16,
	SolidDecor_Tree17,
	SolidDecor_Tree18,
	SolidDecor_FrozenPot,
	SolidDecor_FrozenStatue1,
	SolidDecor_FrozenStatue2,
	SolidDecor_FrozenStatue3,
	SolidDecor_IceGumDrop,
	SolidDecor_IceClump1,
	SolidDecor_IceClump2,
	SolidDecor_IceClump3,
	SolidDecor_IceClump4,
	SolidDecor_IceSpikes3,
	SolidDecor_IceSpikes4,
	SolidDecor_IceSpikes5,
	SolidDecor_RockSpikes1,
	SolidDecor_RockSpikes2,
	SolidDecor_Ankh,
	SolidDecor_LargePyramidLeft,
	SolidDecor_LargePyramidRight,
	SolidDecor_AngryTree,
	SolidDecor_CaveRock1,
	SolidDecor_CaveRock2,
	SolidDecor_Barrel1,
	SolidDecor_CaveRock3,
	SolidDecor_CaveRock4,
	SolidDecor_WesternBox1,
	SolidDecor_WesternBox2,
	SolidDecor_WesternTroughLeft,
	SolidDecor_WesternTroughRight,
	SolidDecor_HayBaleLeft,
	SolidDecor_HayBaleRight,
	SolidDecor_Gravestone1,
	SolidDecor_Gravestone2,
	SolidDecor_WagonWheel,
	SolidDecor_NumVariants,
};

// +==============================+
// |        FragileDecor_t        |
// +==============================+
enum
{
	FragileDecor_WallVines1 = 0x00,
	FragileDecor_Mushrooms1,
	FragileDecor_Plant1,
	FragileDecor_AnimalSkull,
	FragileDecor_Hatch,
	FragileDecor_Vine,
	FragileDecor_Bunch,
	FragileDecor_BrokenShield,
	FragileDecor_BrokenSword,
	FragileDecor_SkullPile,
	FragileDecor_Leaves,
	FragileDecor_Pillar,
	FragileDecor_Plant2,
	FragileDecor_LargeLeaf,
	FragileDecor_SmallMushrooms1,
	FragileDecor_GrassTuft,
	FragileDecor_Rocks,
	FragileDecor_SmallMushrooms2,
	FragileDecor_SmallMushrooms3,
	FragileDecor_SmallMushrooms4,
	FragileDecor_Reeds1,
	FragileDecor_Reeds2,
	FragileDecor_Reeds3,
	FragileDecor_Reeds4,
	FragileDecor_Leafs1,
	FragileDecor_Leafs2,
	FragileDecor_Leafs3,
	FragileDecor_Leafs4,
	FragileDecor_Leafs5,
	FragileDecor_Leafs6,
	FragileDecor_Leafs7,
	FragileDecor_Puddle1,
	FragileDecor_Puddle2,
	FragileDecor_Puddle3,
	FragileDecor_Puddle4,
	FragileDecor_Puddle5,
	FragileDecor_Puddle6,
	FragileDecor_Puddle7,
	FragileDecor_Puddle8,
	FragileDecor_Puddle9,
	FragileDecor_Puddle10,
	FragileDecor_Rocks2,
	FragileDecor_Rocks3,
	FragileDecor_Pebbles,
	FragileDecor_Bamboo,
	FragileDecor_MediumMushroom1,
	FragileDecor_MediumMushroom2,
	FragileDecor_Flower2,
	FragileDecor_Flower3,
	FragileDecor_Flower4,
	FragileDecor_GroundBubbles,
	FragileDecor_Gingerbread,
	FragileDecor_LilyPad,
	FragileDecor_Brambles1,
	FragileDecor_Brambles2,
	FragileDecor_Brambles3,
	FragileDecor_Brambles4,
	FragileDecor_Clippings1,
	FragileDecor_Clippings2,
	FragileDecor_Clippings3,
	FragileDecor_DirtMound,
	FragileDecor_DrySapling1,
	FragileDecor_DrySapling2,
	FragileDecor_Roots1,
	FragileDecor_Roots2,
	FragileDecor_Roots3,
	FragileDecor_Thorns1,
	FragileDecor_Thorns2,
	FragileDecor_Thorns3,
	FragileDecor_Thorns4,
	FragileDecor_Thorns5,
	FragileDecor_Thorns6,
	FragileDecor_Grate,
	FragileDecor_Trash1,
	FragileDecor_Trash2,
	FragileDecor_GlassCover1,
	FragileDecor_GlassCover2,
	FragileDecor_Candy1,
	FragileDecor_Stain1,
	FragileDecor_Stain2,
	FragileDecor_Stain3,
	FragileDecor_Stain4,
	FragileDecor_CloudTuft1,
	FragileDecor_CloudTuft2,
	FragileDecor_CloudTuft3,
	FragileDecor_Slush1,
	FragileDecor_Slush2,
	FragileDecor_SnowyRock1,
	FragileDecor_SnowyRock2,
	FragileDecor_SnowyRock3,
	FragileDecor_Rocks4,
	FragileDecor_WallVines2,
	FragileDecor_WallVines3,
	FragileDecor_WallVines4,
	FragileDecor_Manhole,
	FragileDecor_Veggie1,
	FragileDecor_Veggie2,
	FragileDecor_Veggie3,
	FragileDecor_Veggie4,
	FragileDecor_OilyBubble,
	FragileDecor_IceFlower1,
	FragileDecor_IceFlower2,
	FragileDecor_IcePebbles,
	FragileDecor_IceShrubs,
	FragileDecor_SandRock1,
	FragileDecor_SandRock2,
	FragileDecor_NumVariants,
};

// +==============================+
// |       OverheadDecor_t        |
// +==============================+
enum
{
	OverheadDecor_WoodenBeam = 0x00,
	OverheadDecor_WoodenBeamEnd,
	OverheadDecor_WoodenLattice,
	OverheadDecor_WoodenLatticeEnd,
	OverheadDecor_MetalGrate,
	OverheadDecor_MetalGrateEdge,
	OverheadDecor_MetalGrateCorner,
	OverheadDecor_MetalGrateLarge,
	OverheadDecor_MetalGrateEdgeLarge,
	OverheadDecor_MetalGrateCornerLarge,
	OverheadDecor_Web1,
	OverheadDecor_Web2,
	OverheadDecor_Web3,
	OverheadDecor_Web4,
	OverheadDecor_Web5,
	OverheadDecor_Sparkles1,
	OverheadDecor_Sparkles2,
	OverheadDecor_Sparkles3,
	OverheadDecor_Sparkles4,
	OverheadDecor_RootTangleCenter,
	OverheadDecor_RootTangleCorner,
	OverheadDecor_RootTangleTri,
	OverheadDecor_RootTangleStraight,
	OverheadDecor_RootTangleCap,
	OverheadDecor_Banner1,
	OverheadDecor_Banner2,
	OverheadDecor_BubbleSpike,
	OverheadDecor_CrystalBallPillow,
	OverheadDecor_WallFan,
	OverheadDecor_WallVent1,
	OverheadDecor_WallVent2,
	OverheadDecor_WallVent3,
	OverheadDecor_HardCandy,
	OverheadDecor_OneBalloon,
	OverheadDecor_ThreeBalloons,
	OverheadDecor_Rainbow,
	OverheadDecor_GoldSymbol,
	OverheadDecor_Scarab,
	OverheadDecor_WallVines1,
	OverheadDecor_Horseshoe,
	OverheadDecor_WantedPoster,
	OverheadDecor_ReindeerBust,
	OverheadDecor_BoardedWindow,
	OverheadDecor_NumVariants,
};

// +==============================+
// |        TableVariant_t        |
// +==============================+
enum
{
	TableVariant_Wood1 = 0x00,
	TableVariant_Wood2,
	TableVariant_Wood3,
	TableVariant_Stone,
	TableVariant_Slab,
	TableVariant_Conveyor,
	TableVariant_Stump,
	TableVariant_Wood4,
	TableVariant_NumVariants,
};

// +==============================+
// |         PetVariant_t         |
// +==============================+
enum
{
	PetVariant_DogBrown = 0x00,
	PetVariant_DogGrey,
	PetVariant_DogOrange,
	PetVariant_DogBrown2,
	PetVariant_DogOrange2,
	PetVariant_DogGrey2,
	PetVariant_Shiba,
	PetVariant_NumVariants,
};

// +==============================+
// |       NumberVariant_t        |
// +==============================+
enum
{
	NumberVariant_Default = 0x00,
	NumberVariant_Screen,
	NumberVariant_NumVariants,
};

// +==============================+
// |        TakerVariant_t        |
// +==============================+
enum
{
	TakerVariant_Threshold = 0x00,
	TakerVariant_Meta,
	TakerVariant_NumVariants,
};

// +==============================+
// |         TableDecor_t         |
// +==============================+
enum
{
	TableDecor_Flowers1 = 0x00,
	TableDecor_Flowers2,
	TableDecor_Flowers3,
	TableDecor_Flowers4,
	TableDecor_Flowers5,
	TableDecor_Basket1,
	TableDecor_Basket2,
	TableDecor_Bowl,
	TableDecor_Duck,
	TableDecor_Bear,
	TableDecor_Skull,
	TableDecor_Coins1,
	TableDecor_Coins2,
	TableDecor_Coins3,
	TableDecor_Scroll,
	TableDecor_CrackedSkull,
	TableDecor_DogTreat,
	TableDecor_FlashLight,
	TableDecor_SmallCandles1,
	TableDecor_SmallCandles2,
	TableDecor_SmallCandles3,
	TableDecor_Teacup1,
	TableDecor_Teacup2,
	TableDecor_Teapot,
	TableDecor_FlowerPot,
	TableDecor_Potion,
	TableDecor_FlowerPot2,
	TableDecor_Broom,
	TableDecor_Shuriken,
	TableDecor_Shield,
	TableDecor_Cake,
	TableDecor_Sceptors,
	TableDecor_StoneTablet,
	TableDecor_BalancedScale,
	TableDecor_CowboyBoots,
	TableDecor_WesternJar,
	TableDecor_NumVariants,
};

// +==============================+
// |        GlyphGroup1_t         |
// +==============================+
enum
{
	GlyphGroup1_Arrow1 = 0x00,
	GlyphGroup1_Arrow2,
	GlyphGroup1_Arrow3,
	GlyphGroup1_Recycle1,
	GlyphGroup1_PlayerExplode,
	GlyphGroup1_PlayerGlyph1,
	GlyphGroup1_PlayerGlyph2,
	GlyphGroup1_FireBall,
	GlyphGroup1_Timer,
	GlyphGroup1_TitleBanner,
	GlyphGroup1_PlayerCarrying,
	GlyphGroup1_PlayerAttacking,
	GlyphGroup1_PlayerGrabbing,
	GlyphGroup1_Chest,
	GlyphGroup1_NumVariants,
};
// +==============================+
// |        GlyphGroup2_t         |
// +==============================+
enum
{
	GlyphGroup2_CheckSmall = 0x00,
	GlyphGroup2_CheckLarge,
	GlyphGroup2_CrossSmall,
	GlyphGroup2_CrossLarge,
	GlyphGroup2_CircleSmall,
	GlyphGroup2_CircleLarge,
	GlyphGroup2_SquareSmall,
	GlyphGroup2_SquareLarge,
	GlyphGroup2_NumVariants,
};
// +==============================+
// |        GlyphGroup3_t         |
// +==============================+
enum
{
	GlyphGroup3_Circle = 0x00,
	GlyphGroup3_Square1,
	GlyphGroup3_Square2,
	GlyphGroup3_Square3,
	GlyphGroup3_Square4,
	GlyphGroup3_Square5,
	GlyphGroup3_NumVariants,
};

// +==============================+
// |          Handles_t           |
// +==============================+
enum
{
	Handles_None            = 0x00,
	Handles_One             = Dir2_Down,
	Handles_TwoAdjacent     = Dir2_Down|Dir2_Left,
	Handles_TwoOpposite     = Dir2_Down|Dir2_Up,
	Handles_Three           = Dir2_Down|Dir2_Left|Dir2_Right,
	Handles_Four            = Dir2_Down|Dir2_Left|Dir2_Right|Dir2_Up,
	Handles_NumOptions = 6,
};

// +==============================+
// |        TriggerType_t         |
// +==============================+
enum
{
	TriggerType_OnPress = 0x00,
	TriggerType_OnRelease,
	TriggerType_OnBoth,
	TriggerType_None,
	TriggerType_NumOptions,
};

// +==============================+
// |    HitSwitchTriggerType_t    |
// +==============================+
enum
{
	HitSwitchTriggerType_OnSwitch = 0x00,
	HitSwitchTriggerType_OnSwitchOn,
	HitSwitchTriggerType_OnSwitchOff,
	HitSwitchTriggerType_None,
	HitSwitchTriggerType_NumOptions,
};

// +==============================+
// |     PusherTriggerType_t      |
// +==============================+
enum
{
	PusherTriggerType_None = 0x00,
	PusherTriggerType_Toggle,
	PusherTriggerType_Deactivate,
	PusherTriggerType_Activate,
	PusherTriggerType_Reverse,
	PusherTriggerType_RotateClockwise,
	PusherTriggerType_RotateCounterClockwise,
	PusherTriggerType_NumTypes,
};

// +==============================+
// |   TeleporterTriggerType_t    |
// +==============================+
enum
{
	TeleporterTriggerType_Toggle = 0x00,
	TeleporterTriggerType_Deactivate,
	TeleporterTriggerType_Activate,
	TeleporterTriggerType_None,
	TeleporterTriggerType_NumTypes,
};
// +==============================+
// |       TeleporterType_t       |
// +==============================+
enum
{
	TeleporterType_TwoWay = 0x00,
	TeleporterType_Sender,
	TeleporterType_Receiver,
	TeleporterType_InvisibleReceiver,
	TeleporterType_NumTypes,
};

// +==============================+
// |       PortalVariant_t        |
// +==============================+
enum
{
	PortalVariant_Default = 0x00,
	PortalVariant_Spawner,
	PortalVariant_NumVariants,
};

// +==============================+
// |     PortalTriggerType_t      |
// +==============================+
enum
{
	PortalTriggerType_None = 0x00,
	PortalTriggerType_Toggle,
	PortalTriggerType_Open,
	PortalTriggerType_Close,
	PortalTriggerType_NumTypes,
};

// +==============================+
// |   ToggleWallTriggerType_t    |
// +==============================+
enum
{
	ToggleWallTriggerType_Toggle = 0x00,
	ToggleWallTriggerType_Deactivate,
	ToggleWallTriggerType_Activate,
	ToggleWallTriggerType_None,
	ToggleWallTriggerType_NumTypes,
};

// +==============================+
// |      TrapTriggerType_t       |
// +==============================+
enum
{
	TrapTriggerType_Toggle = 0x00,
	TrapTriggerType_Deactivate,
	TrapTriggerType_Activate,
	TrapTriggerType_None,
	TrapTriggerType_NumTypes,
};

// +==============================+
// |        TrapVariant_t         |
// +==============================+
enum
{
	TrapVariant_Spike = 0x00,
	TrapVariant_Fire,
	TrapVariant_NumVariants,
};

// +==============================+
// |       AiTriggerType_t        |
// +==============================+
enum
{
	AiTriggerType_None = 0x00,
	AiTriggerType_ToggleAi,
	AiTriggerType_DisableAi,
	AiTriggerType_EnableAi,
	AiTriggerType_NumTypes,
};

// +==============================+
// |      ChestTriggerType_t      |
// +==============================+
enum
{
	ChestTriggerType_Show = 0x00,
	ChestTriggerType_Hide,
	ChestTriggerType_Toggle,
	ChestTriggerType_None,
	ChestTriggerType_NumTypes,
};

// +==============================+
// |     MonsterTriggerType_t     |
// +==============================+
enum
{
	MonsterTriggerType_BelowMin = 0x00,
	MonsterTriggerType_AboveMin,
	MonsterTriggerType_CountFalls,
	MonsterTriggerType_CountRises,
	MonsterTriggerType_CountChanges,
	MonsterTriggerType_BelowMinAlways,
	MonsterTriggerType_AboveMinAlways,
	MonsterTriggerType_NumTypes,
};

// +==============================+
// |    SaveSpotTriggerType_t     |
// +==============================+
enum
{
	SaveSpotTriggerType_None = 0x00,
	SaveSpotTriggerType_Activate,
	SaveSpotTriggerType_Deactivate,
	SaveSpotTriggerType_Toggle,
	SaveSpotTriggerType_NumTypes,
};

// +==============================+
// |       BoxTriggerType_t       |
// +==============================+
enum
{
	BoxTriggerType_None = 0x00,
	BoxTriggerType_ToggleLock,
	BoxTriggerType_Unlock,
	BoxTriggerType_Lock,
	BoxTriggerType_NumTypes,
};

// +==============================+
// |      TorchTriggerType_t      |
// +==============================+
enum
{
	TorchTriggerType_Toggle = 0x00,
	TorchTriggerType_Deactivate,
	TorchTriggerType_Activate,
	TorchTriggerType_None,
	TorchTriggerType_NumTypes,
};

// +==============================+
// |      SensorDetectType_t      |
// +==============================+
enum
{
	SensorDetectType_Player = 0x00,
	SensorDetectType_Monsters,
	SensorDetectType_Projectile,
	SensorDetectType_SolidEntity,
	SensorDetectType_AnyBox,
	SensorDetectType_BoxShape,
	SensorDetectType_BarrelShape,
	SensorDetectType_BoulderShape,
	SensorDetectType_WoodMaterial,
	SensorDetectType_SlimeMaterial,
	SensorDetectType_MetalMaterial,
	SensorDetectType_IceMaterial,
	SensorDetectType_VoidMaterial,
	SensorDetectType_TetrisMaterial,
	SensorDetectType_Arrow,
	SensorDetectType_Fireball,
	SensorDetectType_Lightning,
	SensorDetectType_PortalShot,
	SensorDetectType_TableDecor,
	SensorDetectType_NumTypes,
};

// +==============================+
// |      LightTriggerType_t      |
// +==============================+
enum
{
	LightTriggerType_Toggle = 0x00,
	LightTriggerType_Deactivate,
	LightTriggerType_Activate,
	LightTriggerType_None,
	LightTriggerType_NumTypes,
};

// +==============================+
// |      RailTriggerType_t       |
// +==============================+
enum
{
	RailTriggerType_None = 0x00,
	RailTriggerType_ToggleInner,
	RailTriggerType_ToggleOuter,
	RailTriggerType_ToggleBoth,
	RailTriggerType_DeactivateInner,
	RailTriggerType_DeactivateOuter,
	RailTriggerType_DeactivateBoth,
	RailTriggerType_ActivateInner,
	RailTriggerType_ActivateOuter,
	RailTriggerType_ActivateBoth,
	RailTriggerType_NumOptions,
};

// +==============================+
// |     PistonTriggerType_t      |
// +==============================+
enum
{
	PistonTriggerType_Toggle = 0x00,
	PistonTriggerType_Retract,
	PistonTriggerType_Extend,
	PistonTriggerType_None,
	PistonTriggerType_NumTypes,
};

// +==============================+
// |       PistonVariant_t        |
// +==============================+
enum
{
	PistonVariant_Stone = 0x00,
	PistonVariant_Metal,
	PistonVariant_Wood,
	PistonVariant_NumVariants,
};

// +==============================+
// |      PistonArmVariant_t      |
// +==============================+
enum
{
	PistonArmVariant_Default = 0x00,
	PistonArmVariant_DefaultExt,
	PistonArmVariant_Metal,
	PistonArmVariant_MetalExt,
	PistonArmVariant_Wood,
	PistonArmVariant_WoodExt,
	PistonArmVariant_NumVariants,
};

// +==============================+
// |       PistonArmAnim_t        |
// +==============================+
typedef enum
{
	PistonArmAnim_Idle = 0,
	PistonArmAnim_Falling,
	PistonArmAnim_Burning,
	PistonArmAnim_WaterSplashing,
	PistonArmAnim_LavaSplashing,
} PistonArmAnim_t;

// +==============================+
// |    FireStandTriggerType_t    |
// +==============================+
enum
{
	FireStandTriggerType_Toggle = 0x00,
	FireStandTriggerType_Light,
	FireStandTriggerType_Unlight,
	FireStandTriggerType_None,
	FireStandTriggerType_NumTypes,
};

// +==============================+
// |   WallButtonTriggerType_t    |
// +==============================+
enum
{
	WallButtonTriggerType_OnPress = 0x00,
	WallButtonTriggerType_OnRelease,
	WallButtonTriggerType_OnBoth,
	WallButtonTriggerType_None,
	WallButtonTriggerType_NumTypes,
};

// +==============================+
// |    ChestTrapTriggerType_t    |
// +==============================+
enum
{
	ChestTrapTriggerType_OnOpened = 0x00,
	ChestTrapTriggerType_OnOpening,
	ChestTrapTriggerType_OnItemCollect,
	ChestTrapTriggerType_OnShown,
	ChestTrapTriggerType_OnHidden,
	ChestTrapTriggerType_None,
	ChestTrapTriggerType_NumTypes,
};

// +==============================+
// |   DeathTimerTriggerType_t    |
// +==============================+
enum
{
	DeathTimerTriggerType_Toggle = 0x00,
	DeathTimerTriggerType_Start,
	DeathTimerTriggerType_Stop,
	DeathTimerTriggerType_None,
	DeathTimerTriggerType_StartStopOnPolarity,
	DeathTimerTriggerType_StartOnPositive,
	DeathTimerTriggerType_StartOnNegative,
	DeathTimerTriggerType_StopStartOnPolarity,
	DeathTimerTriggerType_StopOnPositive,
	DeathTimerTriggerType_StopOnNegative,
	DeathTimerTriggerType_ToggleOnPositive,
	DeathTimerTriggerType_ToggleOnNegative,
	DeathTimerTriggerType_NumTypes,
};

// +==============================+
// |    DeathTimerTrackType_t     |
// +==============================+
enum
{
	DeathTimerTrackType_Players = 0x00,
	DeathTimerTrackType_Boxes,
	DeathTimerTrackType_Monsters,
	DeathTimerTrackType_Friendlies,
	DeathTimerTrackType_Projectiles,
	DeathTimerTrackType_NumTypes,
};

// +==============================+
// |         ActiveArea_t         |
// +==============================+
typedef enum
{
	ActiveArea_Level = 0x00,
	ActiveArea_Room,
	ActiveArea_Chunk,
	ActiveArea_Region,
	ActiveArea_Adjacent,
	ActiveArea_Orthogonal,
	ActiveArea_SingleSpace,
	ActiveArea_NumOptions,
} ActiveArea_t;

// +==============================+
// |      TimerTriggerType_t      |
// +==============================+
enum
{
	TimerTriggerType_None = 0x00,
	TimerTriggerType_Toggle,
	TimerTriggerType_Stop,
	TimerTriggerType_Start,
	TimerTriggerType_IncreasePeriod,
	TimerTriggerType_DecreasePeriod,
	TimerTriggerType_NumTypes,
};

// +==============================+
// |     RotatorTriggerType_t     |
// +==============================+
enum
{
	RotatorTriggerType_Clockwise = 0x00,
	RotatorTriggerType_CounterClockwise,
	RotatorTriggerType_Flip,
	RotatorTriggerType_Down,
	RotatorTriggerType_Left,
	RotatorTriggerType_Right,
	RotatorTriggerType_Up,
	RotatorTriggerType_Random,
	RotatorTriggerType_RoundRobin,
	RotatorTriggerType_None,
	RotatorTriggerType_NumTypes,
};

// +==============================+
// |   CrystalBallTriggerType_t   |
// +==============================+
enum
{
	CrystalBallTriggerType_None = 0x00,
	CrystalBallTriggerType_OnLookAt,
	CrystalBallTriggerType_OnLookAway,
	CrystalBallTriggerType_OnBoth,
	CrystalBallTriggerType_OnLookConstant,
	CrystalBallTriggerType_NumTypes,
};

// +==============================+
// |    CrystalCameraOffset_t     |
// +==============================+
enum
{
	CrystalCameraOffset_None = 0x00,
	CrystalCameraOffset_Right,
	CrystalCameraOffset_Down,
	CrystalCameraOffset_DownRight,
	CrystalCameraOffset_NumOptions,
};

// +==============================+
// |         GlyphLayer_t         |
// +==============================+
enum
{
	GlyphLayer_Top = 0x00,
	GlyphLayer_Middle,
	GlyphLayer_Bottom,
	GlyphLayer_NumLayers,
}; 

// +==============================+
// |     CandleTriggerType_t      |
// +==============================+
enum
{
	CandleTriggerType_None = 0x00,
	CandleTriggerType_OnLight,
	CandleTriggerType_OnExtinguish,
	CandleTriggerType_OnBoth,
	CandleTriggerType_NumTypes,
};

// +==============================+
// |     TurretTriggerType_t      |
// +==============================+
enum
{
	TurretTriggerType_None = 0x00,
	TurretTriggerType_Shoot,
	TurretTriggerType_Activate,
	TurretTriggerType_Deactivate,
	TurretTriggerType_Toggle,
	TurretTriggerType_DeleteOldPortals,
	TurretTriggerType_RotateClockwise,
	TurretTriggerType_RotateCounterClockwise,
	TurretTriggerType_FlipRotation,
	TurretTriggerType_NumTypes,
};

// +==============================+
// |           TextId_t           |
// +==============================+
enum
{
	TextId_SecretRoom = 0x00,
	TextId_ComingSoon,
	TextId_Test,
	TextId_Sword,
	TextId_Unfinished,
	TextId_Demo,
	TextId_NothingToSeeHere,
	TextId_Impossible,
	TextId_Interact,
	TextId_Move,
	TextId_Undo,
	TextId_Reset,
	TextId_Faster,
	TextId_Slower,
	TextId_MoveJapanese,
	TextId_InteractJapanese,
	TextId_ResetJapanese,
	TextId_UndoJapanese,
	TextId_FasterJapanese,
	TextId_SlowerJapanese,
	TextId_TitleJapanese,
	TextId_OldLevel,
	TextId_MoreLevels,
	TextId_StartFriendshipMode,
	TextId_NumIds,
};

// +==============================+
// |     CounterTriggerType_t     |
// +==============================+
enum
{
	CountType_None = 0x00,
	CountType_IncrementOnTrigger,
	CountType_DecrementOnTrigger,
	CountType_CountEntities,
	CountType_NumTypes,
};

// +==============================+
// |      OperatorVariant_t       |
// +==============================+
enum
{
	OperatorVariant_Plus = 0x00,
	OperatorVariant_Minus,
	OperatorVariant_Times,
	OperatorVariant_Divide,
	OperatorVariant_Xor,
	OperatorVariant_NumVariants,
};

// +==============================+
// |     ComparatorVariant_t      |
// +==============================+
enum
{
	ComparatorVariant_Equal = 0x00,
	ComparatorVariant_NotEqual,
	ComparatorVariant_GreaterThan,
	ComparatorVariant_LessThan,
	ComparatorVariant_GreaterThanOrEqual,
	ComparatorVariant_LessThanOrEqual,
	ComparatorVariant_IsTrue,
	ComparatorVariant_IsFalse,
	ComparatorVariant_OnIncrement,
	ComparatorVariant_OnDecrement,
	ComparatorVariant_NumVariants,
};

// +==============================+
// |     NumberTriggerType_t      |
// +==============================+
enum
{
	NumberTriggerType_None = 0x00,
	NumberTriggerType_Increment,
	NumberTriggerType_Decrement,
	NumberTriggerType_Clear,
	NumberTriggerType_NumTypes,
};

// +==============================+
// |      GiverTriggerType_t      |
// +==============================+
enum
{
	GiverTriggerType_None = 0x00,
	GiverTriggerType_Toggle,
	GiverTriggerType_Enable,
	GiverTriggerType_Disable,
	GiverTriggerType_NumTypes,
};

// +==============================+
// |      TakerTriggerType_t      |
// +==============================+
enum
{
	TakerTriggerType_None = 0x00,
	TakerTriggerType_Toggle,
	TakerTriggerType_Enable,
	TakerTriggerType_Disable,
	TakerTriggerType_NumTypes,
};

// +==============================+
// |    DisruptorTriggerType_t    |
// +==============================+
enum
{
	DisruptorTriggerType_None = 0x00,
	DisruptorTriggerType_Toggle,
	DisruptorTriggerType_Enable,
	DisruptorTriggerType_Disable,
	DisruptorTriggerType_NumTypes,
};

// +==============================+
// |    FriendshipPadVariant_t    |
// +==============================+
enum
{
	FriendshipPadVariant_Player1 = 0x00,
	FriendshipPadVariant_Player2,
	FriendshipPadVariant_NumVariants,
};

// +==============================+
// |      ResetterVariant_t       |
// +==============================+
enum
{
	ResetterVariant_Default = 0x00,
	ResetterVariant_CoopPlayer1,
	ResetterVariant_CoopPlayer2,
	ResetterVariant_Meta,
	ResetterVariant_NumVariants,
};

// +==============================+
// |       ResetCategory_t        |
// +==============================+
enum
{
	ResetCategory_Monsters     = 0x01,
	ResetCategory_KeysAndDoors = 0x02,
	ResetCategory_Powerups     = 0x04,
	ResetCategory_Diamonds     = 0x08,
	ResetCategory_NumCategories = 4,
};

// +==============================+
// |        MonsterType_t         |
// +==============================+
typedef enum
{
	MonsterType_AnyMonster = 0x00,
	MonsterType_Large,
	MonsterType_Small,
	MonsterType_Bats,
	MonsterType_Skeletons,
	MonsterType_Mice,
	MonsterType_Ghosts,
	MonsterType_Robots,
	MonsterType_Goblins,
	MonsterType_Spiders,
	MonsterType_Critters,
	MonsterType_Moths,
	MonsterType_AnyFriendly,
	MonsterType_Penguins,
	MonsterType_Bunnies,
	MonsterType_RainClouds,
	MonsterType_NumMonsterTypes,
} MonsterType_t;

// +==============================+
// |       MonsterAiType_t        |
// +==============================+
typedef enum
{
	MonsterAiType_None = 0x00,
	MonsterAiType_Random,
	MonsterAiType_Bounce,
	MonsterAiType_PatrolRight,
	MonsterAiType_PatrolLeft,
	MonsterAiType_TurnRight,
	MonsterAiType_TurnLeft,
	MonsterAiType_SeePlayer,
	MonsterAiType_FollowPlayer,
	MonsterAiType_CopyPlayer,
	MonsterAiType_OppositePlayer,
	MonsterAiType_MirrorHorizontal,
	MonsterAiType_MirrorVertical,
	MonsterAiType_AutoPull,
	MonsterAiType_Forward,
	MonsterAiType_TurnRandom,
	MonsterAiType_RunFromPlayer,
	MonsterAiType_FacePlayer,
	MonsterAiType_RotatedClockwise,
	MonsterAiType_RotatedCounter,
	MonsterAiType_RotatedMirrorHori,
	MonsterAiType_RotatedMirrorVert,
	MonsterAiType_NumTypes,
} MonsterAiType_t;

// +==============================+
// |       PlayerVariant_t        |
// +==============================+
typedef enum
{
	PlayerVariant_Solo = 0,
	PlayerVariant_CoopPlayer1,
	PlayerVariant_CoopPlayer2,
	PlayerVariant_CoopPlayer3,
	PlayerVariant_NumVariants,
} PlayerVariant_t;

// +==============================+
// |         PlayerAnim_t         |
// +==============================+
typedef enum
{
	PlayerAnim_Idle = 0,
	PlayerAnim_Walking,
	PlayerAnim_Pushing,
	PlayerAnim_LavaSinking,
	PlayerAnim_Drowning,
	PlayerAnim_Falling,
	PlayerAnim_Sliding,
	PlayerAnim_PullingBag,
	PlayerAnim_Exploding,
	PlayerAnim_Resetting,
	PlayerAnim_PullingBox,
	PlayerAnim_SwordSwing,
	PlayerAnim_Warping,
	PlayerAnim_PullingLever,
	PlayerAnim_Burning,
	PlayerAnim_OpeningChest,
	PlayerAnim_Bow,
	PlayerAnim_LevelComplete,
	PlayerAnim_CantMove,
	PlayerAnim_Leaning,
	PlayerAnim_Interacting,
	PlayerAnim_Petting,
	PlayerAnim_PressingWallButton,
	PlayerAnim_HighFive,
	PlayerAnim_Dead,
	PlayerAnim_DeadAfterReset,
	PlayerAnim_Digging,
	PlayerAnim_Spawning,
	PlayerAnim_Dancing,
	PlayerAnim_ActivateResetter,
	PlayerAnim_SwordBonk,
	PlayerAnim_SwordSwingNonLethal,
	PlayerAnim_AutoRespawning,
	PlayerAnim_NumAnims,
} PlayerAnim_t;

const char* GetPlayerAnimStr(u8 playerAnim)
{
	switch (playerAnim)
	{
		case PlayerAnim_Idle:               return "Idle";
		case PlayerAnim_Walking:            return "Walking";
		case PlayerAnim_Pushing:            return "Pushing";
		case PlayerAnim_LavaSinking:        return "LavaSinking";
		case PlayerAnim_Drowning:           return "Drowning";
		case PlayerAnim_Falling:            return "Falling";
		case PlayerAnim_Sliding:            return "Sliding";
		case PlayerAnim_PullingBag:         return "PullingBag";
		case PlayerAnim_Exploding:          return "Exploding";
		case PlayerAnim_Resetting:          return "Resetting";
		case PlayerAnim_PullingBox:         return "PullingBox";
		case PlayerAnim_SwordSwing:         return "SwordSwing";
		case PlayerAnim_Warping:            return "Warping";
		case PlayerAnim_PullingLever:       return "PullingLever";
		case PlayerAnim_Burning:            return "Burning";
		case PlayerAnim_OpeningChest:       return "OpeningChest";
		case PlayerAnim_Bow:                return "Bow";
		case PlayerAnim_LevelComplete:      return "LevelComplete";
		case PlayerAnim_CantMove:           return "CantMove";
		case PlayerAnim_Leaning:            return "Leaning";
		case PlayerAnim_Interacting:        return "Interacting";
		case PlayerAnim_Petting:            return "Petting";
		case PlayerAnim_PressingWallButton: return "PressingWallButton";
		case PlayerAnim_HighFive:           return "HighFive";
		case PlayerAnim_Dead:               return "Dead";
		case PlayerAnim_DeadAfterReset:     return "DeadAfterReset";
		case PlayerAnim_Digging:            return "Digging";
		case PlayerAnim_Spawning:           return "Spawning";
		case PlayerAnim_Dancing:            return "Dancing";
		case PlayerAnim_SwordBonk:          return "SwordBonk";
		case PlayerAnim_SwordSwingNonLethal:return "SwordSwingNonLethal";
		default: return "Unknown";
	}
}

// +==============================+
// |        MonsterAnim_t         |
// +==============================+
typedef enum
{
	MonsterAnim_Idle = 0,
	MonsterAnim_LavaSinking,
	MonsterAnim_Drowning,
	MonsterAnim_Falling,
	MonsterAnim_Exploding,
	MonsterAnim_Squashed,
	MonsterAnim_Burning,
} MonsterAnim_t;

// +==============================+
// |          PetAnim_t           |
// +==============================+
typedef enum
{
	PetAnim_Idle = 0x00,
	PetAnim_BeingPet,
	PetAnim_Barking,
	PetAnim_Eating,
} PetAnim_t;

// +==============================+
// |          BoxAnim_t           |
// +==============================+
typedef enum
{
	BoxAnim_Idle = 0,
	BoxAnim_Moving,
	BoxAnim_Lit,
	BoxAnim_Falling,
	BoxAnim_Burning,
	BoxAnim_WaterSplashing,
	BoxAnim_LavaSplashing,
	BoxAnim_Breaking,
} BoxAnim_t;

// +==============================+
// |     PressurePlateAnim_t      |
// +==============================+
typedef enum
{
	PressurePlateAnim_Idle = 0,
	PressurePlateAnim_Pressed,
} PressurePlateAnim_t;

// +==============================+
// |       ProjectileAnim_t       |
// +==============================+
typedef enum
{
	ProjectileAnim_Default = 0,
	ProjectileAnim_HitHere,
	ProjectileAnim_HitFront,
	ProjectileAnim_MeetInTheMiddle,
	ProjectileAnim_CornerHit,
} ProjectileAnim_t;

// +==============================+
// |         PortalAnim_t         |
// +==============================+
typedef enum
{
	PortalAnim_Idle = 0,
	PortalAnim_Create,
	PortalAnim_Destroy,
} PortalAnim_t;

// +==============================+
// |   PlayerSeparatorVariant_t   |
// +==============================+
enum
{
	PlayerSeparatorVariant_Player1 = 0,
	PlayerSeparatorVariant_Player2,
	PlayerSeparatorVariant_NumVariants,
};

// +==============================+
// | PlayerSeparatorTriggerType_t |
// +==============================+
enum
{
	PlayerSeparatorTriggerType_None = 0,
	PlayerSeparatorTriggerType_Toggle,
	PlayerSeparatorTriggerType_Disable,
	PlayerSeparatorTriggerType_Enable,
	PlayerSeparatorTriggerType_NumTypes,
};

// +==============================+
// |      LevelBorderType_t       |
// +==============================+
enum
{
	LevelBorderType_Grass = 0,
	LevelBorderType_Leaves,
	LevelBorderType_Castle,
	LevelBorderType_Lava,
	LevelBorderType_Crystal,
	LevelBorderType_Simple,
	LevelBorderType_None,
	LevelBorderType_GoldTrim,
	LevelBorderType_NumTypes,
};
// +==============================+
// |    LevelBackgroundType_t     |
// +==============================+
enum
{
	LevelBackgroundType_Grass = 0,
	LevelBackgroundType_Dirt,
	LevelBackgroundType_Lava,
	LevelBackgroundType_Crystal,
	LevelBackgroundType_Diagonals,
	LevelBackgroundType_Solid,
	LevelBackgroundType_NumTypes,
};

// +==============================+
// |       RegionVariant_t        |
// +==============================+
enum
{
	RegionVariant_Default = 0x00,
	RegionVariant_Puzzle,
	RegionVariant_Secret,
	RegionVariant_PortalDeleter,
	RegionVariant_NumVariants,
};

// +==============================+
// |   MonsterDoorTriggerType_t   |
// +==============================+
enum
{
	MonsterDoorTriggerType_None = 0x00,
	MonsterDoorTriggerType_OnOpen,
	MonsterDoorTriggerType_OnDecrease,
	MonsterDoorTriggerType_OnIncrease,
	MonsterDoorTriggerType_OnNumberChange,
	MonsterDoorTriggerType_OnReappear,
	MonsterDoorTriggerType_NumTypes,
};

// +==============================+
// |   DiamondDoorTriggerType_t   |
// +==============================+
enum
{
	DiamondDoorTriggerType_None = 0x00,
	DiamondDoorTriggerType_OnOpen,
	DiamondDoorTriggerType_OnDecrease,
	DiamondDoorTriggerType_OnIncrease,
	DiamondDoorTriggerType_OnNumberChange,
	DiamondDoorTriggerType_OnReappear,
	DiamondDoorTriggerType_NumTypes,
};

// +==============================+
// |   TetrisDoorTriggerType_t    |
// +==============================+
enum
{
	TetrisDoorTriggerType_None = 0x00,
	TetrisDoorTriggerType_OnOpen,
	TetrisDoorTriggerType_OnDecrease,
	TetrisDoorTriggerType_OnIncrease,
	TetrisDoorTriggerType_OnNumberChange,
	TetrisDoorTriggerType_OnReappear,
	TetrisDoorTriggerType_NumTypes,
};

// +==============================+
// |     LedPadTriggerType_t      |
// +==============================+
enum
{
	LedPadTriggerType_None = 0x00,
	LedPadTriggerType_OnLight,
	LedPadTriggerType_OnUnlight,
	LedPadTriggerType_OnBoth,
	LedPadTriggerType_OnPermaLit,
	LedPadTriggerType_NumTypes,
};

// +==============================+
// |     LedPadRequiredType_t     |
// +==============================+
enum
{
	LedPadRequiredType_GenericLed = 0x00,
	LedPadRequiredType_LedBox,
	LedPadRequiredType_LedBarrel,
	LedPadRequiredType_LedBoulder,
	LedPadRequiredType_AnyMonster,
	LedPadRequiredType_Bat,
	LedPadRequiredType_Skeleton,
	LedPadRequiredType_Mouse,
	LedPadRequiredType_Ghost,
	LedPadRequiredType_Robot,
	LedPadRequiredType_Goblin,
	LedPadRequiredType_Spider,
	LedPadRequiredType_AnyFriendly,
	LedPadRequiredType_Penguin,
	LedPadRequiredType_Bunny,
	LedPadRequiredType_RainCloud,
	LedPadRequiredType_NumTypes,
};

// +==============================+
// |       MonsterVariant_t       |
// +==============================+
enum
{
	MonsterVariant_Bat = 0x00,
	MonsterVariant_Skeleton,
	MonsterVariant_Mouse,
	MonsterVariant_Ghost,
	MonsterVariant_Robot,
	MonsterVariant_Goblin,
	MonsterVariant_Spider,
	MonsterVariant_NumVariants,
};

// +==============================+
// |      FriendlyVariant_t       |
// +==============================+
enum
{
	FriendlyVariant_Penguin = 0x00,
	FriendlyVariant_Bunny,
	FriendlyVariant_RainCloud,
	FriendlyVariant_NumVariants,
};

// +==============================+
// |   RequireDoorTriggerType_t   |
// +==============================+
enum
{
	RequireDoorTriggerType_CountOnOff = 0x00,
	RequireDoorTriggerType_CountOffOn,
	RequireDoorTriggerType_Increment,
	RequireDoorTriggerType_Decrement,
	RequireDoorTriggerType_IncrementForOn,
	RequireDoorTriggerType_DecrementForOff,
	RequireDoorTriggerType_DecrementForOn,
	RequireDoorTriggerType_IncrementForOff,
	RequireDoorTriggerType_None,
	RequireDoorTriggerType_NumTypes,
};

// +==============================+
// |        CrackVariant_t        |
// +==============================+
enum
{
	CrackVariant_Hole = 0x00,
	CrackVariant_Water,
	CrackVariant_Lava,
	CrackVariant_NumberHole,
	CrackVariant_NumberWater,
	CrackVariant_NumberLava,
	CrackVariant_NumVariants,
};

// +==============================+
// |  CrystalCameraInputMethod_t  |
// +==============================+
enum
{
	CrystalCameraInputMethod_Any = 0x00,
	CrystalCameraInputMethod_KeyboardMouse,
	CrystalCameraInputMethod_AnyController,
	CrystalCameraInputMethod_XboxController,
	CrystalCameraInputMethod_PlaystationController,
	CrystalCameraInputMethod_NumOptions,
};

// +==============================+
// |     PainterTriggerType_t     |
// +==============================+
enum
{
	PainterTriggerType_Paint = 0x00,
	PainterTriggerType_Alternate,
	PainterTriggerType_None,
	PainterTriggerType_NumTypes,
};

// +==============================+
// |        LaserVariant_t        |
// +==============================+
enum
{
	LaserVariant_Default = 0x00,
	LaserVariant_Deadly,
	LaserVariant_NumVariants,
};

// +==============================+
// |      LaserTriggerType_t      |
// +==============================+
enum
{
	LaserTriggerType_None = 0x00,
	LaserTriggerType_Toggle,
	LaserTriggerType_Enable,
	LaserTriggerType_Disable,
	LaserTriggerType_NumTypes,
};

// +==============================+
// |   LaserReceptacleVariant_t   |
// +==============================+
enum
{
	LaserReceptacleVariant_Default = 0x00,
	LaserReceptacleVariant_Deadly,
	LaserReceptacleVariant_NumVariants,
};

// +==============================+
// | LaserReceptacleTriggerType_t |
// +==============================+
enum
{
	LaserReceptacleTriggerType_None = 0x00,
	LaserReceptacleTriggerType_OnActivated,
	LaserReceptacleTriggerType_OnDeactivated,
	LaserReceptacleTriggerType_OnBoth,
	LaserReceptacleTriggerType_OnNewSideLit,
	LaserReceptacleTriggerType_OnNewSideUnlit,
	LaserReceptacleTriggerType_OnLitCountChanged,
	LaserReceptacleTriggerType_NumTypes,
};

// +==============================+
// |       MirrorVariant_t        |
// +==============================+
enum
{
	MirrorVariant_SinglePane = 0x00,
	MirrorVariant_DoublePane,
	MirrorVariant_SinglePaneHandled,
	MirrorVariant_NumVariants,
};

// +==============================+
// |         MirrorAnim_t         |
// +==============================+
typedef enum
{
	MirrorAnim_Idle = 0x00,
	MirrorAnim_Falling,
	MirrorAnim_LavaSplashing,
	MirrorAnim_WaterSplashing,
	MirrorAnim_Breaking,
} MirrorAnim_t;

// +==============================+
// |      IceBlockVariant_t       |
// +==============================+
enum
{
	IceBlockVariant_Normal = 0x00,
	IceBlockVariant_Weak,
	IceBlockVariant_Bubble,
	IceBlockVariant_NumVariants,
};

typedef enum
{
	IceBlockAnim_Idle = 0x00,
	IceBlockAnim_Forming,
	IceBlockAnim_Melting,
	IceBlockAnim_Popping,
	IceBlockAnim_Falling,
	IceBlockAnim_WaterSplashing,
	IceBlockAnim_LavaSplashing,
	IceBlockAnim_NumAnims,
} IceBlockAnim_t;

// +==============================+
// |      EffectPadVariant_t      |
// +==============================+
enum
{
	EffectPadVariant_Melt = 0x00,
	EffectPadVariant_Freeze,
	EffectPadVariant_Bubble,
	EffectPadVariant_NumVariants,
};

// +==============================+
// |      OcclusionVariant_t      |
// +==============================+
enum
{
	OcclusionVariant_Cloud = 0x00,
	OcclusionVariant_Bubbles,
	OcclusionVariant_NumVariants,
};

// +==============================+
// |     GroundCoverVariant_t     |
// +==============================+
enum
{
	GroundCoverVariant_Dust = 0x00,
	GroundCoverVariant_NumVariants,
};

// +==============================+
// |    EffectPadTriggerType_t    |
// +==============================+
enum
{
	EffectPadTriggerType_None = 0x00,
	EffectPadTriggerType_Toggle,
	EffectPadTriggerType_Activate,
	EffectPadTriggerType_Deactivate,
	EffectPadTriggerType_NumTypes,
};

// +==============================+
// |     CustomGlypVariant_t      |
// +==============================+
enum
{
	CustomGlyphVariant_DoubleScale = 0x00,
	CustomGlyphVariant_SingleScale,
	CustomGlyphVariant_TopLeft,
	CustomGlyphVariant_TopRight,
	CustomGlyphVariant_BottomLeft,
	CustomGlyphVariant_BottomRight,
	CustomGlyphVariant_NumVariants,
};

// +==============================+
// |     DisplayTriggerType_t     |
// +==============================+
enum
{
	DisplayTriggerType_None = 0x00,
	DisplayTriggerType_ToggleDisplay,
	DisplayTriggerType_TurnOff,
	DisplayTriggerType_TurnOn,
	DisplayTriggerType_ToggleSuccess,
	DisplayTriggerType_ShowSuccess,
	DisplayTriggerType_HideSuccess,
	DisplayTriggerType_NumTypes,
};

// +==============================+
// |    AttachedDecorVariant_t    |
// +==============================+
enum
{
	AttachedDecorVariant_GooglyEye = 0x00,
	AttachedDecorVariant_GooglyEyes,
	AttachedDecorVariant_NumVariants,
};

// +==============================+
// |   AttachDecorRotateType_t    |
// +==============================+
typedef enum
{
	AttachDecorRotateType_TopDown = 0x00,
	AttachDecorRotateType_FullStandingRotate,
	AttachDecorRotateType_HalfRotate,
	AttachDecorRotateType_NumTypes,
} AttachDecorRotateType_t;

// +==============================+
// |         SongOption_t         |
// +==============================+
typedef enum
{
	SongOption_None = 0x00,
	SongOption_Max,
	SongOption_Explore,
	SongOption_Breeze,
	SongOption_Pathway,
	SongOption_Treatbeat,
	SongOption_Acoustic,
	SongOption_Discovery,
	SongOption_Aelish,
	SongOption_ComputerComp,
	SongOption_MainMenu,
	SongOption_Trailer,
	SongOption_GrassActive,
	SongOption_GrassPassive,
	SongOption_MagicActive,
	SongOption_MagicPassive,
	SongOption_DesertActive,
	SongOption_DesertPassive,
	SongOption_FactoryActive,
	SongOption_FactoryPassive,
	SongOption_SpaceActive,
	SongOption_SpacePassive,
	SongOption_DungeonAmbient,
	SongOption_NumOptions,
} SongOption_t;

// +==============================+
// |   TriggerGateTriggerType_t   |
// +==============================+
enum
{
	TriggerGateTriggerType_None = 0x00,
	TriggerGateTriggerType_Toggle,
	TriggerGateTriggerType_Enable,
	TriggerGateTriggerType_Disable,
	TriggerGateTriggerType_EnableDisableOnPolarity,
	TriggerGateTriggerType_EnableOnPositive,
	TriggerGateTriggerType_EnableOnNegative,
	TriggerGateTriggerType_DisableEnableOnPolarity,
	TriggerGateTriggerType_DisableOnPositive,
	TriggerGateTriggerType_DisableOnNegative,
	TriggerGateTriggerType_ToggleOnPositive,
	TriggerGateTriggerType_ToggleOnNegative,
	TriggerGateTriggerType_NumTypes,
};

// +==============================+
// |      PassalongVariant_t      |
// +==============================+
enum
{
	PassalongVariant_Corner = 0x00,
	PassalongVariant_Straight,
	PassalongVariant_Single,
	PassalongVariant_NumVariants,
};

// +==============================+
// |      CrackTriggerType_t      |
// +==============================+
enum
{
	CrackTriggerType_None = 0x00,
	CrackTriggerType_Break,
	CrackTriggerType_Crumble1,
	CrackTriggerType_Crumble2,
	CrackTriggerType_Crumble3,
	CrackTriggerType_Enforce1,
	CrackTriggerType_Enforce2,
	CrackTriggerType_Enforce3,
	CrackTriggerType_NumTypes,
};

// +==============================+
// |     ViewTargetVariant_t      |
// +==============================+
enum
{
	ViewTargetVariant_View1 = 0x00,
	ViewTargetVariant_View2,
	ViewTargetVariant_NumVariants,
};

// +==============================+
// |   ViewTargetTriggerType_t    |
// +==============================+
enum
{
	ViewTargetTriggerType_None = 0x00,
	ViewTargetTriggerType_ToggleFocus,
	ViewTargetTriggerType_Focus,
	ViewTargetTriggerType_Unfocus,
	ViewTargetTriggerType_NumTypes,
};

// +==============================+
// |      FloorMarkVariant_t      |
// +==============================+
enum
{
	FloorMarkVariant_SlimeTrail = 0x00,
	FloorMarkVariant_Footsteps,
	FloorMarkVariant_NumVariants,
};

// +==============================+
// |     ColorFieldVariant_t      |
// +==============================+
enum
{
	ColorFieldVariant_SameColor = 0x00,
	ColorFieldVariant_NotSameColor,
	ColorFieldVariant_NumVariants,
};

// +==============================+
// |      ColorFieldShape_t       |
// +==============================+
enum
{
	ColorFieldShape_Straight = 0x00,
	ColorFieldShape_Corner,
	ColorFieldShape_DeadEnd,
	ColorFieldShape_InnerCorner,
	ColorFieldShape_Large,
	ColorFieldShape_LargeCorner,
	ColorFieldShape_NumShapes,
};

#endif //  _APP_ENTITY_TYPES_H
