/*
File:   game_defines.h
Author: Taylor Robbins
Date:   07\10\2020
Description:
	** Defines that actually matter
*/

#ifndef _GAME_DEFINES_H
#define _GAME_DEFINES_H

#define DASH_MIN_HOLD_TIME 180
#define VIEW_FOLLOW_LAG    10 //divisor
#define LEVEL_RENDER_SCALE 2

#define DEFAULT_CHUG_SPEED           ((60.0f*1000.0f) / 131.0f) //about 458.015ms (131 bpm)
#define CHUG_BEAT_OFFSET             49 //ms
#define MAX_CHUG_SYNCH_ERROR         200 //ms
#define TRAIN_DEFAULT_TILE_MOVE_TIME 1200 //ms
#define CHUG_CUTOFF_TIME             0.2f //20% into plume animation is still allowed
#define CONDUCTOR_ANIM_TIME          240 //ms
#define TRAIN_DEATH_ANIM_TIME        500 //ms
#define SHOVELER_IDLE_TIME           2000 //ms
#define FAIL_TRACK_ANIM_TIME         500 //ms
#define FURNACE_ANIM_TIME            400 //ms
#define COMPLETION_ANIM_TIME         1000 //ms
#define COMPLETION_MENU_OPEN_TIME    1000 //ms
#define TRACK_CREATE_ANIM_TIME       500 //ms
#define MESSAGE_WINDOW_WIDTH         500 //px
#define MESSAGE_OPEN_ANIM_TIME       200 //ms
#define MESSAGE_CLOSE_ANIM_TIME      200 //ms
#define DERAILED_MESSAGE_TIME        1000 //ms
#define HORN_EFFECT_DISTANCE         (5*32) //px
#define SHEEP_SCATTER_DISTANCE       (8*32) //px
#define SHEEP_SCATTER_TIME           1000 //ms

#define RESET_MESSAGE "Press 'R' to reset"

#define NUM_CODE_CARDS                  8
#define UNDO_CODE                       "xx"
#define TRACK_STRAIGHT_CODE             "zz"
#define TRACK_CORNER_RIGHT_CODE         "zxx"
#define TRACK_CORNER_LEFT_CODE          "xxz"
#define TRACK_BRIDGE_STRAIGHT_CODE      "xzz"
#define TRACK_BRIDGE_CORNER_RIGHT_CODE  "xzxx"
#define TRACK_BRIDGE_CORNER_LEFT_CODE   "xxxz"
#define TRAIN_HORN_BLOW_CODE            "zzxx"

#define FIRST_MESSAGE      "Oh no! The train's brakes have broken, and the track is unfinished!\n.\rThe captain has jumped out and run ahead to try and finish the track.\n.\rTell him which track to build using the smoke signals you agreed upon."
#define SECOND_MESSAGE     "Use the Z and X buttons to fuel the two different furnaces and change the color of the smoke.\n.\rHit enter to commit the message."
#define LEFT_TURN_MESSAGE  "The wonders of modern Technology have taught us to turn Left!\n.\rUse X,X,Z to Turn Left."
#define RIGHT_TURN_MESSAGE "The engineers just realized three lefts make a right!\n.\rUse Z,X,X to Turn Right."
#define UNDO_MESSAGE       "The track layers love destroying things too!\n.\rUse X,X to remove previously placed track"
#define LEVEL4_MESSAGE     "Your doing great.\n.\rKeep up the good work."
#define SHEEP_MESSAGE      "Quick we dont want to run over innocent animals!\n.\rUse Z,Z,X,X to blow the horn and scare them away."
#define LEVEL6_MESSAGE     "Looks like theres a whole flock of sheep in our way now!\n.\rGood Luck!"
#define BRIDGE_MESSAGE     "It turns out wood can float!\n.\rUse X,Z,Z to place a Bridge Straight"
#define LEVEL8_MESSAGE     "How did all those sheep get there?\n.\rWell lets go say hi."
#define LEVEL9_MESSAGE     "Floating and Turning! What will we think of next?\n.\rUse X,Z,X,X to place a Bridge Right\n.\rUse X,X,X,Z to place a Bridge Right"
#define LEVEL10_MESSAGE    "I heard there is a town off to the west.\n.\rHope I am correct."
#define LEVEL11_MESSAGE    "There should be an entrance to that village somewhere around here.\n.\rWatch out for all those trees."
#define LEVEL12_MESSAGE    "The capital city has been overrun by sheep!\n.\rWe could be heroes if we scare them away."

#endif //  _GAME_DEFINES_H
