/*
File:   game_func_defs.h
Author: Taylor Robbins
Date:   05\10\2019
*/

#ifndef _GAME_FUNC_DEFS_H
#define _GAME_FUNC_DEFS_H

//game_level.cpp
v2i GetChunkPos(const Level_t* level, v2i tilePos);
Room_t* GetRoomForChunk(Level_t* level, v2i chunkPos);
const Room_t* GetRoomForChunk(const Level_t* level, v2i chunkPos);
Room_t* GetRoomForPos(Level_t* level, v2i tilePos);
const Room_t* GetRoomForPos(const Level_t* level, v2i tilePos);

#endif //  _GAME_FUNC_DEFS_H
