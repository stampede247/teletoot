/*
File:   game_functions.cpp
Author: Taylor Robbins
Date:   07\11\2020
Description: 
	** Functions that are only called from game.cpp and use things from other game_ files
*/

bool GameLoadLevel(bool* noLevelFoundOut = nullptr)
{
	if (noLevelFoundOut != nullptr) { *noLevelFoundOut = false; }
	char* levelPath = nullptr;
	if (game->overrideLevelPath == nullptr)
	{
		levelPath = TempPrint(LEVELS_FLDR "level%u.lvl", game->currentLevelNumber);
	}
	else
	{
		levelPath = game->overrideLevelPath;
	}
	FileInfo_t levelFile = platform->ReadEntireFile(levelPath);
	if (levelFile.content == nullptr)
	{
		if (noLevelFoundOut != nullptr) { *noLevelFoundOut = true; }
		return false;
	}
	TempPushMark();
	Level_t pcqLevel;
	bool deserSuccess = DeserializeDesignText(TempArena, &pcqLevel, levelFile.content, levelFile.size);
	Assert(ArenaValidate(mainHeap));
	if (!deserSuccess) { return false; }
	DestroyTootLevel(&game->level);
	CreateTootLevel(mainHeap, &game->level, &pcqLevel);
	TempPopMark();
	platform->FreeFileMemory(&levelFile);
	InitView(&game->view, (NewVec2(game->level.train.tilePos) + Vec2_Half) * 32.0f);
	DynArrayClear(&game->currentMorse.pieces);
	if (game->levelComplete)
	{
		game->levelComplete = false;
		game->completionAnimTime = 0.0f;
		game->completionMenuOpenAnim = 0.0f;
	}
	if (game->overrideLevelPath == nullptr)
	{
		if (game->currentLevelNumber == 1)
        {
            if (MyStrLength32(LEFT_TURN_MESSAGE) > 0) { GameShowMessage(LEFT_TURN_MESSAGE); }
        }
        if (game->currentLevelNumber == 2)
        {
            if (MyStrLength32(RIGHT_TURN_MESSAGE) > 0) { GameShowMessage(RIGHT_TURN_MESSAGE); }
        }
        if (game->currentLevelNumber == 3)
        {
            if (MyStrLength32(UNDO_MESSAGE) > 0) { GameShowMessage(UNDO_MESSAGE); }
        }
		if (game->currentLevelNumber == 4)
		{
			if (MyStrLength32(LEVEL4_MESSAGE) > 0) { GameShowMessage(LEVEL4_MESSAGE); }
		}
		if (game->currentLevelNumber == 5)
		{
			if (MyStrLength32(SHEEP_MESSAGE) > 0) { GameShowMessage(SHEEP_MESSAGE); }
		}
		if (game->currentLevelNumber == 6)
		{
			if (MyStrLength32(LEVEL6_MESSAGE) > 0) { GameShowMessage(LEVEL6_MESSAGE); }
		}
		if (game->currentLevelNumber == 7)
		{
			if (MyStrLength32(BRIDGE_MESSAGE) > 0) { GameShowMessage(BRIDGE_MESSAGE); }
		}
		if (game->currentLevelNumber == 8)
		{
			if (MyStrLength32(LEVEL8_MESSAGE) > 0) { GameShowMessage(LEVEL8_MESSAGE); }
		}
		if (game->currentLevelNumber == 9)
		{
			if (MyStrLength32(LEVEL9_MESSAGE) > 0) { GameShowMessage(LEVEL9_MESSAGE); }
		}
		if (game->currentLevelNumber == 10)
		{
			if (MyStrLength32(LEVEL10_MESSAGE) > 0) { GameShowMessage(LEVEL10_MESSAGE); }
		}
		if (game->currentLevelNumber == 11)
		{
			if (MyStrLength32(LEVEL11_MESSAGE) > 0) { GameShowMessage(LEVEL11_MESSAGE); }
		}
		if (game->currentLevelNumber == 12)
		{
			if (MyStrLength32(LEVEL12_MESSAGE) > 0) { GameShowMessage(LEVEL12_MESSAGE); }
		}
	}
	
	if (game->level.songNumber == 0x01)
	{
		SoftQueueMusicChange(GetMusic(stayOnTrack2));
	}
	else if (game->level.songNumber == 0x02)
	{
		SoftQueueMusicChange(GetMusic(stayOnTrack3));
	}
	else if (game->level.songNumber == 0x03)
	{
		SoftQueueMusicChange(GetMusic(stayOnTrack4));
	}
	else
	{
		SoftQueueMusicChange(GetMusic(stayOnTrack1));
	}
	return true;
}
