/*
File:   game_tile.h
Author: Taylor Robbins
Date:   11\22\2018
*/

#ifndef _GAME_TILE_H
#define _GAME_TILE_H

#define TILE_ID_STR_LENGTH 2 //bytes

typedef enum
{
	//Grounds
	Tile_Ground = 0x00,
	Tile_WoodGround,
	Tile_MetalGround,
	Tile_SlimeGround,
	
	Tile_Nothing, //Used to be Tile_Wall
	
	//Hazards
	Tile_Water,
	Tile_Lava,
	Tile_Hole,
	Tile_SlimeWebHole,
	Tile_Ice,
	
	//Carpets
	Tile_Carpet,
	
	//Pushers
	Tile_Pusher,
	
	//Cracks
	Tile_CrackedGround,
	Tile_CrackedIce,
	
	//Other Tiles
	Tile_FloatingIce,
	Tile_FloatingSlime,
	Tile_LavaFilled,
	Tile_WaterFilled,
	NumTileTypes,
} TileType_t;

const char* GetTileTypeIdStr(TileType_t tileType)
{
	switch (tileType)
	{
		case Tile_Ground:        return "GD";
		case Tile_WoodGround:    return "WG";
		case Tile_MetalGround:   return "MG";
		case Tile_SlimeGround:   return "SG";
		case Tile_Nothing:       return "NO";
		case Tile_Water:         return "WT";
		case Tile_Lava:          return "LV";
		case Tile_Hole:          return "HL";
		case Tile_SlimeWebHole:  return "WB";
		case Tile_Ice:           return "IC";
		case Tile_Carpet:        return "CP";
		case Tile_Pusher:        return "PS";
		case Tile_CrackedGround: return "CG";
		case Tile_CrackedIce:    return "CI";
		case Tile_FloatingIce:   return "FI";
		case Tile_FloatingSlime: return "FS";
		case Tile_LavaFilled:    return "LF";
		case Tile_WaterFilled:   return "WF";
		default: return "UK";
	}
}

typedef enum
{
	RoofVariant_Default = 0x00,
	RoofVariant_Black1,
	RoofVariant_Black2,
	RoofVariant_Gray1,
	RoofVariant_Wood,
	RoofVariant_ForestCanopy,
	RoofVariant_Leaves,
	RoofVariant_Gray2,
	RoofVariant_DefaultGray,
	RoofVariant_WoodGray,
	RoofVariant_ForestCanopyGray,
	RoofVariant_LeavesGray,
	RoofVariant_House,
	RoofVariant_NumVariants,
} RoofVariant_t;

// +==============================+
// |       GroundVariant_t        |
// +==============================+
enum
{
	GroundVariant_Default = 0x00,
	GroundVariant_Grass,
	GroundVariant_Tiles,
	GroundVariant_LargeTiles,
	GroundVariant_Grooved,
	GroundVariant_Dirt,
	GroundVariant_Slime,
	GroundVariant_BrickOutline,
	GroundVariant_Sandstone1,
	GroundVariant_Sandstone2,
	GroundVariant_PowerGrid,
	GroundVariant_Sectional,
	GroundVariant_Brick1,
	GroundVariant_Brick2,
	GroundVariant_Brick3,
	GroundVariant_Brick4,
	GroundVariant_Brick5,
	GroundVariant_Fan,
	GroundVariant_Rocky,
	GroundVariant_LavaUneven,
	GroundVariant_LavaSmooth,
	GroundVariant_IceUneven,
	GroundVariant_IceSmooth,
	GroundVariant_FactoryTile,
	GroundVariant_FactoryCaution,
	GroundVariant_FactoryDecor1,
	GroundVariant_FactoryDecor2,
	GroundVariant_FactoryDecor3,
	GroundVariant_Rainbow,
	GroundVariant_Tessellation,
	GroundVariant_Sand,
	GroundVariant_SandBricks,
	GroundVariant_Soil,
	GroundVariant_Crystal,
	GroundVariant_DungeonBricks,
	GroundVariant_DungeonSmooth,
	GroundVariant_DungeonBordered,
	GroundVariant_GrassBordered,
	GroundVariant_Brick6,
	GroundVariant_Brick7,
	GroundVariant_Brick8,
	GroundVariant_Marble1,
	GroundVariant_WesternWood1,
	GroundVariant_WesternWood2,
	GroundVariant_NumVariants,
};

// +==============================+
// |        HoleVariant_t         |
// +==============================+
enum
{
	HoleVariant_Default = 0x00,
	HoleVariant_Clouds,
	HoleVariant_Space,
	HoleVariant_NumVariants,
};

// +==============================+
// |        WallVariant_t         |
// +==============================+
enum
{
	WallVariant_Default = 0x00,
	WallVariant_Rocky,
	WallVariant_Fence,
	WallVariant_Toggle,
	WallVariant_Brick,
	WallVariant_SlightlyRounded,
	WallVariant_Rounded,
	WallVariant_ReallyRounded,
	WallVariant_RockyRounded,
	WallVariant_Hint,
	WallVariant_Bezel,
	WallVariant_SmoothRock,
	WallVariant_Hedge,
	WallVariant_Palisade,
	WallVariant_Dirt,
	WallVariant_Royal,
	WallVariant_SlopedBrick,
	WallVariant_Foliage,
	WallVariant_Wood1,
	WallVariant_Roots,
	WallVariant_Invisible,
	WallVariant_Cobble,
	WallVariant_Magic1,
	WallVariant_Magic2,
	WallVariant_Magic3,
	WallVariant_Magic4,
	WallVariant_RootsFlat,
	WallVariant_PalisadeFlat,
	WallVariant_CobbleFlat,
	WallVariant_Vines1,
	WallVariant_Sloped,
	WallVariant_Lava,
	WallVariant_Factory,
	WallVariant_DungeonBrick,
	WallVariant_ElectricBrick,
	WallVariant_Bookshelf,
	WallVariant_Forest,
	WallVariant_IceRock,
	WallVariant_KingsPalace,
	WallVariant_Clouds,
	WallVariant_DesertBrick,
	WallVariant_TwistedRoots,
	WallVariant_CrystalCave,
	WallVariant_Space,
	WallVariant_NumVariants,
};

// +==============================+
// |       CarpetVariant_t        |
// +==============================+
enum
{
	CarpetVariant_EdgedPattern1,
	CarpetVariant_EdgedPattern2,
	CarpetVariant_EdgedPattern3,
	CarpetVariant_EdgedPattern4,
	CarpetVariant_EdgedPattern5,
	CarpetVariant_EdgedPattern6,
	CarpetVariant_EdgedPattern7,
	CarpetVariant_Pattern1,
	CarpetVariant_Pattern2,
	CarpetVariant_Pattern3,
	CarpetVariant_Pattern4,
	CarpetVariant_Pattern5,
	CarpetVariant_Pattern6,
	CarpetVariant_Pattern7,
	CarpetVariant_NumVariants,
};

// +==============================+
// |       PusherVariant_t        |
// +==============================+
enum
{
	PusherVariant_Right = 0,
	PusherVariant_Down,
	PusherVariant_Left,
	PusherVariant_Up,
	PusherVariant_NumVariants,
};

// +==============================+
// |     WaterFilledVariant_t     |
// +==============================+
enum
{
	WaterFilled_Box = 0x00,
	WaterFilled_Barrel1,
	WaterFilled_Barrel2,
	WaterFilled_Boulder,
	WaterFilled_NumVariants,
};

// +==============================+
// |     LavaFilledVariant_t      |
// +==============================+
enum
{
	LavaFilled_Box = 0x00,
	LavaFilled_Barrel1,
	LavaFilled_Barrel2,
	LavaFilled_Boulder,
	LavaFilled_NumVariants,
};

typedef enum
{
	TileStepSoundType_None = 0x00,
	TileStepSoundType_Default,
	TileStepSoundType_Loud,
	TileStepSoundType_Carpet,
	TileStepSoundType_Ice,
	TileStepSoundType_Grass,
	TileStepSoundType_Dirt,
	TileStepSoundType_Slush,
	TileStepSoundType_Wood,
	TileStepSoundType_Metal,
	TileStepSoundType_Slime,
	TileStepSoundType_Snow,
	TileStepSoundType_Water,
	TileStepSoundType_Lava,
	TileStepSoundType_Cloud,
	TileStepSoundType_NumTypes,
} TileStepSoundType_t;

struct Tile_t
{
	TileType_t type;
	u8 variant;
	u8 color;
	
	bool hasWall;
	u8 wallVariant;
	u8 wallColor;
	
	bool hasRoof;
	u8 roofVariant;
	u8 roofColor;
	
	u32 roofGroupNumber;
	u8 tileConnections;
	u8 wallConnections;
	u8 roofConnections;
};

#endif //  _GAME_TILE_H
