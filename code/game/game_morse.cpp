/*
File:   game_morse.cpp
Author: Taylor Robbins
Date:   07\10\2020
Description: 
	** Handles the MorseInput_t and related structs
*/

void DestroyMorseInput(MorseInput_t* input)
{
	Assert(input != nullptr);
	DestroyDynamicArray(&input->pieces);
}

void CreateMorseInput(MorseInput_t* input)
{
	Assert(input != nullptr);
	CreateDynamicArray(&input->pieces, mainHeap, sizeof(MorsePiece_t));
}

void CopyMorseInput(MemoryArena_t* memArena, MorseInput_t* dest, const MorseInput_t* src)
{
	Assert(memArena != nullptr);
	Assert(dest != nullptr);
	Assert(src != nullptr);
	MyMemCopy(dest, src, sizeof(MorseInput_t));
	ClearStruct(dest->pieces);
	CreateDynamicArray(&dest->pieces, memArena, sizeof(MorseInput_t), 16, src->pieces.length);
	for (u32 pIndex = 0; pIndex < src->pieces.length; pIndex++)
	{
		const MorsePiece_t* srcPiece = DynArrayGet(&src->pieces, MorsePiece_t, pIndex);
		MorsePiece_t* destPiece = DynArrayAdd(&dest->pieces, MorsePiece_t);
		Assert(srcPiece != nullptr);
		Assert(destPiece != nullptr);
		MyMemCopy(destPiece, srcPiece, sizeof(MorseInput_t));
	}
}

void UpdatePlayingMorse(PlayingMorse_t* playing)
{
	if (playing->isDone) { return; }
	if (playing->time < playing->morse.duration)
	{
		r32 previousTime = playing->time;
		playing->time += ElapsedMs;
		// PrintLine_D("%.1f -> %.1f", previousTime, playing->time);
		
		if (playing->time >= playing->morse.duration)
		{
			playing->time = playing->morse.duration;
			playing->isDone = true;
			PrintLine_D("Done playing %u piece code", playing->morse.pieces.length);
		}
		else
		{
			for (u32 pIndex = 0; pIndex < playing->morse.pieces.length; pIndex++)
			{
				MorsePiece_t* piece = DynArrayGet(&playing->morse.pieces, MorsePiece_t, pIndex);
				Assert(piece != nullptr);
				if (piece->isPrimary)
				{
					
				}
				else
				{
					
				}
			}
		}
	}
}

bool IsMorsePattern(const MorseInput_t* morse, const char* patternStr)
{
	Assert(morse != nullptr);
	Assert(patternStr != nullptr);
	if (MyStrLength32(patternStr) != morse->pieces.length) { return false; }
	for (u32 pIndex = 0; pIndex < morse->pieces.length; pIndex++)
	{
		MorsePiece_t* piece = DynArrayGet(&morse->pieces, MorsePiece_t, pIndex);
		Assert(piece != nullptr);
		if (patternStr[pIndex] == 'z' || patternStr[pIndex] == 'Z')
		{
			if (!piece->isPrimary) { return false; }
		}
		if (patternStr[pIndex] == 'x' || patternStr[pIndex] == 'X')
		{
			if (piece->isPrimary) { return false; }
		}
	}
	return true;
}
