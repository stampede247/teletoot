/*
File:   game_toot_level_update.cpp
Author: Taylor Robbins
Date:   07\11\2020
Description: 
	** Handles updating the train, the tracks, and other level related things
*/

bool ProcessMorseInput(TootLevel_t* level, MorseInput_t* morse, bool* becauseBlocked = nullptr)
{
	Assert(level != nullptr);
	Assert(morse != nullptr);
	
	if (becauseBlocked != nullptr) { *becauseBlocked = false; }
	
	Ground_t* targetGround = GetGroundAt(level, level->placingPos);
	if (targetGround == nullptr) { return false; }
	
	if (IsMorsePattern(morse, TRACK_STRAIGHT_CODE))
	{
		if (targetGround->type == GroundType_Water)
		{
			if (becauseBlocked != nullptr) { *becauseBlocked = true; }
			level->failedTrackAnimTime = 1.0f;
			level->failedTrack.type = TrackType_Straight;
			level->failedTrack.rotation = level->placingDir;
			level->failedTrack.tilePos = level->placingPos;
			level->failedBlockPos = level->placingPos;
			return false;
		}
		if (IsLevelTilePosBlocked(level, level->placingPos))
		{
			if (becauseBlocked != nullptr) { *becauseBlocked = true; }
			level->failedTrackAnimTime = 1.0f;
			level->failedTrack.type = TrackType_Straight;
			level->failedTrack.rotation = level->placingDir;
			level->failedTrack.tilePos = level->placingPos;
			level->failedBlockPos = level->placingPos;
			return false;
		}
		Track_t* newTrack = AllocTrack(level, TrackType_Straight, level->placingPos, level->placingDir);
		Assert(newTrack != nullptr);
		level->placingPos += NewVec2i(level->placingDir);
		SoundEffectGlobal(trackPlaceSound1, 1.0f);
		SoundEffectGlobal(trackPlaceSound2, 1.0f);
		return true;
	}
	else if (IsMorsePattern(morse, UNDO_CODE))
	{
		v2i previousPos = level->placingPos - NewVec2i(level->placingDir);
		v2i previousPreviousPos = level->placingPos - NewVec2i(level->placingDir)*2;
		bool foundTrackToUndo = false;
		for (u32 tIndex = 0; tIndex < level->tracks.length; tIndex++)
		{
			Track_t* track = DynArrayGet(&level->tracks, Track_t, tIndex);
			Assert(track != nullptr);
			if ((track->type == TrackType_Straight || track->type == TrackType_BridgeStraight) && track->tilePos == previousPos && (track->rotation == level->placingDir || track->rotation == Dir2Opposite(level->placingDir)))
			{
				foundTrackToUndo = true;
				SoundEffectGlobal(trackPlaceSound1, 1.0f);
				level->placingPos = previousPos;
				DynArrayRemove(&level->tracks, tIndex);
				break;
			}
			if ((track->type == TrackType_Cross || track->type == TrackType_BridgeCross) && track->tilePos == previousPos)
			{
				foundTrackToUndo = true;
				SoundEffectGlobal(trackPlaceSound1, 1.0f);
				track->type = ((track->type == TrackType_BridgeCross) ? TrackType_BridgeStraight : TrackType_Straight);
				track->rotation = Dir2Clockwise(level->placingDir);
				level->placingPos = previousPos;
				break;
			}
			if ((track->type == TrackType_Corner || track->type == TrackType_BridgeCorner) && track->tilePos == previousPreviousPos && (track->rotation == level->placingDir || track->rotation == Dir2CounterClockwise(level->placingDir)))
			{
				foundTrackToUndo = true;
				SoundEffectGlobal(trackPlaceSound1, 1.0f);
				level->placingPos = previousPreviousPos;
				if (track->rotation == level->placingDir)
				{
					level->placingPos += NewVec2i(Dir2Clockwise(level->placingDir));
					level->placingDir = Dir2CounterClockwise(level->placingDir);
				}
				else
				{
					level->placingPos += NewVec2i(Dir2CounterClockwise(level->placingDir));
					level->placingDir = Dir2Clockwise(level->placingDir);
				}
				DynArrayRemove(&level->tracks, tIndex);
				break;
			}
		}
		return foundTrackToUndo;
	}
	else if (IsMorsePattern(morse, TRACK_CORNER_RIGHT_CODE))
	{
		for (u32 checkIndex = 0; checkIndex < 3; checkIndex++)
		{
			v2i checkPos = level->placingPos;
			if (checkIndex >= 1) { checkPos += NewVec2i(level->placingDir); }
			if (checkIndex >= 2) { checkPos += NewVec2i(Dir2Clockwise(level->placingDir)); }
			Ground_t* checkGround = GetGroundAt(level, checkPos);
			if (checkGround == nullptr || checkGround->type == GroundType_Water)
			{
				if (becauseBlocked != nullptr) { *becauseBlocked = true; }
				level->failedTrackAnimTime = 1.0f;
				level->failedTrack.type = TrackType_Corner;
				level->failedTrack.rotation = Dir2Clockwise(level->placingDir);
				level->failedTrack.tilePos = level->placingPos + NewVec2i(level->placingDir);
				level->failedBlockPos = checkPos;
				return false;
			}
			if (IsLevelTilePosBlocked(level, checkPos))
			{
				if (becauseBlocked != nullptr) { *becauseBlocked = true; }
				level->failedTrackAnimTime = 1.0f;
				level->failedTrack.type = TrackType_Corner;
				level->failedTrack.rotation = Dir2Clockwise(level->placingDir);
				level->failedTrack.tilePos = level->placingPos + NewVec2i(level->placingDir);
				level->failedBlockPos = checkPos;
				return false;
			}
		}
		Track_t* newTrack = AllocTrack(level, TrackType_Corner, level->placingPos + NewVec2i(level->placingDir), Dir2Clockwise(level->placingDir));
		Assert(newTrack != nullptr);
		level->placingPos += NewVec2i(level->placingDir);
		level->placingPos += NewVec2i(Dir2Clockwise(level->placingDir))*2;
		level->placingDir = Dir2Clockwise(level->placingDir);
		SoundEffectGlobal(trackPlaceSound1, 1.0f);
		SoundEffectGlobal(trackPlaceSound2, 1.0f);
		return true;
	}
	else if (IsMorsePattern(morse, TRACK_CORNER_LEFT_CODE))
	{
		for (u32 checkIndex = 0; checkIndex < 3; checkIndex++)
		{
			v2i checkPos = level->placingPos;
			if (checkIndex >= 1) { checkPos += NewVec2i(level->placingDir); }
			if (checkIndex >= 2) { checkPos += NewVec2i(Dir2CounterClockwise(level->placingDir)); }
			Ground_t* checkGround = GetGroundAt(level, checkPos);
			if (checkGround == nullptr || checkGround->type == GroundType_Water)
			{
				if (becauseBlocked != nullptr) { *becauseBlocked = true; }
				level->failedTrackAnimTime = 1.0f;
				level->failedTrack.type = TrackType_Corner;
				level->failedTrack.rotation = Dir2Opposite(level->placingDir);
				level->failedTrack.tilePos = level->placingPos + NewVec2i(level->placingDir);
				level->failedBlockPos = checkPos;
				return false;
			}
			if (IsLevelTilePosBlocked(level, checkPos))
			{
				if (becauseBlocked != nullptr) { *becauseBlocked = true; }
				level->failedTrackAnimTime = 1.0f;
				level->failedTrack.type = TrackType_Corner;
				level->failedTrack.rotation = Dir2Opposite(level->placingDir);
				level->failedTrack.tilePos = level->placingPos + NewVec2i(level->placingDir);
				level->failedBlockPos = checkPos;
				return false;
			}
		}
		Track_t* newTrack = AllocTrack(level, TrackType_Corner, level->placingPos + NewVec2i(level->placingDir), Dir2Opposite(level->placingDir));
		Assert(newTrack != nullptr);
		level->placingPos += NewVec2i(level->placingDir);
		level->placingPos += NewVec2i(Dir2CounterClockwise(level->placingDir))*2;
		level->placingDir = Dir2CounterClockwise(level->placingDir);
		SoundEffectGlobal(trackPlaceSound1, 1.0f);
		SoundEffectGlobal(trackPlaceSound2, 1.0f);
		return true;
	}
	else if (IsMorsePattern(morse, TRACK_BRIDGE_STRAIGHT_CODE))
	{
		if (targetGround->type != GroundType_Water)
		{
			if (becauseBlocked != nullptr) { *becauseBlocked = true; }
			level->failedTrackAnimTime = 1.0f;
			level->failedTrack.type = TrackType_BridgeStraight;
			level->failedTrack.rotation = level->placingDir;
			level->failedTrack.tilePos = level->placingPos;
			level->failedBlockPos = level->placingPos;
			return false;
		}
		if (IsLevelTilePosBlocked(level, level->placingPos))
		{
			if (becauseBlocked != nullptr) { *becauseBlocked = true; }
			level->failedTrackAnimTime = 1.0f;
			level->failedTrack.type = TrackType_Straight;
			level->failedTrack.rotation = level->placingDir;
			level->failedTrack.tilePos = level->placingPos;
			level->failedBlockPos = level->placingPos;
			return false;
		}
		Track_t* newTrack = AllocTrack(level, TrackType_BridgeStraight, level->placingPos, level->placingDir);
		Assert(newTrack != nullptr);
		level->placingPos += NewVec2i(level->placingDir);
		SoundEffectGlobal(trackPlaceSound1, 1.0f);
		SoundEffectGlobal(trackPlaceSound2, 1.0f);
		return true;
	}
	else if (IsMorsePattern(morse, TRACK_BRIDGE_CORNER_RIGHT_CODE))
	{
		for (u32 checkIndex = 0; checkIndex < 3; checkIndex++)
		{
			v2i checkPos = level->placingPos;
			if (checkIndex >= 1) { checkPos += NewVec2i(level->placingDir); }
			if (checkIndex >= 2) { checkPos += NewVec2i(Dir2Clockwise(level->placingDir)); }
			Ground_t* checkGround = GetGroundAt(level, checkPos);
			if (checkGround == nullptr || checkGround->type != GroundType_Water)
			{
				if (becauseBlocked != nullptr) { *becauseBlocked = true; }
				level->failedTrackAnimTime = 1.0f;
				level->failedTrack.type = TrackType_BridgeCorner;
				level->failedTrack.rotation = Dir2Clockwise(level->placingDir);
				level->failedTrack.tilePos = level->placingPos + NewVec2i(level->placingDir);
				level->failedBlockPos = checkPos;
				return false;
			}
			if (IsLevelTilePosBlocked(level, checkPos))
			{
				if (becauseBlocked != nullptr) { *becauseBlocked = true; }
				level->failedTrackAnimTime = 1.0f;
				level->failedTrack.type = TrackType_BridgeCorner;
				level->failedTrack.rotation = Dir2Clockwise(level->placingDir);
				level->failedTrack.tilePos = level->placingPos + NewVec2i(level->placingDir);
				level->failedBlockPos = checkPos;
				return false;
			}
		}
		Track_t* newTrack = AllocTrack(level, TrackType_BridgeCorner, level->placingPos + NewVec2i(level->placingDir), Dir2Clockwise(level->placingDir));
		Assert(newTrack != nullptr);
		level->placingPos += NewVec2i(level->placingDir);
		level->placingPos += NewVec2i(Dir2Clockwise(level->placingDir))*2;
		level->placingDir = Dir2Clockwise(level->placingDir);
		SoundEffectGlobal(trackPlaceSound1, 1.0f);
		SoundEffectGlobal(trackPlaceSound2, 1.0f);
		return true;
	}
	else if (IsMorsePattern(morse, TRACK_BRIDGE_CORNER_LEFT_CODE))
	{
		for (u32 checkIndex = 0; checkIndex < 3; checkIndex++)
		{
			v2i checkPos = level->placingPos;
			if (checkIndex >= 1) { checkPos += NewVec2i(level->placingDir); }
			if (checkIndex >= 2) { checkPos += NewVec2i(Dir2CounterClockwise(level->placingDir)); }
			Ground_t* checkGround = GetGroundAt(level, checkPos);
			if (checkGround == nullptr || checkGround->type != GroundType_Water)
			{
				if (becauseBlocked != nullptr) { *becauseBlocked = true; }
				level->failedTrackAnimTime = 1.0f;
				level->failedTrack.type = TrackType_BridgeCorner;
				level->failedTrack.rotation = Dir2Opposite(level->placingDir);
				level->failedTrack.tilePos = level->placingPos + NewVec2i(level->placingDir);
				level->failedBlockPos = checkPos;
				return false;
			}
			if (IsLevelTilePosBlocked(level, checkPos))
			{
				if (becauseBlocked != nullptr) { *becauseBlocked = true; }
				level->failedTrackAnimTime = 1.0f;
				level->failedTrack.type = TrackType_BridgeCorner;
				level->failedTrack.rotation = Dir2Opposite(level->placingDir);
				level->failedTrack.tilePos = level->placingPos + NewVec2i(level->placingDir);
				level->failedBlockPos = checkPos;
				return false;
			}
		}
		Track_t* newTrack = AllocTrack(level, TrackType_BridgeCorner, level->placingPos + NewVec2i(level->placingDir), Dir2Opposite(level->placingDir));
		Assert(newTrack != nullptr);
		level->placingPos += NewVec2i(level->placingDir);
		level->placingPos += NewVec2i(Dir2CounterClockwise(level->placingDir))*2;
		level->placingDir = Dir2CounterClockwise(level->placingDir);
		SoundEffectGlobal(trackPlaceSound1, 1.0f);
		SoundEffectGlobal(trackPlaceSound2, 1.0f);
		return true;
	}
	else if (IsMorsePattern(morse, TRAIN_HORN_BLOW_CODE))
	{
		SoundEffectGlobal(whistleSound, 1.0f);
		for (u32 dIndex = 0; dIndex < level->decorations.length; dIndex++)
		{
			Decoration_t* decor = DynArrayGet(&level->decorations, Decoration_t, dIndex);
			Assert(decor != nullptr);
			if (decor->type == DecorType_Sheep && !decor->isScattered)
			{
				v2 sheepPos = (NewVec2(decor->tilePos) + Vec2_Half) * 32.0f;
				v2 trainPos = (NewVec2(level->train.tilePos) + Vec2_Half) * 32.0f;
				r32 sheepDist = Vec2Length(sheepPos - trainPos);
				if (sheepDist < HORN_EFFECT_DISTANCE)
				{
					decor->isScattered = true;
					decor->scatterAnimTime = 0.0f;
					decor->scatterVec = Vec2Normalize(sheepPos - trainPos) * SHEEP_SCATTER_DISTANCE;
					SoundEffectSingleGlobal(baaaaSound, 1.0f);
				}
				else
				{
					PrintLine_D("Sheep[%u] is too far away %f > %f", dIndex, sheepDist, (r32)SHEEP_SCATTER_DISTANCE);
				}
			}
		}
		return true;
	}
	else
	{
		WriteLine_E("Unknown pattern");
		return false;
	}
}

void UpdateTrain(TootLevel_t* level, Train_t* train)
{
	Assert(level != nullptr);
	Assert(train != nullptr);
	
	r32 timeScale = (ElapsedMs / (1000.0f / 60.0f));
	
	// +==============================+
	// |       Update Chug Time       |
	// +==============================+
	r32 oldChugTime = train->chugTime;
	bool chugTimeLooped = false;
	train->chugTime += ElapsedMs;
	if (train->chugTime >= train->chugSpeed)
	{
		train->chugTime = ModR32(train->chugTime, train->chugSpeed);
		chugTimeLooped = true;
		if (train->isPrimaryChug)
		{
			// SoundEffectGlobal(chuggaSound1, 1.0f + GetRandR32(appRand, -0.1f, 0.1f));
			SoundEffectGlobal(poofSound1, 1.0f);
		}
		else
		{
			// SoundEffectGlobal(chuggaSound2, 1.0f + GetRandR32(appRand, -0.1f, 0.1f));
			SoundEffectGlobal(poofSound2, 1.0f);
		}
		train->isPrimaryChug = !train->isPrimaryChug;
	}
	if ((oldChugTime / train->chugSpeed < CHUG_CUTOFF_TIME || chugTimeLooped) && train->chugTime / train->chugSpeed >= CHUG_CUTOFF_TIME)
	{
		train->usedThisPlume = false;
		train->plumeIsPrimary = false;
		train->plumeIsSuccess = false;
		train->plumeIsFailure = false;
	}
	
	// +==============================+
	// |    Synchronize with Music    |
	// +==============================+
	if (app->currentMusic != nullptr)
	{
		r32 playOffset = GetSndInstancePlayOffset(app->currentMusic);
		Sound_t* musicSound = GetSoundPntrById(app->currentMusic->soundId);
		if (musicSound != nullptr)
		{
			r32 musicChugOffset = ModR32(playOffset - CHUG_BEAT_OFFSET, DEFAULT_CHUG_SPEED);
			r32 minError = MinR32(AbsR32(musicChugOffset - train->chugTime), AbsR32(musicChugOffset + DEFAULT_CHUG_SPEED - train->chugTime));
			if (minError > MAX_CHUG_SYNCH_ERROR)
			{
				PrintLine_D("Fixing a synchronization error of %.1fms", minError);
				train->chugTime = musicChugOffset;
			}
		}
	}
	
	// +==============================+
	// |    Handle Train Movement     |
	// +==============================+
	if (!train->isDead && !train->reachedEnd)
	{
		r32 moveSpeed = TRAIN_DEFAULT_TILE_MOVE_TIME;
		if (train->fromDir != train->direction) { moveSpeed *= 2.5f; }
		if (ButtonDown(Button_Shift)) { moveSpeed /= 10; }
		if (level->trackComplete) { moveSpeed /= 5; } //TODO: Make sure there are no hazards in the way of the train
		train->movePercent += TIME_SCALED_ANIM(moveSpeed, timeScale);
		while (train->movePercent >= 1.0f)
		{
			train->movePercent -= 1.0f;
			train->fromTilePos = train->tilePos;
			train->fromDir = train->direction;
			
			bool foundSupportingTrack = false;
			for (u32 tIndex = 0; tIndex < level->tracks.length; tIndex++)
			{
				Track_t* track = DynArrayGet(&level->tracks, Track_t, tIndex);
				Assert(track != nullptr);
				if ((track->type == TrackType_Straight || track->type == TrackType_BridgeStraight) && track->tilePos == train->tilePos)
				{
					if ((train->direction == Dir2_Left || train->direction == Dir2_Right) == (track->rotation == Dir2_Left || track->rotation == Dir2_Right))
					{
						foundSupportingTrack = true;
						break;
					}
				}
				if (track->type == TrackType_Cross && track->tilePos == train->tilePos)
				{
					foundSupportingTrack = true;
					break;
				}
				if (track->type == TrackType_End && track->tilePos == train->tilePos)
				{
					if (train->direction == track->rotation || train->direction == Dir2Opposite(track->rotation))
					{
						foundSupportingTrack = true;
						break;
					}
				}
				if ((track->type == TrackType_Corner || track->type == TrackType_BridgeCorner) && track->tilePos == train->tilePos + NewVec2i(train->direction))
				{
					if (train->direction == Dir2Opposite(Dir2Clockwise(track->rotation)) || train->direction == Dir2Opposite(track->rotation))
					{
						foundSupportingTrack = true;
						break;
					}
				}
				if ((track->type == TrackType_Corner || track->type == TrackType_BridgeCorner) && track->tilePos == train->tilePos - NewVec2i(train->direction))
				{
					if (train->direction == Dir2Clockwise(track->rotation) || train->direction == track->rotation)
					{
						foundSupportingTrack = true;
						break;
					}
				}
			}
			
			bool foundBlockingDecor = false;
			for (u32 dIndex = 0; dIndex < level->decorations.length; dIndex++)
			{
				Decoration_t* decor = DynArrayGet(&level->decorations, Decoration_t, dIndex);
				Assert(decor != nullptr);
				if (decor->tilePos == train->tilePos && !decor->isScattered)
				{
					foundBlockingDecor = true;
					break;
				}
			}
			
			if (!foundSupportingTrack || foundBlockingDecor)
			{
				train->isDead = true;
				train->deadAnimPercent = 0.0f;
				SoundEffectGlobal(explosionSound, 1.0f);
				break;
			}
			
			bool foundEndGoal = false;
			for (u32 tIndex = 0; tIndex < level->tracks.length; tIndex++)
			{
				Track_t* track = DynArrayGet(&level->tracks, Track_t, tIndex);
				Assert(track != nullptr);
				if (track->tilePos == train->tilePos && track->type == TrackType_End && track->rotation == Dir2Opposite(train->direction))
				{
					train->reachedEnd = true;
					break;
				}
			}
			
			
			v2i nextPos = train->tilePos + NewVec2i(train->direction);
			for (u32 tIndex = 0; tIndex < level->tracks.length; tIndex++)
			{
				Track_t* track = DynArrayGet(&level->tracks, Track_t, tIndex);
				Assert(track != nullptr);
				if (track->tilePos == nextPos && (track->type == TrackType_Corner || track->type == TrackType_BridgeCorner))
				{
					if (track->rotation == Dir2Opposite(train->direction))
					{
						train->direction = Dir2CounterClockwise(train->direction);
						nextPos += NewVec2i(train->direction);
					}
					else if (track->rotation == Dir2CounterClockwise(Dir2Opposite(train->direction)))
					{
						train->direction = Dir2Clockwise(train->direction);
						nextPos += NewVec2i(train->direction);
					}
				}
			}
			train->tilePos = nextPos;
		}
	}
	
	// +==============================+
	// |   Handle Mouse Btn Clicks    |
	// +==============================+
	bool primaryClicked = false;
	bool secondaryClicked = false;
	bool enterClicked = false;
	if (!train->isDead && !train->reachedEnd)
	{
		if (IsMouseOver("HudPrimaryBtn"))
		{
			appOutput->cursorType = Cursor_Pointer;
			if (ButtonPressed(MouseButton_Left))
			{
				HandleButton(MouseButton_Left);
				primaryClicked = true;
			}
		}
		if (IsMouseOver("HudSecondaryBtn"))
		{
			appOutput->cursorType = Cursor_Pointer;
			if (ButtonPressed(MouseButton_Left))
			{
				HandleButton(MouseButton_Left);
				secondaryClicked = true;
			}
		}
		if (IsMouseOver("HudEnterBtn"))
		{
			appOutput->cursorType = Cursor_Pointer;
			if (ButtonPressed(MouseButton_Left))
			{
				HandleButton(MouseButton_Left);
				enterClicked = true;
			}
		}
	}
	
	// +==================================+
	// | Handle Primary/Secondary Buttons |
	// +==================================+
	if (!train->isDead && !train->reachedEnd)
	{
		if (ButtonPressed(Button_Z) || primaryClicked)
		{
			HandleButton(Button_Z);
			// if (!train->usedThisPlume)
			{
				MorsePiece_t* newPiece = DynArrayAdd(&game->currentMorse.pieces, MorsePiece_t);
				Assert(newPiece != nullptr);
				ClearPointer(newPiece);
				newPiece->time = game->currentMorse.duration;
				newPiece->isPrimary = true;
				SoundEffectGlobal(primarySound, 1.0f);
				train->usedThisPlume = true;
				train->plumeIsPrimary = true;
				train->shovelerThrowing = true;
				train->shovelerPrimary = true;
				train->leftFurnaceAnim = 1.0f;
			}
			// else { SoundEffectGlobal(dohSound1, 1.0f); }
		}
		if (ButtonPressed(Button_X) || secondaryClicked)
		{
			HandleButton(Button_X);
			// if (!train->usedThisPlume)
			{
				MorsePiece_t* newPiece = DynArrayAdd(&game->currentMorse.pieces, MorsePiece_t);
				Assert(newPiece != nullptr);
				ClearPointer(newPiece);
				newPiece->time = game->currentMorse.duration;
				newPiece->isPrimary = false;
				SoundEffectGlobal(secondarySound, 1.0f);
				train->usedThisPlume = true;
				train->plumeIsPrimary = false;
				train->shovelerThrowing = true;
				train->shovelerPrimary = false;
				train->rightFurnaceAnim = 1.0f;
			}
			// else { SoundEffectGlobal(dohSound1, 1.0f); }
		}
	}
	if (train->shovelerThrowing && train->shovelerPrimary)
	{
		if (ButtonDownNoHandling(Button_Z))
		{
			train->shovelerIdleTimer = 1.0f;
		}
		else
		{
			train->shovelerThrowing = false;
			train->shovelerIdleTimer = 1.0f;
		}
	}
	if (train->shovelerThrowing && !train->shovelerPrimary)
	{
		if (ButtonDownNoHandling(Button_X))
		{
			train->shovelerIdleTimer = 1.0f;
		}
		else
		{
			train->shovelerThrowing = false;
			train->shovelerIdleTimer = 1.0f;
		}
	}
	if (train->chugTime / train->chugSpeed >= (4.0f/7.0f) || train->chugTime / train->chugSpeed < CHUG_CUTOFF_TIME)
	{
		train->usedLastPlume = train->usedThisPlume;
		train->plumeWasPrimary = train->plumeIsPrimary;
		train->plumeWasSuccess = train->plumeIsSuccess;
		train->plumeWasFailure = train->plumeIsFailure;
	}
	
	// +==============================+
	// |     Handle Enter Button      |
	// +==============================+
	if (!train->isDead && !train->reachedEnd)
	{
		if (ButtonPressed(Button_Enter) || enterClicked)
		{
			HandleButton(Button_Enter);
			// if (!train->usedThisPlume)
			{
				train->usedThisPlume = true;
				if (game->currentMorse.pieces.length > 0)
				{
					bool wasBlocked = false;
					if (ProcessMorseInput(&game->level, &game->currentMorse, &wasBlocked))
					{
						train->plumeIsSuccess = true;
						for (u32 tIndex = 0; tIndex < level->tracks.length; tIndex++)
						{
							Track_t* track = DynArrayGet(&level->tracks, Track_t, tIndex);
							Assert(track != nullptr);
							if (track->type == TrackType_End && track->tilePos == level->placingPos && track->rotation == Dir2Opposite(level->placingDir))
							{
								level->trackComplete = true;
								break;
							}
						}
						if (level->trackComplete)
						{
							SoundEffectGlobal(dingSound1, 2.0f);
						}
						else { SoundEffectGlobal(dingSound1, 1.0f); }
					}
					else
					{
						SoundEffectGlobal(dingSound1, 0.3f);
						train->plumeIsFailure = true;
					}
					
					DynArrayClear(&game->currentMorse.pieces);
					game->conductorAnimTime = 1.0f;
				}
				else
				{
					SoundEffectGlobal(dingSound1, 0.3f);
					train->plumeIsFailure = true;
				}
			}
			// else { SoundEffectGlobal(dohSound1, 1.0f); }
			
		}
	}
	
	// +==============================+
	// |    Update Death Animation    |
	// +==============================+
	if (train->isDead)
	{
		train->deadAnimPercent += TIME_SCALED_ANIM(TRAIN_DEATH_ANIM_TIME, timeScale);
		if (train->deadAnimPercent >= 1.0f)
		{
			train->deadAnimPercent = 1.0f;
		}
	}
	
	// +==============================+
	// |  Update Furnace Animations   |
	// +==============================+
	if (train->leftFurnaceAnim > 0.0f)
	{
		train->leftFurnaceAnim -= TIME_SCALED_ANIM(FURNACE_ANIM_TIME, timeScale);
		if (train->leftFurnaceAnim <= 0.0f) { train->leftFurnaceAnim = 0.0f; }
	}
	if (train->rightFurnaceAnim > 0.0f)
	{
		train->rightFurnaceAnim -= TIME_SCALED_ANIM(FURNACE_ANIM_TIME, timeScale);
		if (train->rightFurnaceAnim <= 0.0f) { train->rightFurnaceAnim = 0.0f; }
	}
	
	// +==============================+
	// | Update Shoveler Idle Timeout |
	// +==============================+
	if (train->shovelerIdleTimer > 0.0f)
	{
		train->shovelerIdleTimer -= TIME_SCALED_ANIM(SHOVELER_IDLE_TIME, timeScale);
		if (train->deadAnimPercent <= 0.0f) { train->deadAnimPercent = 0.0f; }
	}
}

void UpdateLevel(TootLevel_t* level)
{
	Assert(level != nullptr);
	
	r32 timeScale = (ElapsedMs / (1000.0f / 60.0f));
	
	// +==============================+
	// |   Update Failed Anim Time    |
	// +==============================+
	if (level->failedTrackAnimTime > 0.0f)
	{
		level->failedTrackAnimTime -= TIME_SCALED_ANIM(FAIL_TRACK_ANIM_TIME, timeScale);
		if (level->failedTrackAnimTime <= 0.0f) { level->failedTrackAnimTime = 0.0f; }
	}
	
	// +==============================+
	// | Update Track Creation Anims  |
	// +==============================+
	for (u32 tIndex = 0; tIndex < level->tracks.length; tIndex++)
	{
		Track_t* track = DynArrayGet(&level->tracks, Track_t, tIndex);
		Assert(track != nullptr);
		if (track->creationAnim < 1.0f)
		{
			track->creationAnim += TIME_SCALED_ANIM(TRACK_CREATE_ANIM_TIME, timeScale);
			if (track->creationAnim >= 1.0f) { track->creationAnim = 1.0f; }
		}
	}
	
	// +==============================+
	// |     Update Scatter Anims     |
	// +==============================+
	for (u32 dIndex = 0; dIndex < level->decorations.length; dIndex++)
	{
		Decoration_t* decor = DynArrayGet(&level->decorations, Decoration_t, dIndex);
		Assert(decor != nullptr);
		if (decor->isScattered && decor->scatterAnimTime < 1.0f)
		{
			decor->scatterAnimTime += TIME_SCALED_ANIM(SHEEP_SCATTER_TIME, timeScale);
			if (decor->scatterAnimTime >= 1.0f) { decor->scatterAnimTime = 1.0f; }
		}
	}
}
