/*
File:   win32_main.cpp
Author: Taylor Robbins
Date:   06\04\2017
Description: 
	** This is the file that actually get's compiled in order to produce the platform layer executable.
	** All other platform layer code is included either directly or indirectly from this file
	** This file also contains the main entry point for the application which will allocate memory,
	** create the window, load the game DLL, and run the game update loop
	** In order to keep the size of this file manageable all other functions are put in other files
	** This file acts as a platform layer for the game on windows platforms and makes calls to the Win32 API provided by windows to accomplish many of it's tasks
*/

//Standard Libraries
#include <windows.h>
#include "Shlwapi.h"
#include <iostream>
#include <ctime>
#include <sys/timeb.h>
#include <Winhttp.h>
#include <Shlobj.h> //needed for SHGetSpecialFolderPathA

#ifndef WM_GETDPISCALEDSIZE
 #define WM_GETDPISCALEDSIZE 0x02e4
#endif

#define USE_ASSERT_FAILURE_FUNCTION true
#define THREAD_SAFE_ASSERT          true
#define USE_MY_STD_LIB              false
#define USE_MIN_STD_LIB             true
#define MyMalloc(size)              malloc(size)
#define MyRealloc(pntr, newSize)    realloc(pntr, newSize)
#define MyFree(size)                free(size)

//Project Headers
#include "plat/plat_interface.h"
#include "win32/win32_version.h"

#define STBI_MALLOC(size)             MyMalloc((u32)(size))
#define STBI_REALLOC(pntr, newSize)   MyRealloc(pntr, (u32)(newSize))
#define STBI_FREE(pntr)               MyFree(pntr)
#define STBI_ASSERT(expression)       Assert(expression)
#define STBI_NO_STDIO
#include "stb/stb_image.h"

//Extra my libraries
#include "mylib/my_tempMemory.h"
#include "mylib/my_dynamicArray.h"

// +--------------------------------------------------------------+
// |                 Platform Layer Header Files                  |
// +--------------------------------------------------------------+
#include "win32/win32_defines.h"
#include "win32/win32_memory.h"
#include "win32/win32_file_watching.h"
#include "win32/win32_app_loading.h"
#include "win32/win32_loading_render.h"
#include "win32/win32_main.h"

// +--------------------------------------------------------------+
// |                    Platform Layer Globals                    |
// +--------------------------------------------------------------+
static PlatformData_t platform = {};
static LoadedApp_t app = {};
static u32 numCrashDumps = 0;
ThreadId_t MainThreadId = 0;
#include "mylib/my_tempMemory.cpp" //Defines TempArena global

// +--------------------------------------------------------------+
// |                  Function Pre-Declarations                   |
// +--------------------------------------------------------------+
LockMutex_DEFINITION(Win32_LockMutex);
UnlockMutex_DEFINITION(Win32_UnlockMutex);
IsMainThread_DEFINITION(Win32_IsMainThread);
GetThreadInfo_DEFINITION(Win32_GetThreadInfo);
bool IsThisMainThread();
MemoryArena_t* GetThreadTempArena();

// +--------------------------------------------------------------+
// |                 Platform Layer Source Files                  |
// +--------------------------------------------------------------+
#include "win32/win32_func_defs.h"
#include "win32/win32_performance.cpp"
#include "win32/win32_debug.cpp"
#include "win32/win32_helpers.cpp"
#include "win32/win32_memory.cpp"
#include "win32/win32_files.cpp"
#include "win32/win32_fonts.cpp"
#include "win32/win32_threads.cpp"
#include "win32/win32_tasks.cpp"
#include "win32/win32_app_loading.cpp"
#include "win32/win32_clipboard.cpp"
#include "win32/win32_file_watching.cpp"
#include "win32/win32_loading_render.cpp"
#include "win32/win32_email.cpp"
#include "win32/win32_http.cpp"
#include "win32/win32_audio.cpp"

void Win32_DoUpdateLoopIteration(bool happeningInEventCallback);
#define DoUpdateLoopIteration(happeningInEventCallback) Win32_DoUpdateLoopIteration(happeningInEventCallback)
#define Plat_IsUpsideModeEnabled() Win32_IsUpsideModeEnabled()

#include "plat/plat_monitors.cpp"
#include "plat/plat_input.cpp"
#include "plat/plat_callbacks.cpp"

#define STB_IMAGE_IMPLEMENTATION
#include "stb/stb_image.h"

// +--------------------------------------------------------------+
// |                     Windows Entry Point                      |
// +--------------------------------------------------------------+
#if OPEN_CONSOLE_WINDOW
int main(int argc, char* argv[])
#else
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
#endif
{
	ClearStruct(platform);
	platform.startupFinished = false;
	platform.version.major = PLATFORM_VERSION_MAJOR;
	platform.version.minor = PLATFORM_VERSION_MINOR;
	platform.version.build = PLATFORM_VERSION_BUILD;
	_Win32_PerformanceInit();
	PerfTime_t initStartTime = Win32_GetPerfTime();
	
	// +===============================+
	// | Create platform memory arenas |
	// +===============================+
	//TODO: Should we use something different than malloc for platform memory?
	{
		InitializeMemoryArenaStdHeap(&platform.stdHeap);
		
		platform.heapSpace = malloc(PLATFORM_HEAP_SIZE);
		if (platform.heapSpace == nullptr)
		{
			Win32_ExitOnError("Couldn't allocate platform heap memory with malloc");
		}
		InitializeMemoryArenaHeap(&platform.mainHeap, platform.heapSpace, PLATFORM_HEAP_SIZE);
		
		platform.tempSpace = malloc(PLATFORM_TEMP_SIZE);
		if (platform.tempSpace == nullptr)
		{
			Win32_ExitOnError("Couldn't allocate platform temp memory malloc");
		}
		InitializeMemoryArenaTemp(&platform.tempArena, platform.tempSpace, PLATFORM_TEMP_SIZE, PLATFORM_TEMP_MAX_NUM_MARKS);
		TempArena = &platform.tempArena;
		TempPushMark();
		
		Win32_CreateMutex(&platform.debugOutputMutex);
		CreateDynamicArray(&platform.lineBuffer, &platform.stdHeap, sizeof(char), 2048, 2048);
		CreateDynamicArray(&platform.debugLines, &platform.stdHeap, sizeof(PlatDebugLine_t));
		CreateDynamicArray(&platform.droppedFilePaths, &platform.stdHeap, sizeof(char*));
		
		MyLibDebugOutputFunc = _Win32_DebugOutput;
		MyLibDebugPrintFunc  = _Win32_DebugPrint;
	}
	//NOTE: All debug printouts have to happen after the TempArena has been initialized
	
	// +==============================+
	// |   Initialize Win32 Threads   |
	// +==============================+
	{
		_Win32_ThreadsInitialize();
		_Win32_StartTaskThreads();
	}
	
	PrintLine_I("Starting Win32 Platform v%u.%02u(%u)...", platform.version.major, platform.version.minor, platform.version.build);
	PrintLine_D("Main Thread ID 0x%08X", MainThreadId);
	
	#if AUDIO_ENABLED
	{
		//NOTE: Apprently OpenAL also wants CoInitialize to be called
		HRESULT coInitResult = CoInitialize(NULL);
		if (coInitResult != S_OK && coInitResult != S_FALSE && coInitResult != RPC_E_CHANGED_MODE)
		{
			Win32_ExitOnError("CoInitialize failed!");
		}
		#if USE_CUSTOM_AUDIO
		Win32_InitDeviceEnumeration(&platform.info.audioInfo);
		#endif
	}
	#endif
	
	// +======================================+
	// | Get Directory and Program Arguments  |
	// +======================================+
	{
		TempPushMark();
		char* tempExePath = Win32_GetExecutablePath(TempArena);
		if (tempExePath == nullptr || MyStrLength(tempExePath) == 0)
		{
			Win32_ExitOnError("Failed to get executable directory");
		}
		u32 tempExePathLength = MyStrLength32(tempExePath);
		char* exeFileName = GetFileNamePart(tempExePath);
		u32 exeDirectoryLength = (u32)(exeFileName - tempExePath);
		u32 exeFileNameLength = tempExePathLength - exeDirectoryLength;
		platform.exeDirectory = ArenaString(&platform.mainHeap, tempExePath, exeDirectoryLength);
		platform.exeFileName = ArenaString(&platform.mainHeap, exeFileName, exeFileNameLength);
		PrintLine_I("Our exe is in folder \"%s\" named \"%s\"", platform.exeDirectory, platform.exeFileName);
		TempPopMark();
		
		platform.workingDirectory = Win32_GetWorkingDirectory(&platform.mainHeap);
		if (platform.workingDirectory == nullptr)
		{
			Win32_ExitOnError("Failed to get working directory");
		}
		PrintLine_I("Our working directory is \"%s\"", platform.workingDirectory);
		
		#if OPEN_CONSOLE_WINDOW
		platform.numProgramArguments = (u32)(argc-1);
		if (platform.numProgramArguments > 0)
		{
			platform.programArguments = PushArray(&platform.mainHeap, char*, platform.numProgramArguments);
			for (u32 aIndex = 0; aIndex < platform.numProgramArguments; aIndex++)
			{
				char* argvPntr = argv[1+aIndex];
				u32 argvLength = MyStrLength32(argvPntr);
				platform.programArguments[aIndex] = PushArray(&platform.mainHeap, char, argvLength+1);
				MyMemCopy(platform.programArguments[aIndex], argvPntr, argvLength);
				platform.programArguments[aIndex][argvLength] = '\0';
			}
		}
		#else
		u32 numPieces = 0;
		StrSplitPiece_t* pieces = SplitString(TempArena, lpCmdLine, MyStrLength32(lpCmdLine), " ", 1, &numPieces);
		platform.numProgramArguments = numPieces;
		if (platform.numProgramArguments > 0)
		{
			platform.programArguments = PushArray(&platform.mainHeap, char*, numPieces);
			for (u32 aIndex = 0; aIndex < numPieces; aIndex++)
			{
				platform.programArguments[aIndex] = PushArray(&platform.mainHeap, char, pieces[aIndex].length+1);
				MyMemCopy(platform.programArguments[aIndex], pieces[aIndex].pntr, pieces[aIndex].length);
				platform.programArguments[aIndex][pieces[aIndex].length] = '\0';
			}
		}
		#endif
		
		bool loadFromWorkingDirectory = DEBUG;
		PrintLine_D("%u arguments", platform.numProgramArguments);
		for (u32 aIndex = 0; aIndex < platform.numProgramArguments; aIndex++)
		{
			char* argument = platform.programArguments[aIndex];
			PrintLine_D(" [%u]: \"%s\"", aIndex, argument);
			if (StrCompareIgnoreCaseNt(argument, "-w"))
			{
				loadFromWorkingDirectory = true;
			}
			if (StrCompareIgnoreCaseNt(argument, "-upsidedown"))
			{
				platform.upsideDownModeEnabled = true;
			}
		}
		// platform.upsideDownModeEnabled = true; //TODO: Remove me!
		
		if (loadFromWorkingDirectory)
		{
			platform.baseResourceDirectory = ArenaNtString(&platform.mainHeap, platform.workingDirectory);
			PrintLine_D("Loading resources from working directory: \"%s\"", platform.baseResourceDirectory);
		}
		else
		{
			platform.baseResourceDirectory = ArenaNtString(&platform.mainHeap, platform.exeDirectory);
			PrintLine_D("Loading resources from executable directory: \"%s\"", platform.baseResourceDirectory);
		}
		
		platform.appDllPath = ArenaPrint(&platform.mainHeap, "%s%s", platform.exeDirectory, APPLICATION_DLL_NAME);
		platform.appDllTempPath = ArenaPrint(&platform.mainHeap, "%s%s", platform.exeDirectory, APPLICATION_DLL_TEMP_NAME);
	}
	
	// +==============================+
	// |   Loading Application Code   |
	// +==============================+
	{
		if (PathFileExists(platform.appDllPath) == false)
		{
			Win32_ExitOnError("Could not find application DLL");
		}
		
		Print_R("Loading application DLL%s...", DEBUG ? " (DEBUG)" : "");
		if (Win32_LoadDllCode(platform.appDllPath, platform.appDllTempPath, &app))
		{
			PrintLine_I("Found Application v%u.%02u(%u)", app.version.major, app.version.minor, app.version.build);
		}
		else
		{
			Win32_ExitOnError("Could not load application DLL!");
		}
	}
	
	// +==============================+
	// |     Steam Initialization     |
	// +==============================+
	#if STEAM_BUILD
	{
		WriteLine_I("Initializing Steamworks...");
		//NOTE: I don't really think I like this behavior. I would rather have the executable just exit with a popup
		//      saying that they didn't start the game through Steam. This way it's more clear to the user what's going on.
		#if 0
		bool needToRestart = SteamAPI_RestartAppIfNecessary(EXPECTED_STEAM_APPID);
		if (needToRestart)
		{
			exit(1);
		}
		#endif
		bool initResult = SteamAPI_Init();
		if (!initResult)
		{
			Win32_ExitOnError("Failed to initialize Steam API. Make sure you started the game from Steam and that you are logged in to an account that owns the game.");
		}
		platform.info.steamAppId = SteamUtils()->GetAppID();
	}
	#endif
	
	// +==============================+
	// |     GLFW Initialization      |
	// +==============================+
	{
		WriteLine_R("Initializing GLFW...");
		
		struct timeb glfwInitStartTime; ftime(&glfwInitStartTime);
		glfwSetErrorCallback(GlfwErrorCallback);
		if (!glfwInit(GlfwDebugOutputCallback))
		{
			Win32_ExitOnError("GLFW Initialization Failed!");
		}
		platform.glfwInitialized = true;
		struct timeb glfwInitEndTime; ftime(&glfwInitEndTime);
		u32 glfwInitElapsedTime = (u32)(1000.0 * (glfwInitEndTime.time - glfwInitStartTime.time) + (glfwInitEndTime.millitm - glfwInitStartTime.millitm));
		PrintLine_D("glfwInit took %s", FormattedMilliseconds(glfwInitElapsedTime));
		
		WriteLine_R("Done!");
		
		glfwGetVersion(&platform.info.glfwVersion.major, &platform.info.glfwVersion.minor, &platform.info.glfwVersion.revision);
		PrintLine_D("Compiled with GLFW v%d.%d(%d)", platform.info.glfwVersion.major, platform.info.glfwVersion.minor, platform.info.glfwVersion.revision);
	}
	
	// +==============================+
	// |     Monitor Info Lookup      |
	// +==============================+
	{
		WriteLine_R("Scanning for monitors...");
		
		Plat_FillMonitorInfo();
		
		PrintLine_D("%u Monitors Found", platform.glfwMonitors.length);
		for (u32 mIndex = 0; mIndex < platform.glfwMonitors.length; mIndex++)
		{
			MonitorInfo_t* info = DynArrayGet(&platform.glfwMonitors, MonitorInfo_t, mIndex);
			MonitorDisplayMode_t* currentMode = &info->modes[info->currentMode];
			bool isPrimary = (mIndex == platform.primaryMonitorIndex);
			PrintLine_D("  %s[%u]: \"%s\" pos (%d, %d) resolution (%d, %d) @%.1fHz size (%dmm, %dmm)",
				isPrimary ? "Primary" : "Monitor", mIndex, info->name,
				info->offset.x, info->offset.y,
				currentMode->resolution.x, currentMode->resolution.y, currentMode->refreshRate,
				info->physicalSize.x, info->physicalSize.y
			);
		}
	}
	
	#if AUDIO_ENABLED && USE_CUSTOM_AUDIO
	// +==============================+
	// |     Audio Device Lookup      |
	// +==============================+
	{
		WriteLine_R("Scanning for audio devices...");
		bool enumerateSuccess = Win32_FindAudioDevices(&platform.info.audioInfo, &platform.mainHeap, &platform.audioDevices);
		if (!enumerateSuccess)
		{
			WriteLine_E("WARNING: Failed to enumerate Audio Devices!");
			PrintLine_E("Error: %s", (platform.info.audioInfo.loadError != nullptr) ? platform.info.audioInfo.loadError : "Unknown");
			#if CRASH_ON_AUDIO_INIT_FAILED
			Assert(false);
			Win32_ExitOnError("Failed to enumerate Audio Devices!");
			#endif
		}
		
		#if 1
		PrintLine_I("%u Audio Device%s Found:", platform.audioDevices.length, (platform.audioDevices.length != 1) ? "s" : "");
		for (u32 dIndex = 0; dIndex < platform.audioDevices.length; dIndex++)
		{
			AudioDevice_t* device = DynArrayGet(&platform.audioDevices, AudioDevice_t, dIndex);
			Assert(device != nullptr);
			PrintLine_D("  [%u]: %s [%s] %s%s", dIndex, device->name, device->subName, device->moduleCode, (dIndex == platform.primaryAudioDeviceIndex) ? " (Primary Audio Device)" : "");
		}
		#endif
	}
	#endif
	
	// +==============================+
	// |     Get Startup Options      |
	// +==============================+
	{
		WriteLine_I("Getting startup options from application...");
		
		ClearStruct(platform.startupInfo);
		
		platform.startupInfo.platformType            = Platform_Windows;
		platform.startupInfo.version                 = platform.version;
		platform.startupInfo.glfwVersion             = platform.info.glfwVersion;
		platform.startupInfo.numMonitors             = platform.glfwMonitors.length;
		platform.startupInfo.primaryMonitorIndex     = platform.primaryMonitorIndex;
		platform.startupInfo.monitors                = (const MonitorInfo_t*)platform.glfwMonitors.items;
		platform.startupInfo.numProgramArguments     = platform.numProgramArguments;
		platform.startupInfo.programArguments        = platform.programArguments;
		platform.startupInfo.platformMainHeap        = &platform.mainHeap;
		platform.startupInfo.platformTempArena       = &platform.tempArena;
		platform.startupInfo.platformStdHeap         = &platform.stdHeap;
		platform.startupInfo.FreeFileMemory          = Win32_FreeFileMemory;
		platform.startupInfo.ReadEntireFile          = Win32_ReadEntireFile;
		platform.startupInfo.WriteEntireFile         = Win32_WriteEntireFile;
		platform.startupInfo.GetSpecialFolderPath    = Win32_GetSpecialFolderPath;
		platform.startupInfo.DoesFileExist           = Win32_DoesFileExist;
		#if AUDIO_ENABLED && USE_CUSTOM_AUDIO
		platform.startupInfo.numAudioDevices         = platform.audioDevices.length;
		platform.startupInfo.primaryAudioDeviceIndex = platform.primaryAudioDeviceIndex;
		platform.startupInfo.audioDevices            = (AudioDevice_t*)platform.audioDevices.items;
		#endif
		
		time_t timeStruct = time(NULL);
		u64 timestamp = (u64)timeStruct; //TODO: Will this work on other platforms?
		GetRealTime(timestamp, false, &platform.startupInfo.systemTime);
		SYSTEMTIME localTime = {};
		GetLocalTime(&localTime);
		//TODO: Fill the timestamp
		platform.startupInfo.localTime.year   = localTime.wYear;
		platform.startupInfo.localTime.month  = (u8)(localTime.wMonth-1);
		platform.startupInfo.localTime.day    = (u8)(localTime.wDay-1);
		platform.startupInfo.localTime.hour   = (u8)localTime.wHour;
		platform.startupInfo.localTime.minute = (u8)localTime.wMinute;
		platform.startupInfo.localTime.second = (u8)localTime.wSecond;
		// platform.startupInfo.millisecond      = localTime.wMilliseconds;
		
		ClearStruct(platform.startupOptions);
		platform.startupOptions.windowSize             = NewVec2i(960, 540);
		platform.startupOptions.minWindowSize          = NewVec2i(-1, -1);
		platform.startupOptions.maxWindowSize          = NewVec2i(-1, -1);
		platform.startupOptions.windowPosition         = Vec2i_Zero;
		platform.startupOptions.fullscreen             = false;
		platform.startupOptions.fullscreenMonitorIndex = platform.startupInfo.primaryMonitorIndex;
		platform.startupOptions.autoIconify            = true;
		platform.startupOptions.topmostWindow          = (false && DEBUG);
		platform.startupOptions.decoratedWindow        = true;
		platform.startupOptions.resizableWindow        = true;
		platform.startupOptions.forceAspectRatio       = false;
		platform.startupOptions.aspectRatio            = NewVec2i(16, 9);
		platform.startupOptions.antialiasingNumSamples = 8;
		platform.startupOptions.temporaryMemorySize    = Megabytes(16);
		platform.startupOptions.permanantMemorySize    = Megabytes(1);
		#if AUDIO_ENABLED && USE_CUSTOM_AUDIO
		platform.startupOptions.audioDeviceIndex       = platform.startupInfo.primaryAudioDeviceIndex;
		#endif
		
		platform.startupOptions.loadingColor = Black;
		BufferPrint(platform.startupOptions.iconFilePaths[0], "Resources/Sprites/icon16.png");
		BufferPrint(platform.startupOptions.iconFilePaths[1], "Resources/Sprites/icon24.png");
		BufferPrint(platform.startupOptions.iconFilePaths[2], "Resources/Sprites/icon32.png");
		BufferPrint(platform.startupOptions.iconFilePaths[3], "Resources/Sprites/icon64.png");
		BufferPrint(platform.startupOptions.loadingImagePath, "Resources/Sprites/loading.png");
		
		app.AppGetStartupOptions(&platform.startupInfo, &platform.startupOptions);
		
		Assert(platform.startupOptions.fullscreenMonitorIndex < platform.startupInfo.numMonitors);
		#if AUDIO_ENABLED && USE_CUSTOM_AUDIO
		Assert(platform.startupOptions.audioDeviceIndex < platform.startupInfo.numAudioDevices);
		if (platform.startupOptions.audioDeviceIndex != platform.startupInfo.primaryAudioDeviceIndex)
		{
			PrintLine_I("Application chose non-default audio device: %u instead of %u", platform.startupOptions.audioDeviceIndex, platform.startupInfo.primaryAudioDeviceIndex);
		}
		#endif
		Assert(BufferIsNullTerminated(platform.startupOptions.iconFilePaths[0], ArrayCount(platform.startupOptions.iconFilePaths[0])));
		Assert(BufferIsNullTerminated(platform.startupOptions.iconFilePaths[1], ArrayCount(platform.startupOptions.iconFilePaths[1])));
		Assert(BufferIsNullTerminated(platform.startupOptions.iconFilePaths[2], ArrayCount(platform.startupOptions.iconFilePaths[2])));
		Assert(BufferIsNullTerminated(platform.startupOptions.iconFilePaths[3], ArrayCount(platform.startupOptions.iconFilePaths[3])));
		Assert(BufferIsNullTerminated(platform.startupOptions.loadingImagePath, ArrayCount(platform.startupOptions.loadingImagePath)));
		
		WriteLine_I("Done!");
	}
	
	// +==============================+
	// |     GLFW Window Creation     |
	// +==============================+
	{
		MonitorInfo_t* monitorChoice = nullptr;
		v2i requestSize = NewVec2i(platform.startupOptions.windowSize.width, platform.startupOptions.windowSize.height);
		i32 requestRefreshRate = 60;
		if (platform.startupOptions.fullscreen)
		{
			monitorChoice = DynArrayGet(&platform.glfwMonitors, MonitorInfo_t, platform.startupOptions.fullscreenMonitorIndex);
			Assert(monitorChoice != nullptr);
			Assert(monitorChoice->currentMode < monitorChoice->numModes);
			Assert(monitorChoice->modes != nullptr);
			MonitorDisplayMode_t* currentDisplayMode = &monitorChoice->modes[monitorChoice->currentMode];
			requestSize = currentDisplayMode->resolution;
			requestRefreshRate = RoundR32i(currentDisplayMode->refreshRate);
		}
		
		
		WriteLine_R("Creating GLFW window...");
		
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, OPENGL_REQUEST_VERSION_MAJOR);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, OPENGL_REQUEST_VERSION_MINOR);
		glfwWindowHint(GLFW_CLIENT_API,            GLFW_OPENGL_API);
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, OPENGL_FORCE_FORWARD_COMPAT ? GLFW_TRUE : GLFW_FALSE); //Makes MacOSX happy?
		glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT,  OPENGL_DEBUG_CONTEXT ? GLFW_TRUE : GLFW_FALSE);
		glfwWindowHint(GLFW_OPENGL_PROFILE,        GLFW_OPENGL_ANY_PROFILE);//GLFW_OPENGL_CORE_PROFILE
		
		glfwWindowHint(GLFW_RESIZABLE,      platform.startupOptions.resizableWindow ? GL_TRUE : GL_FALSE);
		glfwWindowHint(GLFW_FLOATING,      	platform.startupOptions.topmostWindow ? GL_TRUE : GL_FALSE);
		glfwWindowHint(GLFW_DECORATED,      platform.startupOptions.decoratedWindow ? GL_TRUE : GL_FALSE);
		glfwWindowHint(GLFW_FOCUSED,        GL_TRUE);
		glfwWindowHint(GLFW_DOUBLEBUFFER,   GL_TRUE);
		glfwWindowHint(GLFW_RED_BITS,       8);
		glfwWindowHint(GLFW_GREEN_BITS,     8);
		glfwWindowHint(GLFW_BLUE_BITS,      8);
		glfwWindowHint(GLFW_ALPHA_BITS,     8);
		glfwWindowHint(GLFW_DEPTH_BITS,     8);
		glfwWindowHint(GLFW_STENCIL_BITS,   8);
		glfwWindowHint(GLFW_SAMPLES,        (i32)platform.startupOptions.antialiasingNumSamples);
		glfwWindowHint(GLFW_REFRESH_RATE,   requestRefreshRate);
		glfwWindowHint(GLFW_AUTO_ICONIFY,   platform.startupOptions.autoIconify ? GL_TRUE : GL_FALSE);
		
		platform.windowTitle = ArenaNtString(&platform.mainHeap, WINDOW_TITLE);
		
		struct timeb glfwCreateWindowStartTime; ftime(&glfwCreateWindowStartTime);
		
		platform.window = glfwCreateWindow(requestSize.width, requestSize.height, platform.windowTitle, (monitorChoice != nullptr) ? monitorChoice->glfwMonitor : nullptr, NULL);
		
		struct timeb glfwCreateWindowEndTime; ftime(&glfwCreateWindowEndTime);
		u32 glfwCreateWindowElapsedTime = (u32)(1000.0 * (glfwCreateWindowEndTime.time - glfwCreateWindowStartTime.time) + (glfwCreateWindowEndTime.millitm - glfwCreateWindowStartTime.millitm));
		PrintLine_D("glfwCreateWindow took %s", FormattedMilliseconds(glfwCreateWindowElapsedTime));
		
		if (platform.window == nullptr)
		{
			Win32_ExitOnError("GLFW window creation failed!");
		}
		glfwMakeContextCurrent(platform.window);
		
		u32 openglVersionMajor = 0;
		u32 openglVersionMinor = 0;
		Win32_GetOpenglContextVersion(&openglVersionMajor, &openglVersionMinor);
		if (IsVersionBelow(openglVersionMajor, openglVersionMinor, MIN_OPENGL_VERSION_MAJOR, MIN_OPENGL_VERSION_MINOR))
		{
			char* errorStr = TempPrint(
				"Your graphics card does not support the minimum OpenGL version of %u.%u. It only supports version %u.%u.\n"
				"Please update your graphics drivers or use a different display device.",
				MIN_OPENGL_VERSION_MAJOR, MIN_OPENGL_VERSION_MINOR,
				openglVersionMajor, openglVersionMinor
			);
			Win32_ExitOnError(errorStr);
		}
		platform.info.openglVersion.major = openglVersionMajor;
		platform.info.openglVersion.minor = openglVersionMinor;
		
		bool foundWindowMonitor = Plat_GetWindowMonitorIndex(platform.window, &platform.windowMonitorIndex, &platform.windowDisplayModeIndex, &platform.windowIsFullscreen, &platform.windowTargetFramerate);
		if (!foundWindowMonitor)
		{
			Win32_ExitOnError("Failed to find the monitor that the window was created on!");
		}
		
		glfwSetWindowSizeLimits(platform.window, platform.startupOptions.minWindowSize.width, platform.startupOptions.minWindowSize.height, platform.startupOptions.maxWindowSize.width, platform.startupOptions.maxWindowSize.height);
		if (platform.startupOptions.forceAspectRatio)
		{
			glfwSetWindowAspectRatio(platform.window, platform.startupOptions.aspectRatio.width, platform.startupOptions.aspectRatio.height);
		}
		glfwGetFramebufferSize(platform.window, &platform.screenSize.width, &platform.screenSize.height);
		glfwGetWindowPos(platform.window, &platform.windowPosition.x, &platform.windowPosition.y);
		
		if (platform.startupOptions.mouseMode == MouseMode_Default) { glfwSetInputMode(platform.window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);   }
		if (platform.startupOptions.mouseMode == MouseMode_Hidden)  { glfwSetInputMode(platform.window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);   }
		if (platform.startupOptions.mouseMode == MouseMode_Delta)   { glfwSetInputMode(platform.window, GLFW_CURSOR, GLFW_CURSOR_DISABLED); }
		platform.currentMouseMode = platform.startupOptions.mouseMode;
		
		//TODO: Fill the monitor info structures in platform.info
		const GLFWvidmode* currentVideoMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
		PrintLine_I("OpenGL Version %s", glGetString(GL_VERSION)); //TODO: Put these in the platform info structure?
		PrintLine_I("Rendering with \"%s\"", glGetString(GL_RENDERER)); //TODO: Put these in the platform info structure?
		PrintLine_I("Monitor Refresh Rate: %dHz", currentVideoMode->refreshRate);
		PrintLine_I("Screen Size: %dx%d", platform.screenSize.width, platform.screenSize.height);
		PrintLine_I("Window Position: %dx%d", platform.windowPosition.x, platform.windowPosition.y);
		
		glfwSwapInterval(WINDOW_SWAP_INTERVAL);
		glViewport(0, 0, platform.screenSize.width, platform.screenSize.height);
		
		WriteLine_R("Done!");
	}
	
	// +==============================+
	// |     GLEW Initialization      |
	// +==============================+
	{
		WriteLine_R("Initializing GLEW...");
		struct timeb glewInitStartTime; ftime(&glewInitStartTime);
		
		glewExperimental = GL_TRUE;
		GLenum glewInitError = glewInit();
		if (glewInitError != GLEW_OK)
		{
			Win32_ExitOnError("Could not initialize GLEW.");
		}
		
		struct timeb glewInitEndTime; ftime(&glewInitEndTime);
		u32 glewInitElapsedTime = (u32)(1000.0 * (glewInitEndTime.time - glewInitStartTime.time) + (glewInitEndTime.millitm - glewInitStartTime.millitm));
		PrintLine_D("glewInit took %s", FormattedMilliseconds(glewInitElapsedTime));
		WriteLine_R("Done!");
	}
	
	// +==============================+
	// |  Get Win32 Context Handles   |
	// +==============================+
	{
		platform.winDeviceContext = wglGetCurrentDC();
		if (platform.winDeviceContext == NULL)
		{
			Win32_ExitOnError("Failed to get the Windows hardware context");
		}
		platform.winGraphicsContext = wglGetCurrentContext();
		if (platform.winGraphicsContext == NULL)
		{
			Win32_ExitOnError("Failed to get the Windows graphics context created by GLFW");
		}
		platform.windowHandle = GetForegroundWindow();
		if (platform.windowHandle == NULL) { platform.windowHandle = GetDesktopWindow(); }
		if (platform.windowHandle == NULL)
		{
			Win32_ExitOnError("Failed to get the Window Handle");
		}
	}
	
	#if AUDIO_ENABLED
	#if USE_CUSTOM_AUDIO
	// +==============================+
	// |   Initialize Direct Sound    |
	// +==============================+
	{
		AudioDevice_t* chosenDevice = nullptr;
		if (platform.audioDevices.length > 0)
		{
			chosenDevice = DynArrayGet(&platform.audioDevices, AudioDevice_t, platform.startupOptions.audioDeviceIndex);
			platform.info.audioInfo.currentAudioDeviceIndex = platform.startupOptions.audioDeviceIndex;
			PrintLine_R("Initializing DirectSound on device[%u] %s [%s]...", platform.startupOptions.audioDeviceIndex, chosenDevice->name, chosenDevice->subName);
			Assert(chosenDevice != nullptr);
		}
		else
		{
			WriteLine_R("Initializing DirectSound...");
			platform.info.audioInfo.currentAudioDeviceIndex = 0;
		}
		bool initSuccess = Win32_InitAudio(&platform.info.audioInfo, chosenDevice);
		if (!initSuccess)
		{
			WriteLine_E("WARNING: Failed to initialize DirectSound!");
			PrintLine_E("Error: %s", (platform.info.audioInfo.loadError != nullptr) ? platform.info.audioInfo.loadError : "Unknown");
			#if CRASH_ON_AUDIO_INIT_FAILED
			Assert(false);
			Win32_ExitOnError("Failed to initialize DirectSound!");
			#endif
		}
		WriteLine_R("Done!");
	}
	#else //!USE_CUSTOM_AUDIO
	// +==============================+
	// |      Initialize OpenAL       |
	// +==============================+
	{
		WriteLine_R("Initializing OpenAL...");
		ALboolean enumeration = alcIsExtensionPresent(NULL, "ALC_ENUMERATION_EXT");
		Win32_PrintOpenAlError("alcIsExtensionPresent");
		if (enumeration != AL_FALSE)
		{
			const ALCchar* devices = alcGetString(NULL, ALC_DEVICE_SPECIFIER);
			Win32_PrintOpenAlError("alcGetString");
			const ALCchar *device = devices, *next = devices + 1;
			size_t len = 0;
			u32 dIndex = 0;
			PrintLine_D("OpenAL Devices list:");
			while (device && *device != '\0' && next && *next != '\0')
			{
				PrintLine_D("[%u]: %s", dIndex, device);
				len = MyStrLength(device);
				device += (len + 1);
				next += (len + 2);
				dIndex++;
			}
		}
		platform.info.alDevice = alcOpenDevice(NULL);
		Win32_PrintOpenAlError("alcOpenDevice");
		if (!platform.info.alDevice)
		{
			Win32_ExitOnError("Could not create OpenAL device");
		}
		platform.info.alContext = alcCreateContext(platform.info.alDevice, NULL);
		Win32_PrintOpenAlError("alcCreateContext");
		if (!alcMakeContextCurrent(platform.info.alContext))
		{
			Win32_ExitOnError("Failed to make OpenAL context current!");
		}
		ALfloat listenerOri[] = {0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f};
		alListener3f(AL_POSITION, 0, 0, 1.0f);
		Win32_PrintOpenAlError("alListener3f(AL_POSITION,...)");
		alListener3f(AL_VELOCITY, 0, 0, 0);
		Win32_PrintOpenAlError("alListener3f(AL_VELOCITY,...)");
		alListenerfv(AL_ORIENTATION, listenerOri);
		Win32_PrintOpenAlError("alListener3f(AL_ORIENTATION,...)");
		WriteLine_R("Done!");
	}
	#endif
	#endif
	
	// +==============================+
	// |    Render Loading Screen     |
	// +==============================+
	{
		WriteLine_I("Preparing for loading screen render...");
		Win32_PrepareForRendering();
		WriteLine_I("Done!");
		
		// while (glfwWindowShouldClose(platform.window) == false) //Enable this to loop forever on the loading screen
		{
			glfwPollEvents();
			Win32_RenderLoadingScreen(0.01f);
			glfwSwapBuffers(platform.window);
		}
	}
	
	// +==============================+
	// |         Window Icon          |
	// +==============================+
	{
		WriteLine_R("Loading window icons...");
		
		FileInfo_t iconFiles[4];
		iconFiles[0] = Win32_ReadEntireFile(platform.startupOptions.iconFilePaths[0]);
		iconFiles[1] = Win32_ReadEntireFile(platform.startupOptions.iconFilePaths[1]);
		iconFiles[2] = Win32_ReadEntireFile(platform.startupOptions.iconFilePaths[2]);
		iconFiles[3] = Win32_ReadEntireFile(platform.startupOptions.iconFilePaths[3]);
		
		if (iconFiles[0].content == nullptr) { Win32_ExitOnError(TempPrint("Couldn't load icon file from \"%s\"", platform.startupOptions.iconFilePaths[0])); }
		if (iconFiles[1].content == nullptr) { Win32_ExitOnError(TempPrint("Couldn't load icon file from \"%s\"", platform.startupOptions.iconFilePaths[1])); }
		if (iconFiles[2].content == nullptr) { Win32_ExitOnError(TempPrint("Couldn't load icon file from \"%s\"", platform.startupOptions.iconFilePaths[2])); }
		if (iconFiles[3].content == nullptr) { Win32_ExitOnError(TempPrint("Couldn't load icon file from \"%s\"", platform.startupOptions.iconFilePaths[3])); }
		
		i32 numChannels[4];
		i32 widths[4], heights[4];
		u8* imageData[4];
		imageData[0] = stbi_load_from_memory((u8*)iconFiles[0].content, iconFiles[0].size, &widths[0], &heights[0], &numChannels[0], 4);
		imageData[1] = stbi_load_from_memory((u8*)iconFiles[1].content, iconFiles[1].size, &widths[1], &heights[1], &numChannels[1], 4);
		imageData[2] = stbi_load_from_memory((u8*)iconFiles[2].content, iconFiles[2].size, &widths[2], &heights[2], &numChannels[2], 4);
		imageData[3] = stbi_load_from_memory((u8*)iconFiles[3].content, iconFiles[3].size, &widths[3], &heights[3], &numChannels[3], 4);
		
		GLFWimage images[4];
		images[0].width = widths[0]; images[0].height = heights[0]; images[0].pixels = imageData[0];
		images[1].width = widths[1]; images[1].height = heights[1]; images[1].pixels = imageData[1];
		images[2].width = widths[2]; images[2].height = heights[2]; images[2].pixels = imageData[2];
		images[3].width = widths[3]; images[3].height = heights[3]; images[3].pixels = imageData[3];
		
		glfwSetWindowIcon(platform.window, ArrayCount(images), images);
		
		stbi_image_free(imageData[0]);
		stbi_image_free(imageData[1]);
		stbi_image_free(imageData[2]);
		stbi_image_free(imageData[3]);
		Win32_FreeFileMemory(&iconFiles[0]);
		Win32_FreeFileMemory(&iconFiles[1]);
		Win32_FreeFileMemory(&iconFiles[2]);
		Win32_FreeFileMemory(&iconFiles[3]);
		
		WriteLine_R("Done!");
	}
	
	glfwPollEvents();
	Win32_RenderLoadingScreen(0.05f);
	glfwSwapBuffers(platform.window);
	
	// +==============================+
	// |           Cursors            |
	// +==============================+
	{
		WriteLine_R("Creating cursors...");
		platform.glfwCursors[Cursor_Default]          = glfwCreateStandardCursor(GLFW_ARROW_CURSOR);
		platform.glfwCursors[Cursor_Text]             = glfwCreateStandardCursor(GLFW_IBEAM_CURSOR);
		platform.glfwCursors[Cursor_Pointer]          = glfwCreateStandardCursor(GLFW_HAND_CURSOR);
		platform.glfwCursors[Cursor_ResizeHorizontal] = glfwCreateStandardCursor(GLFW_HRESIZE_CURSOR);
		platform.glfwCursors[Cursor_ResizeVertical]   = glfwCreateStandardCursor(GLFW_VRESIZE_CURSOR);
		glfwSetCursor(platform.window, platform.glfwCursors[Cursor_Default]);
		platform.currentCursor = Cursor_Default;
		WriteLine_R("Done!");
	}
	
	// +==============================+
	// |    WinHttp Initialization    |
	// +==============================+
	{
		WriteLine_R("Opening WinHTTP session...");
		platform.httpSessionHandle = WinHttpOpen(L"Teletoot/1.0", //TODO: Make this a different client string?
			WINHTTP_ACCESS_TYPE_DEFAULT_PROXY,
			WINHTTP_NO_PROXY_NAME,
			WINHTTP_NO_PROXY_BYPASS,
			WINHTTP_FLAG_ASYNC
		);
		
		DWORD callbackMask = WINHTTP_CALLBACK_FLAG_ALL_COMPLETIONS |
			WINHTTP_CALLBACK_FLAG_ALL_NOTIFICATIONS |
			WINHTTP_CALLBACK_FLAG_RESOLVE_NAME |
			WINHTTP_CALLBACK_FLAG_CONNECT_TO_SERVER |
			WINHTTP_CALLBACK_FLAG_DETECTING_PROXY |
			WINHTTP_CALLBACK_FLAG_DATA_AVAILABLE |
			WINHTTP_CALLBACK_FLAG_HEADERS_AVAILABLE |
			WINHTTP_CALLBACK_FLAG_READ_COMPLETE |
			WINHTTP_CALLBACK_FLAG_REQUEST_ERROR |
			WINHTTP_CALLBACK_FLAG_SEND_REQUEST |
			WINHTTP_CALLBACK_FLAG_SENDREQUEST_COMPLETE |
			WINHTTP_CALLBACK_FLAG_WRITE_COMPLETE |
			WINHTTP_CALLBACK_FLAG_RECEIVE_RESPONSE |
			WINHTTP_CALLBACK_FLAG_CLOSE_CONNECTION |
			WINHTTP_CALLBACK_FLAG_HANDLES |
			WINHTTP_CALLBACK_FLAG_REDIRECT |
			WINHTTP_CALLBACK_FLAG_INTERMEDIATE_RESPONSE |
			WINHTTP_CALLBACK_FLAG_SECURE_FAILURE;
		
		if (WinHttpSetStatusCallback(platform.httpSessionHandle, Win32_HttpStatusCallback, callbackMask, NULL) == WINHTTP_INVALID_STATUS_CALLBACK)
		{
			Win32_ExitOnError("Couldn't set WinHTTP callback");
		}
		
		if (!platform.httpSessionHandle)
		{
			Win32_ExitOnError("Couldn't open WinHTTP session handle");
		}
		
		WriteLine_R("Session created!");
	}
	
	glfwPollEvents();
	Win32_RenderLoadingScreen(0.10f);
	glfwSwapBuffers(platform.window);
	
	// +==============================+
	// |      Register Callbacks      |
	// +==============================+
	{
		Write_R("Registering GLFW callbacks...");
		glfwSetWindowCloseCallback(platform.window,     GlfwWindowCloseCallback);
		glfwSetFramebufferSizeCallback(platform.window, GlfwWindowSizeCallback);
		glfwSetWindowPosCallback(platform.window,       GlfwWindowMoveCallback);
		glfwSetWindowIconifyCallback(platform.window,   GlfwWindowMinimizeCallback);
		glfwSetWindowFocusCallback(platform.window,     GlfwWindowFocusCallback);
		glfwSetKeyCallback(platform.window,             GlfwKeyPressedCallback);
		glfwSetCharCallback(platform.window,            GlfwCharPressedCallback);
		glfwSetCursorPosCallback(platform.window,       GlfwCursorPosCallback);
		glfwSetMouseButtonCallback(platform.window,     GlfwMousePressCallback);
		glfwSetScrollCallback(platform.window,          GlfwMouseScrollCallback);
		glfwSetCursorEnterCallback(platform.window,     GlfwCursorEnteredCallback);
		glfwSetSystemEventCallback(platform.window,     GlfwSystemEventCallback);
		// glfwSetJoystickCallback(               GlfwJoystickCallback);
		glfwSetDropCallback(platform.window,            GlfwFileDropCallback);
		WriteLine_R("Done!");
	}
	
	// +==============================+
	// |      Fill Platform Info      |
	// +==============================+
	{
		platform.info.platformType   = Platform_Windows;
		platform.info.version        = platform.version;
		platform.info.window         = platform.window;
		
		platform.info.numProgramArguments = platform.numProgramArguments;
		platform.info.programArguments    = platform.programArguments;
		
		platform.info.numMonitors         = platform.glfwMonitors.length;
		platform.info.primaryMonitorIndex = platform.primaryMonitorIndex;
		platform.info.monitors            = (const MonitorInfo_t*)platform.glfwMonitors.items;
		
		#if AUDIO_ENABLED && USE_CUSTOM_AUDIO
		platform.info.numAudioDevices         = platform.audioDevices.length;
		platform.info.primaryAudioDeviceIndex = platform.primaryAudioDeviceIndex;
		platform.info.audioDevices            = (const AudioDevice_t*)platform.audioDevices.items;
		#endif
		
		#if DEBUG
		platform.info.appAllocationsPntr = &platform.appAllocations;
		#endif
		
		platform.info.GetPerfTime             = Win32_GetPerfTime;
		platform.info.GetPerfTimeDiff         = Win32_GetPerfTimeDiff;
		platform.info.DebugOutput             = Win32_DebugOutput;
		platform.info.DebugPrintVa            = Win32_DebugPrintVa;
		platform.info.DebugPrint              = Win32_DebugPrint;
		platform.info.CompleteRelativePath    = Win32_CompleteRelativePath;
		platform.info.DoesFolderExist         = Win32_DoesFolderExist;
		platform.info.CreateFolder            = Win32_CreateFolder;
		platform.info.DoesFileExist           = Win32_DoesFileExist;
		platform.info.FreeFileMemory          = Win32_FreeFileMemory;
		platform.info.ReadEntireFile          = Win32_ReadEntireFile;
		platform.info.WriteEntireFile         = Win32_WriteEntireFile;
		platform.info.DeleteFile              = Win32_DeleteFile;
		platform.info.OpenFile                = Win32_OpenFile;
		platform.info.WriteToFile             = Win32_WriteToFile;
		platform.info.SeekToOffsetInFile      = Win32_SeekToOffsetInFile;
		platform.info.MoveFileCursor          = Win32_MoveFileCursor;
		platform.info.ReadFromFile            = Win32_ReadFromFile;
		platform.info.CloseFile               = Win32_CloseFile;
		platform.info.MoveFile                = Win32_MoveFile;
		platform.info.LaunchFile              = Win32_LaunchFile;
		platform.info.GetNumFilesInDirectory  = Win32_GetNumFilesInDirectory;
		platform.info.GetFileInDirectory      = Win32_GetFileInDirectory;
		platform.info.ShowSourceFile          = Win32_ShowSourceFile;
		platform.info.ShowFile                = Win32_ShowFile;
		platform.info.GetSpecialFolderPath    = Win32_GetSpecialFolderPath;
		platform.info.CopyToClipboard         = Win32_CopyToClipboard;
		platform.info.CopyFromClipboard       = Win32_CopyFromClipboard;
		platform.info.WatchFile               = Win32_WatchFile;
		platform.info.UnwatchFile             = Win32_UnwatchFile;
		platform.info.IsWatchingFile          = Win32_IsWatchingFile;
		platform.info.AllocateMemory          = Win32_AllocateMemory;
		platform.info.ReallocateMemory        = Win32_ReallocateMemory;
		platform.info.FreeMemory              = Win32_FreeMemory;
		platform.info.UpdateLoadingBar        = Win32_UpdateLoadingBar;
		platform.info.WasWindowCloseRequested = Win32_WasWindowCloseRequested;
		platform.info.OpenEmailPrompt         = Win32_OpenEmailPrompt;
		platform.info.HttpRequestStart        = Win32_HttpRequestStart;
		platform.info.HttpRequestInProgress   = Win32_HttpRequestInProgress;
		platform.info.OpenPlatformFont        = Win32_OpenPlatformFont;
		platform.info.ClosePlatformFont       = Win32_ClosePlatformFont;
		platform.info.CreateMutex             = Win32_CreateMutex;
		platform.info.LockMutex               = Win32_LockMutex;
		platform.info.UnlockMutex             = Win32_UnlockMutex;
		platform.info.GetThisThreadId         = Win32_GetThisThreadId;
		platform.info.IsMainThread            = Win32_IsMainThread;
		platform.info.SleepForMs              = Win32_SleepForMs;
		platform.info.QueueThreadedTask       = Win32_QueueThreadedTask;
		platform.info.GetThreadInfo           = Win32_GetThreadInfo;
	}
	
	// +==============================+
	// | Allocate Application Memory  |
	// +==============================+
	{
		WriteLine_R("Allocating application memory...");
		platform.appMemory.permanantSize = platform.startupOptions.permanantMemorySize;
		platform.appMemory.transientSize = platform.startupOptions.temporaryMemorySize;
		
		PrintLine_D("Allocating %s for permanant application memory...", FormattedSizeStr(platform.appMemory.permanantSize));
		platform.appMemory.permanantPntr = (void*)VirtualAlloc(0, platform.appMemory.permanantSize, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
		
		PrintLine_D("Allocating %s for temporary application memory...", FormattedSizeStr(platform.appMemory.transientSize));
		platform.appMemory.transientPntr = (void*)VirtualAlloc(0, platform.appMemory.transientSize, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
		
		if (platform.appMemory.permanantPntr == nullptr || platform.appMemory.transientPntr == nullptr)
		{
			Win32_ExitOnError("Could not allocate enough memory for application!");
		}
		WriteLine_R("Done!");
		
		Win32_CreateMutex(&platform.appAllocationsMutex);
		//NOTE: It's important that this dynamic array be on a thread safe memory arena like stdHeap
		CreateDynamicArray(&platform.appAllocations, &platform.stdHeap, sizeof(Win32Allocation_t));
	}
	
	#if DEBUG
	// +==============================+
	// |      Start DLL Watching      |
	// +==============================+
	{
		Write_R("Starting application DLL watching...");
		Win32_CreateMutex(&platform.dllWatchInfo.mutex);
		platform.dllWatchInfo.dllFilePath = platform.appDllPath;
		platform.dllWatchInfo.lastWriteTime = app.lastWriteTime;
		platform.dllWatchThread = _Win32_CreateThread(DllWatchingFunction, &platform.dllWatchInfo);
		if (platform.dllWatchThread == nullptr)
		{
			Win32_ExitOnError("Could not start DLL watching thread!");
		}
		WriteLine_R("Done!");
	}
	#endif
	
	LARGE_INTEGER perfCountFrequencyLarge;
	QueryPerformanceFrequency(&perfCountFrequencyLarge);
	platform.perfCountFrequency = (i64)perfCountFrequencyLarge.QuadPart;
	PrintLine_D("Performance Counter Frequency: %ld", platform.perfCountFrequency);
	platform.doSleepWait = true;
	MMRESULT timeBeginPeriodResult = timeBeginPeriod(1); //Set windows scheduler granularity to 1ms
	if (timeBeginPeriodResult != TIMERR_NOERROR)
	{
		WriteLine_E("Warning! Unable to set the scheduler timer! Our update loop will simply do a spin-lock, wasting CPU time");
		platform.doSleepWait = false;
	}
	platform.doingManualUpdateLoopWait = true;
	
	// +==============================+
	// |      Init File Watching      |
	// +==============================+
	_Win32_InitFileWatching();
	
	// +==============================+
	// |   Fill Initial AppInput_t    |
	// +==============================+
	{
		platform.currentInput = &platform.appInputs[0];
		ClearPointer(platform.currentInput);
		
		platform.currentInput->currentMonitorIndex = platform.windowMonitorIndex;
		platform.currentInput->currentDisplayModeIndex = platform.windowDisplayModeIndex;
		platform.currentInput->isFullscreen = platform.windowIsFullscreen;
		platform.currentInput->targetFramerate = platform.windowTargetFramerate;
		
		glfwGetFramebufferSize(platform.window, &platform.currentInput->screenSize.width, &platform.currentInput->screenSize.height);
		glfwGetWindowPos(platform.window, &platform.currentInput->windowPosition.x, &platform.currentInput->windowPosition.y);
		platform.currentInput->windowHasFocus = (glfwGetWindowAttrib(platform.window, GLFW_FOCUSED) == GLFW_TRUE);
		platform.currentInput->windowIsMinimized = (glfwGetWindowAttrib(platform.window, GLFW_ICONIFIED) == GLFW_TRUE);
		
		r64 mousePosX, mousePosY;
		glfwGetCursorPos(platform.window, &mousePosX, &mousePosY);
		platform.currentInput->mousePos = NewVec2((r32)mousePosX, (r32)mousePosY);
		platform.currentInput->mouseInsideWindow = (mousePosX >= 0 && mousePosY >= 0 && mousePosX < platform.currentInput->screenSize.width && mousePosY < platform.currentInput->screenSize.height);
		
		for (i32 gIndex = 0; gIndex < MAX_NUM_GAMEPADS; gIndex++)
		{
			GamepadState_t* gamepad = &platform.currentInput->gamepads[gIndex];
			gamepad->isConnected = (glfwJoystickPresent(gIndex) == GLFW_TRUE);
			gamepad->wasConnected = gamepad->isConnected;
			if (gamepad->isConnected) { PrintLine_D("Gamepad %u connected at startup", gIndex); }
		}
		
		glfwGetWindowFrameSize(platform.window, &platform.currentInput->windowBorderSizeLeft, &platform.currentInput->windowBorderSizeTop, &platform.currentInput->windowBorderSizeRight, &platform.currentInput->windowBorderSizeBottom);
		
		Win32_UpdateAppInputTimeInfo(platform.currentInput);
	}
	
	glfwPollEvents();
	Win32_RenderLoadingScreen(0.15f);
	glfwSwapBuffers(platform.window);
	
	// +==============================+
	// |      Call AppInitialize      |
	// +==============================+
	PrintLine_I("+==============================+");
	PrintLine_I("|    Calling AppInitialize     |");
	PrintLine_I("+==============================+");
	#if 1
	struct timeb appInitStartTime; ftime(&appInitStartTime);
	app.AppInitialize(&platform.info, &platform.appMemory, platform.currentInput);
	struct timeb appInitEndTime; ftime(&appInitEndTime);
	u32 appInitElapsedTime = (u32)(1000.0 * (appInitEndTime.time - appInitStartTime.time) + (appInitEndTime.millitm - appInitStartTime.millitm));
	PrintLine_D("AppInitialize took %s", FormattedMilliseconds(appInitElapsedTime));
	#else
	Win32_CreateMutex(&platform.openglContextMutex);
	platform.appLoadingFinished = false;
	//TODO: Time this section
	Thread_t* loadingThread = _Win32_CreateThread(Win32_CallAppInitialize, nullptr);
	while (!platform.appLoadingFinished && !glfwWindowShouldClose(platform.window))
	{
		glfwPollEvents();
		r32 loadingPercent = platform.appLoadingPercent;
		Win32_RenderLoadingScreen(0.15f + (0.85f * loadingPercent));
		glfwSwapBuffers(platform.window);
	}
	if (glfwWindowShouldClose(platform.window) && _Win32_IsThreadAlive(loadingThread)) { _Win32_DestroyThread(loadingThread); }
	glfwMakeContextCurrent(platform.window);
	#endif
	WriteLine_R("AppInitialize finished!");
	
	glfwPollEvents();
	Win32_RenderLoadingScreen(0.95f);
	glfwSwapBuffers(platform.window);
	
	// struct timeb initEndTime;
	// ftime(&initEndTime);
	// u32 initElapsedTime = (u32)(1000.0 * (initEndTime.time - initStartTime.time) + (initEndTime.millitm - initStartTime.millitm));
	// PrintLine_D("Startup took %s", FormattedMilliseconds(initElapsedTime));
	PerfTime_t initEndTime = Win32_GetPerfTime();
	PrintLine_D("Startup took %s", FormattedMillisecondsR64(Win32_GetPerfTimeDiff(&initStartTime, &initEndTime)));
	PrintLine_I("+==============================+");
	PrintLine_I("|   Entering Main Game Loop    |");
	PrintLine_I("+==============================+");
	platform.startupFinished = true;
	TempPopMark();
	Assert(ArenaNumMarks(TempArena) == 0);
	
	// +--------------------------------------------------------------+
	// |                       Main Update Loop                       |
	// +--------------------------------------------------------------+
	while (glfwWindowShouldClose(platform.window) == false)
	{
		glfwPollEvents();
		Win32_DoUpdateLoopIteration(false);
	}
	WriteLine_W("We have exited the main game loop");
	
	WriteLine_R("Calling AppClosing...");
	app.AppClosing(&platform.info, &platform.appMemory);
	WriteLine_R("AppClosing done!");
	
	WriteLine_I("Waiting for tasks to finish...");
	_Win32_WaitForThreadedTasksToFinish();
	_Win32_ClearOutFinishedTasks();
	_Win32_StopTaskThreads();
	WriteLine_I("All tasks finished!");
	
	#if AUDIO_ENABLED
	CoUninitialize();
	#endif
	
	#if STEAM_BUILD
	WriteLine_I("Shutting down Steamworks...");
	// SteamworksDestroyHandle();
	SteamAPI_Shutdown();
	#endif
	
	WriteLine_I("Terminating GLFW...");
	glfwTerminate();
	
	WriteLine_I("Terminating threads...");
	_Win32_TerminateAllThreads();
	
	WriteLine_E("Program finished. Goodbye :D");
	free(platform.heapSpace);
	free(platform.tempSpace);
	//TODO: Should we free any other allocated memory? Not sure if it matters.
	return 0;
}

void Win32_DoUpdateLoopIteration(bool happeningInEventCallback) //pre-declared above
{
	// +================================+
	// | Keep Track of Frame Start Time |
	// +================================+
	LARGE_INTEGER frameStartCounter;
	QueryPerformanceCounter(&frameStartCounter);
	u64 frameStartCycleCount = __rdtsc();
	
	TempPushMark();
	{
		#if DEBUG
		if (_Win32_AreAllApplicationTasksFinished())
		{
			bool appReloaded = Win32_CheckAppDllForChanges();
			if (appReloaded) { platform.currentInput->windowInteractionHappened = true; }
		}
		#endif
		
		_Win32_ClearOutFinishedTasks();
		
		// +==============================+
		// |       External Updates       |
		// +==============================+
		#if STEAM_BUILD
		SteamAPI_RunCallbacks();
		#endif
		_Win32_UpdateFileWatching();
		_Win32_UpdateHttpRequest();
		Plat_CheckForMonitorChanges();
		Plat_GetWindowMonitorIndex(platform.window, &platform.windowMonitorIndex, &platform.windowDisplayModeIndex, &platform.windowIsFullscreen, &platform.windowTargetFramerate);
		
		// +==============================+
		// |        Swap AppInputs        |
		// +==============================+
		//Swap the pointers for current and last input
		AppInput_t* currentInput = platform.currentInput; //this one will be passed to the game on this update loop
		AppInput_t* newInput = &platform.appInputs[0];
		if (newInput == currentInput) { newInput++; }
		
		Plat_UpdateAppInputGamepadInfo(currentInput);
		Plat_UpdateAppInputMonitorInfo(currentInput);
		Win32_UpdateAppInputTimeInfo(currentInput);
		_Win32_UpdateAppInputTaskInfo(currentInput);
		Win32_UpdateManualFrameWaitingEnabled(currentInput);
		glfwGetFramebufferSize(platform.window, &currentInput->screenSize.width, &currentInput->screenSize.height);
		
		u32 numDebugLinesPassed = 0;
		PlatDebugLine_t* passedLines = _Win32_PopDebugLines(&numDebugLinesPassed);
		currentInput->numDebugLines = numDebugLinesPassed;
		currentInput->debugLines = passedLines;
		
		u32 numPassedDroppedFiles = 0;
		char** passedDroppedFilePaths = nullptr;
		if (platform.droppedFilePaths.length > 0)
		{
			numPassedDroppedFiles = platform.droppedFilePaths.length;
			passedDroppedFilePaths = PushArray(&platform.mainHeap, char*, platform.droppedFilePaths.length);
			MyMemCopy(passedDroppedFilePaths, platform.droppedFilePaths.items, sizeof(char*) * platform.droppedFilePaths.length);
			DynArrayClear(&platform.droppedFilePaths);
		}
		currentInput->numDroppedFiles = numPassedDroppedFiles;
		currentInput->droppedFilePaths = (const char**)passedDroppedFilePaths;
		
		//update the pointer in the platform so that any windows callbacks during the update will start filling the next appInput
		Plat_DestroyAppInput(newInput);
		Plat_SwapAppInputs(currentInput, newInput);
		platform.currentInput = newInput;
		
		platform.info.numMonitors         = platform.glfwMonitors.length;
		platform.info.primaryMonitorIndex = platform.primaryMonitorIndex;
		platform.info.monitors            = (const MonitorInfo_t*)platform.glfwMonitors.items;
		
		#if AUDIO_ENABLED && USE_CUSTOM_AUDIO
		platform.info.numAudioDevices         = platform.audioDevices.length;
		platform.info.primaryAudioDeviceIndex = platform.primaryAudioDeviceIndex;
		platform.info.audioDevices            = (const AudioDevice_t*)platform.audioDevices.items;
		#endif
		
		// +==============================+
		// |        Call AppUpdate        |
		// +==============================+
		ClearStruct(platform.appOutput);
		platform.appOutput.cursorType = platform.currentCursor;
		platform.appOutput.mouseMode = platform.currentMouseMode;
		glViewport(0, 0, currentInput->screenSize.width, currentInput->screenSize.height);
		r64 appUpdateStart = glfwGetTime() * 1000.0;
		app.AppUpdate(&platform.info, &platform.appMemory, currentInput, &platform.appOutput);
		#if AUDIO_ENABLED && USE_CUSTOM_AUDIO
		// +==============================+
		// |         Update Audio         |
		// +==============================+
		r64 audioUpdateStart = glfwGetTime() * 1000.0;
		Win32_UpdateAudio(&platform.info.audioInfo);
		r64 audioUpdateEnd = glfwGetTime() * 1000.0;
		#endif
		r64 appUpdateEnd = glfwGetTime() * 1000.0;
		platform.currentInput->lastUpdateElapsedMs = appUpdateEnd - appUpdateStart;
		
		// +==============================+
		// |      Swap Back-Buffers       |
		// +==============================+
		glfwSwapBuffers(platform.window);
		
		#if 0
		while (glfwGetKey(platform.window, GLFW_KEY_SPACE) == GLFW_RELEASE) { glfwPollEvents(); }
		#endif
		
		// +==============================+
		// |       Handle AppOutput       |
		// +==============================+
		{
			if (platform.appOutput.cursorType != platform.currentCursor)
			{
				glfwSetCursor(platform.window, platform.glfwCursors[platform.appOutput.cursorType]);
				platform.currentCursor = platform.appOutput.cursorType;
			}
			if (platform.appOutput.mouseMode != platform.currentMouseMode)
			{
				if (platform.appOutput.mouseMode == MouseMode_Default) { glfwSetInputMode(platform.window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);   }
				if (platform.appOutput.mouseMode == MouseMode_Hidden)  { glfwSetInputMode(platform.window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);   }
				if (platform.appOutput.mouseMode == MouseMode_Delta)   { glfwSetInputMode(platform.window, GLFW_CURSOR, GLFW_CURSOR_DISABLED); }
				platform.currentMouseMode = platform.appOutput.mouseMode;
			}
			if (platform.appOutput.closeWindow)
			{
				glfwSetWindowShouldClose(platform.window, true);
			}
			
			if (platform.appOutput.windowTitle[0] != '\0')
			{
				Assert(BufferIsNullTerminated(&platform.appOutput.windowTitle[0], ArrayCount(platform.appOutput.windowTitle)));
				Win32_UpdateWindowTitle(&platform.appOutput.windowTitle[0]);
			}
			
			if (platform.appOutput.makeFullscreen)
			{
				if (!platform.windowIsFullscreen || platform.appOutput.fullscreenMonitorIndex != platform.currentInput->currentMonitorIndex) //TODO: Make this support changes to resolution as well
				{
					Assert(platform.appOutput.fullscreenMonitorIndex < platform.glfwMonitors.length);
					MonitorInfo_t* requestedMonitor = DynArrayGet(&platform.glfwMonitors, MonitorInfo_t, platform.appOutput.fullscreenMonitorIndex);
					Assert(requestedMonitor != nullptr);
					Assert(platform.appOutput.fullscreenDisplayModeIndex < requestedMonitor->numModes);
					Assert(requestedMonitor->modes != nullptr);
					MonitorDisplayMode_t* requestedMode = &requestedMonitor->modes[platform.appOutput.fullscreenDisplayModeIndex];
					PrintLine_I("Request Fullscreen on monitor %u mode %u (%dx%d %ubit %.1ffps)",
						platform.appOutput.fullscreenMonitorIndex, platform.appOutput.fullscreenDisplayModeIndex,
						requestedMode->resolution.width, requestedMode->resolution.height, requestedMode->bitDepth, requestedMode->refreshRate
					);
					glfwSetWindowMonitor(platform.window, requestedMonitor->glfwMonitor, 0, 0, requestedMode->resolution.width, requestedMode->resolution.height, (i32)requestedMode->refreshRate);
					glfwSwapInterval(WINDOW_SWAP_INTERVAL);
				}
				else
				{
					WriteLine_E("Application requested fullscreen but is already in fullscreen mode!");
				}
			}
			if (platform.appOutput.makeWindowed)
			{
				if (platform.windowIsFullscreen)
				{
					PrintLine_I("Request Windowed at (%d,%d) with resolution (%d,%d)",
						platform.appOutput.windowedPosition.x, platform.appOutput.windowedPosition.y,
						platform.appOutput.windowedResolution.width, platform.appOutput.windowedResolution.height
					);
					glfwSetWindowMonitor(platform.window, nullptr, platform.appOutput.windowedPosition.x, platform.appOutput.windowedPosition.y, platform.appOutput.windowedResolution.width, platform.appOutput.windowedResolution.height, 60);
					glfwSwapInterval(WINDOW_SWAP_INTERVAL);
				}
				else
				{
					WriteLine_E("Application requested windowed but is already in windowed mode!");
				}
			}
			if (platform.appOutput.changeResolution)
			{
				if (!platform.windowIsFullscreen)
				{
					PrintLine_I("Request Windowed resolution (%d,%d)",
						platform.appOutput.newResolution.width, platform.appOutput.newResolution.height
					);
					glfwSetWindowSize(platform.window, platform.appOutput.newResolution.width, platform.appOutput.newResolution.height);
					glfwSwapInterval(WINDOW_SWAP_INTERVAL);
				}
				else
				{
					WriteLine_E("Application requested resolution change while in fullscreen mode!");
				}
			}
			if (platform.appOutput.changeDisplayMode)
			{
				if (platform.windowIsFullscreen)
				{
					Assert(platform.currentInput->currentMonitorIndex < platform.glfwMonitors.length);
					MonitorInfo_t* requestedMonitor = DynArrayGet(&platform.glfwMonitors, MonitorInfo_t, platform.currentInput->currentMonitorIndex);
					Assert(requestedMonitor != nullptr);
					Assert(platform.appOutput.newDisplayModeIndex < requestedMonitor->numModes);
					Assert(requestedMonitor->modes != nullptr);
					MonitorDisplayMode_t* requestedMode = &requestedMonitor->modes[platform.appOutput.newDisplayModeIndex];
					PrintLine_I("Request Fullscreen DisplayMode (%d,%d) %ubit %.1ffps",
						requestedMode->resolution.width, requestedMode->resolution.height,
						requestedMode->bitDepth, requestedMode->refreshRate
					);
					glfwSetWindowMonitor(platform.window, requestedMonitor->glfwMonitor, 0, 0, requestedMode->resolution.width, requestedMode->resolution.height, (i32)requestedMode->refreshRate);
					glfwSwapInterval(WINDOW_SWAP_INTERVAL);
				}
				else
				{
					WriteLine_E("Application requested display mode change while in windowed mode!");
				}
			}
			if (platform.appOutput.changeAutoIconify)
			{
				PrintLine_D("Auto-Iconify %s", platform.appOutput.autoIconify ? "Enabled" : "Disabled");
				glfwSetWindowAttrib(platform.window, GLFW_AUTO_ICONIFY, platform.appOutput.autoIconify ? GL_TRUE : GL_FALSE);
			}
			
			if (platform.appOutput.shouldShowErrorAndClose)
			{
				Win32_ExitOnError(platform.appOutput.errorMessage);
			}
		}
		
		// +==============================+
		// |    Deallocate Debug Lines    |
		// +==============================+
		_Win32_DeallocatePoppedDebugLines(passedLines, numDebugLinesPassed);
		if (numPassedDroppedFiles > 0)
		{
			Assert(passedDroppedFilePaths != nullptr);
			for (u32 fIndex = 0; fIndex < numPassedDroppedFiles; fIndex++)
			{
				Assert(passedDroppedFilePaths[fIndex] != nullptr);
				ArenaPop(&platform.mainHeap, passedDroppedFilePaths[fIndex]);
			}
			ArenaPop(&platform.mainHeap, passedDroppedFilePaths);
		}
	}
	TempPopMark();
	Assert(ArenaNumMarks(TempArena) == 0 || happeningInEventCallback);
	
	// +==============================+
	// |  Manual Wait For Framerate   |
	// +==============================+
	u64 frameEndCycleCount = __rdtsc();
	u64 cyclesElapsed = frameEndCycleCount - frameStartCycleCount;
	r32 targetElapsedTime = (1.0f / platform.windowTargetFramerate);
	LARGE_INTEGER frameEndCounter;
	QueryPerformanceCounter(&frameEndCounter);
	r32 frameElapsedTime = ((r32)(frameEndCounter.QuadPart - frameStartCounter.QuadPart) / (r32)platform.perfCountFrequency);
	// PrintLine_D("Cycles: %llu ElapsedMs: %f (%lld %lld)", cyclesElapsed, frameElapsedTime, frameEndCounter.QuadPart, frameStartCounter.QuadPart);
	if (platform.doingManualUpdateLoopWait && !happeningInEventCallback)
	{
		if (frameElapsedTime < targetElapsedTime && targetElapsedTime - frameElapsedTime >= (PLAT_MIN_FRAME_WAIT_TIME / 1000.0f))
		{
			platform.currentInput->timeSpentWaitingLastFrame = (targetElapsedTime - frameElapsedTime) * 1000.0f;
			while (frameElapsedTime < targetElapsedTime)
			{
				if (true && platform.doSleepWait)
				{
					DWORD timeToSleep = (DWORD)FloorR32i((targetElapsedTime - frameElapsedTime) * 1000.0f);
					Sleep(timeToSleep);
				}
				QueryPerformanceCounter(&frameEndCounter);
				frameElapsedTime = ((r32)(frameEndCounter.QuadPart - frameStartCounter.QuadPart) / (r32)platform.perfCountFrequency);
			}
		}
		else { platform.currentInput->timeSpentWaitingLastFrame = 0; }
	}
	else { platform.currentInput->timeSpentWaitingLastFrame = 0; }
}

#if (ASSERTIONS_ACTIVE && USE_ASSERT_FAILURE_FUNCTION)
//This function is declared in my_assert.h and needs to be implemented by us for a debug build to compile successfully
void AssertFailure(const char* function, const char* filename, int lineNumber, const char* expressionStr)
{
	u32 fileNameStart = 0;
	for (u32 cIndex = 0; filename[cIndex] != '\0'; cIndex++)
	{
		if (filename[cIndex] == '\\' || filename[cIndex] == '/')
		{
			fileNameStart = cIndex+1;
		}
	}
	
	if (IsThisMainThread() && CRASH_DUMP_ASSERTS && numCrashDumps < MAX_CRASH_DUMPS)
	{
		AppInput_t dummyInput = {};
		Win32_UpdateAppInputTimeInfo(&dummyInput);
		
		u32 crashDumpSize = 0;
		char* crashDump = Win32_CrashDump(&crashDumpSize, &filename[fileNameStart], lineNumber, function, expressionStr, &dummyInput.systemTime, dummyInput.programTime);
		
		if (crashDump != nullptr)
		{
			char* crashFilename = MallocPrint("crash_%u-%u-%u_%u-%u-%u.txt", dummyInput.localTime.year, dummyInput.localTime.month+1, dummyInput.localTime.day+1, dummyInput.localTime.hour, dummyInput.localTime.minute, dummyInput.localTime.second);
			if (crashFilename == nullptr) { crashFilename = (char*)"crash.txt"; }
			Win32_WriteEntireFile(crashFilename, crashDump, crashDumpSize);
			numCrashDumps++;
			PrintLine_E("Saved crash dump to \"%s\"", crashFilename);
			free(crashDump);
			free(crashFilename);
		}
		else
		{
			WriteLine_E("No crash dump generated");
		}
	}
	else if (IsThisMainThread())
	{
		PrintLine_E("Assertion Failure! %s in \"%s\" line %d: (%s) is not true", function, &filename[fileNameStart], lineNumber, expressionStr);
	}
	else
	{
		PrintLine_E("Assertion Failure on Thread 0x%08X! %s in \"%s\" line %d: (%s) is not true", GetCurrentThreadId(), function, &filename[fileNameStart], lineNumber, expressionStr);
	}
}
#endif
