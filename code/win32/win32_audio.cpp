/*
File:   win32_audio.cpp
Author: Taylor Robbins
Date:   05\13\2020
Description: 
	** Holds functions that help interact with DirectSound to do custom audio output
*/

#if AUDIO_ENABLED && USE_CUSTOM_AUDIO

//NOTE: I can't figure out how to get dsound.h to actually define it's DSDEVID_DefaultPlayback
//      GUID as a static value rather than an extern that would need to be linked at compile time
//      So I decided to try and pull it's definition in myself
#define MY_DEFINE_GUID(name, l, w1, w2, b1, b2, b3, b4, b5, b6, b7, b8) const GUID name = { l, w1, w2, { b1, b2,  b3,  b4,  b5,  b6,  b7,  b8 } }
MY_DEFINE_GUID(MY_DSDEVID_DefaultPlayback, 0xdef00000, 0x9c6d, 0x47ed, 0xaa, 0xf1, 0x4d, 0xda, 0x8f, 0x2b, 0x5c, 0x03);

const char* Win32_GetDirectSoundErrorStr(HRESULT errorCode)
{
	switch (errorCode)
	{
		case DSERR_ALLOCATED:       return "DSERR_ALLOCATED";
		case DSERR_BADFORMAT:       return "DSERR_BADFORMAT";
		case DSERR_BUFFERLOST:      return "DSERR_BUFFERLOST";
		case DSERR_BUFFERTOOSMALL:  return "DSERR_BUFFERTOOSMALL";
		case DSERR_CONTROLUNAVAIL:  return "DSERR_CONTROLUNAVAIL";
		case DSERR_DS8_REQUIRED:    return "DSERR_DS8_REQUIRED";
		case DSERR_INVALIDCALL:     return "DSERR_INVALIDCALL";
		case DSERR_INVALIDPARAM:    return "DSERR_INVALIDPARAM";
		case DSERR_NOAGGREGATION:   return "DSERR_NOAGGREGATION";
		case DSERR_NODRIVER:        return "DSERR_NODRIVER";
		case DSERR_OUTOFMEMORY:     return "DSERR_OUTOFMEMORY";
		case DSERR_PRIOLEVELNEEDED: return "DSERR_PRIOLEVELNEEDED";
		case DSERR_UNINITIALIZED:   return "DSERR_UNINITIALIZED";
		case DSERR_UNSUPPORTED:     return "DSERR_UNSUPPORTED";
		default: return TempPrint("DSERR_UNKNOWN[%ld]", errorCode);
	}
}

void Win32_DestroyAudioDevice(AudioDevice_t* device)
{
	Assert(device != nullptr);
	if (device->name != nullptr)
	{
		Assert(device->allocArena != nullptr);
		ArenaPop(device->allocArena, device->name);
	}
	if (device->subName != nullptr)
	{
		Assert(device->allocArena != nullptr);
		ArenaPop(device->allocArena, device->subName);
	}
	if (device->moduleCode != nullptr)
	{
		Assert(device->allocArena != nullptr);
		ArenaPop(device->allocArena, device->moduleCode);
	}
	ClearPointer(device);
}

void Win32_DestroySampleDisplay(SampleDisplay_t* display)
{
	Assert(display != nullptr);
	if (display->chunkValues != nullptr)
	{
		ArenaPop(&platform.mainHeap, display->chunkValues);
		display->chunkValues = nullptr;
	}
	display->numChunks = 0; 
}

bool Win32_ClearAudioBuffer(Win32_AudioBuffer_t* buffer)
{
	Assert(buffer != nullptr);
	Assert(buffer->pntr != nullptr);
	VOID* regionPntrs[2] = {};
	DWORD regionSizes[2] = {};
	HRESULT lockResult = buffer->pntr->Lock(0, (DWORD)(buffer->numSamples * buffer->sampleSize * buffer->numChannels), &regionPntrs[0], &regionSizes[0], &regionPntrs[1], &regionSizes[1], 0);
	if (lockResult != DS_OK)
	{
		PrintLine_E("WARNING: Failed to lock audio buffer to clear it: %s", Win32_GetDirectSoundErrorStr(lockResult));
		DebugAssert(false);
		return false;
	}
	
	for (u32 rIndex = 0; rIndex < 2; rIndex++)
	{
		Assert(regionPntrs[rIndex] != nullptr || regionSizes[rIndex] == 0);
		for (DWORD bIndex = 0; bIndex < regionSizes[rIndex]; bIndex++)
		{
			((u8*)regionPntrs[rIndex])[bIndex] = 0;
		}
	}
	
	HRESULT unlockResult = buffer->pntr->Unlock(regionPntrs[0], regionSizes[0], regionPntrs[1], regionSizes[1]);
	Assert(unlockResult == DS_OK);
	return true;
}
bool Win32_FillAudioBufferWithSineWave(Win32_AudioBuffer_t* buffer, r32 frequency, r32 volume, SampleDisplay_t* display = nullptr)
{
	Assert(buffer != nullptr);
	Assert(buffer->pntr != nullptr);
	Assert(buffer->sampleRate > 0);
	Assert(buffer->numSamples > 0);
	Assert(buffer->numChannels > 0);
	Assert(buffer->sampleSize == 2); //this function only does 16-bit samples
	
	VOID* regionPntrs[2] = {};
	DWORD regionSizes[2] = {};
	HRESULT lockResult = buffer->pntr->Lock(0, (DWORD)(buffer->numSamples * buffer->sampleSize * buffer->numChannels), &regionPntrs[0], &regionSizes[0], &regionPntrs[1], &regionSizes[1], 0);
	if (lockResult != DS_OK)
	{
		PrintLine_E("WARNING: Failed to lock audio buffer to clear it: %s", Win32_GetDirectSoundErrorStr(lockResult));
		DebugAssert(false);
		return false;
	}
	
	PrintLine_D("Got %u byte and %u byte regions", regionSizes[0], regionSizes[1]);
	
	u32 sampleIndex = 0;
	for (u32 rIndex = 0; rIndex < 2; rIndex++)
	{
		Assert(regionPntrs[rIndex] != nullptr || regionSizes[rIndex] == 0);
		i16* samplePntr = (i16*)regionPntrs[rIndex];
		u32 numSamplesInRegion = (u32)(regionSizes[rIndex]/(buffer->sampleSize * buffer->numChannels));
		for (u32 sIndex = 0; sIndex < numSamplesInRegion; sIndex++)
		{
			Assert(sampleIndex < buffer->numSamples);
			r32 timePerSample = 1.0f / (r32)buffer->sampleRate;
			r32 time = sampleIndex * timePerSample;
			r32 value = SinR32(time*Pi32*2 * frequency);
			i16 sampleValue = ClampR32toI16(Sample16_MaxValue * volume * value);
			for (u32 cIndex = 0; cIndex < buffer->numChannels; cIndex++)
			{
				samplePntr[(sIndex * buffer->numChannels) + cIndex] = sampleValue;
			}
			// if (sampleIndex < 20) { PrintLine_D("[%u]: %d", sampleIndex, (i32)sampleValue); }
			if (display != nullptr && display->chunkValues != nullptr && display->numChunks > 0)
			{
				u32 numSamplesPerChunk = CeilR32i((r32)buffer->numSamples / (r32)display->numChunks);
				u32 chunkIndex = sampleIndex / numSamplesPerChunk;
				Assert(chunkIndex < display->numChunks);
				r32 chunkValue = display->chunkValues[chunkIndex];
				chunkValue = ((chunkValue * (numSamplesPerChunk-1)) + ((r32)sampleValue / (r32)Sample16_MaxValue)) / numSamplesPerChunk;
				display->chunkValues[chunkIndex] = chunkValue;
			}
			sampleIndex++;
		}
	}
	
	HRESULT unlockResult = buffer->pntr->Unlock(regionPntrs[0], regionSizes[0], regionPntrs[1], regionSizes[1]);
	Assert(unlockResult == DS_OK);
	return true;
}

//NOTE: The names seem to come in the format demonstrated by the following examples:
//         Speakers (Yeti Nano)
//         DELL P2715Q (NVIDIA High Definition Audio)
//      This function tries to identify the parenthesised region and separate the string into 2 parts
bool Win32_SplitAudioDeviceName(const char* fullName, StrSplitPiece_t* firstPieceOut, StrSplitPiece_t* secondPieceOut)
{
	Assert(fullName != nullptr);
	Assert(firstPieceOut != nullptr);
	Assert(secondPieceOut != nullptr);
	
	bool foundOpenParen = false;
	bool foundCloseParen = false;
	u32 openParenPos = 0;
	u32 lastSignifCharBeforeParen = 0;
	for (u32 cIndex = 0; fullName[cIndex] != '\0'; cIndex++)
	{
		char nextChar = fullName[cIndex];
		if (!IsCharClassWhitespace(nextChar) && foundCloseParen)
		{
			return false; //can't have important characters coming after the parenthesised region
		}
		else if (nextChar == '(' && !foundOpenParen)
		{
			if (lastSignifCharBeforeParen < 1) { return false; } //No name before open parentheses
			firstPieceOut->pntr = &fullName[0];
			firstPieceOut->length = lastSignifCharBeforeParen;
			openParenPos = cIndex+1;
			foundOpenParen = true;
		}
		else if (nextChar == ')' && !foundCloseParen)
		{
			if (!foundOpenParen) { return false; } //found close parentheses before open
			Assert(cIndex >= openParenPos);
			secondPieceOut->pntr = &fullName[openParenPos];
			secondPieceOut->length = cIndex - openParenPos;
			foundCloseParen = true;
		}
		else if (!IsCharClassWhitespace(nextChar) && !foundOpenParen)
		{
			lastSignifCharBeforeParen = cIndex+1;
		}
	}
	if (!foundOpenParen) { return false; }
	if (!foundCloseParen) { return false; }
	return true;
}

bool Win32_AudioDeviceUniqueIdStrMatches(const AudioDevice_t* device, const char* uniqueIdStr)
{
	Assert(device != nullptr);
	if (uniqueIdStr == nullptr) { return false; }
	if (device->moduleCode != nullptr)
	{
		if (StrCompareIgnoreCaseNt(device->moduleCode, uniqueIdStr)) { return true; }
		//TODO: What if the string doesn't exactly match but contains the same GUID string somewhere?
	}
	return false;
}

BOOL CALLBACK Win32_DirectSoundEnumerateCallback(LPGUID guidPntr, LPCSTR description, LPCSTR module, LPVOID userPntr)
{
	Assert(userPntr != nullptr);
	DynArray_t* deviceArray = (DynArray_t*)userPntr;
	Assert(deviceArray->items != nullptr);
	Assert(deviceArray->itemSize == sizeof(AudioDevice_t));
	if (guidPntr == nullptr)
	{
		//TODO: This is the primary audio device. It get's looped over twice, first time with guidPntr being null.
		//      Should we use this loop iteration to identify which of the devices is the primary one somehow?
		// PrintLine_D("Primary Audio Device found after %u devices: %s %s", deviceArray->length, description, module);
	}
	else
	{
		AudioDevice_t* newDevice = DynArrayAdd(deviceArray, AudioDevice_t);
		Assert(newDevice != nullptr);
		ClearPointer(newDevice);
		newDevice->allocArena = deviceArray->allocArena;
		MyMemCopy(&newDevice->guid, guidPntr, sizeof(GUID));
		if (description != nullptr)
		{
			StrSplitPiece_t firstPiece;
			StrSplitPiece_t secondPiece;
			if (Win32_SplitAudioDeviceName(description, &firstPiece, &secondPiece))
			{
				Assert(firstPiece.pntr != nullptr && firstPiece.length > 0);
				Assert(secondPiece.pntr != nullptr && secondPiece.length > 0);
				newDevice->name = ArenaString(deviceArray->allocArena, secondPiece.pntr, secondPiece.length);
				Assert(newDevice->name != nullptr);
				newDevice->subName = ArenaString(deviceArray->allocArena, firstPiece.pntr, firstPiece.length);
				Assert(newDevice->subName != nullptr);
			}
			else
			{
				newDevice->name = ArenaNtString(deviceArray->allocArena, description);
				Assert(newDevice->name != nullptr);
				newDevice->subName = ArenaNtString(deviceArray->allocArena, "Device");
				Assert(newDevice->subName != nullptr);
			}
		}
		if (module != nullptr)
		{
			newDevice->moduleCode = ArenaNtString(deviceArray->allocArena, module);
			Assert(newDevice->moduleCode != nullptr);
		}
	}
	return TRUE; //this makes us continue enumerating
}

void Win32_ShutdownAudioOutput(AudioInfo_t* info)
{
	Assert(info != nullptr);
	if (platform.audioDevices.items != nullptr)
	{
		for (u32 dIndex = 0; dIndex < platform.audioDevices.length; dIndex++)
		{
			AudioDevice_t* device = DynArrayGet(&platform.audioDevices, AudioDevice_t, dIndex);
			Assert(device != nullptr);
			Win32_DestroyAudioDevice(device);
		}
		DestroyDynamicArray(&platform.audioDevices);
	}
	
	Win32_LockMutex(&platform.audioDeviceChangeMutex, INFINITE);
	if (info->notificationClient != nullptr)
	{
		Assert(info->deviceEnumerator != nullptr);
		info->deviceEnumerator->UnregisterEndpointNotificationCallback(info->notificationClient);
		delete info->notificationClient;
	}
	if (info->deviceEnumerator != nullptr) { delete info->deviceEnumerator; }
	Win32_UnlockMutex(&platform.audioDeviceChangeMutex);
	if (platform.newPrimaryAudioDeviceIdStr != nullptr) { ArenaPop(&platform.stdHeap, platform.newPrimaryAudioDeviceIdStr); platform.newPrimaryAudioDeviceIdStr = nullptr; }
	
	if (info->loaded)
	{
		//TODO: Do we need to close any audio buffers or devices?
		Win32_DestroySampleDisplay(&info->bufferDisplay);
		info->secondaryBuffer.pntr->Stop();
		FreeLibrary(info->module);
		info->module = NULL;
		info->loaded = false;
	}
	
	Plat_PushInputEventSimple(platform.currentInput, InputEventType_AudioShutdown);
}

void Win32_InitDeviceEnumeration(AudioInfo_t* info)
{
	Assert(info != nullptr);
	
	Win32_CreateMutex(&platform.audioDeviceChangeMutex);
	
	HRESULT coCreateResult = CoCreateInstance(
		__uuidof(MMDeviceEnumerator), NULL,
		CLSCTX_ALL, __uuidof(IMMDeviceEnumerator),
		(void**)&info->deviceEnumerator
	);
	if (coCreateResult != S_OK)
	{
		PrintLine_E("Failed to get the device enumerator from CoCreateInstance: %u", coCreateResult);
	}
}

bool Win32_FindAudioDevices(AudioInfo_t* info, MemoryArena_t* memArena, DynArray_t* deviceArray)
{
	Assert(info != nullptr);
	Assert(deviceArray != nullptr);
	if (deviceArray->items != nullptr)
	{
		for (u32 dIndex = 0; dIndex < deviceArray->length; dIndex++)
		{
			AudioDevice_t* device = DynArrayGet(deviceArray, AudioDevice_t, dIndex);
			Assert(device != nullptr);
			Win32_DestroyAudioDevice(device);
		}
		DestroyDynamicArray(deviceArray);
	}
	
	if (info->module == NULL)
	{
		info->module = LoadLibraryA("dsound.dll");
		if (info->module == NULL)
		{
			info->loadError = ArenaNtString(&platform.mainHeap, "Couldn't find dsound.dll. Make sure DirectSound is installed");
			return false;
		}
	}
	
	DirectSoundEnumerate_f* DirectSoundEnumerate = (DirectSoundEnumerate_f*)GetProcAddress(info->module, "DirectSoundEnumerateA");
	if (DirectSoundEnumerate == nullptr)
	{
		info->loadError = ArenaNtString(&platform.mainHeap, "Couldn't find DirectSoundEnumerate in dsound.dll");
		return false;
	}
	
	CreateDynamicArray(deviceArray, memArena, sizeof(AudioDevice_t));
	HRESULT enumerateResult = DirectSoundEnumerate(Win32_DirectSoundEnumerateCallback, deviceArray);
	if (enumerateResult != DS_OK)
	{
		info->loadError = ArenaPrint(&platform.mainHeap, "Failed to enumerate over Audio Devices: %s", Win32_GetDirectSoundErrorStr(enumerateResult));
		return false;
	}
	
	if (platform.newPrimaryAudioDeviceIdStr != nullptr)
	{
		//NOTE: If we got a notification telling us the primary device has changed then we will trust that instead
		//      of whatever OpenAL reports since it doesn't seem to have support for the device changing after startup
		//NOTE: Turns out DirectSound actually reports the proper primary playback device, I was just dumb. We could undo
		//      this system now if we wanted but I'm not sure if we need to or not.
		platform.primaryAudioDeviceIndex = 0;
		Win32_LockMutex(&platform.audioDeviceChangeMutex, INFINITE);
		
		bool foundPrimaryDevice = false;
		for (u32 dIndex = 0; dIndex < deviceArray->length; dIndex++)
		{
			AudioDevice_t* device = DynArrayGet(deviceArray, AudioDevice_t, dIndex);
			Assert(device != nullptr);
			if (Win32_AudioDeviceUniqueIdStrMatches(device, platform.newPrimaryAudioDeviceIdStr))
			{
				// PrintLine_D("Primary Device is set to %u", dIndex);
				platform.primaryAudioDeviceIndex = dIndex;
				foundPrimaryDevice = true;
				break;
			}
		}
		if (!foundPrimaryDevice)
		{
			PrintLine_W("WARNING: Failed to find the primary playback device out of %u devices: %s", deviceArray->length, platform.newPrimaryAudioDeviceIdStr);
			return true; //early out but we'll treat it as success and assume the first device is the primary device
		}
		
		Win32_UnlockMutex(&platform.audioDeviceChangeMutex);
	}
	else
	{
		//NOTE: It seems like the primary audio device always get's enumerated first so this might not be necassary
		//      to look it up using GetDeviceID. But we will try and treat failure of any of these steps as acceptable
		platform.primaryAudioDeviceIndex = 0;
		
		GetDeviceID_f* GetDeviceID = (GetDeviceID_f*)GetProcAddress(info->module, "GetDeviceID");
		if (GetDeviceID == nullptr)
		{
			PrintLine_W("WARNING: Couldn't find GetDeviceID in dsound.dll");
			return true; //early out but we'll treat it as success and assume the first device is the primary device
		}
		
		GUID primaryDeviceGuid = {};
		HRESULT getDeviceResult = GetDeviceID(&MY_DSDEVID_DefaultPlayback, &primaryDeviceGuid);
		if (getDeviceResult != DS_OK)
		{
			PrintLine_W("WARNING: Failed to get the default playback device: %s", Win32_GetDirectSoundErrorStr(getDeviceResult));
			return true; //early out but we'll treat it as success and assume the first device is the primary device
		}
		
		bool foundPrimaryDevice = false;
		for (u32 dIndex = 0; dIndex < deviceArray->length; dIndex++)
		{
			AudioDevice_t* device = DynArrayGet(deviceArray, AudioDevice_t, dIndex);
			Assert(device != nullptr);
			if (MyMemCompare(&device->guid, &primaryDeviceGuid, sizeof(GUID)) == 0)
			{
				platform.primaryAudioDeviceIndex = dIndex;
				foundPrimaryDevice = true;
				break;
			}
		}
		if (!foundPrimaryDevice)
		{
			PrintLine_W("WARNING: Failed to find the primary playback device out of %u devices", deviceArray->length);
			return true; //early out but we'll treat it as success and assume the first device is the primary device
		}
	}
	
	return true;
}

bool Win32_InitAudio(AudioInfo_t* info, AudioDevice_t* chosenDevice)
{
	Assert(info != nullptr);
	
	if (info->loaded)
	{
		Assert(info->module != NULL);
		Assert(info->interfacePntr != nullptr);
		Assert(info->primaryBuffer.pntr != nullptr);
		Assert(info->secondaryBuffer.pntr != nullptr);
		HRESULT stopResult = info->secondaryBuffer.pntr->Stop();
		if (stopResult != DS_OK)
		{
			info->loadError = ArenaNtString(&platform.mainHeap, "Couldn't stop old audio buffer!");
			return false;
		}
		Win32_DestroySampleDisplay(&info->bufferDisplay);
	}
	
	if (info->module == NULL)
	{
		info->module = LoadLibraryA("dsound.dll");
		if (info->module == NULL)
		{
			info->loadError = ArenaNtString(&platform.mainHeap, "Couldn't find dsound.dll. Make sure DirectSound is installed");
			return false;
		}
	}
	if (info->DirectSoundCreate == nullptr)
	{
		Assert(!info->loaded);
		info->DirectSoundCreate = (DirectSoundCreate_f*)GetProcAddress(info->module, "DirectSoundCreate");
		if (info->DirectSoundCreate == nullptr)
		{
			info->loadError = ArenaNtString(&platform.mainHeap, "Couldn't find DirectSoundCreate in dsound.dll");
			FreeLibrary(info->module);
			info->module = NULL;
			return false;
		}
	}
	
	HRESULT createResult = info->DirectSoundCreate((chosenDevice != nullptr) ? &chosenDevice->guid : NULL, &info->interfacePntr, NULL);
	if (createResult != DS_OK)
	{
		info->loadError = ArenaPrint(&platform.mainHeap, "Couldn't create DirectSound Interface: %s", Win32_GetDirectSoundErrorStr(createResult));
		if (!info->loaded) { FreeLibrary(info->module); info->module = NULL; }
		return false;
	}
	
	// DSCAPS deviceCapabilities = {};
	// HRESULT getCapsResult = info->interfacePntr->GetCaps(&deviceCapabilities);
	
	HRESULT setCoopLevelResult = info->interfacePntr->SetCooperativeLevel(platform.windowHandle, DSSCL_PRIORITY);
	if (setCoopLevelResult != DS_OK)
	{
		info->loadError = ArenaPrint(&platform.mainHeap, "SetCooperativeLevel Failed: %s", Win32_GetDirectSoundErrorStr(setCoopLevelResult));
		if (!info->loaded) { FreeLibrary(info->module); info->module = NULL; }
		return false;
	}
	
	DSBUFFERDESC primaryBufferDesc = {};
	primaryBufferDesc.dwSize = sizeof(primaryBufferDesc);
	primaryBufferDesc.dwFlags = DSBCAPS_PRIMARYBUFFER;
	
	HRESULT createPrimaryBufferResult = info->interfacePntr->CreateSoundBuffer(&primaryBufferDesc, &info->primaryBuffer.pntr, 0);
	if (createPrimaryBufferResult != DS_OK)
	{
		info->loadError = ArenaPrint(&platform.mainHeap, "Failed to Create Primary Buffer: %s", Win32_GetDirectSoundErrorStr(createPrimaryBufferResult));
		if (!info->loaded) { FreeLibrary(info->module); info->module = NULL; }
		return false;
	}
	
	Assert((AUDIO_OUTPUT_BIT_DEPTH%8) == 0);
	info->secondaryBuffer.numChannels = AUDIO_OUTPUT_CHANNEL_COUNT;
	info->secondaryBuffer.sampleRate = AUDIO_OUTPUT_SAMPLE_RATE;
	info->secondaryBuffer.sampleSize = AUDIO_OUTPUT_BIT_DEPTH/8;
	info->secondaryBuffer.numSamples = AUDIO_OUTPUT_BUFFER_LENGTH * AUDIO_OUTPUT_SAMPLE_RATE;
	
	WAVEFORMATEX waveFormat = {};
	waveFormat.wFormatTag = WAVE_FORMAT_PCM;
	waveFormat.nChannels = (WORD)info->secondaryBuffer.numChannels;
	waveFormat.nSamplesPerSec = info->secondaryBuffer.sampleRate;
	waveFormat.wBitsPerSample = (WORD)(info->secondaryBuffer.sampleSize*8);
	waveFormat.nBlockAlign = (WORD)(info->secondaryBuffer.numChannels * info->secondaryBuffer.sampleSize);
	waveFormat.nAvgBytesPerSec = info->secondaryBuffer.sampleRate * waveFormat.nBlockAlign;
	waveFormat.cbSize = 0;
	
	HRESULT setFormatResult = info->primaryBuffer.pntr->SetFormat(&waveFormat);
	if (setFormatResult != DS_OK)
	{
		info->loadError = ArenaPrint(&platform.mainHeap, "Failed to Set Buffer Format to %u channel, %u bit, %.1fkHz: %s", info->secondaryBuffer.numChannels, info->secondaryBuffer.sampleSize*8, info->secondaryBuffer.sampleRate/1000.0f, Win32_GetDirectSoundErrorStr(setFormatResult));
		if (!info->loaded) { FreeLibrary(info->module); info->module = NULL; }
		return false;
	}
	
	DSBUFFERDESC secondaryBufferDesc = {};
	secondaryBufferDesc.dwSize = sizeof(secondaryBufferDesc);
	secondaryBufferDesc.dwFlags = DSBCAPS_GETCURRENTPOSITION2;
	//TODO: Should we do DSBCAPS_GLOBALFOCUS when in release mode?
	#if DEBUG
	secondaryBufferDesc.dwFlags |= DSBCAPS_GLOBALFOCUS;
	#endif
	secondaryBufferDesc.dwBufferBytes = info->secondaryBuffer.numSamples * info->secondaryBuffer.sampleSize * info->secondaryBuffer.numChannels;
	secondaryBufferDesc.lpwfxFormat = &waveFormat;
	
	// PrintLine_D("Requesting %u byte buffer: %u samples %u bytes %u channels", secondaryBufferDesc.dwBufferBytes, info->secondaryBuffer.numSamples, info->secondaryBuffer.sampleSize, info->secondaryBuffer.numChannels);
	HRESULT createSecondaryBufferResult = info->interfacePntr->CreateSoundBuffer(&secondaryBufferDesc, &info->secondaryBuffer.pntr, 0);
	if (createSecondaryBufferResult != DS_OK)
	{
		info->loadError = ArenaPrint(&platform.mainHeap, "Failed to Create Secondary Buffer: %s", Win32_GetDirectSoundErrorStr(createSecondaryBufferResult));
		if (!info->loaded) { FreeLibrary(info->module); info->module = NULL; }
		return false;
	}
	
	info->bufferDisplay.numChunks = AUDIO_BUFFER_DISPLAY_CHUNK_COUNT;
	info->bufferDisplay.chunkValues = PushArray(&platform.mainHeap, r32, info->bufferDisplay.numChunks);
	Assert(info->bufferDisplay.chunkValues != nullptr);
	MyMemSet(info->bufferDisplay.chunkValues, 0x00, sizeof(r32) * info->bufferDisplay.numChunks);
	
	Assert(info->secondaryBuffer.pntr != nullptr);
	// bool clearSuccess = Win32_ClearAudioBuffer(&info->secondaryBuffer, &info->bufferDisplay);
	bool clearSuccess = Win32_FillAudioBufferWithSineWave(&info->secondaryBuffer, 261.0f, 0.5f, &info->bufferDisplay);
	if (!clearSuccess)
	{
		info->loadError = ArenaNtString(&platform.mainHeap, "Failed to Clear Audio Buffer");
		if (!info->loaded) { FreeLibrary(info->module); info->module = NULL; }
		return false;
	}
	
	HRESULT playResult = info->secondaryBuffer.pntr->Play(0, 0, DSBPLAY_LOOPING);
	if (playResult != DS_OK)
	{
		info->loadError = ArenaPrint(&platform.mainHeap, "Failed to play Audio Buffer: %s", Win32_GetDirectSoundErrorStr(playResult));
		if (!info->loaded) { FreeLibrary(info->module); info->module = NULL; }
		return false;
	}
	
	if (info->deviceEnumerator != nullptr && info->notificationClient == nullptr)
	{
		info->notificationClient = new EndpointNotificationClient();
		HRESULT registerCallbackResult = info->deviceEnumerator->RegisterEndpointNotificationCallback(info->notificationClient);
		if (registerCallbackResult != S_OK)
		{
			WriteLine_E("WARNING: Failed to register IMMNotificationClient");
			delete info->notificationClient;
			info->notificationClient = nullptr;
		}
	}
	
	info->loaded = true;
	return true;
}

bool Win32_ChangeAudioOutputDevice(AudioInfo_t* info, AudioDevice_t* chosenDevice)
{
	Assert(info != nullptr);
	return Win32_InitAudio(info, chosenDevice);
}

void Win32_HandleAudioDevicesChanged(AudioInfo_t* info)
{
	if (info->loaded && platform.audioDevices.length > 0)
	{
		bool devicesChanged = false;
		
		Assert(info->currentAudioDeviceIndex < platform.audioDevices.length);
		Assert(platform.primaryAudioDeviceIndex < platform.audioDevices.length);
		u32 oldDeviceCount = platform.audioDevices.length;
		GUID oldCurrentDeviceGuid;
		GUID oldPrimaryDeviceGuid;
		{
			AudioDevice_t* currentDevice = DynArrayGet(&platform.audioDevices, AudioDevice_t, info->currentAudioDeviceIndex);
			Assert(currentDevice != nullptr);
			MyMemCopy(&oldCurrentDeviceGuid, &currentDevice->guid, sizeof(GUID));
			AudioDevice_t* primaryDevice = DynArrayGet(&platform.audioDevices, AudioDevice_t, platform.primaryAudioDeviceIndex);
			Assert(primaryDevice != nullptr);
			MyMemCopy(&oldPrimaryDeviceGuid, &primaryDevice->guid, sizeof(GUID));
		}
		
		bool findSuccess = Win32_FindAudioDevices(info, &platform.mainHeap, &platform.audioDevices);
		if (!findSuccess)
		{
			PrintLine_E("Failed to enumerate audio devices after devices changed: %s", info->loadError);
			WriteLine_E("Turning off audio for  the game");
			info->currentAudioDeviceIndex = 0;
			Win32_ShutdownAudioOutput(info);
			return;
		}
		
		if (oldDeviceCount != platform.audioDevices.length)
		{
			#if 1
			PrintLine_I("Audio Device Count Changed: %u -> %u", oldDeviceCount, platform.audioDevices.length);
			for (u32 dIndex = 0; dIndex < platform.audioDevices.length; dIndex++)
			{
				AudioDevice_t* device = DynArrayGet(&platform.audioDevices, AudioDevice_t, dIndex);
				Assert(device != nullptr);
				PrintLine_D("  [%u]: %s [%s] %s%s", dIndex, device->name, device->subName, device->moduleCode, (dIndex == platform.primaryAudioDeviceIndex) ? " (Primary Audio Device)" : "");
			}
			#endif
			devicesChanged = true;
		}
		
		bool foundCurrentDevice = false;
		bool foundPrimaryDevice = false;
		bool primaryDeviceChanged = false;
		for (u32 dIndex = 0; dIndex < platform.audioDevices.length; dIndex++)
		{
			AudioDevice_t* device = DynArrayGet(&platform.audioDevices, AudioDevice_t, dIndex);
			Assert(device != nullptr);
			if (!foundCurrentDevice && MyMemCompare(&device->guid, &oldCurrentDeviceGuid, sizeof(GUID)) == 0)
			{
				if (info->currentAudioDeviceIndex != dIndex) { PrintLine_D("Current audio device index changed: %u -> %u", info->currentAudioDeviceIndex, dIndex); }
				info->currentAudioDeviceIndex = dIndex;
				foundCurrentDevice = true;
			}
			if (!foundPrimaryDevice && MyMemCompare(&device->guid, &oldPrimaryDeviceGuid, sizeof(GUID)) == 0)
			{
				foundPrimaryDevice = true;
				if (dIndex != platform.primaryAudioDeviceIndex)
				{
					primaryDeviceChanged = true;
				}
			}
		}
		if (!foundCurrentDevice)
		{
			if (platform.audioDevices.length > 0)
			{
				Assert(platform.primaryAudioDeviceIndex < platform.audioDevices.length);
				AudioDevice_t* primaryDevice = DynArrayGet(&platform.audioDevices, AudioDevice_t, platform.primaryAudioDeviceIndex);
				Assert(primaryDevice != nullptr);
				WriteLine_E("The audio device we were using has been disconnected! Changing to the default audio device.");
				info->currentAudioDeviceIndex = platform.primaryAudioDeviceIndex;
				bool changeSuccess = Win32_ChangeAudioOutputDevice(info, primaryDevice);
				if (changeSuccess)
				{
					Plat_PushInputEventSimple(platform.currentInput, InputEventType_AudioReset);
				}
				else
				{
					WriteLine_E("Failed to change to primary audio device!");
					PrintLine_E("Error: %s", info->loadError);
				}
			}
			else
			{
				WriteLine_E("All audio devices have been disconnected.");
				WriteLine_E("Turning off audio for the game!");
				Win32_ShutdownAudioOutput(info);
				return;
			}
		}
		if (!foundPrimaryDevice) { WriteLine_W("Couldn't find old primary audio device!"); }
		
		if (!foundPrimaryDevice || primaryDeviceChanged)
		{
			Plat_PushInputEventSimple(platform.currentInput, InputEventType_PrimaryAudioDeviceChanged);
		}
		
		#if 1
		// +======================================+
		// | Auto-Change Device to Primary Device |
		// +======================================+
		if (primaryDeviceChanged && info->currentAudioDeviceIndex != platform.primaryAudioDeviceIndex)
		{
			WriteLine_I("Auto-changing to primary audio device");
			AudioDevice_t* primaryDevice = DynArrayGet(&platform.audioDevices, AudioDevice_t, platform.primaryAudioDeviceIndex);
			Assert(primaryDevice != nullptr);
			info->currentAudioDeviceIndex = platform.primaryAudioDeviceIndex;
			bool changeSuccessful = Win32_ChangeAudioOutputDevice(info, primaryDevice);
			if (changeSuccessful)
			{
				Plat_PushInputEventSimple(platform.currentInput, InputEventType_AudioReset);
			}
			else
			{
				WriteLine_E("Failed to change to primary audio device!");
				PrintLine_E("Error: %s", info->loadError);
			}
		}
		#endif
		
		if (devicesChanged)
		{
			Plat_PushInputEventSimple(platform.currentInput, InputEventType_AudioDevicesChanged);
		}
	}
}

void Win32_UpdateAudio(AudioInfo_t* info)
{
	Assert(info != nullptr);
	if (info->loaded)
	{
		Assert(info->secondaryBuffer.pntr != nullptr);
		
		if (platform.audioDeviceChanged)
		{
			Win32_LockMutex(&platform.audioDeviceChangeMutex, INFINITE);
			platform.audioDeviceChanged = false;
			Win32_UnlockMutex(&platform.audioDeviceChangeMutex);
			Win32_HandleAudioDevicesChanged(info);
			if (!info->loaded) { return; }
		}
		
		Win32_AudioBuffer_t* buffer = &info->secondaryBuffer;
		DWORD playCursor = 0;
		DWORD writeCursor = 0;
		HRESULT getCursorResult = buffer->pntr->GetCurrentPosition(&playCursor, &writeCursor);
		if (getCursorResult == DS_OK)
		{
			info->bufferLength = buffer->numSamples;
			u32 pageSize = (buffer->sampleSize * buffer->numChannels);
			Assert((playCursor % pageSize) == 0);
			Assert((writeCursor % pageSize) == 0);
			info->playCursorPos = (u32)(playCursor / pageSize);
			info->writeCursorPos = (u32)(writeCursor / pageSize);
			Assert(info->playCursorPos < buffer->numSamples);
			Assert(info->writeCursorPos < buffer->numSamples);
			
			Assert(info->lastWriteBufferIndex < buffer->numSamples);
			u64 writeSampleIndex = info->lastWriteSampleIndex;
			if (info->writeCursorPos >= info->lastWriteBufferIndex)
			{
				writeSampleIndex += info->writeCursorPos - info->lastWriteBufferIndex;
			}
			else //looped around
			{
				writeSampleIndex += (buffer->numSamples - info->lastWriteBufferIndex) + info->writeCursorPos;
			}
			platform.audioOutput.sampleIndex = writeSampleIndex;
			
			info->lastWriteBufferIndex = info->writeCursorPos;
			info->lastWriteSampleIndex = platform.audioOutput.sampleIndex;
			
			u32 numSamplesNeeded = AUDIO_WRITE_AHEAD_LENGTH; //TODO: Calculate me better
			if (platform.audioOutput.samples == nullptr || platform.audioOutput.numSamples != numSamplesNeeded)
			{
				if (platform.audioOutput.samples != nullptr)
				{
					ArenaPop(&platform.mainHeap, platform.audioOutput.samples);
					platform.audioOutput.samples = nullptr;
				}
				platform.audioOutput.numSamples = numSamplesNeeded;
				platform.audioOutput.samples = PushArray(&platform.mainHeap, i16, platform.audioOutput.numSamples * buffer->numChannels);
				Assert(platform.audioOutput.samples != nullptr);
				MyMemSet(platform.audioOutput.samples, 0x00, sizeof(i16) * buffer->numChannels * platform.audioOutput.numSamples);
			}
			platform.audioOutput.sampleRate  = buffer->sampleRate;
			platform.audioOutput.numChannels = buffer->numChannels;
			platform.audioOutput.sampleSize  = buffer->sampleSize;
			
			app.AppGetAudioOutput(&platform.info, &platform.appMemory, &platform.audioOutput);
			
			getCursorResult = buffer->pntr->GetCurrentPosition(&playCursor, &writeCursor);
			Assert(getCursorResult == DS_OK);
			Assert((writeCursor % pageSize) == 0);
			u32 newWritePos = (u32)(writeCursor / pageSize);
			u32 numSamplesConsumedWhileCalculating = 0;
			if (newWritePos >= info->writeCursorPos)
			{
				numSamplesConsumedWhileCalculating = newWritePos - info->writeCursorPos;
			}
			else
			{
				numSamplesConsumedWhileCalculating = (buffer->numSamples - info->writeCursorPos) + newWritePos;
			}
			
			if (numSamplesConsumedWhileCalculating < platform.audioOutput.numSamples)
			{
				u32 numBytesToLock = (platform.audioOutput.numSamples - numSamplesConsumedWhileCalculating) * buffer->sampleSize * buffer->numChannels;
				VOID* regionPntrs[2];
				DWORD regionSizes[2];
				u32 lockOffset = writeCursor;
				HRESULT lockResult = buffer->pntr->Lock((DWORD)lockOffset, (DWORD)numBytesToLock, &regionPntrs[0], &regionSizes[0], &regionPntrs[1], &regionSizes[1], 0);
				if (lockResult == DS_OK)
				{
					u32 numSamplesPerChunk = 0;
					if (info->bufferDisplay.numChunks > 0 && info->bufferDisplay.chunkValues != nullptr)
					{
						numSamplesPerChunk = CeilR32i((r32)buffer->numSamples / (r32)info->bufferDisplay.numChunks);
					}
					u32 sampleIndex = numSamplesConsumedWhileCalculating;
					i16* samplePntr = &platform.audioOutput.samples[numSamplesConsumedWhileCalculating * platform.audioOutput.numChannels];
					for (u32 rIndex = 0; rIndex < 2; rIndex++)
					{
						Assert(regionPntrs[rIndex] != nullptr || regionSizes[rIndex] == 0);
						Assert((regionSizes[rIndex]%pageSize) == 0);
						i16* regionPntr16 = (i16*)regionPntrs[rIndex];
						u32 numSamplesInRegion = (regionSizes[rIndex] / pageSize);
						for (u32 sIndex = 0; sIndex < numSamplesInRegion; sIndex++)
						{
							Assert(sampleIndex < platform.audioOutput.numSamples);
							r32 sampleAverage = 0;
							for (u32 cIndex = 0; cIndex < platform.audioOutput.numChannels; cIndex++)
							{
								regionPntr16[(sIndex * platform.audioOutput.numChannels) + cIndex] = *samplePntr;
								sampleAverage += (((r32)*samplePntr) / Sample16_MaxValue);
								samplePntr++;
							}
							sampleAverage /= (r32)platform.audioOutput.numChannels;
							sampleIndex++;
							if (info->bufferDisplay.numChunks > 0 && info->bufferDisplay.chunkValues != nullptr)
							{
								u32 chunkIndex = ((newWritePos + sampleIndex) % buffer->numSamples) / numSamplesPerChunk;
								Assert(chunkIndex < info->bufferDisplay.numChunks);
								r32 chunkValue = info->bufferDisplay.chunkValues[chunkIndex];
								chunkValue = ((chunkValue * (numSamplesPerChunk-1)) + sampleAverage) / numSamplesPerChunk;
								info->bufferDisplay.chunkValues[chunkIndex] = chunkValue;
							}
						}
					}
					HRESULT unlockResult = buffer->pntr->Unlock(regionPntrs[0], regionSizes[0], regionPntrs[1], regionSizes[1]);
					Assert(unlockResult == DS_OK);
				}
				else
				{
					PrintLine_W("WARNING: Failed to lock audio output buffer: %s", Win32_GetDirectSoundErrorStr(lockResult));
				}
			}
			else
			{
				PrintLine_W("WARNING: We blew past our calculated samples: %u samples consumed while calculating, only %u samples calculated", numSamplesConsumedWhileCalculating, platform.audioOutput.numSamples);
			}
		}
		else
		{
			PrintLine_E("WARNING: Failed to get Audio Buffer cursor position: %s", Win32_GetDirectSoundErrorStr(getCursorResult));
		}
	}
}

HRESULT EndpointNotificationClient::OnDefaultDeviceChanged(EDataFlow flow, ERole role, LPCWSTR pwstrDefaultDeviceId)
{
	#if 1
	if (flow == eRender)
	{
		Win32_LockMutex(&platform.audioDeviceChangeMutex, INFINITE);
		
		if (platform.newPrimaryAudioDeviceIdStr != nullptr)
		{
			ArenaPop(&platform.stdHeap, platform.newPrimaryAudioDeviceIdStr);
		}
		platform.newPrimaryAudioDeviceIdStr = ConvertWideStrInArena(&platform.stdHeap, pwstrDefaultDeviceId);
		Assert(platform.newPrimaryAudioDeviceIdStr != nullptr);
		platform.audioDeviceChanged = true;
		
		char* testPrint = ArenaPrint(&platform.stdHeap, "Default Audio Device Changed! \"%s\"", platform.newPrimaryAudioDeviceIdStr);
		WriteLine_I(testPrint);
		ArenaPop(&platform.stdHeap, testPrint);
		
		Win32_UnlockMutex(&platform.audioDeviceChangeMutex);
	}
	else
	{
		char* testPrint = ArenaPrint(&platform.stdHeap, "Other flow Default Audio Device Changed: %u", flow);
		WriteLine_D(testPrint);
		ArenaPop(&platform.stdHeap, testPrint);
	}
	#endif
	return S_OK;
}
HRESULT EndpointNotificationClient::OnDeviceAdded(LPCWSTR pwstrDeviceId)
{
	#if 1
	Win32_LockMutex(&platform.audioDeviceChangeMutex, INFINITE);
	platform.audioDeviceChanged = true;
	Win32_UnlockMutex(&platform.audioDeviceChangeMutex);
	WriteLine_I("New Audio Device Added");
	#endif
	return S_OK;
}
HRESULT EndpointNotificationClient::OnDeviceRemoved(LPCWSTR pwstrDeviceId)
{
	#if 1
	Win32_LockMutex(&platform.audioDeviceChangeMutex, INFINITE);
	platform.audioDeviceChanged = true;
	Win32_UnlockMutex(&platform.audioDeviceChangeMutex);
	WriteLine_W("Audio Device Removed");
	#endif
	return S_OK;
}
HRESULT EndpointNotificationClient::OnDeviceStateChanged(LPCWSTR pwstrDeviceId, DWORD dwNewState)
{
	#if 0
	//TODO: Do we need to handle these events?
	WriteLine_W("Audio Device State Changed");
	#endif
	return S_OK;
}
HRESULT EndpointNotificationClient::OnPropertyValueChanged(LPCWSTR pwstrDeviceId, const PROPERTYKEY key)
{
	#if 0
	WriteLine_W("Audio Device Property Value Changed");
	#endif
	return S_OK;
}

#endif //AUDIO_ENABLED && USE_CUSTOM_AUDIO
