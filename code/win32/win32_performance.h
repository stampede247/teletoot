/*
File:   win32_performance.h
Author: Taylor Robbins
Date:   01\26\2020
*/

#ifndef _WIN_32_PERFORMANCE_H
#define _WIN_32_PERFORMANCE_H

struct PerfTime_t
{
	// r64 programTime; //sourced from GLFW currently
	LARGE_INTEGER perfCount;
	u64 cycleCount;
};

#endif //  _WIN_32_PERFORMANCE_H
