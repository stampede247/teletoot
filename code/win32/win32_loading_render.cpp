/*
File:   win32_loading_render.cpp
Author: Taylor Robbins
Date:   09\24\2019
Description: 
	** This file handles setting up and rendering to the GLFW window during the time that the application is loading it's data in AppInitialize
*/

void Win32_DestroyTexture(Win32_Texture_t* texturePntr)
{
	Assert(texturePntr != nullptr);
	glDeleteTextures(1, &texturePntr->id);
	ClearPointer(texturePntr);
}
Win32_Texture_t Win32_CreateTexture(const u8* bitmapData, i32 width, i32 height, bool pixelated, bool repeat)
{
	Win32_Texture_t result = {};
	
	result.width = width;
	result.height = height;
	
	glGenTextures(1, &result.id);
	glBindTexture(GL_TEXTURE_2D, result.id);
	
	glTexImage2D(
		GL_TEXTURE_2D, 		//bound texture type
		0,					//image level
		GL_RGBA,			//internal format
		width,		        //image width
		height,		        //image height
		0,					//border
		GL_RGBA,			//format
		GL_UNSIGNED_BYTE,	//type
		bitmapData  		//data
	);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, pixelated ? GL_NEAREST_MIPMAP_NEAREST : GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, pixelated ? GL_NEAREST : GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, repeat ? GL_REPEAT : GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, repeat ? GL_REPEAT : GL_CLAMP_TO_EDGE);
	glGenerateMipmap(GL_TEXTURE_2D);
	
	return result;
}
Win32_Texture_t Win32_LoadTexture(const char* filePath, bool pixelated, bool repeat)
{
	Win32_Texture_t result = {};
	result.isValid = false;
	
	FileInfo_t textureFile = Win32_ReadEntireFile(filePath);
	if (textureFile.content == nullptr)
	{
		PrintLine_E("Failed to open texture file at \"%s\"", filePath);
		return result;
	}
	
	i32 numChannels;
	i32 width, height;
	u8* imageData = stbi_load_from_memory(
		(u8*)textureFile.content, textureFile.size,
		&width, &height, &numChannels, 4);
	
	if (imageData == nullptr)
	{
		PrintLine_E("Failed to parse image file at \"%s\"", filePath);
		Win32_FreeFileMemory(&textureFile);
		return result;
	}
	Assert(width > 0 && height > 0);
	
	result = Win32_CreateTexture(imageData, width, height, pixelated, repeat);
	
	stbi_image_free(imageData);
	Win32_FreeFileMemory(&textureFile);
	
	result.isValid = true;
	return result;
}


void Win32_DestroyVertexBuffer(Win32_VertexBuffer_t* bufferPntr)
{
	Assert(bufferPntr != nullptr);
	glDeleteBuffers(1, &bufferPntr->id);
	ClearPointer(bufferPntr);
}
Win32_VertexBuffer_t Win32_CreateVertexBuffer(const Win32_Vertex_t* vertices, u32 numVertices)
{
	Win32_VertexBuffer_t result = {};
	result.numVertices = numVertices;
	
	glGenBuffers(1, &result.id);
	glBindBuffer(GL_ARRAY_BUFFER, result.id);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Win32_Vertex_t) * numVertices, vertices, GL_STATIC_DRAW);
	
	return result;
}

u32 Win32_FindFragmentShaderSplit(const char* fileContents, u32 fileLength)
{
	Assert(fileContents != nullptr);
	if (fileLength == 0) { return 0; }
	u32 result = 0;
	
	//Find the titleStr
	const char* titleStr = "FRAGMENT_SHADER";
	u32 titleStrLength = MyStrLength32(titleStr);
	bool foundTitleStr = false;
	u32 titleStrIndex = 0;
	for (u32 cIndex = 0; cIndex+titleStrLength < fileLength; cIndex++)
	{
		if (MyStrCompare(&fileContents[cIndex], titleStr, titleStrLength) == 0)
		{
			foundTitleStr = true;
			titleStrIndex = cIndex;
			break;
		}
	}
	if (!foundTitleStr) { return 0; }
	
	//Go back to previous line break
	bool foundLineBreak = false;
	for (u32 cIndex = titleStrIndex; cIndex > 0; cIndex--)
	{
		if (fileContents[cIndex] == '\n' || fileContents[cIndex] == '\r')
		{
			titleStrIndex = cIndex+1;
			foundLineBreak = true;
			break;
		}
	}
	if (!foundLineBreak) { return 0; }
	
	return titleStrIndex;
}

void Win32_DestroyShader(Win32_Shader_t* shaderPntr)
{
	Assert(shaderPntr != nullptr);
	glDeleteVertexArrays(1, &shaderPntr->vertexArray);
	glDeleteProgram(shaderPntr->programId);
	glDeleteShader(shaderPntr->vertId);
	glDeleteShader(shaderPntr->fragId);
	ClearPointer(shaderPntr);
}
Win32_Shader_t Win32_LoadShader(const char* filePath)
{
	Win32_Shader_t result = {};
	result.isValid = false;
	GLint compiled;
	int logLength;
	char* logBuffer;
	
	FileInfo_t shaderFile = Win32_ReadEntireFile(filePath);
	if (shaderFile.content == nullptr)
	{
		PrintLine_E("Couldn't open shader file \"%s\"", filePath);
		return result;
	}
	
	u32 splitIndex = Win32_FindFragmentShaderSplit((const char*)shaderFile.content, shaderFile.size);
	if (splitIndex == 0 || splitIndex >= shaderFile.size)
	{
		PrintLine_E("Couldn't find the split between vertex and fragment shader in \"%s\"", filePath);
		return result;
	}
	
	TempPushMark();
	u32 vertShaderLength = splitIndex;
	char* vertShader = PushArray(TempArena, char, vertShaderLength+1);
	MyMemCopy(vertShader, (const char*)shaderFile.content, vertShaderLength);
	vertShader[vertShaderLength] = '\0';
	
	u32 fragShaderLength = shaderFile.size - splitIndex;
	char* fragShader = PushArray(TempArena, char, fragShaderLength+1);
	MyMemCopy(fragShader, (const char*)shaderFile.content + splitIndex, fragShaderLength);
	fragShader[fragShaderLength] = '\0';
	
	// PrintLine_D("Vertex Shader: \"%s\"", vertShader);
	// PrintLine_D("Fragment Shader: \"%s\"", fragShader);
	
	result.vertId = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(result.vertId, 1, (const GLchar* const*)&vertShader, NULL);
	glCompileShader(result.vertId);
	
	glGetShaderiv(result.vertId, GL_COMPILE_STATUS, &compiled);
	glGetShaderiv(result.vertId, GL_INFO_LOG_LENGTH, &logLength);
	if (compiled) { PrintLine_D("%s: Compiled Successfully: %d byte log", GetFileNamePart(filePath), logLength); }
	else { PrintLine_E("%s: Compilation Failed: %d byte log", GetFileNamePart(filePath), logLength); }
	if (logLength > 0)
	{
		logBuffer = TempString(logLength+1);
		logBuffer[logLength] = '\0';
		glGetShaderInfoLog(result.vertId, logLength, NULL, logBuffer);
		PrintLine_W("Log: \"%s\"", logBuffer);
	}
	
	result.fragId = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(result.fragId, 1, (const GLchar* const*)&fragShader, NULL);
	glCompileShader(result.fragId);
	
	glGetShaderiv(result.fragId, GL_COMPILE_STATUS, &compiled);
	glGetShaderiv(result.fragId, GL_INFO_LOG_LENGTH, &logLength);
	if (compiled) { PrintLine_D("%s: Compiled Successfully: %d byte log", GetFileNamePart(filePath), logLength); }
	else { PrintLine_E("%s: Compilation Failed: %d byte log", GetFileNamePart(filePath), logLength); }
	if (logLength > 0)
	{
		logBuffer = TempString(logLength+1);
		logBuffer[logLength] = '\0';
		glGetShaderInfoLog(result.fragId, logLength, NULL, logBuffer);
		PrintLine_W("Log: \"%s\"", logBuffer);
	}
	
	Win32_FreeFileMemory(&shaderFile);
	TempPopMark();
	
	result.programId = glCreateProgram();
	glAttachShader(result.programId, result.fragId);
	glAttachShader(result.programId, result.vertId);
	glLinkProgram(result.programId);
	
	glGetProgramiv(result.programId, GL_LINK_STATUS, &compiled);
	glGetProgramiv(result.programId, GL_INFO_LOG_LENGTH, &logLength);
	if (compiled) { PrintLine_D("Shader: Linked Successfully: %d byte log", logLength); }
	else { PrintLine_E("Shader: Linking Failed: %d byte log", logLength); }
	if (logLength > 0)
	{
		logBuffer = TempString(logLength+1);
		logBuffer[logLength] = '\0';
		glGetProgramInfoLog(result.programId, logLength, NULL, logBuffer);
		PrintLine_W("Log: \"%s\"", logBuffer);
	}
	
	result.locations.positionAttrib = glGetAttribLocation(result.programId, "inPosition");
	result.locations.colorAttrib    = glGetAttribLocation(result.programId, "inColor");
	result.locations.texCoordAttrib = glGetAttribLocation(result.programId, "inTexCoord");
	
	result.locations.worldMatrix      = glGetUniformLocation(result.programId, "WorldMatrix");
	result.locations.viewMatrix       = glGetUniformLocation(result.programId, "ViewMatrix");
	result.locations.projectionMatrix = glGetUniformLocation(result.programId, "ProjectionMatrix");
	result.locations.texture          = glGetUniformLocation(result.programId, "Texture");
	result.locations.textureSize      = glGetUniformLocation(result.programId, "TextureSize");
	result.locations.sourceRectangle  = glGetUniformLocation(result.programId, "SourceRectangle");
	result.locations.primaryColor     = glGetUniformLocation(result.programId, "PrimaryColor");
	
	// PrintLine_D("positionAttrib: %d", result.locations.positionAttrib);
	// PrintLine_D("colorAttrib:    %d", result.locations.colorAttrib);
	// PrintLine_D("texCoordAttrib: %d", result.locations.texCoordAttrib);
	// PrintLine_D("worldMatrix:      %d", result.locations.worldMatrix);
	// PrintLine_D("viewMatrix:       %d", result.locations.viewMatrix);
	// PrintLine_D("projectionMatrix: %d", result.locations.projectionMatrix);
	// PrintLine_D("texture:          %d", result.locations.texture);
	// PrintLine_D("textureSize:      %d", result.locations.textureSize);
	// PrintLine_D("sourceRectangle:  %d", result.locations.sourceRectangle);
	// PrintLine_D("primaryColor:     %d", result.locations.primaryColor);
	
	glGenVertexArrays(1, &result.vertexArray);
	glBindVertexArray(result.vertexArray);
	glEnableVertexAttribArray(result.locations.positionAttrib);
	glEnableVertexAttribArray(result.locations.colorAttrib);
	glEnableVertexAttribArray(result.locations.texCoordAttrib);
	
	result.isValid = true;
	return result;
}

void Win32_BindTexture(const Win32_Texture_t* texturePntr)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texturePntr->id);
	glUniform1i(platform.mainShader.locations.texture, 0);
	glUniform2f(platform.mainShader.locations.textureSize, (r32)texturePntr->width, (r32)texturePntr->height);
}

void Win32_DrawRectangle(rec rectangle)
{
	mat4 worldMatrix = Mat4_Identity;
	Mat4Transform(worldMatrix, Mat4Scale2(rectangle.width, rectangle.height));
	Mat4Transform(worldMatrix, Mat4Translate2(rectangle.x, rectangle.y));
	glUniformMatrix4fv(platform.mainShader.locations.worldMatrix, 1, GL_FALSE, &worldMatrix.values[0][0]);
	
	glBindVertexArray(platform.mainShader.vertexArray);
	glBindBuffer(GL_ARRAY_BUFFER, platform.squareBuffer.id);
	glVertexAttribPointer(platform.mainShader.locations.positionAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(Win32_Vertex_t), (void*)0);
	glVertexAttribPointer(platform.mainShader.locations.colorAttrib,    4, GL_FLOAT, GL_FALSE, sizeof(Win32_Vertex_t), (void*)sizeof(v3));
	glVertexAttribPointer(platform.mainShader.locations.texCoordAttrib, 2, GL_FLOAT, GL_FALSE, sizeof(Win32_Vertex_t), (void*)(sizeof(v3)+sizeof(v4)));
	glDrawArrays(GL_TRIANGLES, 0, platform.squareBuffer.numVertices);
}

void Win32_PrepareForRendering()
{
	TempPushMark();
	char* absoluteFilePath = Win32_CompleteRelativePath(TempArena, platform.startupOptions.loadingImagePath, MyStrLength32(platform.startupOptions.loadingImagePath));
	platform.loadingImage = Win32_LoadTexture(absoluteFilePath, true, false);
	if (!platform.loadingImage.isValid)
	{
		Win32_ExitOnError(TempPrint("Failed to load the loading image from \"%s\"", absoluteFilePath));
	}
	
	Color_t dotTextureData = White;
	platform.dotTexture = Win32_CreateTexture((u8*)&dotTextureData, 1, 1, true, true);
	platform.dotTexture.isValid = true;
	
	Win32_Vertex_t squareVertices[] =
	{
		{  {0.0f, 0.0f, 0.0f}, NewVec4(1, 1, 1, 1), {0.0f, 0.0f} },
		{  {1.0f, 0.0f, 0.0f}, NewVec4(1, 1, 1, 1), {1.0f, 0.0f} },
		{  {0.0f, 1.0f, 0.0f}, NewVec4(1, 1, 1, 1), {0.0f, 1.0f} },
		
		{  {0.0f, 1.0f, 0.0f}, NewVec4(1, 1, 1, 1), {0.0f, 1.0f} },
		{  {1.0f, 0.0f, 0.0f}, NewVec4(1, 1, 1, 1), {1.0f, 0.0f} },
		{  {1.0f, 1.0f, 0.0f}, NewVec4(1, 1, 1, 1), {1.0f, 1.0f} },
	};
	platform.squareBuffer = Win32_CreateVertexBuffer(squareVertices, ArrayCount(squareVertices));
	
	const char* shaderFilePath = "Resources/Shaders/loading.glsl";
	absoluteFilePath = Win32_CompleteRelativePath(TempArena, shaderFilePath, MyStrLength32(shaderFilePath));
	platform.mainShader = Win32_LoadShader(absoluteFilePath);
	if (!platform.mainShader.isValid)
	{
		Win32_ExitOnError("Failed to load and compile shader in \"Resources/Shaders/loading.glsl");
	}
	TempPopMark();
}

void Win32_RenderLoadingScreen(r32 loadingPercentage)
{
	bool flipYAxis = true;
	glfwGetFramebufferSize(platform.window, &platform.screenSize.width, &platform.screenSize.height);
	mat4 worldMatrix = Mat4_Identity;
	mat4 viewMatrix = Mat4_Identity;
	mat4 projMatrix = Mat4_Identity;
	Mat4Transform(projMatrix, Mat4Translate2(-platform.screenSize.width/2.0f, -platform.screenSize.height/2.0f));
	Mat4Transform(projMatrix, Mat4Scale2(2.0f/platform.screenSize.width, (flipYAxis ? -1 : 1) * 2.0f/platform.screenSize.height));
	reci viewportRec = NewReci(0, 0, platform.screenSize.width, platform.screenSize.height);
	
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
	
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GEQUAL, 0.1f);
	
	glUseProgram(platform.mainShader.programId);
	glViewport(viewportRec.x, viewportRec.y, viewportRec.width, viewportRec.height);
	glUniformMatrix4fv(platform.mainShader.locations.worldMatrix,      1, GL_FALSE, &worldMatrix.values[0][0]);
	glUniformMatrix4fv(platform.mainShader.locations.viewMatrix,       1, GL_FALSE, &viewMatrix.values[0][0]);
	glUniformMatrix4fv(platform.mainShader.locations.projectionMatrix, 1, GL_FALSE, &projMatrix.values[0][0]);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	
	v4 backColorVec = NewVec4FromColor(platform.startupOptions.loadingColor);
	glClearColor(backColorVec.r, backColorVec.g, backColorVec.b, backColorVec.a);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	rec loadingImageRec = NewRec(0, 0, (r32)platform.loadingImage.width, (r32)platform.loadingImage.height);
	r32 scale = 1.0f;
	while (loadingImageRec.width*(scale+1) < platform.screenSize.width && loadingImageRec.height*(scale+1) < platform.screenSize.height) { scale += 1; }
	loadingImageRec.size = loadingImageRec.size * scale;
	loadingImageRec.topLeft = Vec2Round(NewVec2(platform.screenSize)/2 - loadingImageRec.size/2);
	Win32_BindTexture(&platform.loadingImage);
	glUniform4f(platform.mainShader.locations.sourceRectangle, 0, 0, (r32)platform.loadingImage.width, (r32)platform.loadingImage.height);
	glUniform4f(platform.mainShader.locations.primaryColor, 1.0f, 1.0f, 1.0f, 1.0f);
	Win32_DrawRectangle(loadingImageRec);
	
	rec loadingBarRec = NewRec((r32)platform.screenSize.width/8, (r32)platform.screenSize.height-30 - 50, (r32)platform.screenSize.width*3/4, 50);
	r32 borderWidth = 2;
	rec loadingBarTop    = NewRec(loadingBarRec.x, loadingBarRec.y, loadingBarRec.width, borderWidth);
	rec loadingBarLeft   = NewRec(loadingBarRec.x, loadingBarRec.y, borderWidth, loadingBarRec.height);
	rec loadingBarBottom = NewRec(loadingBarRec.x, loadingBarRec.y + loadingBarRec.height-borderWidth, loadingBarRec.width, borderWidth);
	rec loadingBarRight  = NewRec(loadingBarRec.x + loadingBarRec.width-borderWidth, loadingBarRec.y, borderWidth, loadingBarRec.height);
	rec barRec = RecDeflate(loadingBarRec, borderWidth);
	barRec.width *= loadingPercentage;
	Win32_BindTexture(&platform.dotTexture);
	glUniform4f(platform.mainShader.locations.sourceRectangle, 0, 0, 1, 1);
	v4 colorVec = Vec4_One;
	glUniform4f(platform.mainShader.locations.primaryColor, colorVec.r, colorVec.g, colorVec.b, colorVec.a);
	Win32_DrawRectangle(loadingBarTop);
	Win32_DrawRectangle(loadingBarLeft);
	Win32_DrawRectangle(loadingBarBottom);
	Win32_DrawRectangle(loadingBarRight);
	colorVec = Vec4_One;
	glUniform4f(platform.mainShader.locations.primaryColor, colorVec.r, colorVec.g, colorVec.b, colorVec.a);
	Win32_DrawRectangle(barRec);
}

// +==============================+
// |    Win32_UpdateLoadingBar    |
// +==============================+
// void UpdateLoadingBar(r32 percentDone)
UpdateLoadingBar_DEFINITION(Win32_UpdateLoadingBar)
{
	Assert(!platform.appLoadingFinished);
	platform.appLoadingPercent = percentDone;
	
	//NOTE: This is only here because we couldn't get the multi-threaded loading working like we want
	// PrintLine_I("Loading %.0f%%", percentDone*100);
	glfwPollEvents();
	Win32_RenderLoadingScreen(0.15f + 0.85f*percentDone);
	glfwSwapBuffers(platform.window);
}

// +===============================+
// | Win32_WasWindowCloseRequested |
// +===============================+
// bool WasWindowCloseRequested()
WasWindowCloseRequested_DEFINITION(Win32_WasWindowCloseRequested)
{
	if (glfwWindowShouldClose(platform.window)) { return true; }
	else { return false; }
}
