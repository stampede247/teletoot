/*
File:   win32_memory.h
Author: Taylor Robbins
Date:   07\02\2019
*/

#ifndef _WIN_32_MEMORY_H
#define _WIN_32_MEMORY_H

struct Win32Allocation_t
{
	u32 size;
	void* base;
};

#endif //  _WIN_32_MEMORY_H
