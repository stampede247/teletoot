/*
File:   win32_file_watching.cpp
Author: Taylor Robbins
Date:   10\22\2018
Description: 
	** Holds functions that handle keeping track of files and watching for changes
	** made by monitoring changes to the file write time.
	** This file is one of the few places where we call an application DLL function from somewhere besides win32_main.cpp
	** This file also provides some functions that allow the application to start and stop file watching
*/

WIN32_THREAD_FUNCTION(FileWatchingFunction, userPntr)
{
	WatchFilesInfo_t* info = (WatchFilesInfo_t*)userPntr;
	if (info == nullptr) { return 1; }
	if (info->mutex.handle == NULL) { return 1; }
	
	while (true)
	{
		Win32_LockMutex(&info->mutex, INFINITE);
		for (u32 fIndex = 0; fIndex < info->files.length; fIndex++)
		{
			WatchedFile_t* file = DynArrayGet(&info->files, WatchedFile_t, fIndex);
			FILETIME newWriteTime = Win32_GetFileWriteTime(file->filePath);
			if (CompareFileTime(&newWriteTime, &file->lastWriteTime) != 0)
			{
				Win32_UnlockMutex(&info->mutex);
				Sleep(1000); //Fixed wait time to make sure writing completes
				Win32_LockMutex(&info->mutex, INFINITE);
				file->changed = true;
				file->lastWriteTime = Win32_GetFileWriteTime(file->filePath);
				break;
			}
		}
		Win32_UnlockMutex(&info->mutex);
		Sleep(500);
	}
	
	return 0;
}

void _Win32_InitFileWatching()
{
	Win32_CreateMutex(&platform.watchFilesInfo.mutex);
	CreateDynamicArray(&platform.watchFilesInfo.files, &platform.stdHeap, sizeof(WatchedFile_t));
	_Win32_CreateThread(FileWatchingFunction, &platform.watchFilesInfo);
}

void _Win32_UpdateFileWatching()
{
	if (Win32_LockMutex(&platform.watchFilesInfo.mutex, 0))
	{
		for (u32 fIndex = 0; fIndex < platform.watchFilesInfo.files.length; fIndex++)
		{
			WatchedFile_t* file = DynArrayGet(&platform.watchFilesInfo.files, WatchedFile_t, fIndex);
			if (file->changed)
			{
				// Win32_PrintLine("File changed: \"%s\"", watchedFile->filePath);
				file->changed = false;
				InputEvent_t* newEvent = Plat_PushInputEvent(platform.currentInput);
				if (newEvent != nullptr)
				{
					newEvent->type = InputEventType_FileChanged;
					newEvent->filePath = ArenaNtString(&platform.mainHeap, file->filePath);
					Assert(newEvent->filePath != nullptr);
				}
			}
		}
		Win32_UnlockMutex(&platform.watchFilesInfo.mutex);
	}
}

bool _Win32_IsFileWatched(const char* filePath)
{
	bool result = false;
	Win32_LockMutex(&platform.watchFilesInfo.mutex, INFINITE);
	for (u32 fIndex = 0; fIndex < platform.watchFilesInfo.files.length; fIndex++)
	{
		WatchedFile_t* file = DynArrayGet(&platform.watchFilesInfo.files, WatchedFile_t, fIndex);
		if (MyStrCompareNt(file->filePath, filePath) == 0)
		{
			result = true;
			break;
		}
	}
	Win32_UnlockMutex(&platform.watchFilesInfo.mutex);
	return result;
}

void _Win32_AddWatchedFile(const char* filePath)
{
	Assert(filePath != nullptr);
	Assert(!_Win32_IsFileWatched(filePath));
	
	Win32_LockMutex(&platform.watchFilesInfo.mutex, INFINITE);
	WatchedFile_t* newFile = DynArrayAdd(&platform.watchFilesInfo.files, WatchedFile_t);
	ClearPointer(newFile);
	newFile->filePath = ArenaString(&platform.stdHeap, filePath, MyStrLength32(filePath));
	newFile->lastWriteTime = Win32_GetFileWriteTime(filePath);
	newFile->changed = false;
	Win32_UnlockMutex(&platform.watchFilesInfo.mutex);
}

void _Win32_RemoveWatchedFile(const char* filePath)
{
	Win32_LockMutex(&platform.watchFilesInfo.mutex, INFINITE);
	for (u32 fIndex = 0; fIndex < platform.watchFilesInfo.files.length; fIndex++)
	{
		WatchedFile_t* watchedFile = DynArrayGet(&platform.watchFilesInfo.files, WatchedFile_t, fIndex);
		if (MyStrCompareNt(watchedFile->filePath, filePath) == 0)
		{
			ArenaPop(&platform.stdHeap, watchedFile->filePath);
			DynArrayRemove(&platform.watchFilesInfo.files, fIndex);
			break;
		}
	}
	Win32_UnlockMutex(&platform.watchFilesInfo.mutex);
}

// +--------------------------------------------------------------+
// |                      Platform Services                       |
// +--------------------------------------------------------------+

//TODO: This function should handle platform independent folder paths
// +==============================+
// |       Win32_WatchFile        |
// +==============================+
// bool WatchFile(const char* filePath)
WatchFile_DEFINITION(Win32_WatchFile)
{
	Assert(filePath != nullptr);
	if (_Win32_IsFileWatched(filePath)) { return false; }
	if (!Win32_DoesFileExist(filePath)) { return false; }
	
	_Win32_AddWatchedFile(filePath);
	Assert(_Win32_IsFileWatched(filePath));
	return true;
}

//TODO: This function should handle platform independent folder paths
// +==============================+
// |      Win32_UnwatchFile       |
// +==============================+
// bool UnwatchFile(const char* filePath)
UnwatchFile_DEFINITION(Win32_UnwatchFile)
{
	Assert(filePath != nullptr);
	if (!_Win32_IsFileWatched(filePath)) { return false; }
	_Win32_RemoveWatchedFile(filePath);
	return true;
}

//TODO: This function should handle platform independent folder paths
// +==============================+
// |     Win32_IsWatchingFile     |
// +==============================+
// bool IsWatchingFile(const char* filePath)
IsWatchingFile_DEFINITION(Win32_IsWatchingFile)
{
	Assert(filePath != nullptr);
	return _Win32_IsFileWatched(filePath);
}
