/*
File:   win32_fonts.h
Author: Taylor Robbins
Date:   10\09\2019
*/

#ifndef _WIN_32_FONTS_H
#define _WIN_32_FONTS_H

struct PlatformFont_t
{
	bool isValid;
	HFONT handle;
	
	char* requestName;
	char* actualName;
	i32 size;
	bool isBold;
	bool isItalic;
	
	u32 dataSize;
	u8* dataPntr;
};

#endif //  _WIN_32_FONTS_H
