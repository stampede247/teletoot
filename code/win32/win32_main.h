/*
File:   win32_main.h
Author: Taylor Robbins
Date:   07\01\2019
*/

#ifndef _WIN_32_MAIN_H
#define _WIN_32_MAIN_H

// +--------------------------------------------------------------+
// |                           Globals                            |
// +--------------------------------------------------------------+
struct PlatformData_t
{
	PlatformInfo_t info;
	Version_t version;
	StartupInfo_t startupInfo;
	StartupOptions_t startupOptions;
	AppMemory_t appMemory;
	AppOutput_t appOutput;
	AppInput_t appInputs[2];
	AppInput_t* currentInput;
	bool startupFinished;
	
	MemoryArena_t tempArena;
	MemoryArena_t mainHeap;
	MemoryArena_t stdHeap;
	void* heapSpace;
	void* tempSpace;
	DynArray_t appAllocations;
	Mutex_t appAllocationsMutex;
	
	Mutex_t threadsMutex;
	Thread_t threads[MAX_NUM_THREADS];
	
	WatchFilesInfo_t watchFilesInfo = {};
	// Thread_t* FileWatchingThread = nullptr;
	
	bool glfwInitialized;
	CursorType_t currentCursor;
	MouseMode_t currentMouseMode;
	GLFWcursor* glfwCursors[Cursor_NumTypes];
	char* windowTitle;
	
	GLFWwindow* window;
	v2i screenSize;
	v2i windowPosition;
	bool windowIsFullscreen;
	u32 windowMonitorIndex;
	u32 windowDisplayModeIndex;
	r32 windowTargetFramerate;
	bool doingManualUpdateLoopWait;
	u32 numFramesFlipSuccessfullySynced;
	u32 numFramesFlipFailedToSync;
	
	u32 primaryMonitorIndex;
	DynArray_t glfwMonitors;
	#if USE_CUSTOM_AUDIO
	u32 primaryAudioDeviceIndex;
	DynArray_t audioDevices;
	
	Mutex_t audioDeviceChangeMutex;
	bool audioDeviceChanged;
	char* newPrimaryAudioDeviceIdStr;
	
	AudioOutput_t audioOutput;
	#endif
	
	HDC winDeviceContext;
	HGLRC winGraphicsContext;
	HWND windowHandle;
	
	char* exeDirectory;
	char* exeFileName;
	char* workingDirectory;
	char* baseResourceDirectory;
	char* appDllPath;
	char* appDllTempPath;
	
	u32 numProgramArguments;
	char** programArguments;
	
	#if DEBUG
	Thread_t* dllWatchThread;
	DllWatchingInfo_t dllWatchInfo;
	#endif
	
	i32 glfwLastErrorCode;
	char* glfwLastErrorStr;
	
	Win32_Texture_t loadingImage;
	Win32_Texture_t dotTexture;
	Win32_VertexBuffer_t squareBuffer;
	Win32_Shader_t mainShader;
	bool appLoadingFinished;
	r32 appLoadingPercent;
	
	HINTERNET httpSessionHandle;
	bool performingHttpRequest = false;
	bool httpRequestFailed = false;
	bool httpRequestSucceeded = false;
	HttpRequest_t httpRequest;
	HINTERNET httpConnectionHandle;
	HINTERNET httpRequestHandle;
	bool httpRequestWriting = false;
	u32 httpEncodedDataLength = 0;
	char* httpEncodedData = nullptr;
	bool httpRequestReading = false;
	u32 httpResponseDataLength = 0;
	char* httpResponseData = nullptr;
	
	Mutex_t debugOutputMutex;
	PlatDebugLine_t workingLine;
	DynArray_t lineBuffer;
	DynArray_t debugLines;
	
	DynArray_t droppedFilePaths;
	
	u32 nextTaskId;
	DynArray_t taskThreads;
	DynArray_t taskThreadInfos;
	Win32Semaphore_t taskThreadsSemaphore;
	ThreadedTask_t taskBuffer[MAX_QUEUED_TASKS];
	
	i64 perfCountFrequency;
	bool doSleepWait;
	
	bool upsideDownModeEnabled;
};

#endif //  _WIN_32_MAIN_H
