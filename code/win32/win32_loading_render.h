/*
File:   win32_loading_render.h
Author: Taylor Robbins
Date:   09\24\2019
*/

#ifndef _WIN_32_LOADING_RENDER_H
#define _WIN_32_LOADING_RENDER_H

struct Win32_Texture_t
{
	GLuint id;
	bool isValid;
	union
	{
		v2i size;
		struct { i32 width, height; };
	};
};

struct Win32_Shader_t
{
	bool isValid;
	GLuint vertId;
	GLuint fragId;
	GLuint programId;
	GLuint vertexArray;
	struct
	{
		//Attributes
		GLint positionAttrib;
		GLint colorAttrib;
		GLint texCoordAttrib;
		
		//Uniform Locations
		GLint worldMatrix;
		GLint viewMatrix;
		GLint projectionMatrix;
		GLint texture;
		GLint textureSize;
		GLint sourceRectangle;
		GLint primaryColor;
	} locations;
};

struct Win32_VertexBuffer_t
{
	bool isDynamic;
	GLuint id;
	u32 numVertices;
};

struct Win32_Vertex_t
{
	v3 position;
	v4 color;
	v2 texCoord;
};

#endif //  _WIN_32_LOADING_RENDER_H
