/*
File:   win32_fonts.cpp
Author: Taylor Robbins
Date:   10\09\2019
Description: 
	** Holds some functions that help the application find and open font files from the platform layer
*/

// +==============================+
// |    Win32_OpenPlatformFont    |
// +==============================+
// bool OpenPlatformFont(const char* fontName, i32 fontSize, bool bold, bool italic, PlatformFont_t* fontOut)
OpenPlatformFont_DEFINITION(Win32_OpenPlatformFont)
{
	Assert(fontName != nullptr);
	Assert(fontOut != nullptr);
	Assert(MyStrLength32(fontName)+1 <= 32); //required by CreateFontA
	
	ClearPointer(fontOut);
	fontOut->isValid = false;
	
	HFONT fontHandle = CreateFontA(
		fontSize,                   //height
		0,                          //width
		0,                          //escapement
		0,                          //orientation
		bold ? FW_BOLD : FW_NORMAL, //weight
		italic,                     //italic
		false,                      //underline
		false,                      //strikeout
		ANSI_CHARSET,               //charset
		OUT_DEFAULT_PRECIS,         //output precision
		CLIP_DEFAULT_PRECIS,        //clipping precision
		DEFAULT_QUALITY,            //quality
		DEFAULT_PITCH|FF_DONTCARE,  //pitch and family
		fontName                    //Face Name
	);
	if (fontHandle == NULL)
	{
		PrintLine_E("Failed to file font \"%s\" at size %d", fontName, fontSize);
		return false;
	}
	
	SelectObject(platform.winDeviceContext, fontHandle);
	DWORD getFontDataResult1 = GetFontData(
		platform.winDeviceContext, //hardware device context
		0, //table
		0, //offset
		nullptr, //buffer
		0 //buffer size
	);
	if (getFontDataResult1 == GDI_ERROR)
	{
		PrintLine_E("Failed to get font file contents \"%s\" at size %d", fontName, fontSize);
		DeleteObject(fontHandle);
		return false;
	}
	if (getFontDataResult1 <= 0)
	{
		PrintLine_E("No data retrieved from GetFontData for font \"%s\" at size %d", fontName, fontSize);
		DeleteObject(fontHandle);
		return false;
	}
	
	fontOut->handle = fontHandle;
	fontOut->dataSize = (u32)getFontDataResult1;
	fontOut->dataPntr = (u8*)VirtualAlloc(0, fontOut->dataSize+1, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
	Assert(fontOut->dataPntr != nullptr);
	
	DWORD getFontDataResult2 = GetFontData(
		platform.winDeviceContext, //hardware device context
		0, //table
		0, //offset
		fontOut->dataPntr, //buffer
		fontOut->dataSize //buffer size
	);
	Assert(getFontDataResult2 == getFontDataResult1);
	fontOut->dataPntr[fontOut->dataSize] = '\0';
	
	fontOut->requestName = ArenaNtString(&platform.mainHeap, fontName);
	Assert(fontOut->requestName != nullptr);
	
	//TODO: Actually get this information from windows!
	fontOut->actualName = ArenaNtString(&platform.mainHeap, fontName);
	Assert(fontOut->actualName != nullptr);
	
	fontOut->size = fontSize;
	fontOut->isBold = bold;
	fontOut->isItalic = italic;
	fontOut->isValid = true;
	return true;
}

// +==============================+
// |   Win32_ClosePlatformFont    |
// +==============================+
// void ClosePlatformFont(PlatformFont_t* font)
ClosePlatformFont_DEFINITION(Win32_ClosePlatformFont)
{
	Assert(font != nullptr);
	if (font->handle != NULL)
	{
		DeleteObject(font->handle);
		font->handle = NULL;
	}
	if (font->requestName != nullptr)
	{
		ArenaPop(&platform.mainHeap, font->requestName);
		font->requestName = nullptr;
	}
	if (font->actualName != nullptr)
	{
		ArenaPop(&platform.mainHeap, font->actualName);
		font->actualName = nullptr;
	}
	if (font->dataPntr != nullptr)
	{
		VirtualFree(font->dataPntr, 0, MEM_RELEASE);
		font->dataPntr = nullptr;
	}
}

