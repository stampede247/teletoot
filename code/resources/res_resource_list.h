/*
File:   res_resource_list.h
Author: Taylor Robbins
Date:   05\04\2020
Description:
	** This is a special file that can be included multiple times to obtain different effects. Before the file is included you must define
	** RESOURCE_TEXTURE, RESOURCE_SHEET, RESOURCE_FONT_TTF, RESOURCE_FONT_BITMAP, RESOURCE_SHADER, and or RESOURCE_MUSIC in order for
	** this file to have any real effect (besides defining NUM_RESOURCE_... defines)
*/

// +--------------------------------------------------------------+
// |                        Sprite Sheets                         |
// +--------------------------------------------------------------+
#ifndef NUM_RESOURCE_SHEETS
#define NUM_RESOURCE_SHEETS 15
#endif
#ifndef RESOURCE_SHEET
#define RESOURCE_SHEET(varName, readableName, filePath, sheetSize, appStatesBits) //nothing
#endif

RESOURCE_SHEET(entitiesSheet,         "Entities Sheet",   SHEETS_FLDR "_entities.png",      NewVec2i(32, 32), AppStateBit_Game);
RESOURCE_SHEET(groundSheet,           "Ground Sheet",     SHEETS_FLDR "_grounds.png",       NewVec2i(32, 32), AppStateBit_Game);
RESOURCE_SHEET(trainSheet,            "Train Sheet",      SHEETS_FLDR "_train.png",         NewVec2i(17, 1),  AppStateBit_Game);
RESOURCE_SHEET(hudStackAnimSheet,     "Chug Anim Sheet",  SHEETS_FLDR "hud_stack_anim.png", NewVec2i(6, 1),   AppStateBit_Game);
RESOURCE_SHEET(hudPlumeAnimSheet,     "Plume Anim Sheet", SHEETS_FLDR "hud_plume_anim.png", NewVec2i(6, 1),   AppStateBit_Game);

RESOURCE_SHEET(cutsceneIcons,         "Cutscene Icons", SHEETS_FLDR "cutscene_icons.png", NewVec2i(32,  3), 0x00);

RESOURCE_SHEET(shovelerSheet,         "Shoveler Sheet",           SHEETS_FLDR "shoveler.png",               NewVec2i(8,  1), AppStateBit_Game);
RESOURCE_SHEET(conductorSheet,        "Conductor Sheet",          SHEETS_FLDR "conductor.png",              NewVec2i(4,  1), AppStateBit_Game);
RESOURCE_SHEET(explosionSheet,        "Explosion Sheet",          SHEETS_FLDR "explosion.png",              NewVec2i(5,  1), AppStateBit_Game);
RESOURCE_SHEET(trackIcons,            "Track Icons",              SHEETS_FLDR "track_icons.png",            NewVec2i(8,  1), AppStateBit_Game);
RESOURCE_SHEET(plumeSuccessAnimSheet, "Plume Success Anim Sheet", SHEETS_FLDR "hud_plume_success_anim.png", NewVec2i(6,  1), AppStateBit_Game);
RESOURCE_SHEET(trainSmokeSheet,       "Train Smoke Sheet",        SHEETS_FLDR "train_smoke.png",            NewVec2i(9,  1), AppStateBit_Game);
RESOURCE_SHEET(furnaceFireSheet,      "Furnace Fire Sheet",       SHEETS_FLDR "furnace_fire.png",           NewVec2i(9,  1), AppStateBit_Game);
RESOURCE_SHEET(buttonsSheet,          "Buttons Sheet",            SHEETS_FLDR "_buttons.png",               NewVec2i(9,  6), AppStateBit_Game);
RESOURCE_SHEET(endTownSheet,          "End Town Sheet",           SHEETS_FLDR "end_town.png",               NewVec2i(4,  1), AppStateBit_Game);

#undef RESOURCE_SHEET

// +--------------------------------------------------------------+
// |                           Textures                           |
// +--------------------------------------------------------------+
#ifndef NUM_RESOURCE_TEXTURES
#define NUM_RESOURCE_TEXTURES 23
#endif
#ifndef RESOURCE_TEXTURE
#define RESOURCE_TEXTURE(varName, readableName, filePath, pixelated, looping, appStatesBits) //nothing
#endif

RESOURCE_TEXTURE(logoSprite, "Logo Sprite", SPRITES_FLDR "logo.png", true, false, AppStateBit_MainMenu);

RESOURCE_TEXTURE(alphaTexture, "Alpha Texture", TEXTURES_FLDR "alpha.png", true, false, 0x00);

RESOURCE_TEXTURE(primarySprite,             "Primary Sprite",          SPRITES_FLDR "primary.png",               true, false, AppStateBit_Game);
RESOURCE_TEXTURE(secondarySprite,           "Secondary Sprite",        SPRITES_FLDR "secondary.png",             true, false, AppStateBit_Game);
RESOURCE_TEXTURE(cardBackSprite,            "Card Back",               SPRITES_FLDR "card_back.png",             true, false, AppStateBit_Game);
RESOURCE_TEXTURE(inputBackSprite,           "Input Back",              SPRITES_FLDR "input_back.png",            true, false, AppStateBit_Game);
RESOURCE_TEXTURE(enterButtonSprite,         "Enter Button",            SPRITES_FLDR "enter_button.png",          true, false, AppStateBit_Game);
RESOURCE_TEXTURE(trackStraightSprite,       "Track Straight",          SPRITES_FLDR "track_straight.png",        true, false, AppStateBit_Game);
RESOURCE_TEXTURE(trackEndSprite,            "Track End",               SPRITES_FLDR "track_end.png",             true, false, AppStateBit_Game);
RESOURCE_TEXTURE(trackCornerSprite,         "Track Corner",            SPRITES_FLDR "track_corner.png",          true, false, AppStateBit_Game);
RESOURCE_TEXTURE(trackCornerSprite2,        "Track Corner2",           SPRITES_FLDR "track_corner2.png",         true, false, AppStateBit_Game);
RESOURCE_TEXTURE(trackBridgeStraightSprite, "Track Bridge Straight",   SPRITES_FLDR "track_bridge_straight.png", true, false, AppStateBit_Game);
RESOURCE_TEXTURE(trackBridgeCornerSprite,   "Track Bridge Corner",     SPRITES_FLDR "track_bridge_corner.png",   true, false, AppStateBit_Game);
RESOURCE_TEXTURE(trackBridgeCornerSprite2,  "Track Bridge Corner2",    SPRITES_FLDR "track_bridge_corner2.png",  true, false, AppStateBit_Game);
RESOURCE_TEXTURE(hudBackSprite,             "HUD Back Sprite",         SPRITES_FLDR "hud_back.png",              true, false, AppStateBit_Game);
RESOURCE_TEXTURE(hudBackButtonsSprite,      "HUD Back Buttons Sprite", SPRITES_FLDR "hud_back_buttons.png",      true, false, AppStateBit_Game);
RESOURCE_TEXTURE(trackCrossSprite,          "Track Cross",             SPRITES_FLDR "track_cross.png",           true, false, AppStateBit_Game);
RESOURCE_TEXTURE(trackBridgeCrossSprite,    "Track Bridge Cross",      SPRITES_FLDR "track_bridge_cross.png",    true, false, AppStateBit_Game);
RESOURCE_TEXTURE(blockSpotSprite,           "Block Spot Sprite",       SPRITES_FLDR "block_spot.png",            true, false, AppStateBit_Game);
RESOURCE_TEXTURE(buildSpotSprite,           "Build Spot Sprite",       SPRITES_FLDR "build_spot.png",            true, false, AppStateBit_Game);
RESOURCE_TEXTURE(completionBackSprite,      "Completion Back Sprite",  SPRITES_FLDR "completion_back.png",       true, false, AppStateBit_Game);
RESOURCE_TEXTURE(derailedSprite,            "Derailed Sprite",         SPRITES_FLDR "derailed.png",              true, false, AppStateBit_Game);
RESOURCE_TEXTURE(youWinSprite,              "You Win Sprite",          SPRITES_FLDR "you_win.png",               true, false, AppStateBit_Game);

#undef RESOURCE_TEXTURE

// +--------------------------------------------------------------+
// |                            Musics                            |
// +--------------------------------------------------------------+
#ifndef NUM_RESOURCE_MUSICS
#define NUM_RESOURCE_MUSICS 4
#endif
#ifndef RESOURCE_MUSIC
#define RESOURCE_MUSIC(varName, readableName, filePath) //nothing
#endif

RESOURCE_MUSIC(stayOnTrack1, "Stay On Track 1", MUSIC_FLDR "stay_on_track.ogg");
RESOURCE_MUSIC(stayOnTrack2, "Stay On Track 2", MUSIC_FLDR "stay_on_track2.ogg");
RESOURCE_MUSIC(stayOnTrack3, "Stay On Track 3", MUSIC_FLDR "stay_on_track3.ogg");
RESOURCE_MUSIC(stayOnTrack4, "Stay On Track 4", MUSIC_FLDR "stay_on_track4.ogg");

#undef RESOURCE_MUSIC

// +--------------------------------------------------------------+
// |                            Fonts                             |
// +--------------------------------------------------------------+
#ifndef NUM_RESOURCE_FONTS
#define NUM_RESOURCE_FONTS 2
#endif
#ifndef RESOURCE_FONT_TTF
#define RESOURCE_FONT_TTF(varName, readableName, fontName, fontSize, isBold, isItalic, firstChar, numChars, bitmapSize) //nothing
#endif
#ifndef RESOURCE_FONT_BITMAP
#define RESOURCE_FONT_BITMAP(varName, readableName, filePath, charSize, charUnderhang, firstChar, numChars) //nothing
#endif

RESOURCE_FONT_TTF(debugFont,    "Debug Font", "Consolas", 18.0f, true, false, ' ', 96, 256);
RESOURCE_FONT_BITMAP(pixelFont, "Pixel Font", FONTS_FLDR "pixelFont.png", NewVec2i( 6,  8), 0, ' ', 96);

#undef RESOURCE_FONT_TTF
#undef RESOURCE_FONT_BITMAP

// +--------------------------------------------------------------+
// |                           Shaders                            |
// +--------------------------------------------------------------+
#ifndef NUM_RESOURCE_SHADERS
#define NUM_RESOURCE_SHADERS 4
#endif
#ifndef RESOURCE_SHADER
#define RESOURCE_SHADER(varName, readableName, filePath) //nothing
#endif

RESOURCE_SHADER(mainShader,         "Main Shader",          SHADERS_FLDR "main.glsl");
RESOURCE_SHADER(tileShader,         "Tile Shader",          SHADERS_FLDR "tile.glsl");
RESOURCE_SHADER(wipeShader,         "Wipe Shader",          SHADERS_FLDR "wipe.glsl");
RESOURCE_SHADER(fontShader,         "Font Shader",          SHADERS_FLDR "font.glsl");

#undef RESOURCE_SHADER

#ifndef NUM_RESOURCES
#define NUM_RESOURCES (NUM_RESOURCE_SHEETS + NUM_RESOURCE_TEXTURES + NUM_RESOURCE_MUSICS + NUM_RESOURCE_FONTS + NUM_RESOURCE_SHADERS)
#endif
