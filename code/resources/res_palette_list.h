/*
File:   res_palette_list.h
Author: Taylor Robbins
Date:   06\25\2020
Description:
	** This is a special file that can be included multiple times to obtain different effects. Before the file is included you must define
	** PALETTE_ENTRY in order for this file to have any real effect (besides defining NUM_PALETTE_COLORS when you include it for the first time)
*/


#ifndef NUM_PALETTE_COLORS
#define NUM_PALETTE_COLORS 11
#endif

#ifndef PALETTE_ENTRY
#define PALETTE_ENTRY(varName, readableName) //nothing
#endif

PALETTE_ENTRY(target,       "Target");       //0
PALETTE_ENTRY(variant1,     "Red");          //1
PALETTE_ENTRY(pink,         "Pink");         //2
PALETTE_ENTRY(furnaceBoth,  "FurnaceBoth");  //3
PALETTE_ENTRY(furnaceRed,   "FurnaceRed");   //4
PALETTE_ENTRY(furnaceBlue,  "FurnaceBlue");  //5
PALETTE_ENTRY(plume,        "Plume");        //6
PALETTE_ENTRY(plumeSuccess, "Plume Success");//7
PALETTE_ENTRY(fireRed,      "Fire Red");     //8
PALETTE_ENTRY(fireYellow,   "Fire Yellow");  //9
PALETTE_ENTRY(fireBlue,     "Fire Blue");    //10

#undef PALETTE_ENTRY
