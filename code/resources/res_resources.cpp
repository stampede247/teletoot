/*
File:   res_resources.cpp
Author: Taylor Robbins
Date:   10\23\2018
Description: 
	** Holds functions that handle loading and reloading various kinds of resource files
*/

// +--------------------------------------------------------------+
// |                 Resource Handling Functions                  |
// +--------------------------------------------------------------+
bool IsResourceLoaded(Resource_t* resourcePntr)
{
	Assert(resourcePntr != nullptr);
	switch (resourcePntr->type)
	{
		case ResourceType_SpriteSheet: return resourcePntr->sheet.texture.isValid;
		case ResourceType_Texture:     return resourcePntr->texture.isValid;
		case ResourceType_Music:       return resourcePntr->music.isValid;
		case ResourceType_Font:        return resourcePntr->font.isValid;
		case ResourceType_Shader:      return resourcePntr->shader.isValid;
		default: Assert(false); return false;
	}
}
void UpdateResourceAccessTime(Resource_t* resource)
{
	Assert(resource != nullptr);
	resource->lastAccessTime = ProgramTime;
}

void UnloadResource(Resource_t* resourcePntr)
{
	Assert(resourcePntr != nullptr);
	Assert(resourcePntr->type < ResourceType_NumTypes);
	if (!IsResourceLoaded(resourcePntr)) { return; }
	switch (resourcePntr->type)
	{
		case ResourceType_SpriteSheet:
		{
			DestroySpriteSheet(&resourcePntr->sheet);
			Assert(resourcePntr->filePath != nullptr);
			#if DEVELOPER
			platform->UnwatchFile(resourcePntr->filePath);
			#endif
			ArenaPop(staticHeap, resourcePntr->filePath);
			resourcePntr->filePath = nullptr;
		} break;
		case ResourceType_Texture:
		{
			DestroyTexture(&resourcePntr->texture);
			Assert(resourcePntr->filePath != nullptr);
			#if DEVELOPER
			platform->UnwatchFile(resourcePntr->filePath);
			#endif
			ArenaPop(staticHeap, resourcePntr->filePath);
			resourcePntr->filePath = nullptr;
		} break;
		case ResourceType_Music:
		{
			DestroySound(&resourcePntr->music);
			Assert(resourcePntr->filePath != nullptr);
			#if DEVELOPER
			platform->UnwatchFile(resourcePntr->filePath);
			#endif
			ArenaPop(staticHeap, resourcePntr->filePath);
			resourcePntr->filePath = nullptr;
		} break;
		case ResourceType_Font:
		{
			DestroyFont(&resourcePntr->font);
			Assert(resourcePntr->filePath != nullptr);
			#if DEVELOPER
			if (resourcePntr->font.isBitmapFont)
			{
				char* metaFilePath = StrReplace(TempArena, resourcePntr->filePath, MyStrLength32(resourcePntr->filePath), ".png", 4, ".meta", 5);
				platform->UnwatchFile(resourcePntr->filePath);
				platform->UnwatchFile(metaFilePath);
			}
			#endif
			ArenaPop(staticHeap, resourcePntr->filePath);
			resourcePntr->filePath = nullptr;
		} break;
		case ResourceType_Shader:
		{
			DestroyShader(&resourcePntr->shader);
			Assert(resourcePntr->filePath != nullptr);
			#if DEVELOPER
			platform->UnwatchFile(resourcePntr->filePath);
			#endif
			ArenaPop(staticHeap, resourcePntr->filePath);
			resourcePntr->filePath = nullptr;
		} break;
		default: Assert(false); break;
	}
}
void LoadResource(Resource_t* resourcePntr)
{
	Assert(resourcePntr != nullptr);
	Assert(resourcePntr->type < ResourceType_NumTypes);
	switch (resourcePntr->type)
	{
		case ResourceType_SpriteSheet:
		{
			if (!resourcePntr->sheet.texture.isValid) { DestroySpriteSheet(&resourcePntr->sheet); }
			if (resourcePntr->filePath != nullptr)
			{
				#if DEVELOPER
				platform->UnwatchFile(resourcePntr->filePath);
				#endif
				ArenaPop(staticHeap, resourcePntr->filePath);
			}
			const char* resourcePath = GetSpriteSheetFilePath(resourcePntr->subIndex);
			if (WasProgramArgumentPassed("mouse") && StrCompareIgnoreCaseNt(resourcePntr->readableName, "Player Sheet"))
			{
				// PrintLine_I("Redirecting \"%s\" to \"%s\"", resourcePath, SHEETS_FLDR "_mouse.png");
				resourcePath = SHEETS_FLDR "_mouse.png";
			}
			v2i sheetSize = GetSpriteSheetSize(resourcePntr->subIndex);
			//TODO: Do a look up to figure out how much padding the sheet should have and whether or not it should be pixelated?
			resourcePntr->sheet = LoadSpriteSheet(staticHeap, resourcePath, 1, sheetSize, true);
			resourcePntr->filePath = ArenaNtString(staticHeap, resourcePath);
			Assert(resourcePntr->filePath != nullptr);
			#if DEVELOPER
			platform->WatchFile(resourcePath);
			#endif
		} break;
		
		case ResourceType_Texture:
		{
			if (!resourcePntr->texture.isValid) { DestroyTexture(&resourcePntr->texture); }
			if (resourcePntr->filePath != nullptr)
			{
				#if DEVELOPER
				platform->UnwatchFile(resourcePntr->filePath);
				#endif
				ArenaPop(staticHeap, resourcePntr->filePath);
			}
			const char* resourcePath = GetTextureFilePath(resourcePntr->subIndex);
			bool isLooping = false;
			bool isPixelated = IsTextureResourcePixelated(resourcePntr->subIndex, &isLooping);
			resourcePntr->texture = LoadTexture(resourcePath, isPixelated, isLooping);
			resourcePntr->filePath = ArenaNtString(staticHeap, resourcePath);
			Assert(resourcePntr->filePath != nullptr);
			#if DEVELOPER
			platform->WatchFile(resourcePath);
			#endif
		} break;
		
		case ResourceType_Music:
		{
			if (!resourcePntr->music.isValid) { DestroySound(&resourcePntr->music); }
			if (resourcePntr->filePath != nullptr)
			{
				#if DEVELOPER
				platform->UnwatchFile(resourcePntr->filePath);
				#endif
				ArenaPop(staticHeap, resourcePntr->filePath);
			}
			const char* resourcePath = GetMusicFilePath(resourcePntr->subIndex);
			resourcePntr->music = LoadSound(resourcePath);
			resourcePntr->filePath = ArenaNtString(staticHeap, resourcePath);
			Assert(resourcePntr->filePath != nullptr);
			#if DEVELOPER
			platform->WatchFile(resourcePath);
			#endif
		} break;
		
		case ResourceType_Font:
		{
			if (!resourcePntr->font.isValid) { DestroyFont(&resourcePntr->font); }
			if (resourcePntr->filePath != nullptr)
			{
				#if DEVELOPER
				if (resourcePntr->font.isBitmapFont)
				{
					char* metaFilePath = StrReplace(TempArena, resourcePntr->filePath, MyStrLength32(resourcePntr->filePath), ".png", 4, ".meta", 5);
					platform->UnwatchFile(resourcePntr->filePath);
					platform->UnwatchFile(metaFilePath);
				}
				#endif
				ArenaPop(staticHeap, resourcePntr->filePath);
			}
			const char* resourcePath = GetFontFilePath(resourcePntr->subIndex);
			ResourceFontOptions_t options = {};
			GetResourceFontOptions(resourcePntr->subIndex, &options);
			if (IsFilePathExtensionNt(resourcePath, "png"))
			{
				char* metaFilePath = StrReplace(TempArena, resourcePath, MyStrLength32(resourcePath), ".png", 4, ".meta", 5);
				bool loadSuccess = LoadBitmapFont(staticHeap, resourcePath, metaFilePath, &resourcePntr->font);
				if (!loadSuccess) { PrintLine_E("Failed to load bitmap font resource from \"%s\" and \"%s\"", resourcePath, metaFilePath); }
				#if DEVELOPER
				platform->WatchFile(resourcePath);
				platform->WatchFile(metaFilePath);
				#endif
			}
			else if (IsFilePathExtensionNt(resourcePath, "ttf"))
			{
				resourcePntr->font = LoadFont(staticHeap, resourcePath, options.size, options.bitmapSize, options.bitmapSize, options.firstCharacter, options.numCharacters);
				//NOTE: No need to follow ttf files actively
			}
			else
			{
				resourcePntr->font = LoadPlatformFont(staticHeap, resourcePath, options.size, options.bitmapSize, options.bitmapSize, options.firstCharacter, options.numCharacters, options.bold, options.italic);
				//NOTE: No need to follow ttf files actively
			}
			resourcePntr->filePath = ArenaNtString(staticHeap, resourcePath);
			Assert(resourcePntr->filePath != nullptr);
		} break;
		
		case ResourceType_Shader:
		{
			if (!resourcePntr->shader.isValid) { DestroyShader(&resourcePntr->shader); }
			if (resourcePntr->filePath != nullptr)
			{
				#if DEVELOPER
				platform->UnwatchFile(resourcePntr->filePath);
				#endif
				ArenaPop(staticHeap, resourcePntr->filePath);
			}
			const char* resourcePath = GetShaderFilePath(resourcePntr->subIndex);
			resourcePntr->shader = LoadShader(staticHeap, resourcePath);
			resourcePntr->filePath = ArenaNtString(staticHeap, resourcePath);
			Assert(resourcePntr->filePath != nullptr);
			#if DEVELOPER
			platform->WatchFile(resourcePath);
			#endif
		} break;
		
		default:
		{
			Assert(false);
		} break;
	}
	resourcePntr->lastLoadTime = ProgramTime;
	UpdateResourceAccessTime(resourcePntr);
}

//TODO: Update the platform layer loading bar during this process we load each item
void InitializeResourceArrays(ResourceArrays_t* arrays, r32 baseLoadingPercentage, r32 resourceLoadingPercentage)
{
	Assert(arrays != nullptr);
	CreateBucketArray(&arrays->sheets,   staticHeap, sizeof(Resource_t), 16, NUM_RESOURCE_SHEETS);
	CreateBucketArray(&arrays->textures, staticHeap, sizeof(Resource_t), 16, NUM_RESOURCE_TEXTURES);
	CreateBucketArray(&arrays->musics,   staticHeap, sizeof(Resource_t), 16, NUM_RESOURCE_MUSICS);
	CreateBucketArray(&arrays->fonts,    staticHeap, sizeof(Resource_t), 16, NUM_RESOURCE_FONTS);
	CreateBucketArray(&arrays->shaders,  staticHeap, sizeof(Resource_t), 16, NUM_RESOURCE_SHADERS);
	
	u32 numPreloadedResources = 0;
	for (u32 sIndex = 0; sIndex < NUM_RESOURCE_SHEETS; sIndex++)   { if (ShouldLoadResourceAlways(ResourceType_SpriteSheet, sIndex)) { numPreloadedResources++; } }
	for (u32 tIndex = 0; tIndex < NUM_RESOURCE_TEXTURES; tIndex++) { if (ShouldLoadResourceAlways(ResourceType_Texture,     tIndex)) { numPreloadedResources++; } }
	for (u32 mIndex = 0; mIndex < NUM_RESOURCE_MUSICS; mIndex++)   { if (ShouldLoadResourceAlways(ResourceType_Music,       mIndex)) { numPreloadedResources++; } }
	for (u32 fIndex = 0; fIndex < NUM_RESOURCE_FONTS; fIndex++)    { if (ShouldLoadResourceAlways(ResourceType_Font,        fIndex)) { numPreloadedResources++; } }
	for (u32 sIndex = 0; sIndex < NUM_RESOURCE_SHADERS; sIndex++)  { if (ShouldLoadResourceAlways(ResourceType_Shader,      sIndex)) { numPreloadedResources++; } }
	r32 currentLoadPercent = baseLoadingPercentage;
	r32 perResourceLoadPercent = resourceLoadingPercentage / numPreloadedResources;
	
	for (u32 sIndex = 0; sIndex < NUM_RESOURCE_SHEETS; sIndex++)
	{
		Resource_t* newResource = BktArrayAdd(&arrays->sheets, Resource_t);
		Assert(newResource != nullptr);
		ClearPointer(newResource);
		newResource->type = ResourceType_SpriteSheet;
		newResource->subIndex = sIndex;
		newResource->readableName = ArenaNtString(staticHeap, GetSpriteSheetReadableName(sIndex));
		Assert(newResource->readableName != nullptr);
		if (ShouldLoadResourceAlways(ResourceType_SpriteSheet, sIndex))
		{
			LoadResource(newResource);
			newResource->wasPreloaded = true;
			if (RoundR32i(currentLoadPercent*100) != RoundR32i((currentLoadPercent + perResourceLoadPercent)*100)) { platform->UpdateLoadingBar(currentLoadPercent+perResourceLoadPercent); }
			currentLoadPercent += perResourceLoadPercent;
		}
	}
	for (u32 tIndex = 0; tIndex < NUM_RESOURCE_TEXTURES; tIndex++)
	{
		Resource_t* newResource = BktArrayAdd(&arrays->textures, Resource_t);
		Assert(newResource != nullptr);
		ClearPointer(newResource);
		newResource->type = ResourceType_Texture;
		newResource->subIndex = tIndex;
		newResource->readableName = ArenaNtString(staticHeap, GetTextureReadableName(tIndex));
		Assert(newResource->readableName != nullptr);
		if (ShouldLoadResourceAlways(ResourceType_Texture, tIndex))
		{
			LoadResource(newResource);
			newResource->wasPreloaded = true;
			if (RoundR32i(currentLoadPercent*100) != RoundR32i((currentLoadPercent + perResourceLoadPercent)*100)) { platform->UpdateLoadingBar(currentLoadPercent+perResourceLoadPercent); }
			currentLoadPercent += perResourceLoadPercent;
		}
	}
	for (u32 mIndex = 0; mIndex < NUM_RESOURCE_MUSICS; mIndex++)
	{
		Resource_t* newResource = BktArrayAdd(&arrays->musics, Resource_t);
		Assert(newResource != nullptr);
		ClearPointer(newResource);
		newResource->type = ResourceType_Music;
		newResource->subIndex = mIndex;
		newResource->readableName = ArenaNtString(staticHeap, GetMusicReadableName(mIndex));
		Assert(newResource->readableName != nullptr);
		if (ShouldLoadResourceAlways(ResourceType_Music, mIndex))
		{
			LoadResource(newResource);
			newResource->wasPreloaded = true;
			if (RoundR32i(currentLoadPercent*100) != RoundR32i((currentLoadPercent + perResourceLoadPercent)*100)) { platform->UpdateLoadingBar(currentLoadPercent+perResourceLoadPercent); }
			currentLoadPercent += perResourceLoadPercent;
		}
	}
	for (u32 fIndex = 0; fIndex < NUM_RESOURCE_FONTS; fIndex++)
	{
		Resource_t* newResource = BktArrayAdd(&arrays->fonts, Resource_t);
		Assert(newResource != nullptr);
		ClearPointer(newResource);
		newResource->type = ResourceType_Font;
		newResource->subIndex = fIndex;
		newResource->readableName = ArenaNtString(staticHeap, GetFontReadableName(fIndex));
		Assert(newResource->readableName != nullptr);
		if (ShouldLoadResourceAlways(ResourceType_Font, fIndex))
		{
			LoadResource(newResource);
			newResource->wasPreloaded = true;
			if (RoundR32i(currentLoadPercent*100) != RoundR32i((currentLoadPercent + perResourceLoadPercent)*100)) { platform->UpdateLoadingBar(currentLoadPercent+perResourceLoadPercent); }
			currentLoadPercent += perResourceLoadPercent;
		}
	}
	for (u32 sIndex = 0; sIndex < NUM_RESOURCE_SHADERS; sIndex++)
	{
		Resource_t* newResource = BktArrayAdd(&arrays->shaders, Resource_t);
		Assert(newResource != nullptr);
		ClearPointer(newResource);
		newResource->type = ResourceType_Shader;
		newResource->subIndex = sIndex;
		newResource->readableName = ArenaNtString(staticHeap, GetShaderReadableName(sIndex));
		Assert(newResource->readableName != nullptr);
		if (ShouldLoadResourceAlways(ResourceType_Shader, sIndex))
		{
			LoadResource(newResource);
			newResource->wasPreloaded = true;
			if (RoundR32i(currentLoadPercent*100) != RoundR32i((currentLoadPercent + perResourceLoadPercent)*100)) { platform->UpdateLoadingBar(currentLoadPercent+perResourceLoadPercent); }
			currentLoadPercent += perResourceLoadPercent;
		}
	}
}

void CheckResourceArrayChanges(ResourceArrays_t* arrays)
{
	Assert(arrays);
	for (u32 sIndex = 0; sIndex < NUM_RESOURCE_SHEETS; sIndex++)
	{
		Resource_t* thisResource = nullptr;
		if (sIndex < arrays->sheets.length)
		{
			Resource_t* sheetResource = BktArrayGet(&arrays->sheets, Resource_t, sIndex);
			Assert(sheetResource != nullptr);
			if (IsResourceLoaded(sheetResource))
			{
				Assert(sheetResource->filePath != nullptr);
				const char* newFilePath = GetSpriteSheetFilePath(sIndex);
				u32 filePathLength = MyStrLength32(sheetResource->filePath);
				u32 newFilePathLength = MyStrLength32(newFilePath);
				v2i newSpriteSheetSize = GetSpriteSheetSize(sIndex);
				if (newFilePathLength != filePathLength || !StrCompareIgnoreCaseNt(sheetResource->filePath, newFilePath))
				{
					PrintLine_I("Spritesheet[%u] changed file path: \"%s\" -> \"%s\"", sIndex, sheetResource->filePath, newFilePath);
					LoadResource(sheetResource);
				}
				else if (newSpriteSheetSize != sheetResource->sheet.sheetSize)
				{
					PrintLine_I("Spritesheet[%u] changed size: (%d, %d) -> (%d, %d)", sIndex, sheetResource->sheet.framesX, sheetResource->sheet.framesY, newSpriteSheetSize.width, newSpriteSheetSize.y);
					LoadResource(sheetResource);
				}
			}
			u32 readableNameLength = MyStrLength32(sheetResource->readableName);
			const char* newReadableName = GetSpriteSheetReadableName(sIndex);
			u32 newReadableNameLength = MyStrLength32(newReadableName);
			if (readableNameLength != newReadableNameLength || !StrCompareIgnoreCaseNt(sheetResource->readableName, newReadableName))
			{
				PrintLine_I("SpriteSheet[%u] changed names: \"%s\" -> \"%s\"", sIndex, sheetResource->readableName, newReadableName);
				ArenaPop(staticHeap, sheetResource->readableName);
				sheetResource->readableName = ArenaNtString(staticHeap, newReadableName);
				Assert(sheetResource->readableName != nullptr);
			}
			thisResource = sheetResource;
		}
		else 
		{
			Assert(sIndex == arrays->sheets.length);
			WriteLine_I("New SpriteSheet resource found");
			Resource_t* newResource = BktArrayAdd(&arrays->sheets, Resource_t);
			Assert(newResource != nullptr);
			ClearPointer(newResource);
			newResource->type = ResourceType_SpriteSheet;
			newResource->subIndex = sIndex;
			newResource->readableName = ArenaNtString(staticHeap, GetSpriteSheetReadableName(sIndex));
			Assert(newResource->readableName != nullptr);
			thisResource = newResource;
		}
		if (ShouldLoadResourceAlways(ResourceType_SpriteSheet, sIndex) && !IsResourceLoaded(thisResource))
		{
			PrintLine_I("SpriteSheet[%u] %s should be loaded always now", sIndex, thisResource->readableName);
			LoadResource(thisResource);
			thisResource->wasPreloaded = true;
		}
	}
	for (u32 tIndex = 0; tIndex < NUM_RESOURCE_TEXTURES; tIndex++)
	{
		Resource_t* thisResource = nullptr;
		if (tIndex < arrays->textures.length)
		{
			Resource_t* textureResource = BktArrayGet(&arrays->textures, Resource_t, tIndex);
			Assert(textureResource != nullptr);
			if (IsResourceLoaded(textureResource))
			{
				Assert(textureResource->filePath != nullptr);
				const char* newFilePath = GetTextureFilePath(tIndex);
				u32 filePathLength = MyStrLength32(textureResource->filePath);
				u32 newFilePathLength = MyStrLength32(newFilePath);
				if (newFilePathLength != filePathLength || !StrCompareIgnoreCaseNt(textureResource->filePath, newFilePath))
				{
					PrintLine_I("Texture[%u] changed file path: \"%s\" -> \"%s\"", tIndex, textureResource->filePath, newFilePath);
					LoadResource(textureResource);
				}
			}
			u32 readableNameLength = MyStrLength32(textureResource->readableName);
			const char* newReadableName = GetTextureReadableName(tIndex);
			u32 newReadableNameLength = MyStrLength32(newReadableName);
			if (readableNameLength != newReadableNameLength || !StrCompareIgnoreCaseNt(textureResource->readableName, newReadableName))
			{
				PrintLine_I("Texture[%u] changed names: \"%s\" -> \"%s\"", tIndex, textureResource->readableName, newReadableName);
				ArenaPop(staticHeap, textureResource->readableName);
				textureResource->readableName = ArenaNtString(staticHeap, newReadableName);
				Assert(textureResource->readableName != nullptr);
			}
			thisResource = textureResource;
		}
		else 
		{
			Assert(tIndex == arrays->textures.length);
			WriteLine_I("New Texture resource found");
			Resource_t* newResource = BktArrayAdd(&arrays->textures, Resource_t);
			Assert(newResource != nullptr);
			ClearPointer(newResource);
			newResource->type = ResourceType_Texture;
			newResource->subIndex = tIndex;
			newResource->readableName = ArenaNtString(staticHeap, GetTextureReadableName(tIndex));
			Assert(newResource->readableName != nullptr);
			thisResource = newResource;
		}
		if (ShouldLoadResourceAlways(ResourceType_Texture, tIndex) && !IsResourceLoaded(thisResource))
		{
			PrintLine_I("Texture[%u] %s should be loaded always now", tIndex, thisResource->readableName);
			LoadResource(thisResource);
			thisResource->wasPreloaded = true;
		}
	}
	for (u32 mIndex = 0; mIndex < NUM_RESOURCE_MUSICS; mIndex++)
	{
		Resource_t* thisResource = nullptr;
		if (mIndex < arrays->musics.length)
		{
			Resource_t* musicResource = BktArrayGet(&arrays->musics, Resource_t, mIndex);
			Assert(musicResource != nullptr);
			if (IsResourceLoaded(musicResource))
			{
				Assert(musicResource->filePath != nullptr);
				const char* newFilePath = GetMusicFilePath(mIndex);
				u32 filePathLength = MyStrLength32(musicResource->filePath);
				u32 newFilePathLength = MyStrLength32(newFilePath);
				if (newFilePathLength != filePathLength || !StrCompareIgnoreCaseNt(musicResource->filePath, newFilePath))
				{
					if (GetSndInstanceBySrc(&musicResource->music) == nullptr)
					{
						PrintLine_I("Music[%u] changed file path: \"%s\" -> \"%s\"", mIndex, musicResource->filePath, newFilePath);
						LoadResource(musicResource);
					}
					else
					{
						PrintLine_W("Can't reload Music[%u] because it is currently playing: \"%s\" -> \"%s\"", mIndex, musicResource->filePath, newFilePath);
					}
				}
			}
			u32 readableNameLength = MyStrLength32(musicResource->readableName);
			const char* newReadableName = GetMusicReadableName(mIndex);
			u32 newReadableNameLength = MyStrLength32(newReadableName);
			if (readableNameLength != newReadableNameLength || !StrCompareIgnoreCaseNt(musicResource->readableName, newReadableName))
			{
				PrintLine_I("Music[%u] changed names: \"%s\" -> \"%s\"", mIndex, musicResource->readableName, newReadableName);
				ArenaPop(staticHeap, musicResource->readableName);
				musicResource->readableName = ArenaNtString(staticHeap, newReadableName);
				Assert(musicResource->readableName != nullptr);
			}
			thisResource = musicResource;
		}
		else 
		{
			Assert(mIndex == arrays->musics.length);
			WriteLine_I("New Music resource found");
			Resource_t* newResource = BktArrayAdd(&arrays->musics, Resource_t);
			Assert(newResource != nullptr);
			ClearPointer(newResource);
			newResource->type = ResourceType_Music;
			newResource->subIndex = mIndex;
			newResource->readableName = ArenaNtString(staticHeap, GetMusicReadableName(mIndex));
			Assert(newResource->readableName != nullptr);
			thisResource = newResource;
		}
		if (ShouldLoadResourceAlways(ResourceType_Music, mIndex) && !IsResourceLoaded(thisResource))
		{
			PrintLine_I("Music[%u] %s should be loaded always now", mIndex, thisResource->readableName);
			LoadResource(thisResource);
			thisResource->wasPreloaded = true;
		}
	}
	for (u32 fIndex = 0; fIndex < NUM_RESOURCE_FONTS; fIndex++)
	{
		Resource_t* thisResource = nullptr;
		if (fIndex < arrays->fonts.length)
		{
			Resource_t* fontResource = BktArrayGet(&arrays->fonts, Resource_t, fIndex);
			Assert(fontResource != nullptr);
			if (IsResourceLoaded(fontResource))
			{
				Assert(fontResource->filePath != nullptr);
				const char* newFilePath = GetFontFilePath(fIndex);
				u32 filePathLength = MyStrLength32(fontResource->filePath);
				u32 newFilePathLength = MyStrLength32(newFilePath);
				if (newFilePathLength != filePathLength || !StrCompareIgnoreCaseNt(fontResource->filePath, newFilePath))
				{
					PrintLine_I("Font[%u] changed file path: \"%s\" -> \"%s\"", fIndex, fontResource->filePath, newFilePath);
					LoadResource(fontResource);
				}
			}
			u32 readableNameLength = MyStrLength32(fontResource->readableName);
			const char* newReadableName = GetFontReadableName(fIndex);
			u32 newReadableNameLength = MyStrLength32(newReadableName);
			if (readableNameLength != newReadableNameLength || !StrCompareIgnoreCaseNt(fontResource->readableName, newReadableName))
			{
				PrintLine_I("Font[%u] changed names: \"%s\" -> \"%s\"", fIndex, fontResource->readableName, newReadableName);
				ArenaPop(staticHeap, fontResource->readableName);
				fontResource->readableName = ArenaNtString(staticHeap, newReadableName);
				Assert(fontResource->readableName != nullptr);
			}
			thisResource = fontResource;
		}
		else 
		{
			Assert(fIndex == arrays->fonts.length);
			WriteLine_I("New Font resource found");
			Resource_t* newResource = BktArrayAdd(&arrays->fonts, Resource_t);
			Assert(newResource != nullptr);
			ClearPointer(newResource);
			newResource->type = ResourceType_Font;
			newResource->subIndex = fIndex;
			newResource->readableName = ArenaNtString(staticHeap, GetFontReadableName(fIndex));
			Assert(newResource->readableName != nullptr);
			thisResource = newResource;
		}
		if (ShouldLoadResourceAlways(ResourceType_Font, fIndex) && !IsResourceLoaded(thisResource))
		{
			PrintLine_I("Font[%u] %s should be loaded always now", fIndex, thisResource->readableName);
			LoadResource(thisResource);
			thisResource->wasPreloaded = true;
		}
	}
	for (u32 sIndex = 0; sIndex < NUM_RESOURCE_SHADERS; sIndex++)
	{
		Resource_t* thisResource = nullptr;
		if (sIndex < arrays->shaders.length)
		{
			Resource_t* shaderResource = BktArrayGet(&arrays->shaders, Resource_t, sIndex);
			Assert(shaderResource != nullptr);
			if (IsResourceLoaded(shaderResource))
			{
				Assert(shaderResource->filePath != nullptr);
				const char* newFilePath = GetShaderFilePath(sIndex);
				u32 filePathLength = MyStrLength32(shaderResource->filePath);
				u32 newFilePathLength = MyStrLength32(newFilePath);
				if (newFilePathLength != filePathLength || !StrCompareIgnoreCaseNt(shaderResource->filePath, newFilePath))
				{
					PrintLine_I("Shader[%u] changed file path: \"%s\" -> \"%s\"", sIndex, shaderResource->filePath, newFilePath);
					LoadResource(shaderResource);
				}
			}
			u32 readableNameLength = MyStrLength32(shaderResource->readableName);
			const char* newReadableName = GetShaderReadableName(sIndex);
			u32 newReadableNameLength = MyStrLength32(newReadableName);
			if (readableNameLength != newReadableNameLength || !StrCompareIgnoreCaseNt(shaderResource->readableName, newReadableName))
			{
				PrintLine_I("Shader[%u] changed names: \"%s\" -> \"%s\"", sIndex, shaderResource->readableName, newReadableName);
				ArenaPop(staticHeap, shaderResource->readableName);
				shaderResource->readableName = ArenaNtString(staticHeap, newReadableName);
				Assert(shaderResource->readableName != nullptr);
			}
			thisResource = shaderResource;
		}
		else 
		{
			Assert(sIndex == arrays->shaders.length);
			WriteLine_I("New Shader resource found");
			Resource_t* newResource = BktArrayAdd(&arrays->shaders, Resource_t);
			Assert(newResource != nullptr);
			ClearPointer(newResource);
			newResource->type = ResourceType_Shader;
			newResource->subIndex = sIndex;
			newResource->readableName = ArenaNtString(staticHeap, GetShaderReadableName(sIndex));
			Assert(newResource->readableName != nullptr);
			thisResource = newResource;
		}
		if (ShouldLoadResourceAlways(ResourceType_Shader, sIndex) && !IsResourceLoaded(thisResource))
		{
			PrintLine_I("Shader[%u] %s should be loaded always now", sIndex, thisResource->readableName);
			LoadResource(thisResource);
			thisResource->wasPreloaded = true;
		}
	}
}

// +--------------------------------------------------------------+
// |                  Lookup By Global Functions                  |
// +--------------------------------------------------------------+
Resource_t* GetResourceByType(ResourceType_t type, u32 subIndex)
{
	Assert(resources != nullptr);
	switch (type)
	{
		case ResourceType_SpriteSheet:
		{
			Assert(subIndex < resources->sheets.length);
			Resource_t* result = BktArrayGet(&resources->sheets, Resource_t, subIndex);
			Assert(result != nullptr);
			Assert(result->type == type);
			return result;
		} break;
		case ResourceType_Texture:
		{
			Assert(subIndex < resources->textures.length);
			Resource_t* result = BktArrayGet(&resources->textures, Resource_t, subIndex);
			Assert(result != nullptr);
			Assert(result->type == type);
			return result;
		} break;
		case ResourceType_Music:
		{
			Assert(subIndex < resources->musics.length);
			Resource_t* result = BktArrayGet(&resources->musics, Resource_t, subIndex);
			Assert(result != nullptr);
			Assert(result->type == type);
			return result;
		} break;
		case ResourceType_Font:
		{
			Assert(subIndex < resources->fonts.length);
			Resource_t* result = BktArrayGet(&resources->fonts, Resource_t, subIndex);
			Assert(result != nullptr);
			Assert(result->type == type);
			return result;
		} break;
		case ResourceType_Shader:
		{
			Assert(subIndex < resources->shaders.length);
			Resource_t* result = BktArrayGet(&resources->shaders, Resource_t, subIndex);
			Assert(result != nullptr);
			Assert(result->type == type);
			return result;
		} break;
		default: Assert(false); return nullptr;
	}
}
Resource_t* GetResourceByResourceIndex(u32 resourceIndex)
{
	Assert(resourceIndex < NUM_RESOURCES);
	u32 subIndex = resourceIndex;
	if (subIndex < NUM_RESOURCE_SHEETS)
	{
		return GetResourceByType(ResourceType_SpriteSheet, subIndex);
	}
	subIndex -= NUM_RESOURCE_SHEETS;
	if (subIndex < NUM_RESOURCE_TEXTURES)
	{
		return GetResourceByType(ResourceType_Texture, subIndex);
	}
	subIndex -= NUM_RESOURCE_TEXTURES;
	if (subIndex < NUM_RESOURCE_MUSICS)
	{
		return GetResourceByType(ResourceType_Music, subIndex);
	}
	subIndex -= NUM_RESOURCE_MUSICS;
	if (subIndex < NUM_RESOURCE_FONTS)
	{
		return GetResourceByType(ResourceType_Font, subIndex);
	}
	subIndex -= NUM_RESOURCE_FONTS;
	if (subIndex < NUM_RESOURCE_SHADERS)
	{
		return GetResourceByType(ResourceType_Shader, subIndex);
	}
	subIndex -= NUM_RESOURCE_SHADERS;
	Assert(false);
	return nullptr;
}
Resource_t* GetResourceByVoidPntr(const void* pntr)
{
	if (pntr == nullptr) { return nullptr; }
	for (u32 rIndex = 0; rIndex < NUM_RESOURCES; rIndex++)
	{
		Resource_t* resource = GetResourceByResourceIndex(rIndex);
		Assert(resource != nullptr);
		switch (resource->type)
		{
			case ResourceType_SpriteSheet: if (pntr == (const void*)(&resource->sheet))   { return resource; } break;
			case ResourceType_Texture:     if (pntr == (const void*)(&resource->texture)) { return resource; } break;
			case ResourceType_Music:       if (pntr == (const void*)(&resource->music))   { return resource; } break;
			case ResourceType_Font:        if (pntr == (const void*)(&resource->font))    { return resource; } break;
			case ResourceType_Shader:      if (pntr == (const void*)(&resource->shader))  { return resource; } break;
		}
	}
	return nullptr;
}
Resource_t* GetResourceByFilePath(const char* filePath, u32 pathLength)
{
	Assert(filePath != nullptr || pathLength == 0);
	if (pathLength == 0) { return nullptr; }
	for (u32 rIndex = 0; rIndex < NUM_RESOURCES; rIndex++)
	{
		Resource_t* resource = GetResourceByResourceIndex(rIndex);
		Assert(resource != nullptr);
		if (resource->filePath != nullptr)
		{
			u32 resourcePathLength = MyStrLength32(resource->filePath);
			if (pathLength == resourcePathLength && StrCompareIgnoreCase(filePath, resource->filePath, resourcePathLength))
			{
				return resource;
			}
		}
		else
		{
			const char* wouldBePath = nullptr;
			switch (resource->type)
			{
				case ResourceType_SpriteSheet: wouldBePath = GetSpriteSheetFilePath(resource->subIndex); break;
				case ResourceType_Texture:     wouldBePath = GetTextureFilePath(resource->subIndex);     break;
				case ResourceType_Music:       wouldBePath = GetMusicFilePath(resource->subIndex);       break;
				case ResourceType_Font:        wouldBePath = GetFontFilePath(resource->subIndex);        break;
				case ResourceType_Shader:      wouldBePath = GetShaderFilePath(resource->subIndex);      break;
			}
			Assert(wouldBePath != nullptr);
			u32 wouldBePathLength = MyStrLength32(wouldBePath);
			if (pathLength == wouldBePathLength && StrCompareIgnoreCase(filePath, wouldBePath, wouldBePathLength))
			{
				return resource;
			}
		}
	}
	return nullptr;
}
Resource_t* GetResourceByFilePathNt(const char* filePath)
{
	return GetResourceByFilePath(filePath, MyStrLength32(filePath));
}
#if !USE_CUSTOM_AUDIO
Resource_t* GetResourceBySoundId(ALuint soundId)
{
	for (u32 mIndex = 0; mIndex < NUM_RESOURCE_MUSICS; mIndex++)
	{
		Resource_t* musicResource = GetResourceByType(ResourceType_Music, mIndex);
		Assert(musicResource != nullptr);
		if (IsResourceLoaded(musicResource) && musicResource->music.id == soundId)
		{
			return musicResource;
		}
	}
	return nullptr;
}
#endif

bool IsFileResource(const char* filePath, u32* resourceIndexOut = nullptr)
{
	Assert(filePath != nullptr);
	for (u32 rIndex = 0; rIndex < NUM_RESOURCES; rIndex++)
	{
		Resource_t* resource = GetResourceByResourceIndex(rIndex);
		Assert(resource != nullptr);
		if (resource->filePath != nullptr && StrCompareIgnoreCaseNt(resource->filePath, filePath))
		{
			if (resourceIndexOut != nullptr) { *resourceIndexOut = rIndex; }
			return true;
		}
		if (resource->filePath != nullptr && resource->type == ResourceType_Font)
		{
			TempPushMark();
			char* metaFilePath = StrReplace(TempArena, resource->filePath, MyStrLength32(resource->filePath), ".png", 4, ".meta", 5);
			if (MyStrCompareNt(metaFilePath, filePath) == 0)
			{
				if (resourceIndexOut != nullptr) { *resourceIndexOut = rIndex; }
				TempPopMark();
				return true;
			}
			TempPopMark();
		}
	}
	return false;
}

// +--------------------------------------------------------------+
// |                            Update                            |
// +--------------------------------------------------------------+
void UpdateResources()
{
	Assert(resources != nullptr);
	for (u32 rIndex = 0; rIndex < NUM_RESOURCES; rIndex++)
	{
		Resource_t* resource = GetResourceByResourceIndex(rIndex);
		if (IsResourceLoaded(resource) && !ShouldLoadResourceAlways(resource->type, resource->subIndex))
		{
			if (TimeSince(resource->lastAccessTime) >= RESOURCE_AUTO_UNLOAD_TIME)
			{
				PrintLine_D("Unloading %s[%u]: %s \"%s\"", GetResourceTypeStr(resource->type), resource->subIndex, resource->readableName, GetFileNamePart(resource->filePath));
				// PrintLine_D("%llu -> %llu", resource->lastAccessTime, ProgramTime);
				UnloadResource(resource);
			}
		}
	}
}

// +--------------------------------------------------------------+
// |              Get Resource Functions and Macros               |
// +--------------------------------------------------------------+
Texture_t* GetTexture_(u32 textureIndex, bool isPreload)
{
	Assert(resources != nullptr);
	Assert(textureIndex < resources->textures.length);
	Resource_t* textureResource = BktArrayGet(&resources->textures, Resource_t, textureIndex);
	Assert(textureResource != nullptr);
	Assert(textureResource->type == ResourceType_Texture);
	if (!IsResourceLoaded(textureResource))
	{
		LoadResource(textureResource);
		textureResource->wasPreloaded = isPreload;
		textureResource->lastLoadTime = ProgramTime;
	}
	UpdateResourceAccessTime(textureResource);
	return &textureResource->texture;
}
bool IsTextureLoaded_(u32 textureIndex)
{
	Assert(resources != nullptr);
	Assert(textureIndex < resources->textures.length);
	Resource_t* textureResource = BktArrayGet(&resources->textures, Resource_t, textureIndex);
	Assert(textureResource != nullptr);
	return IsResourceLoaded(textureResource);
}
#define GetTextureIndexByName(textureName) (u32)(&((ResourceTextures_t*)0)->textureName - &((ResourceTextures_t*)0)->array[0])
#define GetTexture(textureName)            GetTexture_(GetTextureIndexByName(textureName), false)
#define PreloadTexture(textureName)        GetTexture_(GetTextureIndexByName(textureName), true)
#define IsTextureLoaded(textureName)       IsTextureLoaded_(GetTextureIndexByName(textureName))
#define GetTextureWidth(textureName)       (GetTexture(textureName)->width)
#define GetTextureHeight(textureName)      (GetTexture(textureName)->height)
#define GetTextureSize(textureName)        (GetTexture(textureName)->size)
#define GetTextureSizef(textureName)       NewVec2(GetTexture(textureName)->size)

SpriteSheet_t* GetSheet_(u32 sheetIndex, bool isPreload)
{
	Assert(resources != nullptr);
	Assert(sheetIndex < resources->sheets.length);
	Resource_t* sheetResource = BktArrayGet(&resources->sheets, Resource_t, sheetIndex);
	Assert(sheetResource != nullptr);
	Assert(sheetResource->type == ResourceType_SpriteSheet);
	if (!IsResourceLoaded(sheetResource))
	{
		LoadResource(sheetResource);
		sheetResource->wasPreloaded = isPreload;
		sheetResource->lastLoadTime = ProgramTime;
	}
	UpdateResourceAccessTime(sheetResource);
	return &sheetResource->sheet;
}
bool IsSheetLoaded_(u32 sheetIndex)
{
	Assert(resources != nullptr);
	Assert(sheetIndex < resources->sheets.length);
	Resource_t* sheetResource = BktArrayGet(&resources->sheets, Resource_t, sheetIndex);
	Assert(sheetResource != nullptr);
	return IsResourceLoaded(sheetResource);
}
#define GetSheetIndexByName(sheetName) (u32)(&((ResourceSheets_t*)0)->sheetName - &((ResourceSheets_t*)0)->array[0])
#define GetSheet(sheetName)            GetSheet_(GetSheetIndexByName(sheetName), false)
#define PreloadSheet(sheetName)        GetSheet_(GetSheetIndexByName(sheetName), true)
#define IsSheetLoaded(sheetName)       IsSheetLoaded_(GetSheetIndexByName(sheetName))
#define GetSheetWidth(sheetName)       (GetSheet(sheetName)->framesX)
#define GetSheetHeight(sheetName)      (GetSheet(sheetName)->framesY)
#define GetSheetSize(sheetName)        (GetSheet(sheetName)->sheetSize)
#define GetSheetSizef(sheetName)       NewVec2(GetSheet(sheetName)->sheetSize)
#define GetSheetFrameWidth(sheetName)  (GetSheet(sheetName)->innerX)
#define GetSheetFrameHeight(sheetName) (GetSheet(sheetName)->innerY)
#define GetSheetFrameSize(sheetName)   (GetSheet(sheetName)->innerFrameSize)
#define GetSheetFrameSizef(sheetName)  NewVec2(GetSheet(sheetName)->innerFrameSize)

Sound_t* GetMusic_(u32 musicIndex)
{
	Assert(resources != nullptr);
	Assert(musicIndex < resources->musics.length);
	Resource_t* musicResource = BktArrayGet(&resources->musics, Resource_t, musicIndex);
	Assert(musicResource != nullptr);
	Assert(musicResource->type == ResourceType_Music);
	UpdateResourceAccessTime(musicResource);
	return &musicResource->music;
}
#define GetMusicIndexByName(musicName)    (u32)(&((ResourceMusics_t*)0)->musicName - &((ResourceMusics_t*)0)->array[0])
#define GetMusic(musicName)               GetMusic_(GetMusicIndexByName(musicName))

Font_t* GetFont_(u32 fontIndex, bool isPreload)
{
	Assert(resources != nullptr);
	Assert(fontIndex < resources->fonts.length);
	Resource_t* fontResource = BktArrayGet(&resources->fonts, Resource_t, fontIndex);
	Assert(fontResource != nullptr);
	Assert(fontResource->type == ResourceType_Font);
	if (!IsResourceLoaded(fontResource))
	{
		LoadResource(fontResource);
		fontResource->wasPreloaded = isPreload;
		fontResource->lastLoadTime = ProgramTime;
	}
	UpdateResourceAccessTime(fontResource);
	return &fontResource->font;
}
bool IsFontLoaded_(u32 fontIndex)
{
	Assert(resources != nullptr);
	Assert(fontIndex < resources->fonts.length);
	Resource_t* fontResource = BktArrayGet(&resources->fonts, Resource_t, fontIndex);
	Assert(fontResource != nullptr);
	return IsResourceLoaded(fontResource);
}
#define GetFontIndexByName(fontName)    (u32)(&((ResourceFonts_t*)0)->fontName - &((ResourceFonts_t*)0)->array[0])
#define GetFont(fontName)               GetFont_(GetFontIndexByName(fontName), false)
#define PreloadFont(fontName)           GetFont_(GetFontIndexByName(fontName), true)
#define IsFontLoaded(fontName)          IsFontLoaded_(GetFontIndexByName(fontName))
#define GetFontFontSize(fontName)       (GetFont(fontName)->fontSize)
#define GetFontMaxExtendUp(fontName)    (GetFont(fontName)->maxExtendUp)
#define GetFontMaxExtendDown(fontName)  (GetFont(fontName)->maxExtendDown)
#define GetFontLineHeight(fontName)     (GetFont(fontName)->lineHeight)
#define IsFontBitmap(fontName)          (GetFont(fontName)->isBitmapFont)

Shader_t* GetShader_(u32 shaderIndex, bool isPreload)
{
	Assert(resources != nullptr);
	Assert(shaderIndex < resources->shaders.length);
	Resource_t* shaderResource = BktArrayGet(&resources->shaders, Resource_t, shaderIndex);
	Assert(shaderResource != nullptr);
	Assert(shaderResource->type == ResourceType_Shader);
	if (!IsResourceLoaded(shaderResource))
	{
		LoadResource(shaderResource);
		shaderResource->wasPreloaded = isPreload;
		shaderResource->lastLoadTime = ProgramTime;
	}
	UpdateResourceAccessTime(shaderResource);
	return &shaderResource->shader;
}
bool IsShaderLoaded_(u32 shaderIndex)
{
	Assert(resources != nullptr);
	Assert(shaderIndex < resources->shaders.length);
	Resource_t* shaderResource = BktArrayGet(&resources->shaders, Resource_t, shaderIndex);
	Assert(shaderResource != nullptr);
	return IsResourceLoaded(shaderResource);
}
#define GetShaderIndexByName(shaderName) (u32)(&((ResourceShaders_t*)0)->shaderName - &((ResourceShaders_t*)0)->array[0])
#define GetShader(shaderName)            GetShader_(GetShaderIndexByName(shaderName), false)
#define PreloadShader(shaderName)        GetShader_(GetShaderIndexByName(shaderName), true)
#define IsShaderLoaded(shaderName)       IsShaderLoaded_(GetShaderIndexByName(shaderName))

// +--------------------------------------------------------------+
// |                       Preload Function                       |
// +--------------------------------------------------------------+
void PreloadResourcesForAppState(AppState_t appState)
{
	#define RESOURCE_SHEET(varName, readableName, filePath, sheetSize, appStatesBits) do \
	{                                                                                    \
		u32 stateBits = (u32)(appStatesBits);                                            \
		if (IsAppStateInAppStateBits(stateBits, appState))                               \
		{                                                                                \
			PreloadSheet(varName);                                                       \
		}                                                                                \
	} while(0)
	#define RESOURCE_TEXTURE(varName, readableName, filePath, pixelated, looping, appStatesBits) do \
	{                                                                                               \
		u32 stateBits = (u32)(appStatesBits);                                                       \
		if (IsAppStateInAppStateBits(stateBits, appState))                                          \
		{                                                                                           \
			PreloadTexture(varName);                                                                \
		}                                                                                           \
	} while(0)
	#include "resources/res_resource_list.h"
}
