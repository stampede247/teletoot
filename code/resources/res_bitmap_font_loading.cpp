/*
File:   res_bitmap_font_loading.cpp
Author: Taylor Robbins
Date:   01\27\2020
Description: 
	** Holds functions that handle serializing and deserializing bitmap fonts
	** to and from .png + .meta file pairs
*/

#define BITMAP_FONT_META_FILE_HEADER "# Teletoot Font Meta :3"
#define BITMAP_FONT_META_FILE_HEADER_LENGTH 23

struct BitmapFontCharMetaInfo_t
{
	u8 charValue;
	v2i position;
	v2i size;
	v2 offset;
	r32 advanceX;
};
struct BitmapFontMetaInfo_t
{
	u8 firstCharacter;
	u8 numCharacters;
	v2i cellSize;
	r32 lineHeight;
	v2i defaultCharPos;
	v2i defaultCharSize;
	v2 defaultCharOffset;
	r32 defaultAdvanceX;
	u8 unknownCharValue;
	DynArray_t charInfos;
};

// +--------------------------------------------------------------+
// |                  Deserialize Font Meta File                  |
// +--------------------------------------------------------------+
bool TryDeserializeBitmapFontMetaFile(MemoryArena_t* memArena, BitmapFontMetaInfo_t* metaInfoOut, const char* fileContents, u32 fileSize)
{
	Assert(memArena != nullptr);
	Assert(metaInfoOut != nullptr);
	Assert(fileSize == 0 || fileContents != nullptr);
	if (fileSize == 0) { return false; } //can't be an empty file
	if (fileSize < BITMAP_FONT_META_FILE_HEADER_LENGTH) { return false; } //has to have space for header
	if (MyMemCompare(&fileContents[0], BITMAP_FONT_META_FILE_HEADER, BITMAP_FONT_META_FILE_HEADER_LENGTH) != 0) { return false; } //header has to be correct
	
	const char* errorStr = nullptr;
	TextParser_t parser = {};
	CreateTextParser(&parser, &fileContents[BITMAP_FONT_META_FILE_HEADER_LENGTH], fileSize - BITMAP_FONT_META_FILE_HEADER_LENGTH);
	
	bool foundFirstCharacter      = false; //First Character
	u8 firstCharacter             = 0x00;
	bool foundNumCharacters       = false; //Num Characters
	u8 numCharacters              = 0;
	bool foundCellSize            = false; //Cell Size
	v2i cellSize                  = Vec2i_Zero;
	bool foundLineHeight          = false; //Line Height
	r32 lineHeight                = 0;
	bool foundDefaultCharPosition = false; //Default Char Position
	v2i defaultCharPosition       = Vec2i_Zero;
	bool foundDefaultCharSize     = false; //Default Char Size
	v2i defaultCharSize           = Vec2i_Zero;
	bool foundDefaultCharOffset   = false; //Default Char Offset
	v2 defaultCharOffset          = Vec2_Zero;
	bool foundDefaultAdvanceX     = false; //Default Advance X
	r32 defaultAdvanceX           = 0;
	bool foundNumCharInfos        = false; //Num Char Infos
	u32 numCharInfos              = 0;
	bool foundUnknownCharValue    = false; //Unknown Char
	u8 unknownCharValue           = 0x00;
	
	// +==============================+
	// |        Parse Headers         |
	// +==============================+
	ParsingToken_t token;
	bool foundCharactersTag = false;
	while (parser.position < parser.contentLength && !foundCharactersTag)
	{
		errorStr = ParserConsumeToken(&parser, &token);
		if (errorStr != nullptr)
		{
			PrintLine_W("Error while parsing bitmap font meta line %u: %s", parser.lineNumber, errorStr);
			return false;
		}
		if (!parser.foundToken)
		{
			Assert(parser.position >= parser.contentLength);
			break;
		}
		
		if (token.type == ParsingTokenType_KeyValue)
		{
			if (DoesSplitPieceEqual(&token.key, "First Character"))
			{
				errorStr = TryDeserializeU8(token.value.pntr, token.value.length, &firstCharacter);
				if (errorStr == nullptr) { foundFirstCharacter = true; }
				else
				{
					PrintLine_E("Couldn't parse bitmap font meta \"First Character\" Value: \"%.*s\" %s", token.value.length, token.value.pntr, errorStr);
					return false;
				}
			}
			else if (DoesSplitPieceEqual(&token.key, "Num Characters"))
			{
				errorStr = TryDeserializeU8(token.value.pntr, token.value.length, &numCharacters);
				if (errorStr == nullptr) { foundNumCharacters = true; }
				else
				{
					PrintLine_E("Couldn't parse bitmap font meta \"Num Characters\" Value: \"%.*s\" %s", token.value.length, token.value.pntr, errorStr);
					return false;
				}
			}
			else if (DoesSplitPieceEqual(&token.key, "Cell Size"))
			{
				errorStr = TryDeserializeVec2i(token.value.pntr, token.value.length, &cellSize);
				if (errorStr == nullptr) { foundCellSize = true; }
				else
				{
					PrintLine_E("Couldn't parse bitmap font meta \"Cell Size\" Value: \"%.*s\" %s", token.value.length, token.value.pntr, errorStr);
					return false;
				}
			}
			else if (DoesSplitPieceEqual(&token.key, "Line Height"))
			{
				errorStr = TryDeserializeR32(token.value.pntr, token.value.length, &lineHeight);
				if (errorStr == nullptr) { foundLineHeight = true; }
				else
				{
					PrintLine_E("Couldn't parse bitmap font meta \"Line Height\" Value: \"%.*s\" %s", token.value.length, token.value.pntr, errorStr);
					return false;
				}
			}
			else if (DoesSplitPieceEqual(&token.key, "Default Char Position"))
			{
				errorStr = TryDeserializeVec2i(token.value.pntr, token.value.length, &defaultCharPosition);
				if (errorStr == nullptr) { foundDefaultCharPosition = true; }
				else
				{
					PrintLine_E("Couldn't parse bitmap font meta \"Default Char Position\" Value: \"%.*s\" %s", token.value.length, token.value.pntr, errorStr);
					return false;
				}
			}
			else if (DoesSplitPieceEqual(&token.key, "Default Char Size"))
			{
				errorStr = TryDeserializeVec2i(token.value.pntr, token.value.length, &defaultCharSize);
				if (errorStr == nullptr) { foundDefaultCharSize = true; }
				else
				{
					PrintLine_E("Couldn't parse bitmap font meta \"Default Char Size\" Value: \"%.*s\" %s", token.value.length, token.value.pntr, errorStr);
					return false;
				}
			}
			else if (DoesSplitPieceEqual(&token.key, "Default Char Offset"))
			{
				errorStr = TryDeserializeVec2(token.value.pntr, token.value.length, &defaultCharOffset);
				if (errorStr == nullptr) { foundDefaultCharOffset = true; }
				else
				{
					PrintLine_E("Couldn't parse bitmap font meta \"Default Char Offset\" Value: \"%.*s\" %s", token.value.length, token.value.pntr, errorStr);
					return false;
				}
			}
			else if (DoesSplitPieceEqual(&token.key, "Default Advance X"))
			{
				errorStr = TryDeserializeR32(token.value.pntr, token.value.length, &defaultAdvanceX);
				if (errorStr == nullptr) { foundDefaultAdvanceX = true; }
				else
				{
					PrintLine_E("Couldn't parse bitmap font meta \"Default Advance X\" Value: \"%.*s\" %s", token.value.length, token.value.pntr, errorStr);
					return false;
				}
			}
			else if (DoesSplitPieceEqual(&token.key, "Num Char Infos"))
			{
				errorStr = TryDeserializeU32(token.value.pntr, token.value.length, &numCharInfos);
				if (errorStr == nullptr) { foundNumCharInfos = true; }
				else
				{
					PrintLine_E("Couldn't parse bitmap font meta \"Num Char Infos\" Value: \"%.*s\" %s", token.value.length, token.value.pntr, errorStr);
					return false;
				}
			}
			else if (DoesSplitPieceEqual(&token.key, "Unknown Char"))
			{
				errorStr = TryDeserializeU8(token.value.pntr, token.value.length, &unknownCharValue);
				if (errorStr == nullptr) { foundUnknownCharValue = true; }
				else
				{
					PrintLine_E("Couldn't parse bitmap font meta \"Unknown Char\" Value: \"%.*s\" %s", token.value.length, token.value.pntr, errorStr);
					return false;
				}
			}
			else
			{
				PrintLine_W("Unknown key in font meta info file: \"%.*s\"", token.key.length, token.key.pntr);
			}
		}
		else if (token.type == ParsingTokenType_Header)
		{
			if (DoesSplitPieceEqual(&token.header, "Characters"))
			{
				foundCharactersTag = true;
			}
			else
			{
				PrintLine_W("Unknown tag in font meta info file: \"%.*s\"", token.header.length, token.header.pntr);
			}
		}
		else if (token.type == ParsingTokenType_Comment)
		{
			//Comments are fine
		}
		else
		{
			PrintLine_W("1Unknown %u byte token found in font meta info file line %u: 0x%02X \"%.*s\"", token.length, parser.lineNumber, token.type, token.length, &parser.content[token.start]);
		}
	}
	
	// +===============================+
	// | Make Sure All Headers Defined |
	// +===============================+
	bool foundAllHeaders = true;
	if (!foundFirstCharacter)      { WriteLine_E("No \"First Character\" defined in bitmap font meta info");       foundAllHeaders = false; }
	if (!foundNumCharacters)       { WriteLine_E("No \"Num Characters\" defined in bitmap font meta info");        foundAllHeaders = false; }
	if (!foundCellSize)            { WriteLine_E("No \"Cell Size\" defined in bitmap font meta info");             foundAllHeaders = false; }
	if (!foundLineHeight)          { WriteLine_E("No \"Line Height\" defined in bitmap font meta info");           foundAllHeaders = false; }
	if (!foundDefaultCharPosition) { WriteLine_E("No \"Default Char Position\" defined in bitmap font meta info"); foundAllHeaders = false; }
	if (!foundDefaultCharSize)     { WriteLine_E("No \"Default Char Size\" defined in bitmap font meta info");     foundAllHeaders = false; }
	if (!foundDefaultCharOffset)   { WriteLine_E("No \"Default Char Offset\" defined in bitmap font meta info");   foundAllHeaders = false; }
	if (!foundDefaultAdvanceX)     { WriteLine_E("No \"Default Advance X\" defined in bitmap font meta info");     foundAllHeaders = false; }
	if (!foundNumCharInfos)        { WriteLine_E("No \"Num Char Infos\" defined in bitmap font meta info");        foundAllHeaders = false; }
	if (!foundUnknownCharValue)    { WriteLine_E("No \"Unknown Char\" defined in bitmap font meta info");          foundAllHeaders = false; }
	if (!foundAllHeaders) { return false; }
	
	ClearPointer(metaInfoOut);
	CreateDynamicArray(&metaInfoOut->charInfos, memArena, sizeof(BitmapFontMetaInfo_t), numCharacters, numCharacters);
	metaInfoOut->firstCharacter    = firstCharacter;
	metaInfoOut->numCharacters     = numCharacters;
	metaInfoOut->cellSize          = cellSize;
	metaInfoOut->lineHeight        = lineHeight;
	metaInfoOut->defaultCharPos    = defaultCharPosition;
	metaInfoOut->defaultCharSize   = defaultCharSize;
	metaInfoOut->defaultCharOffset = defaultCharOffset;
	metaInfoOut->defaultAdvanceX   = defaultAdvanceX;
	metaInfoOut->unknownCharValue  = unknownCharValue;
	
	// +==============================+
	// |    Parse Character Infos     |
	// +==============================+
	u32 numCharInfosFound = 0;
	while (parser.position < parser.contentLength)
	{
		errorStr = ParserConsumeToken(&parser, &token);
		if (errorStr != nullptr)
		{
			PrintLine_W("Error while parsing bitmap font meta line %u: %s", parser.lineNumber, errorStr);
			return false;
		}
		if (!parser.foundToken)
		{
			Assert(parser.position >= parser.contentLength);
			break;
		}
		
		if (token.type == ParsingTokenType_KeyValue)
		{
			if (numCharInfosFound >= numCharInfos)
			{
				PrintLine_E("Found too many char infos in bitmap font meta info file: %u declared", numCharInfos);
				if (memArena->type != MemoryArenaType_Temp) { DestroyDynamicArray(&metaInfoOut->charInfos); }
				return false;
			}
			
			u8 charValue = 0x00;
			errorStr = TryDeserializeU8(token.key.pntr, token.key.length, &charValue);
			if (errorStr == nullptr)
			{
				BitmapFontCharMetaInfo_t* newCharInfo = DynArrayAdd(&metaInfoOut->charInfos, BitmapFontCharMetaInfo_t);
				Assert(newCharInfo != nullptr);
				ClearPointer(newCharInfo);
				newCharInfo->charValue = charValue;
				newCharInfo->position = metaInfoOut->defaultCharPos;
				newCharInfo->size     = metaInfoOut->defaultCharSize;
				newCharInfo->offset   = metaInfoOut->defaultCharOffset;
				newCharInfo->advanceX = metaInfoOut->defaultAdvanceX;
				
				TrimSplitPieceWhitespace(&token.value);
				if (token.value.length < 2 || token.value.pntr[0] != '{' || token.value.pntr[token.value.length-1] != '}')
				{
					PrintLine_E("Character 0x%02X meta info didn't contain proper curly brace format in bitmap font meta file: \"%.*s\"", charValue, token.value.length, token.value.pntr);
					if (memArena->type != MemoryArenaType_Temp) { DestroyDynamicArray(&metaInfoOut->charInfos); }
					return false;
				}
				TempPushMark();
				u32 numInfoParts = 0;
				StrSplitPiece_t* infoParts = SplitString(TempArena, &token.value.pntr[1], token.value.length-2, "|", 1, &numInfoParts);
				Assert(infoParts != nullptr || numInfoParts == 0);
				for (u32 pIndex = 0; pIndex < numInfoParts; pIndex++)
				{
					StrSplitPiece_t* infoPart = &infoParts[pIndex];
					TrimSplitPieceWhitespace(infoPart);
					u32 numSubParts = 0;
					StrSplitPiece_t* subParts = SplitString(TempArena, infoPart->pntr, infoPart->length, "=", 1, &numSubParts);
					if (numSubParts == 2)
					{
						TrimSplitPieceWhitespace(&subParts[0]);
						TrimSplitPieceWhitespace(&subParts[1]);
						if (DoesSplitPieceEqualIgnoreCase(&subParts[0], "pos"))
						{
							errorStr = TryDeserializeVec2i(subParts[1].pntr, subParts[1].length, &newCharInfo->position);
							if (errorStr != nullptr)
							{
								PrintLine_E("Failed to parse char pos value in bitmap font meta file: \"%.*s\" %s", subParts[1].length, subParts[1].pntr, errorStr);
								if (memArena->type != MemoryArenaType_Temp) { DestroyDynamicArray(&metaInfoOut->charInfos); }
								TempPopMark();
								return false;
							}
						}
						else if (DoesSplitPieceEqualIgnoreCase(&subParts[0], "size"))
						{
							errorStr = TryDeserializeVec2i(subParts[1].pntr, subParts[1].length, &newCharInfo->size);
							if (errorStr != nullptr)
							{
								PrintLine_E("Failed to parse char size value in bitmap font meta file: \"%.*s\" %s", subParts[1].length, subParts[1].pntr, errorStr);
								if (memArena->type != MemoryArenaType_Temp) { DestroyDynamicArray(&metaInfoOut->charInfos); }
								TempPopMark();
								return false;
							}
						}
						else if (DoesSplitPieceEqualIgnoreCase(&subParts[0], "offset"))
						{
							errorStr = TryDeserializeVec2(subParts[1].pntr, subParts[1].length, &newCharInfo->offset);
							if (errorStr != nullptr)
							{
								PrintLine_E("Failed to parse char offset value in bitmap font meta file: \"%.*s\" %s", subParts[1].length, subParts[1].pntr, errorStr);
								if (memArena->type != MemoryArenaType_Temp) { DestroyDynamicArray(&metaInfoOut->charInfos); }
								TempPopMark();
								return false;
							}
						}
						else if (DoesSplitPieceEqualIgnoreCase(&subParts[0], "advance"))
						{
							errorStr = TryDeserializeR32(subParts[1].pntr, subParts[1].length, &newCharInfo->advanceX);
							if (errorStr != nullptr)
							{
								PrintLine_E("Failed to parse char advance value in bitmap font meta file: \"%.*s\" %s", subParts[1].length, subParts[1].pntr, errorStr);
								if (memArena->type != MemoryArenaType_Temp) { DestroyDynamicArray(&metaInfoOut->charInfos); }
								TempPopMark();
								return false;
							}
						}
						else
						{
							PrintLine_W("Found unknown key in char meta info \"%.*s\" in bitmap font meta file", subParts[0].length, subParts[0].pntr);
						}
					}
					else
					{
						PrintLine_E("Expected key=value in char info definition in bitmap meta file: \"%.*s\"", infoPart->length, infoPart->pntr);
						if (memArena->type != MemoryArenaType_Temp) { DestroyDynamicArray(&metaInfoOut->charInfos); }
						TempPopMark();
						return false;
					}
				}
				TempPopMark();
			}
			else
			{
				PrintLine_E("Couldn't parse key as u8 in font meta info file: \"%.*s\" %s", token.key.length, token.key.pntr, errorStr);
				if (memArena->type != MemoryArenaType_Temp) { DestroyDynamicArray(&metaInfoOut->charInfos); }
				return false;
			}
			
			numCharInfosFound++;
		}
		else if (token.type == ParsingTokenType_Comment)
		{
			//Comments are fine
		}
		else
		{
			PrintLine_W("2Unknown %u byte token found in font meta info file line %u: 0x%02X \"%.*s\"", token.length, parser.lineNumber, token.type, token.length, &parser.content[token.start]);
		}
	}
	
	if (numCharInfosFound < numCharInfos)
	{
		PrintLine_W("Found %u/%u expected char infos in bitmap font meta file", numCharInfosFound, numCharInfos);
	}
	
	return true;
}
bool LoadBitmapFont(MemoryArena_t* memArena, const char* filePath, const char* metaFilePath, Font_t* fontOut) //pre-declared in app_func_defs.h
{
	Assert(memArena != nullptr);
	Assert(filePath != nullptr);
	Assert(metaFilePath != nullptr);
	Assert(fontOut != nullptr);
	
	FileInfo_t bitmapFile = platform->ReadEntireFile(filePath);
	if (bitmapFile.content == nullptr || bitmapFile.size == 0)
	{
		PrintLine_E("Couldn't open bitmap font at \"%s\"", filePath);
		return false;
	}
	FileInfo_t metaFile = platform->ReadEntireFile(metaFilePath);
	if (metaFile.content == nullptr || metaFile.size == 0)
	{
		PrintLine_E("Couldn't find bitmap font meta info at \"%s\"", metaFilePath);
		platform->FreeFileMemory(&bitmapFile);
		return false;
	}
	
	TempPushMark();
	BitmapFontMetaInfo_t metaInfo = {};
	bool deserializeMetaSuccess = TryDeserializeBitmapFontMetaFile(TempArena, &metaInfo, (const char*)metaFile.content, metaFile.size);
	if (!deserializeMetaSuccess)
	{
		PrintLine_E("Failed to deserialize bitmap font meta info at \"%s\"", metaFilePath);
		platform->FreeFileMemory(&bitmapFile);
		platform->FreeFileMemory(&metaFile);
		TempPopMark();
		return false;
	}
	
	Texture_t bitmap = LoadTextureFromData(bitmapFile.content, bitmapFile.size, true, false);
	if (!bitmap.isValid)
	{
		PrintLine_E("Failed to deserialize bitmap font image file at \"%s\"", filePath);
		platform->FreeFileMemory(&bitmapFile);
		platform->FreeFileMemory(&metaFile);
		TempPopMark();
		return false;
	}
	
	u32 numColumns = (u32)(bitmap.width / metaInfo.cellSize.width);
	u32 numRows = (u32)(bitmap.height / metaInfo.cellSize.height);
	if (numColumns * numRows < metaInfo.numCharacters)
	{
		PrintLine_E("Bitmap is not large enough for %u characters: %ux%u sheet holds %u cells", metaInfo.numCharacters, numColumns, numRows, numColumns*numRows);
		// PrintLine_D("%dx%d %dx%d", bitmap.width, bitmap.height, metaInfo.cellSize.width, metaInfo.cellSize.height);
		platform->FreeFileMemory(&bitmapFile);
		platform->FreeFileMemory(&metaFile);
		TempPopMark();
		return false;
	}
	
	ClearPointer(fontOut);
	fontOut->allocArena = memArena;
	fontOut->isValid = true;
	fontOut->bitmap = bitmap;
	fontOut->fontSize = metaInfo.lineHeight;
	fontOut->lineHeight = metaInfo.lineHeight;
	fontOut->isBitmapFont = true;
	fontOut->firstChar = metaInfo.firstCharacter;
	fontOut->numChars = metaInfo.numCharacters;
	if (metaInfo.unknownCharValue < metaInfo.firstCharacter || metaInfo.unknownCharValue >= metaInfo.firstCharacter + metaInfo.numCharacters)
	{
		PrintLine_W("WARNING: Unknown character 0x%02X is not within bitmap font range 0x%02X-0x%02X", metaInfo.unknownCharValue, metaInfo.firstCharacter, metaInfo.firstCharacter + metaInfo.numCharacters-1);
		metaInfo.unknownCharValue = metaInfo.firstCharacter + metaInfo.numCharacters-1;
	}
	fontOut->unknownCharIndex = metaInfo.unknownCharValue - metaInfo.firstCharacter;
	
	fontOut->charInfos = PushArray(memArena, FontCharInfo_t, metaInfo.numCharacters);
	Assert(fontOut->charInfos != nullptr);
	MyMemSet(fontOut->charInfos, 0x00, sizeof(FontCharInfo_t)*metaInfo.numCharacters);
	
	fontOut->maxCharWidth = 0;
	fontOut->maxCharHeight = 0;
	fontOut->maxExtendUp = 0;
	fontOut->maxExtendDown = 0;
	for (u32 cIndex = 0; cIndex < metaInfo.numCharacters; cIndex++)
	{
		FontCharInfo_t* charInfo = &fontOut->charInfos[cIndex];
		u8 charValue = (u8)(metaInfo.firstCharacter + cIndex);
		BitmapFontCharMetaInfo_t* matchedMetaInfo = nullptr;
		for (u32 cIndex2 = 0; cIndex2 < metaInfo.charInfos.length; cIndex2++)
		{
			BitmapFontCharMetaInfo_t* charMetaInfo = DynArrayGet(&metaInfo.charInfos, BitmapFontCharMetaInfo_t, cIndex2);
			Assert(charMetaInfo != nullptr);
			if (charMetaInfo->charValue == charValue) { matchedMetaInfo = charMetaInfo; break; }
		}
		
		if (matchedMetaInfo != nullptr)
		{
			charInfo->position = matchedMetaInfo->position;
			charInfo->size     = matchedMetaInfo->size;
			charInfo->offset   = matchedMetaInfo->offset;
			charInfo->advanceX = matchedMetaInfo->advanceX;
		}
		else
		{
			charInfo->position = metaInfo.defaultCharPos;
			charInfo->size     = metaInfo.defaultCharSize;
			charInfo->offset   = metaInfo.defaultCharOffset;
			charInfo->advanceX = metaInfo.defaultAdvanceX;
		}
		v2i cellPos = NewVec2i((i32)(cIndex % numColumns), (i32)(cIndex / numColumns));
		charInfo->position.x += cellPos.x * metaInfo.cellSize.width;
		charInfo->position.y += cellPos.y * metaInfo.cellSize.height;
		if (fontOut->maxCharHeight < charInfo->size.height) { fontOut->maxCharHeight = (r32)charInfo->size.height; }
		if (fontOut->maxCharWidth < charInfo->size.width) { fontOut->maxCharWidth = (r32)charInfo->size.width; }
		if (fontOut->maxExtendUp < -charInfo->offset.y) { fontOut->maxExtendUp = -charInfo->offset.y; }
		if (fontOut->maxExtendDown < charInfo->size.height + charInfo->offset.y) { fontOut->maxExtendDown = charInfo->size.height + charInfo->offset.y; }
	}
	
	platform->FreeFileMemory(&bitmapFile);
	platform->FreeFileMemory(&metaFile);
	TempPopMark();
	return true;
}

