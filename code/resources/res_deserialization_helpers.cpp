/*
File:   res_deserialization_helpers.cpp
Author: Taylor Robbins
Date:   09\04\2019
Description: 
	** This file holds a bunch of functions that help us deserialize information from text and binary files
*/

#define STRUCT_VAR_SIZE(structureName, variableName) sizeof(((const structureName*)0)->variableName)
#define STRUCT_VAR_OFFSET(structureName, variableName) (u32)((const u8*)&((const structureName*)0)->variableName - (const u8*)((const structureName*)0))
#define STRUCT_VAR_END_OFFSET(structureName, variableName) (u32)(((const u8*)&((const structureName*)0)->variableName + sizeof(((const structureName*)0)->variableName)) - (const u8*)((const structureName*)0))
#define IS_VAR_IN_X_BYTES_OF_STRUCT(structureName, numBytes, variableName) ((numBytes) >= STRUCT_VAR_END_OFFSET((structureName), (variableName)))

#define BIN_CONTENT_CONSUME(contentPntr, index, maxIndex, type, variableToAssign, returnStatements, deserializationPurpose) do \
{                                                                                                                              \
	if ((index) + sizeof(type) > (maxIndex))                                                                                   \
	{                                                                                                                          \
		PrintLine_W("Expected %u more bytes for " #type " in %s deserialization", sizeof(type), (deserializationPurpose));     \
		returnStatements;                                                                                                      \
	}                                                                                                                          \
	variableToAssign = *((const type*)((contentPntr) + (index)));                                                              \
	index += sizeof(type);                                                                                                     \
} while(0)
#define BIN_CONTENT_CONSUME_PNTR(contentPntr, index, maxIndex, type, pointerToAssign, returnStatements, deserializationPurpose) do \
{                                                                                                                                  \
	if ((index) + sizeof(type) > (maxIndex))                                                                                       \
	{                                                                                                                              \
		PrintLine_W("Expected %u more bytes for " #type " in %s deserialization", sizeof(type), (deserializationPurpose));         \
		returnStatements;                                                                                                          \
	}                                                                                                                              \
	pointerToAssign = (const type*)((contentPntr) + (index));                                                                      \
	index += sizeof(type);                                                                                                         \
} while(0)
#define BIN_CONTENT_CONSUME_BYTES(contentPntr, index, maxIndex, numBytes, valuePntr, returnStatements, deserializationPurpose) do \
{                                                                                                                                 \
	if ((index) + (numBytes) > (maxIndex))                                                                                        \
	{                                                                                                                             \
		PrintLine_W("Expected %u more bytes in %s deserialization", (numBytes), (deserializationPurpose));                        \
		returnStatements;                                                                                                         \
	}                                                                                                                             \
	MyMemCopy((valuePntr), (contentPntr) + (index), (numBytes));                                                                  \
	index += (numBytes);                                                                                                          \
} while(0)

#define BIN_OPEN_FILE_CONSUME(openFilePntr, type, variableToAssign, returnStatements, deserializationPurpose) do           \
{                                                                                                                          \
	if (!platform->ReadFromFile((openFilePntr), (void*)&variableToAssign, sizeof(type), true))                             \
	{                                                                                                                      \
		PrintLine_W("Expected %u more bytes for " #type " in %s deserialization", sizeof(type), (deserializationPurpose)); \
		returnStatements;                                                                                                  \
	}                                                                                                                      \
} while(0)
#define BIN_OPEN_FILE_CONSUME_BYTES(openFilePntr, numBytes, valuePntr, returnStatements, deserializationPurpose) do \
{                                                                                                                   \
	if (!platform->ReadFromFile((openFilePntr), (void*)valuePntr, (numBytes), true))                                \
	{                                                                                                               \
		PrintLine_W("Expected %u more bytes in %s deserialization", (numBytes), (deserializationPurpose));          \
		returnStatements;                                                                                           \
	}                                                                                                               \
} while(0)

struct ValueListEntryInfo_t
{
	SrlType_t type;
	void* pointer;
	u32 size;
	bool optional;
	bool conditional;
	u32 conditionalIndex;
};
#define VALUE_TYPE_LIST(numValues) ValueListEntryInfo_t entryInfos[numValues] = {}
#define VALUE_ENTRY(index, name, srlType, pntr) entryInfos[index].type = (srlType); entryInfos[index].pointer = (void*)(pntr); entryInfos[index].size = sizeof(*(pntr))
#define VALUE_ENTRY_OPTIONAL(index, name, srlType, pntr) entryInfos[index].type = (srlType); entryInfos[index].pointer = (void*)(pntr); entryInfos[index].size = sizeof(*(pntr)); entryInfos[index].optional = true
#define VALUE_ENTRY_CONDITIONAL(index, name, srlType, pntr, condIndex) entryInfos[index].type = (srlType); entryInfos[index].pointer = (void*)(pntr); entryInfos[index].size = sizeof(*(pntr)); entryInfos[index].conditional = true; entryInfos[index].conditionalIndex = (condIndex)

// +--------------------------------------------------------------+
// |                 Deserialize Function Helpers                 |
// +--------------------------------------------------------------+
const char* TryDeserializeNumber(const char* strPntr, u32 strLength, u32* absValueOut, bool* isNegativeOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as number"; }
	const char* charPntr = strPntr;
	u32 numCharsLeft = strLength;
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[0])) { charPntr++; numCharsLeft--; } //chop off leading whitespace
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[numCharsLeft-1])) { numCharsLeft--; } //chop off trailing whitespace
	if (numCharsLeft == 0) { return "Whitespace can't be parsed as number"; }
	
	bool isHex = false;
	bool isNegative = false;
	bool isPositive = false;
	if (numCharsLeft >= 2 && charPntr[0] == '0' && GetLowercaseChar(charPntr[1]) == 'x' && !isHex)
	{
		isHex = true;
		charPntr += 2;
		numCharsLeft -= 2;
	}
	else if (numCharsLeft >= 1 && charPntr[0] == '-' && !isNegative)
	{
		isNegative = true;
		charPntr++;
		numCharsLeft--;
	}
	else if (numCharsLeft >= 1 && charPntr[0] == '+' && !isPositive)
	{
		isPositive = true;
		charPntr++;
		numCharsLeft--;
	}
	
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[0])) { charPntr++; numCharsLeft--; } //chop off any more whitespace
	if (numCharsLeft == 0) { return "No number found after prefix"; }
	
	if (isNegativeOut != nullptr) { *isNegativeOut = isNegative; }
	if (isHex)
	{
		if (TryParseHexU32(charPntr, numCharsLeft, absValueOut)) { return nullptr; }
		else { return TempPrint("Couldn't parse \"%.*s\" as hex number", numCharsLeft, charPntr); }
	}
	else
	{
		if (TryParseU32(charPntr, numCharsLeft, absValueOut)) { return nullptr; }
		else { return TempPrint("Couldn't parse \"%.*s\" as number", numCharsLeft, charPntr); }
	}
}

StrSplitPiece_t* FindNumbersInString(MemoryArena_t* memArena, const char* strPntr, u32 strLength, bool floatingPoint, bool signedValue, u32* numNumbersOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (numNumbersOut != nullptr) { *numNumbersOut = 0; }
	if (strLength == 0) { return nullptr; }
	
	StrSplitPiece_t* result = nullptr;
	u32 resultNumNumbers = 0;
	for (u8 pass = 0; pass < 2; pass++)
	{
		u32 prevSep = 0;
		u32 numNumbers = 0;
		for (u32 cIndex = 0; cIndex <= strLength; cIndex++)
		{
			char newChar = (cIndex < strLength) ? strPntr[cIndex] : '\0';
			if (IsCharClassNumeric(newChar) || (floatingPoint && newChar == '.') || (signedValue && cIndex == prevSep && newChar == '-') || (signedValue && cIndex == prevSep && newChar == '+'))
			{
				//this character is part of a number, we'll start moving away from prevSep
			}
			else
			{
				if (cIndex > prevSep)
				{
					if (result != nullptr)
					{
						Assert(numNumbers < resultNumNumbers);
						result[numNumbers].pntr = &strPntr[prevSep];
						result[numNumbers].length = cIndex - prevSep;
					}
					numNumbers++;
				}
				prevSep = cIndex+1;
			}
		}
		
		if (pass == 0)
		{
			if (numNumbersOut != nullptr) { *numNumbersOut = numNumbers; }
			if (memArena == nullptr || numNumbers == 0) { return nullptr; }
			resultNumNumbers = numNumbers;
			result = PushArray(memArena, StrSplitPiece_t, numNumbers);
			MyMemSet(result, 0x00, sizeof(StrSplitPiece_t) * numNumbers);
			Assert(result != nullptr);
		}
		else { Assert(numNumbers == resultNumNumbers); }
	}
	
	return result;
}

// +--------------------------------------------------------------+
// |                    Deserialize Functions                     |
// +--------------------------------------------------------------+
const char* TryDeserializeSrlType(const char* strPntr, u32 strLength, SrlType_t* valueOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as SrlType"; }
	const char* charPntr = strPntr;
	u32 numCharsLeft = strLength;
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[0])) { charPntr++; numCharsLeft--; } //chop off leading whitespace
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[numCharsLeft-1])) { numCharsLeft--; } //chop off trailing whitespace
	if (numCharsLeft == 0) { return "Whitespace can't be parsed as SrlType"; }
	
	for (u32 tIndex = 0; tIndex < SrlType_NumTypes; tIndex++)
	{
		SrlType_t srlType = (SrlType_t)tIndex;
		const char* srlTypeStr = GetSrlTypeStr(srlType);
		Assert(srlTypeStr != nullptr);
		u32 srlTypeStrLength = MyStrLength32(srlTypeStr);
		if (numCharsLeft == srlTypeStrLength && StrCompareIgnoreCase(srlTypeStr, charPntr, numCharsLeft))
		{
			if (valueOut != nullptr) { *valueOut = srlType; }
			return nullptr;
		}
	}
	return TempPrint("Invalid string given for SrlType: \"%.*s\"", numCharsLeft, charPntr);
}

const char* TryDeserializeU8(const char* strPntr, u32 strLength, u8* valueOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as u8"; }
	
	u32 absValue = 0;
	bool isNegative = false;
	const char* errorStr = TryDeserializeNumber(strPntr, strLength, &absValue, &isNegative);
	if (errorStr != nullptr) { return errorStr; }
	
	if (isNegative) { return TempPrint("Negative number can't be u8: \"%.*s\"", strLength, strPntr); }
	if (absValue > 0xFF) { return TempPrint("%u is too large for u8! \"%.*s\"", absValue, strLength, strPntr); }
	
	if (valueOut != nullptr) { *valueOut = (u8)absValue; }
	return nullptr;
}
const char* TryDeserializeU16(const char* strPntr, u32 strLength, u16* valueOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as u16"; }
	
	u32 absValue = 0;
	bool isNegative = false;
	const char* errorStr = TryDeserializeNumber(strPntr, strLength, &absValue, &isNegative);
	if (errorStr != nullptr) { return errorStr; }
	
	if (isNegative) { return TempPrint("Negative number can't be u16: \"%.*s\"", strLength, strPntr); }
	if (absValue > 0xFFFF) { return TempPrint("%u is too large for u16! \"%.*s\"", absValue, strLength, strPntr); }
	
	if (valueOut != nullptr) { *valueOut = (u16)absValue; }
	return nullptr;
}
const char* TryDeserializeU32(const char* strPntr, u32 strLength, u32* valueOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as u32"; }
	
	u32 absValue = 0;
	bool isNegative = false;
	const char* errorStr = TryDeserializeNumber(strPntr, strLength, &absValue, &isNegative);
	if (errorStr != nullptr) { return errorStr; }
	
	if (isNegative) { return TempPrint("Negative number can't be u32: \"%.*s\"", strLength, strPntr); }
	
	if (valueOut != nullptr) { *valueOut = absValue; }
	return nullptr;
}

const char* TryDeserializeI32(const char* strPntr, u32 strLength, i32* valueOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as i32"; }
	
	u32 absValue = 0;
	bool isNegative = false;
	const char* errorStr = TryDeserializeNumber(strPntr, strLength, &absValue, &isNegative);
	if (errorStr != nullptr) { return errorStr; }
	
	if ((isNegative && absValue >= 0x80000000) || (!isNegative && absValue >= 0x7FFFFFFF))
	{
		return TempPrint("%s%u is too large for i32! \"%.*s\"", isNegative ? "-" : "", absValue, strLength, strPntr);
	}
	
	if (valueOut != nullptr) { *valueOut = (i32)absValue * (isNegative ? -1 : 1); }
	return nullptr;
}

//TODO: Make this function work on it's own without relying on atof so that we can capture errors and invalid syntax
const char* TryDeserializeR32(const char* strPntr, u32 strLength, r32* valueOut)
{
	TempPushMark();
	char* tempStr = ArenaString(TempArena, strPntr, strLength);
	r64 r64Value = atof(tempStr);
	TempPopMark();
	if (valueOut != nullptr) { *valueOut = (r32)r64Value; }
	return nullptr;
}

const char* TryDeserializeBool(const char* strPntr, u32 strLength, bool* valueOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as Bool"; }
	const char* charPntr = strPntr;
	u32 numCharsLeft = strLength;
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[0])) { charPntr++; numCharsLeft--; } //chop off leading whitespace
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[numCharsLeft-1])) { numCharsLeft--; } //chop off trailing whitespace
	if (numCharsLeft == 0) { return "Whitespace can't be parsed as Bool"; }
	
	u32 absValue = 0;
	bool isNegative = false;
	const char* errorStr = TryDeserializeNumber(charPntr, numCharsLeft, &absValue, &isNegative);
	if (errorStr == nullptr)
	{
		if (isNegative) { return TempPrint("Negative number given for Bool: \"%.*s\"", numCharsLeft, charPntr); }
		if (valueOut != nullptr) { *valueOut = (absValue != 0); }
		return nullptr;
	}
	else
	{
		if (StrCompareIgnoreCase(charPntr, "true", numCharsLeft))
		{
			if (valueOut != nullptr) { *valueOut = true; }
			return nullptr;
		}
		if (StrCompareIgnoreCase(charPntr, "false", numCharsLeft))
		{
			if (valueOut != nullptr) { *valueOut = false; }
			return nullptr;
		}
		return TempPrint("Invalid string given for Bool: \"%.*s\"", numCharsLeft, charPntr);
	}
}

const char* TryDeserializeVec2(const char* strPntr, u32 strLength, v2* valueOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as Vec2"; }
	const char* charPntr = strPntr;
	u32 numCharsLeft = strLength;
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[0])) { charPntr++; numCharsLeft--; } //chop off leading whitespace
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[numCharsLeft-1])) { numCharsLeft--; } //chop off trailing whitespace
	if (numCharsLeft == 0) { return "Whitespace can't be parsed as Vec2"; }
	
	u32 numNumbers = 0;
	StrSplitPiece_t* numbers = FindNumbersInString(TempArena, charPntr, numCharsLeft, true, true, &numNumbers);
	if (numNumbers != 2) { return TempPrint("Found %u instead of 2 numbers for Vec2: \"%.*s\"", numCharsLeft, charPntr); }
	
	v2 result = {};
	const char* errorStr = TryDeserializeR32(numbers[0].pntr, numbers[0].length, &result.x);
	if (errorStr != nullptr) { return errorStr; }
	errorStr = TryDeserializeR32(numbers[1].pntr, numbers[1].length, &result.y);
	if (errorStr != nullptr) { return errorStr; }
	
	if (valueOut != nullptr) { *valueOut = result; }
	return nullptr;
}
const char* TryDeserializeVec3(const char* strPntr, u32 strLength, v3* valueOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as Vec3"; }
	const char* charPntr = strPntr;
	u32 numCharsLeft = strLength;
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[0])) { charPntr++; numCharsLeft--; } //chop off leading whitespace
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[numCharsLeft-1])) { numCharsLeft--; } //chop off trailing whitespace
	if (numCharsLeft == 0) { return "Whitespace can't be parsed as Vec3"; }
	
	u32 numNumbers = 0;
	StrSplitPiece_t* numbers = FindNumbersInString(TempArena, charPntr, numCharsLeft, true, true, &numNumbers);
	if (numNumbers != 3) { return TempPrint("Found %u instead of 3 numbers for Vec3: \"%.*s\"", numCharsLeft, charPntr); }
	
	v3 result = {};
	const char* errorStr = TryDeserializeR32(numbers[0].pntr, numbers[0].length, &result.x);
	if (errorStr != nullptr) { return errorStr; }
	errorStr = TryDeserializeR32(numbers[1].pntr, numbers[1].length, &result.y);
	if (errorStr != nullptr) { return errorStr; }
	errorStr = TryDeserializeR32(numbers[2].pntr, numbers[2].length, &result.z);
	if (errorStr != nullptr) { return errorStr; }
	
	if (valueOut != nullptr) { *valueOut = result; }
	return nullptr;
}
const char* TryDeserializeVec4(const char* strPntr, u32 strLength, v4* valueOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as Vec4"; }
	const char* charPntr = strPntr;
	u32 numCharsLeft = strLength;
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[0])) { charPntr++; numCharsLeft--; } //chop off leading whitespace
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[numCharsLeft-1])) { numCharsLeft--; } //chop off trailing whitespace
	if (numCharsLeft == 0) { return "Whitespace can't be parsed as Vec4"; }
	
	u32 numNumbers = 0;
	StrSplitPiece_t* numbers = FindNumbersInString(TempArena, charPntr, numCharsLeft, true, true, &numNumbers);
	if (numNumbers != 4) { return TempPrint("Found %u instead of 4 numbers for Vec4: \"%.*s\"", numCharsLeft, charPntr); }
	
	v4 result = {};
	const char* errorStr = TryDeserializeR32(numbers[0].pntr, numbers[0].length, &result.x);
	if (errorStr != nullptr) { return errorStr; }
	errorStr = TryDeserializeR32(numbers[1].pntr, numbers[1].length, &result.y);
	if (errorStr != nullptr) { return errorStr; }
	errorStr = TryDeserializeR32(numbers[2].pntr, numbers[2].length, &result.z);
	if (errorStr != nullptr) { return errorStr; }
	errorStr = TryDeserializeR32(numbers[3].pntr, numbers[3].length, &result.w);
	if (errorStr != nullptr) { return errorStr; }
	
	if (valueOut != nullptr) { *valueOut = result; }
	return nullptr;
}

const char* TryDeserializeVec2i(const char* strPntr, u32 strLength, v2i* valueOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as Vec2i"; }
	const char* charPntr = strPntr;
	u32 numCharsLeft = strLength;
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[0])) { charPntr++; numCharsLeft--; } //chop off leading whitespace
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[numCharsLeft-1])) { numCharsLeft--; } //chop off trailing whitespace
	if (numCharsLeft == 0) { return "Whitespace can't be parsed as Vec2i"; }
	
	u32 numNumbers = 0;
	StrSplitPiece_t* numbers = FindNumbersInString(TempArena, charPntr, numCharsLeft, false, true, &numNumbers);
	if (numNumbers != 2) { return TempPrint("Found %u instead of 2 numbers for Vec2i: \"%.*s\"", numCharsLeft, charPntr); }
	
	v2i result = {};
	const char* errorStr = TryDeserializeI32(numbers[0].pntr, numbers[0].length, &result.x);
	if (errorStr != nullptr) { return errorStr; }
	errorStr = TryDeserializeI32(numbers[1].pntr, numbers[1].length, &result.y);
	if (errorStr != nullptr) { return errorStr; }
	
	if (valueOut != nullptr) { *valueOut = result; }
	return nullptr;
}
const char* TryDeserializeVec3i(const char* strPntr, u32 strLength, v3i* valueOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as Vec3i"; }
	const char* charPntr = strPntr;
	u32 numCharsLeft = strLength;
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[0])) { charPntr++; numCharsLeft--; } //chop off leading whitespace
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[numCharsLeft-1])) { numCharsLeft--; } //chop off trailing whitespace
	if (numCharsLeft == 0) { return "Whitespace can't be parsed as Vec3i"; }
	
	u32 numNumbers = 0;
	StrSplitPiece_t* numbers = FindNumbersInString(TempArena, charPntr, numCharsLeft, false, true, &numNumbers);
	if (numNumbers != 3) { return TempPrint("Found %u instead of 3 numbers for Vec3i: \"%.*s\"", numCharsLeft, charPntr); }
	
	v3i result = {};
	const char* errorStr = TryDeserializeI32(numbers[0].pntr, numbers[0].length, &result.x);
	if (errorStr != nullptr) { return errorStr; }
	errorStr = TryDeserializeI32(numbers[1].pntr, numbers[1].length, &result.y);
	if (errorStr != nullptr) { return errorStr; }
	errorStr = TryDeserializeI32(numbers[2].pntr, numbers[2].length, &result.z);
	if (errorStr != nullptr) { return errorStr; }
	
	if (valueOut != nullptr) { *valueOut = result; }
	return nullptr;
}
const char* TryDeserializeVec4i(const char* strPntr, u32 strLength, v4i* valueOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as Vec4i"; }
	const char* charPntr = strPntr;
	u32 numCharsLeft = strLength;
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[0])) { charPntr++; numCharsLeft--; } //chop off leading whitespace
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[numCharsLeft-1])) { numCharsLeft--; } //chop off trailing whitespace
	if (numCharsLeft == 0) { return "Whitespace can't be parsed as Vec4i"; }
	
	u32 numNumbers = 0;
	StrSplitPiece_t* numbers = FindNumbersInString(TempArena, charPntr, numCharsLeft, false, true, &numNumbers);
	if (numNumbers != 4) { return TempPrint("Found %u instead of 4 numbers for Vec4i: \"%.*s\"", numCharsLeft, charPntr); }
	
	v4i result = {};
	const char* errorStr = TryDeserializeI32(numbers[0].pntr, numbers[0].length, &result.x);
	if (errorStr != nullptr) { return errorStr; }
	errorStr = TryDeserializeI32(numbers[1].pntr, numbers[1].length, &result.y);
	if (errorStr != nullptr) { return errorStr; }
	errorStr = TryDeserializeI32(numbers[2].pntr, numbers[2].length, &result.z);
	if (errorStr != nullptr) { return errorStr; }
	errorStr = TryDeserializeI32(numbers[3].pntr, numbers[3].length, &result.w);
	if (errorStr != nullptr) { return errorStr; }
	
	if (valueOut != nullptr) { *valueOut = result; }
	return nullptr;
}

const char* TryDeserializeRec(const char* strPntr, u32 strLength, rec* valueOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as Rec"; }
	const char* charPntr = strPntr;
	u32 numCharsLeft = strLength;
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[0])) { charPntr++; numCharsLeft--; } //chop off leading whitespace
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[numCharsLeft-1])) { numCharsLeft--; } //chop off trailing whitespace
	if (numCharsLeft == 0) { return "Whitespace can't be parsed as Rec"; }
	
	u32 numNumbers = 0;
	StrSplitPiece_t* numbers = FindNumbersInString(TempArena, charPntr, numCharsLeft, true, true, &numNumbers);
	if (numNumbers != 4) { return TempPrint("Found %u instead of 4 numbers for Rec: \"%.*s\"", numCharsLeft, charPntr); }
	
	rec result = {};
	const char* errorStr = TryDeserializeR32(numbers[0].pntr, numbers[0].length, &result.x);
	if (errorStr != nullptr) { return errorStr; }
	errorStr = TryDeserializeR32(numbers[1].pntr, numbers[1].length, &result.y);
	if (errorStr != nullptr) { return errorStr; }
	errorStr = TryDeserializeR32(numbers[2].pntr, numbers[2].length, &result.width);
	if (errorStr != nullptr) { return errorStr; }
	errorStr = TryDeserializeR32(numbers[3].pntr, numbers[3].length, &result.height);
	if (errorStr != nullptr) { return errorStr; }
	
	if (valueOut != nullptr) { *valueOut = result; }
	return nullptr;
}
const char* TryDeserializeReci(const char* strPntr, u32 strLength, reci* valueOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as Reci"; }
	const char* charPntr = strPntr;
	u32 numCharsLeft = strLength;
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[0])) { charPntr++; numCharsLeft--; } //chop off leading whitespace
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[numCharsLeft-1])) { numCharsLeft--; } //chop off trailing whitespace
	if (numCharsLeft == 0) { return "Whitespace can't be parsed as Reci"; }
	
	u32 numNumbers = 0;
	StrSplitPiece_t* numbers = FindNumbersInString(TempArena, charPntr, numCharsLeft, false, true, &numNumbers);
	if (numNumbers != 4) { return TempPrint("Found %u instead of 4 numbers for Reci: \"%.*s\"", numCharsLeft, charPntr); }
	
	reci result = {};
	const char* errorStr = TryDeserializeI32(numbers[0].pntr, numbers[0].length, &result.x);
	if (errorStr != nullptr) { return errorStr; }
	errorStr = TryDeserializeI32(numbers[1].pntr, numbers[1].length, &result.y);
	if (errorStr != nullptr) { return errorStr; }
	errorStr = TryDeserializeI32(numbers[2].pntr, numbers[2].length, &result.width);
	if (errorStr != nullptr) { return errorStr; }
	errorStr = TryDeserializeI32(numbers[3].pntr, numbers[3].length, &result.height);
	if (errorStr != nullptr) { return errorStr; }
	
	if (valueOut != nullptr) { *valueOut = result; }
	return nullptr;
}

const char* TryDeserializeDir2(const char* strPntr, u32 strLength, Dir2_t* valueOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as Dir2"; }
	const char* charPntr = strPntr;
	u32 numCharsLeft = strLength;
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[0])) { charPntr++; numCharsLeft--; } //chop off leading whitespace
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[numCharsLeft-1])) { numCharsLeft--; } //chop off trailing whitespace
	if (numCharsLeft == 0) { return "Whitespace can't be parsed as Dir2"; }
	
	u8 u8Value = 0;
	const char* errorStr = TryDeserializeU8(charPntr, numCharsLeft, &u8Value);
	if (errorStr == nullptr)
	{
		if (u8Value >= 0x10) { return TempPrint("Invalid number for Dir2: 0x%02X", u8Value); }
		if (valueOut != nullptr) { *valueOut = (Dir2_t)u8Value; }
		return nullptr;
	}
	else
	{
		if (StrCompareIgnoreCase(charPntr, "left",  numCharsLeft)) { if (valueOut != nullptr) { *valueOut = Dir2_Left;  } return nullptr; }
		if (StrCompareIgnoreCase(charPntr, "right", numCharsLeft)) { if (valueOut != nullptr) { *valueOut = Dir2_Right; } return nullptr; }
		if (StrCompareIgnoreCase(charPntr, "up",    numCharsLeft)) { if (valueOut != nullptr) { *valueOut = Dir2_Up;    } return nullptr; }
		if (StrCompareIgnoreCase(charPntr, "down",  numCharsLeft)) { if (valueOut != nullptr) { *valueOut = Dir2_Down;  } return nullptr; }
		if (StrCompareIgnoreCase(charPntr, "all",   numCharsLeft)) { if (valueOut != nullptr) { *valueOut = Dir2_All;   } return nullptr; }
		if (StrCompareIgnoreCase(charPntr, "none",  numCharsLeft)) { if (valueOut != nullptr) { *valueOut = Dir2_None;  } return nullptr; }
		return TempPrint("Invalid string given for Dir2: \"%.*s\"", numCharsLeft, charPntr);
	}
}
const char* TryDeserializeDir2Diag(const char* strPntr, u32 strLength, Dir2Diag_t* valueOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as Dir2Diag"; }
	const char* charPntr = strPntr;
	u32 numCharsLeft = strLength;
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[0])) { charPntr++; numCharsLeft--; } //chop off leading whitespace
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[numCharsLeft-1])) { numCharsLeft--; } //chop off trailing whitespace
	if (numCharsLeft == 0) { return "Whitespace can't be parsed as Dir2Diag"; }
	
	u8 u8Value = 0;
	const char* errorStr = TryDeserializeU8(charPntr, numCharsLeft, &u8Value);
	if (errorStr == nullptr)
	{
		if (u8Value >= 0x20) { return TempPrint("Invalid number for Dir2Diag: 0x%02X", u8Value); }
		if (valueOut != nullptr) { *valueOut = (Dir2Diag_t)u8Value; }
		return nullptr;
	}
	else
	{
		if (numCharsLeft == 4 && StrCompareIgnoreCase(charPntr, "None",       numCharsLeft)) { if (valueOut != nullptr) { *valueOut = Dir2Diag_None;      } return nullptr; }
		if (numCharsLeft == 5 && StrCompareIgnoreCase(charPntr, "Right",      numCharsLeft)) { if (valueOut != nullptr) { *valueOut = Dir2Diag_Right;     } return nullptr; }
		if (numCharsLeft == 4 && StrCompareIgnoreCase(charPntr, "Down",       numCharsLeft)) { if (valueOut != nullptr) { *valueOut = Dir2Diag_Down;      } return nullptr; }
		if (numCharsLeft == 4 && StrCompareIgnoreCase(charPntr, "Left",       numCharsLeft)) { if (valueOut != nullptr) { *valueOut = Dir2Diag_Left;      } return nullptr; }
		if (numCharsLeft == 2 && StrCompareIgnoreCase(charPntr, "Up",         numCharsLeft)) { if (valueOut != nullptr) { *valueOut = Dir2Diag_Up;        } return nullptr; }
		if (numCharsLeft == 9 && StrCompareIgnoreCase(charPntr, "RightDown",  numCharsLeft)) { if (valueOut != nullptr) { *valueOut = Dir2Diag_RightDown; } return nullptr; }
		if (numCharsLeft == 9 && StrCompareIgnoreCase(charPntr, "DownRight",  numCharsLeft)) { if (valueOut != nullptr) { *valueOut = Dir2Diag_DownRight; } return nullptr; }
		if (numCharsLeft == 8 && StrCompareIgnoreCase(charPntr, "DownLeft",   numCharsLeft)) { if (valueOut != nullptr) { *valueOut = Dir2Diag_DownLeft;  } return nullptr; }
		if (numCharsLeft == 8 && StrCompareIgnoreCase(charPntr, "LeftDown",   numCharsLeft)) { if (valueOut != nullptr) { *valueOut = Dir2Diag_LeftDown;  } return nullptr; }
		if (numCharsLeft == 6 && StrCompareIgnoreCase(charPntr, "LeftUp",     numCharsLeft)) { if (valueOut != nullptr) { *valueOut = Dir2Diag_LeftUp;    } return nullptr; }
		if (numCharsLeft == 6 && StrCompareIgnoreCase(charPntr, "UpLeft",     numCharsLeft)) { if (valueOut != nullptr) { *valueOut = Dir2Diag_UpLeft;    } return nullptr; }
		if (numCharsLeft == 7 && StrCompareIgnoreCase(charPntr, "UpRight",    numCharsLeft)) { if (valueOut != nullptr) { *valueOut = Dir2Diag_UpRight;   } return nullptr; }
		if (numCharsLeft == 7 && StrCompareIgnoreCase(charPntr, "RightUp",    numCharsLeft)) { if (valueOut != nullptr) { *valueOut = Dir2Diag_RightUp;   } return nullptr; }
		return TempPrint("Invalid string given for Dir2Diag: \"%.*s\"", numCharsLeft, charPntr);
	}
}

const char* TryDeserializeKeyboardBtn(const char* strPntr, u32 strLength, Buttons_t* valueOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as KeyboardBtn"; }
	const char* charPntr = strPntr;
	u32 numCharsLeft = strLength;
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[0])) { charPntr++; numCharsLeft--; } //chop off leading whitespace
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[numCharsLeft-1])) { numCharsLeft--; } //chop off trailing whitespace
	if (numCharsLeft == 0) { return "Whitespace can't be parsed as KeyboardBtn"; }
	
	u32 u32Value = 0;
	const char* errorStr = TryDeserializeU32(charPntr, numCharsLeft, &u32Value);
	if (errorStr == nullptr)
	{
		if (u32Value > Buttons_NumButtons) { return TempPrint("Invalid number for KeyboardBtn: %u", u32Value); }
		if (valueOut != nullptr) { *valueOut = (Buttons_t)u32Value; }
		return nullptr;
	}
	else if (StrCompareIgnoreCase(charPntr, "[unbound]", numCharsLeft))
	{
		if (valueOut != nullptr) { *valueOut = Buttons_NumButtons; }
		return nullptr;
	}
	else
	{
		for (u32 bIndex = 0; bIndex < Buttons_NumButtons; bIndex++)
		{
			Buttons_t btn = (Buttons_t)bIndex;
			const char* btnStr = GetButtonName(btn);
			Assert(btnStr != nullptr);
			u32 btnStrLength = MyStrLength32(btnStr);
			if (strLength == btnStrLength && StrCompareIgnoreCase(btnStr, strPntr, strLength))
			{
				if (valueOut != nullptr) { *valueOut = btn; }
				return nullptr;
			}
		}
		return TempPrint("Invalid string given for KeyboardBtn: \"%.*s\"", numCharsLeft, charPntr);
	}
}
const char* TryDeserializeGamepadBtn(const char* strPntr, u32 strLength, GamepadButtons_t* valueOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as GamepadBtn"; }
	const char* charPntr = strPntr;
	u32 numCharsLeft = strLength;
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[0])) { charPntr++; numCharsLeft--; } //chop off leading whitespace
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[numCharsLeft-1])) { numCharsLeft--; } //chop off trailing whitespace
	if (numCharsLeft == 0) { return "Whitespace can't be parsed as GamepadBtn"; }
	
	u32 u32Value = 0;
	const char* errorStr = TryDeserializeU32(charPntr, numCharsLeft, &u32Value);
	if (errorStr == nullptr)
	{
		if (u32Value > Gamepad_NumButtons) { return TempPrint("Invalid number for GamepadBtn: %u", u32Value); }
		if (valueOut != nullptr) { *valueOut = (GamepadButtons_t)u32Value; }
		return nullptr;
	}
	else if (StrCompareIgnoreCase(charPntr, "[unbound]", numCharsLeft))
	{
		if (valueOut != nullptr) { *valueOut = Gamepad_NumButtons; }
		return nullptr;
	}
	else
	{
		for (u32 bIndex = 0; bIndex < Gamepad_NumButtons; bIndex++)
		{
			GamepadButtons_t btn = (GamepadButtons_t)bIndex;
			const char* btnStr = GetGamepadButtonName(btn);
			Assert(btnStr != nullptr);
			u32 btnStrLength = MyStrLength32(btnStr);
			if (strLength == btnStrLength && StrCompareIgnoreCase(btnStr, strPntr, strLength))
			{
				if (valueOut != nullptr) { *valueOut = btn; }
				return nullptr;
			}
		}
		return TempPrint("Invalid string given for GamepadBtn: \"%.*s\"", numCharsLeft, charPntr);
	}
}

const char* TryDeserializePaletteColor(const char* strPntr, u32 strLength, u32* valueOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as PaletteColor"; }
	const char* charPntr = strPntr;
	u32 numCharsLeft = strLength;
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[0])) { charPntr++; numCharsLeft--; } //chop off leading whitespace
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[numCharsLeft-1])) { numCharsLeft--; } //chop off trailing whitespace
	if (numCharsLeft == 0) { return "Whitespace can't be parsed as PaletteColor"; }
	
	u32 u32Value = 0;
	const char* errorStr = TryDeserializeU32(charPntr, numCharsLeft, &u32Value);
	if (errorStr == nullptr)
	{
		if (GetColorIndexFromPackedIndex(u32Value) >= NUM_PALETTE_COLORS) { return TempPrint("Invalid number for PaletteColor: 0x%08X", u32Value); }
		if (valueOut != nullptr) { *valueOut = u32Value; }
		return nullptr;
	}
	else
	{
		for (u32 cIndex = 0; cIndex < NUM_PALETTE_COLORS; cIndex++)
		{
			const char* colorStr = GetPaletteColorStr(cIndex);
			u32 colorStrLength = MyStrLength32(colorStr);
			for (u8 subIndex = 0; subIndex < 4; subIndex++)
			{
				const char* subStr = GetPaletteSubIndexStr(subIndex);
				u32 subStrLength = MyStrLength32(subStr);
				if (numCharsLeft == colorStrLength + subStrLength && StrCompareIgnoreCase(colorStr, &charPntr[0], colorStrLength) && StrCompareIgnoreCase(subStr, &charPntr[colorStrLength], subStrLength))
				{
					if (valueOut != nullptr) { *valueOut = GetPackedIndex(cIndex, subIndex); }
					return nullptr;
				}
			}
		}
		return TempPrint("Invalid string given for PaletteColor: \"%.*s\"", numCharsLeft, charPntr);
	}
}
const char* TryDeserializePickedColor(const char* strPntr, u32 strLength, PickedColor_t* valueOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as PickedColor"; }
	const char* charPntr = strPntr;
	u32 numCharsLeft = strLength;
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[0])) { charPntr++; numCharsLeft--; } //chop off leading whitespace
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[numCharsLeft-1])) { numCharsLeft--; } //chop off trailing whitespace
	bool isPickedColor = false;
	if (numCharsLeft > 0 && charPntr[0] == '@') { isPickedColor = true; charPntr++; numCharsLeft--; }
	if (numCharsLeft == 0) { return "Whitespace can't be parsed as PickedColor"; }
	
	
	u32 u32Value = 0;
	const char* errorStr = TryDeserializeU32(charPntr, numCharsLeft, &u32Value);
	if (errorStr == nullptr)
	{
		if (isPickedColor && GetColorIndexFromPackedIndex(u32Value) >= NUM_PALETTE_COLORS) { return TempPrint("Invalid number for Palette PickedColor: 0x%08X", u32Value); }
		if (valueOut != nullptr) { valueOut->isPalette = isPickedColor; valueOut->packedIndex = u32Value; }
		return nullptr;
	}
	else if (isPickedColor)
	{
		for (u32 cIndex = 0; cIndex < NUM_PALETTE_COLORS; cIndex++)
		{
			const char* colorStr = GetPaletteColorStr(cIndex);
			u32 colorStrLength = MyStrLength32(colorStr);
			for (u8 subIndex = 0; subIndex < NUM_REPLACE_COLORS; subIndex++)
			{
				const char* subStr = GetPaletteSubIndexStr(subIndex);
				u32 subStrLength = MyStrLength32(subStr);
				if (numCharsLeft == colorStrLength + subStrLength && StrCompareIgnoreCase(colorStr, &charPntr[0], colorStrLength) && StrCompareIgnoreCase(subStr, &charPntr[colorStrLength], subStrLength))
				{
					if (valueOut != nullptr) { *valueOut = NewPickedColorByIndex(cIndex, subIndex); }
					return nullptr;
				}
			}
		}
		return TempPrint("Invalid string given for PickedColor: \"%.*s\"", numCharsLeft, charPntr);
	}
	else
	{
		return TempPrint("Couldn't parse \"%.*s\" as Custom PickedColor u32", numCharsLeft, charPntr);
	}
}

const char* TryDeserializeTileType(const char* strPntr, u32 strLength, TileType_t* valueOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as TileType"; }
	const char* charPntr = strPntr;
	u32 numCharsLeft = strLength;
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[0])) { charPntr++; numCharsLeft--; } //chop off leading whitespace
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[numCharsLeft-1])) { numCharsLeft--; } //chop off trailing whitespace
	if (numCharsLeft == 0) { return "Whitespace can't be parsed as TileType"; }
	
	u8 u8Value = 0;
	const char* errorStr = TryDeserializeU8(charPntr, numCharsLeft, &u8Value);
	if (errorStr == nullptr)
	{
		if (u8Value >= NumTileTypes) { return TempPrint("Invalid number for TileType: %u", u8Value); }
		if (valueOut != nullptr) { *valueOut = (TileType_t)u8Value; }
		return nullptr;
	}
	else
	{
		if (numCharsLeft == TILE_ID_STR_LENGTH)
		{
			for (u32 tIndex = 0; tIndex < NumTileTypes; tIndex++)
			{
				TileType_t type = (TileType_t)tIndex;
				const char* idStr = GetTileTypeIdStr(type);
				Assert(idStr != nullptr);
				if (StrCompareIgnoreCase(idStr, charPntr, TILE_ID_STR_LENGTH))
				{
					if (valueOut != nullptr) { *valueOut = type; }
					return nullptr;
				}
			}
		}
		return TempPrint("Invalid string given for TileType: \"%.*s\"", numCharsLeft, charPntr);
	}
}
const char* TryDeserializeEntityType(const char* strPntr, u32 strLength, EntityType_t* valueOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as EntityType"; }
	const char* charPntr = strPntr;
	u32 numCharsLeft = strLength;
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[0])) { charPntr++; numCharsLeft--; } //chop off leading whitespace
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[numCharsLeft-1])) { numCharsLeft--; } //chop off trailing whitespace
	if (numCharsLeft == 0) { return "Whitespace can't be parsed as EntityType"; }
	
	u8 u8Value = 0;
	const char* errorStr = TryDeserializeU8(charPntr, numCharsLeft, &u8Value);
	if (errorStr == nullptr)
	{
		if (u8Value >= NumEntityTypes) { return TempPrint("Invalid number for EntityType: %u", u8Value); }
		if (valueOut != nullptr) { *valueOut = (EntityType_t)u8Value; }
		return nullptr;
	}
	else
	{
		if (numCharsLeft == ENTITY_ID_STR_LENGTH)
		{
			for (u32 tIndex = 0; tIndex < NumEntityTypes; tIndex++)
			{
				EntityType_t type = (EntityType_t)tIndex;
				const char* idStr = GetEntityTypeIdStr(type);
				Assert(idStr != nullptr);
				if (StrCompareIgnoreCase(idStr, charPntr, ENTITY_ID_STR_LENGTH))
				{
					if (valueOut != nullptr) { *valueOut = type; }
					return nullptr;
				}
			}
		}
		return TempPrint("Invalid string given for EntityType: \"%.*s\"", numCharsLeft, charPntr);
	}
}

const char* TryDeserializeResource(const char* strPntr, u32 strLength, Resource_t** valueOut)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string can't be parsed as Resource"; }
	const char* charPntr = strPntr;
	u32 numCharsLeft = strLength;
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[0])) { charPntr++; numCharsLeft--; } //chop off leading whitespace
	while (numCharsLeft > 0 && IsCharClassWhitespace(charPntr[numCharsLeft-1])) { numCharsLeft--; } //chop off trailing whitespace
	if (numCharsLeft == 0) { return "Whitespace can't be parsed as Resource"; }
	
	Resource_t* resourcePntr = GetResourceByFilePath(charPntr, numCharsLeft);
	if (resourcePntr != nullptr)
	{
		if (valueOut != nullptr) { *valueOut = resourcePntr; }
		return nullptr;
	}
	else
	{
		return TempPrint("Couldn't find resource for path: \"%.*s\"", numCharsLeft, charPntr);
	}
}

// +==================================+
// | Generic Deserialization Function |
// +==================================+
const char* TryDeserialize(SrlType_t type, const char* strPntr, u32 strLength, void* valueOut, u32 valueOutSize)
{
	Assert(type < SrlType_NumTypes);
	
	switch (type)
	{
		case SrlType_Type:
		{
			if (valueOut != nullptr && valueOutSize != sizeof(SrlType_t)) { return "Invalid output size for SrlType"; }
			return TryDeserializeSrlType(strPntr, strLength, (SrlType_t*)valueOut);
		} break;
		case SrlType_u8:
		{
			if (valueOut != nullptr && valueOutSize != sizeof(u8)) { return "Invalid output size for u8"; }
			return TryDeserializeU8(strPntr, strLength, (u8*)valueOut);
		} break;
		case SrlType_u16:
		{
			if (valueOut != nullptr && valueOutSize != sizeof(u16)) { return "Invalid output size for u16"; }
			return TryDeserializeU16(strPntr, strLength, (u16*)valueOut);
		} break;
		case SrlType_u32:
		{
			if (valueOut != nullptr && valueOutSize != sizeof(u32)) { return "Invalid output size for u32"; }
			return TryDeserializeU32(strPntr, strLength, (u32*)valueOut);
		} break;
		case SrlType_i32:
		{
			if (valueOut != nullptr && valueOutSize != sizeof(i32)) { return "Invalid output size for i32"; }
			return TryDeserializeI32(strPntr, strLength, (i32*)valueOut);
		} break;
		case SrlType_r32:
		{
			if (valueOut != nullptr && valueOutSize != sizeof(r32)) { return "Invalid output size for r32"; }
			return TryDeserializeR32(strPntr, strLength, (r32*)valueOut);
		} break;
		case SrlType_Bool:
		{
			if (valueOut != nullptr && valueOutSize != sizeof(bool)) { return "Invalid output size for Bool"; }
			return TryDeserializeBool(strPntr, strLength, (bool*)valueOut);
		} break;
		case SrlType_Vec2:
		{
			if (valueOut != nullptr && valueOutSize != sizeof(v2)) { return "Invalid output size for Vec2"; }
			return TryDeserializeVec2(strPntr, strLength, (v2*)valueOut);
		} break;
		case SrlType_Vec3:
		{
			if (valueOut != nullptr && valueOutSize != sizeof(v3)) { return "Invalid output size for Vec3"; }
			return TryDeserializeVec3(strPntr, strLength, (v3*)valueOut);
		} break;
		case SrlType_Vec4:
		{
			if (valueOut != nullptr && valueOutSize != sizeof(v4)) { return "Invalid output size for Vec4"; }
			return TryDeserializeVec4(strPntr, strLength, (v4*)valueOut);
		} break;
		case SrlType_Vec2i:
		{
			if (valueOut != nullptr && valueOutSize != sizeof(v2i)) { return "Invalid output size for Vec2i"; }
			return TryDeserializeVec2i(strPntr, strLength, (v2i*)valueOut);
		} break;
		case SrlType_Vec3i:
		{
			if (valueOut != nullptr && valueOutSize != sizeof(v3i)) { return "Invalid output size for Vec3i"; }
			return TryDeserializeVec3i(strPntr, strLength, (v3i*)valueOut);
		} break;
		case SrlType_Vec4i:
		{
			if (valueOut != nullptr && valueOutSize != sizeof(v4i)) { return "Invalid output size for Vec4i"; }
			return TryDeserializeVec4i(strPntr, strLength, (v4i*)valueOut);
		} break;
		case SrlType_Rec:
		{
			if (valueOut != nullptr && valueOutSize != sizeof(rec)) { return "Invalid output size for Rec"; }
			return TryDeserializeRec(strPntr, strLength, (rec*)valueOut);
		} break;
		case SrlType_Reci:
		{
			if (valueOut != nullptr && valueOutSize != sizeof(reci)) { return "Invalid output size for Reci"; }
			return TryDeserializeReci(strPntr, strLength, (reci*)valueOut);
		} break;
		case SrlType_Dir2:
		{
			if (valueOut != nullptr && valueOutSize != sizeof(Dir2_t)) { return "Invalid output size for Dir2"; }
			return TryDeserializeDir2(strPntr, strLength, (Dir2_t*)valueOut);
		} break;
		case SrlType_Dir2Diag:
		{
			if (valueOut != nullptr && valueOutSize != sizeof(Dir2Diag_t)) { return "Invalid output size for Dir2Diag"; }
			return TryDeserializeDir2Diag(strPntr, strLength, (Dir2Diag_t*)valueOut);
		} break;
		case SrlType_PaletteColor:
		{
			if (valueOut != nullptr && valueOutSize != sizeof(u32)) { return "Invalid output size for PaletteColor"; }
			return TryDeserializePaletteColor(strPntr, strLength, (u32*)valueOut);
		} break;
		case SrlType_PickedColor:
		{
			if (valueOut != nullptr && valueOutSize != sizeof(PickedColor_t)) { return "Invalid output size for PickedColor"; }
			return TryDeserializePickedColor(strPntr, strLength, (PickedColor_t*)valueOut);
		} break;
		case SrlType_TileType:
		{
			if (valueOut != nullptr && valueOutSize != sizeof(TileType_t)) { return "Invalid output size for TileType"; }
			return TryDeserializeTileType(strPntr, strLength, (TileType_t*)valueOut);
		} break;
		case SrlType_EntityType:
		{
			if (valueOut != nullptr && valueOutSize != sizeof(EntityType_t)) { return "Invalid output size for EntityType"; }
			return TryDeserializeEntityType(strPntr, strLength, (EntityType_t*)valueOut);
		} break;
		case SrlType_Resource:
		{
			if (valueOut != nullptr && valueOutSize != sizeof(Resource_t*)) { return "Invalid output size for Resource"; }
			return TryDeserializeResource(strPntr, strLength, (Resource_t**)valueOut);
		} break;
		default:
		{
			Assert(false);
			return "Invalid SrlType given";
		} break;
	}
}

// +--------------------------------------------------------------+
// |               Consumption/Validation Functions               |
// +--------------------------------------------------------------+
const char* ValidateValueList(const char* strPntr, u32 strLength, char sepChar, const SrlType_t* types, u32 numValues, u32 numValuesRequired, u32* numValuesOut = nullptr, u32 valueOffset = 0, bool allowExtraValues = false)
{
	Assert(types != nullptr);
	Assert(numValues > 0);
	Assert(numValuesRequired <= numValues);
	Assert(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return "Empty string given for ValueList"; }
	
	u32 prevSep = 0;
	u32 numValuesFound = 0;
	for (u32 cIndex = 0; cIndex <= strLength; cIndex++)
	{
		char newChar = (cIndex < strLength) ? strPntr[cIndex] : sepChar;
		if (newChar == sepChar)
		{
			if (numValuesFound >= valueOffset)
			{
				const char* valuePntr = &strPntr[prevSep];
				u32 valueLength = (cIndex - prevSep);
				if (numValuesFound-valueOffset < numValues)
				{
					const char* errorStr = TryDeserialize(types[numValuesFound-valueOffset], valuePntr, valueLength, nullptr, 0);
					if (errorStr != nullptr) { return errorStr; }
				}
				else if (!allowExtraValues)
				{
					return TempPrint("Too many values found. Unexpected at \"%.*s\"", valueLength, valuePntr);
				}
			}
			numValuesFound++;
			prevSep = cIndex+1;
		}
	}
	if (numValuesFound < valueOffset + numValuesRequired) { return TempPrint("Found %u/%u expected values", numValuesFound, valueOffset + numValuesRequired); }
	if (numValuesOut != nullptr) { *numValuesOut = numValuesFound; }
	return nullptr;
}

const char* ParseValueList(MemoryArena_t* memArena, const char* strPntr, u32 strLength, char sepChar, const ValueListEntryInfo_t* entryInfos, u32 numEntries, u32 valueOffset = 0, bool allowExtraValues = false, StrSplitPiece_t** piecesOut = nullptr, u32* numPiecesOut = nullptr)
{
	Assert(entryInfos != nullptr);
	Assert(numEntries > 0);
	Assert(strPntr != nullptr || strLength == 0);
	
	u32 numPieces = 0;
	StrSplitPiece_t* pieces = SplitString(memArena, strPntr, strLength, ",", 1, &numPieces);
	Assert(pieces != nullptr || numPieces == 0);
	if (piecesOut != nullptr) { *piecesOut = pieces; }
	if (numPiecesOut != nullptr) { *numPiecesOut = numPieces; }
	bool ignoreDependents = (numPieces == numEntries);
	
	bool startedOptionals = false;
	u32 entryIndex = 0;
	for (u32 pIndex = 0; pIndex < numPieces; pIndex++)
	{
		StrSplitPiece_t* piece = &pieces[pIndex];
		if (pIndex >= valueOffset)
		{
			//Skip over conditionals who's condition entry was false
			if (entryIndex < numEntries && !ignoreDependents)
			{
				const ValueListEntryInfo_t* entryInfo = &entryInfos[entryIndex];
				while (entryIndex < numEntries && entryInfo->conditional)
				{
					Assert(entryInfo->conditionalIndex < numEntries);
					Assert(entryInfo->conditionalIndex < entryIndex);
					const ValueListEntryInfo_t* conditionalInfo = &entryInfos[entryInfo->conditionalIndex];
					Assert(conditionalInfo->type == SrlType_Bool);
					Assert(conditionalInfo->pointer != nullptr);
					bool* boolPntr = (bool*)conditionalInfo->pointer;
					if ((*boolPntr) == false) { entryInfo++; entryIndex++; }
					else { break; }
				}
			}
			
			if (entryIndex < numEntries)
			{
				const ValueListEntryInfo_t* entryInfo = &entryInfos[entryIndex];
				TrimSplitPieceWhitespace(piece);
				const char* errorStr = TryDeserialize(entryInfo->type, piece->pntr, piece->length, entryInfo->pointer, entryInfo->size);
				if (errorStr != nullptr) { return errorStr; }
				entryIndex++;
			}
			else if (!allowExtraValues)
			{
				return TempPrint("Too many values found. Unexpected at \"%.*s\"", piece->length, piece->pntr);
			}
		}
	}
	while (entryIndex < numEntries)
	{
		const ValueListEntryInfo_t* entryInfo = &entryInfos[entryIndex];
		if (!entryInfo->optional) { return TempPrint("Expected non-optional entry[%u] after %u pieces", entryIndex, numPieces); }
		else { entryIndex++; }
	}
	return nullptr;
}

void CreateTextParser(TextParser_t* parser, const char* content, u32 contentLength)
{
	Assert(parser != nullptr);
	Assert(content != nullptr || contentLength == 0);
	
	ClearPointer(parser);
	parser->content = content;
	parser->contentLength = contentLength;
	parser->position = 0;
	parser->lineNumber = 0;
	parser->lineStart = 0;
	parser->numTokensFound = 0;
}

const char* ParserConsumeToken(TextParser_t* parser, ParsingToken_t* tokenOut = nullptr)
{
	Assert(parser != nullptr);
	Assert(parser->content != nullptr || parser->contentLength == 0);
	
	parser->foundToken = false;
	if (tokenOut != nullptr) { tokenOut->type = ParsingTokenType_Unknown; }
	if (parser->position >= parser->contentLength) { return nullptr; }
	
	bool foundToken = false;
	ParsingToken_t token = {};
	
	u32 startPosition = parser->position;
	bool inComment = false;
	u32 commentStart = 0;
	bool inCurlyBraces = false;
	u32 curlyContentStart = 0;
	bool inKeyValue = false;
	u32 keyEnd = 0;
	u32 valueStart = 0;
	bool inHeader = false;
	u32 headerContentStart = 0;
	while (parser->position <= parser->contentLength)
	{
		char newChar = (parser->position < parser->contentLength) ? parser->content[parser->position] : '\0';
		char nextChar = (parser->position+1 < parser->contentLength) ? parser->content[parser->position+1] : '\0';
		
		if (newChar == '\n' || newChar == '\r' || newChar == '\0') //new-line or end of file
		{
			if (!inComment && !inCurlyBraces && !inHeader && !inKeyValue && parser->position > startPosition)
			{
				token.type = ParsingTokenType_Other;
				token.start = startPosition;
				token.length = parser->position - startPosition;
				token.pieces[0].pntr = &parser->content[startPosition];
				token.pieces[0].length = parser->position - startPosition;
				foundToken = true;
			}
			if (inComment)
			{
				token.type = ParsingTokenType_Comment;
				token.start = commentStart-2;
				token.length = parser->position - (commentStart-2);
				token.comment.pntr = &parser->content[commentStart];
				token.comment.length = parser->position - commentStart;
				foundToken = true;
				inComment = false;
			}
			if (inCurlyBraces)
			{
				return TempPrint("No closing curly brace found: \"%.*s\"", (parser->position+2) - (curlyContentStart-1), &parser->content[curlyContentStart-1]);
			}
			if (inHeader)
			{
				return TempPrint("No closing square bracket found: \"%.*s\"", (parser->position+2) - (headerContentStart-1), &parser->content[headerContentStart-1]);
			}
			if (inKeyValue)
			{
				token.type = ParsingTokenType_KeyValue;
				token.start = parser->lineStart;
				token.length = parser->position - parser->lineStart;
				token.key.pntr = &parser->content[parser->lineStart];
				token.key.length = keyEnd - parser->lineStart;
				token.value.pntr = &parser->content[valueStart];
				token.value.length = parser->position - valueStart;
				foundToken = true;
			}
			parser->position++;
			if ((newChar == '\n' && nextChar == '\r') || (newChar == '\r' && nextChar == '\n'))
			{
				parser->position++;
			}
			parser->lineNumber++;
			startPosition = parser->position;
			parser->lineStart = parser->position;
		}
		else if (!inComment && newChar == '/' && nextChar == '/')
		{
			if (inCurlyBraces)
			{
				return TempPrint("Found comment in curly braces: \"%.*s\"", (parser->position+2) - (curlyContentStart-1), &parser->content[curlyContentStart-1]);
			}
			else if (inHeader)
			{
				return TempPrint("Found comment in header: \"%.*s\"", (parser->position+2) - (headerContentStart-1), &parser->content[headerContentStart-1]);
			}
			else if (inKeyValue)
			{
				token.type = ParsingTokenType_KeyValue;
				token.start = parser->lineStart;
				token.length = parser->position - parser->lineStart;
				token.key.pntr = &parser->content[parser->lineStart];
				token.key.length = keyEnd - parser->lineStart;
				token.value.pntr = &parser->content[valueStart];
				token.value.length = parser->position - valueStart;
				foundToken = true;
				//NOTE: Don't increment parser->position
			}
			else if (parser->position > startPosition)
			{
				token.type = ParsingTokenType_Other;
				token.start = startPosition;
				token.length = parser->position - startPosition;
				token.pieces[0].pntr = &parser->content[startPosition];
				token.pieces[0].length = parser->position - startPosition;
				foundToken = true;
				//NOTE: Don't increment parser->position
			}
			else
			{
				inComment = true;
				commentStart = parser->position+2;
				parser->position += 2;
			}
		}
		else if (!inKeyValue && !inComment && !inCurlyBraces && !inHeader && newChar == '{')
		{
			if (parser->position > startPosition)
			{
				token.type = ParsingTokenType_Other;
				token.start = startPosition;
				token.length = parser->position - startPosition;
				token.pieces[0].pntr = &parser->content[startPosition];
				token.pieces[0].length = parser->position - startPosition;
				foundToken = true;
			}
			else
			{
				inCurlyBraces = true;
				curlyContentStart = parser->position+1;
				parser->position++;
			}
		}
		else if (inCurlyBraces && !inComment && newChar == '}')
		{
			token.type = ParsingTokenType_ValueList;
			token.start = curlyContentStart-1;
			token.length = (parser->position+1) - (curlyContentStart-1);
			token.content.pntr = &parser->content[curlyContentStart];
			token.content.length = parser->position - curlyContentStart;
			foundToken = true;
			parser->position++;
		}
		else if (!inCurlyBraces && !inComment && !inKeyValue && !inHeader && newChar == ':')
		{
			inKeyValue = true;
			keyEnd = parser->position;
			valueStart = parser->position+1;
			parser->position++;
		}
		else if (inKeyValue && parser->position == valueStart && IsCharClassWhitespace(newChar))
		{
			valueStart = parser->position+1;
			parser->position++;
		}
		else if (!inComment && !inCurlyBraces && !inKeyValue && !inHeader && newChar == '[')
		{
			if (parser->position > startPosition)
			{
				token.type = ParsingTokenType_Other;
				token.start = startPosition;
				token.length = parser->position - startPosition;
				token.pieces[0].pntr = &parser->content[startPosition];
				token.pieces[0].length = parser->position - startPosition;
				foundToken = true;
			}
			else
			{
				inHeader = true;
				headerContentStart = parser->position+1;
				parser->position++;
			}
		}
		else if (inHeader && !inComment && newChar == ']')
		{
			token.type = ParsingTokenType_Header;
			token.start = headerContentStart-1;
			token.length = (parser->position+1) - (headerContentStart-1);
			token.header.pntr = &parser->content[headerContentStart];
			token.header.length = parser->position - headerContentStart;
			foundToken = true;
			parser->position++;
		}
		else
		{
			if (IsCharClassWhitespace(newChar) && parser->position == startPosition) { startPosition++; }
			parser->position++;
		}
		
		if (foundToken) { break; }
	}
	
	if (!foundToken && parser->position >= parser->contentLength)
	{
		if (inCurlyBraces || inKeyValue || inComment || inHeader) { return "Unexpected end of file"; }
	}
	
	if (foundToken)
	{
		if (token.type == ParsingTokenType_Unknown) { Assert(false); }
		parser->numTokensFound++;
		if (tokenOut != nullptr) { MyMemCopy(tokenOut, &token, sizeof(ParsingToken_t)); }
		parser->foundToken = true;
	}
	return nullptr;
}
