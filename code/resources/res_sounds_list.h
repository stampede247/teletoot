/*
File:   res_sounds_list.h
Author: Taylor Robbins
Date:   11\21\2019
Description:
	** This is a special file that can be included multiple times to obatin different effects. Before the file is included you must define
	** SOUND_EFFECT_ENTRY(internalName, looping, readableName, fileName)
*/

#ifndef SOUND_EFFECT_ENTRY
#error res_sounds_list.h was included without SOUND_EFFECT_ENTRY being defined
#endif

#ifndef NUM_SOUND_EFFECT_RESOURCES
#define NUM_SOUND_EFFECT_RESOURCES 23
#endif

SOUND_EFFECT_ENTRY(testSound,          false, "Test",                   "test.ogg");
SOUND_EFFECT_ENTRY(notificationSound,  false, "Notification",           "notification.ogg");
SOUND_EFFECT_ENTRY(chuggaSound1,       false, "Chugga Sound 1",         "chugga1.ogg");
SOUND_EFFECT_ENTRY(chuggaSound2,       false, "Chugga Sound 2",         "chugga2.ogg");
SOUND_EFFECT_ENTRY(poofSound1,         false, "Poof Sound 1",           "poof1.ogg");
SOUND_EFFECT_ENTRY(poofSound2,         false, "Poof Sound 2",           "poof2.ogg");
SOUND_EFFECT_ENTRY(chooSound1,         false, "Choo Sound 1",           "choo1.ogg");
SOUND_EFFECT_ENTRY(raSound1,           false, "Ra Sound 1",             "ra1.ogg");
SOUND_EFFECT_ENTRY(primarySound,       false, "Primary Sound",          "primary.ogg");
SOUND_EFFECT_ENTRY(secondarySound,     false, "Secondary Sound",        "secondary.ogg");
SOUND_EFFECT_ENTRY(dingSound1,         false, "Ding Sound 1",           "ding1.ogg");
SOUND_EFFECT_ENTRY(dohSound1,          false, "Doh Sound 1",            "doh1.ogg");
SOUND_EFFECT_ENTRY(explosionSound,     false, "Explosion Sound",        "explosion.ogg");
SOUND_EFFECT_ENTRY(trackPlaceSound1,   false, "Track Place 1",          "track_place1.ogg");
SOUND_EFFECT_ENTRY(trackPlaceSound2,   false, "Track Place 2",          "track_place2.ogg");
SOUND_EFFECT_ENTRY(devonWooHooSound,   false, "Devon Woo Hoo Sound",    "kyle_woo_hoo.ogg");
SOUND_EFFECT_ENTRY(kyleWooHooSound,    false, "Kyle Woo Hoo Sound",     "devon_woo_hoo.ogg");
SOUND_EFFECT_ENTRY(taylorWooHooSound1, false, "Taylor Woo Hoo Sound 1", "taylor_woo_hoo1.ogg");
SOUND_EFFECT_ENTRY(taylorWooHooSound2, false, "Taylor Woo Hoo Sound 2", "taylor_woo_hoo2.ogg");
SOUND_EFFECT_ENTRY(clappingSound,      false, "Clapping Sound",         "clapping1.ogg");
SOUND_EFFECT_ENTRY(derailedSound,      false, "Derailed Sound",         "derailed.ogg");
SOUND_EFFECT_ENTRY(whistleSound,       false, "Whistle Sound",          "whistle.ogg");
SOUND_EFFECT_ENTRY(baaaaSound,         false, "Baaaa Sound",            "baaaa.ogg");

#undef SOUND_EFFECT_ENTRY
